require 'highline/import'

def p(raw_parameters)
  ActionController::Parameters.new(raw_parameters)
end

def create_object(params, creator_class)
  creator_class.new(p(params), Editor.first).saved_object.tap do |object|
    raise ArgumentError unless object.persisted?
  end
end

def create_category(params)
  create_object(params, Savers::CategoriesCreator)
end

def create_option(params)
  create_object(params, Savers::OptionsCreator)
end

def create_sandbox(params)
  create_object(params, Savers::SandboxesCreator)
end

def create_list(params)
  create_object(params, Savers::ListsCreator)
end

def build_page(params)
  Savers::PagesCreator.new(p(params), Editor.first)
end

def create_page(params)
  build_page(params).saved_object.tap do |page|
    raise ArgumentError unless page.persisted?
  end
end

if ENV['INTERACTIVE']
  login      = ask('Login:      ') { |q| q.default = 'admin' }
  email      = ask('Email:      ') { |q| q.default = 'admin@example.com' }
  first_name = ask('First name: ') { |q| q.default = 'Vasya' }
  last_name  = ask('Last name:  ') { |q| q.default = 'Pupkin' }
  password   = ask('Password:   ') { |q| q.default = 'secret'; q.echo = '*' }
else
  login      = 'admin'
  email      = 'admin@example.com'
  first_name = 'Vasya'
  last_name  = 'Pupkin'
  password   = 'secret'
end

admins = Group.create! do |group|
  group.slug     = 'administrators'
  group.title    = 'Администраторы'
  group.position = 10
end

proofreaders = Group.create! do |group|
  group.slug     = 'proofreaders'
  group.title    = 'Корректоры'
  group.position = 20
end

editors = Group.create! do |group|
  group.slug     = 'editors'
  group.title    = 'Редакторы сайта'
  group.position = 30
end

commers = Group.create! do |group|
  group.slug     = 'commers'
  group.title    = 'Коммерческий отдел'
  group.position = 40
end

newspaper_editors = Group.create! do |group|
  group.slug     = 'newspaper_editors'
  group.title    = 'Редакторы газеты'
  group.position = 50
end

photo_editors = Group.create! do |group|
  group.slug     = 'photo_editors'
  group.title    = 'Фоторедакторы'
  group.position = 60
end

video_editors = Group.create! do |group|
  group.slug     = 'video_editors'
  group.title    = 'Видеоредакторы'
  group.position = 70
end

admin = Editor.create! do |editor|
  editor.login      = login
  editor.email      = email
  editor.first_name = first_name
  editor.last_name  = last_name
  editor.password   = password
  editor.enabled    = true
  editor.group_ids  = [admins.id]
end

kinds = create_category(
  slug:              'kinds',
  title:             'Типы документов',
  nested_categories: %w(autoselected required single immutable)
)

materials = create_category(
  slug:      'materials',
  title:     'Материалы',
  parent_id: kinds.id,
  setting_keys: %w(additions.common)
)

authors = create_category(
  slug:         'authors',
  title:        'Авторы',
  position:     10,
  parent_id:    kinds.id,
  setting_keys: %w(head.author title_image.author urls.author)
)

theme_cycles = create_category(
  slug:         'theme_cycles',
  title:        'Тематические циклы',
  parent_id:    kinds.id,
  position:     20,
  setting_keys: %w(head.thematic_cycles urls.theme)
)

service_pages = create_category(
  slug:         'service_pages',
  title:        'Служебные страницы',
  parent_id:    kinds.id,
  position:     30,
)

links = create_category(
  slug:         'links',
  title:        'Ссылка по теме',
  parent_id:    kinds.id,
  position:     40,
  setting_keys: %w(urls.outer_link source.outer_link)
)

newsrelease = create_category(
  slug:         'newsrelease',
  title:        'Газетный выпуск',
  parent_id:    kinds.id,
  position:     50,
  setting_keys: %w(urls.newsrelease)
)

companies = create_category(
  slug:         'companies',
  title:        'Компании',
  parent_id:    kinds.id,
  position:     60,
  setting_keys: %w(urls.companies head.companies title_image.company links.industry)
)

press_releases = create_category(
  slug:         'press_releases',
  title:        'Пресс-релизы',
  parent_id:    kinds.id,
  position:     70,
  setting_keys: %w(urls.press_releases head.press_releases links.company title_image.press_release)
)

industries = create_category(
  slug:         'industries',
  title:        'Отрасли',
  parent_id:    kinds.id,
  position:     80,
  setting_keys: %w(urls.industries)
)

story_page = create_category(
  slug:         'story_page',
  title:        'Страница сюжета',
  parent_id:    kinds.id,
  position:     90,
  setting_keys: %w(head.article title_image.story_page urls.story_page)
)
domains = create_category(
  slug:              'domains',
  title:             'Домены',
  position:          10,
  setting_keys:      %w(attachments.common boxes.common),
  nested_categories: %w(autoselected single required)
)

domain = create_category(
  slug:      'vedomosti.ru',
  title:     'vedomosti.ru',
  parent_id: domains.id,
  position:  0
)

parts = create_category(
  slug:              'parts',
  title:             'Типы материалов',
  position:          20,
  setting_keys:      %w(urls.parts source.parts assignees.parts links.parts),
  nested_categories: %w(required single)
)

news = create_category(
  slug:         'news',
  title:        'Новости',
  parent_id:    parts.id,
  setting_keys: %w(boxes.news)
)

articles = create_category(
  slug:         'articles',
  title:        'Статья',
  parent_id:    parts.id,
  position:     10,
  setting_keys: %w(boxes.article head.article title_image.article links.article)
)

galleries = create_category(
  slug:         'galleries',
  title:        'Галерея',
  parent_id:    parts.id,
  position:     20,
  setting_keys: %w(boxes.gallery head.gallery title_image.gallery)
)

columns = create_category(
  slug:         'columns',
  title:        'Колонка',
  parent_id:    parts.id,
  position:     30,
  setting_keys: %w(boxes.article head.column)
)

blogs = create_category(
  slug:         'blogs',
  title:        'Блог',
  parent_id:    parts.id,
  position:     40,
  setting_keys: %w(boxes.article head.blog lists.rubrics)
)

quotes = create_category(
  slug:         'quotes',
  title:        'Цитата',
  parent_id:    parts.id,
  position:     50,
  setting_keys: %w(boxes.article head.quote title_image.quote)
)

video = create_category(
  slug:         'video',
  title:        'Видео',
  parent_id:    parts.id,
  position:     60,
  setting_keys: %w(boxes.video head.video title_image.article links.article)
)

characters = create_category(
  slug:         'characters',
  title:        'Интервью и профайл',
  parent_id:    parts.id,
  position:     60,
  setting_keys: %w(boxes.article head.character title_image.character)
)

# Псевдокатегория Рубрики
rubrics = create_category(
  slug:              'rubrics',
  title:             'Рубрики',
  position:          30,
  setting_keys:      %w(lists.rubrics urls.parts),
  nested_categories: %w(required)
)

# Темы
theme = create_category(
  slug: 'themes',
  title: 'Тематические циклы',
  position: 50,
  nested_categories: %w(single)
)

# Темы / Цикл статей
cycle_materials = create_category(
  slug:         'cycle_materials',
  title:        'Цикл статей',
  position:     10,
  parent_id:    theme.id,
  setting_keys: %w(title_image.article lists.rubrics)
)

# Темы / Цикл статей / Названия циклов
cycle_materials_list = create_category(
  slug: 'cycle_list',
  title: 'Названия циклов',
  parent_id: cycle_materials.id,
  nested_categories: %w(single required)
)

# Названия циклов статей
{
  crisis:          'Кризис',
  hybrid:          'Гибридные режимы',
  extra_jus:       'Extra Jus',
  east_turnaround: 'Поворот на Восток',
  gorod_project:   'Gorod Project'
}.each do |slug, title|
  create_category(
    slug: slug,
    title: title,
    parent_id: cycle_materials_list.id
  )
end

# Темы / Цикл колонок
cycle_columns = create_category(
  slug:         'cycle_columns',
  title:        'Цикл колонок',
  position:     20,
  parent_id:    theme.id,
  setting_keys: %w(head.cycle_columns lists.rubrics)
)

# Темы / Цикл колонок / Названия циклов
cycle_columns_list = create_category(
  slug: 'cycle_list',
  title: 'Названия циклов',
  parent_id: cycle_columns.id,
  nested_categories: %w(single required)
)

# Названия циклов колонок
{
  game_rules:        'Правила игры',
  conditions:        'Конъюнктура',
  ours:              'Наше «мы»',
  republic:          'Республика',
  notebook:          'Политический дневник',
  political_economy: 'Политэкономия',
  civil:             'Гражданское общество',
  diploma:           'Аттестат зрелости'
}.each do |slug, title|
  create_category(
    slug: slug,
    title: title,
    parent_id: cycle_columns_list.id
  )
end

# Склад (библиотека)
storage = create_category(
  slug: 'library',
  title: 'Библиотека',
  position: 60,
  nested_categories: %w(single)
)

trash_news = create_category(
  slug: 'trash_news',
  title: 'Трэш-новости',
  parent_id: storage.id,
  setting_keys: %w(urls.trash_news)
)

lib_characters = create_category(
  slug: 'characters',
  title: 'Действующее лицо',
  parent_id: storage.id
)

lib_investigation = create_category(
  slug: 'investigations',
  title: 'Расследование',
  parent_id: storage.id
)

# Раздел газета
newspaper = create_category(
  slug:      'newspaper',
  title:     'Газета',
  position:  25,
  nested_categories: %w(single),
  setting_keys: %w(links.newspaper head.newspaper urls.newspaper_doc)
)

# Газетные полосы
{
  'front'                => 'Первая полоса',
  'politics'             => 'Власть и деньги',
  'economics'            => 'Деньги и власть',
  'opinion'              => 'Комментарии',
  'persons'              => 'Действующие лица',
  'personal_finance'     => 'Личный счет',
  'companies_markets'    => 'Компании и рынки',
  'industry_energy'      => 'Индустрия и энергоресурсы',
  'finance'              => 'Финансы',
  'technology'           => 'Технологии и телекоммуникации',
  'consumer'             => 'Потребительский рынок',
  'management'           => 'Карьера и менеджмент',
  'investigation'        => 'Расследования',
  'culture'              => 'Культура',
  'media'                => 'Медиа',
  'north_west'           => 'Северо-Запад',
  'west_siberia'         => 'Западная Сибирь',
  'south'                => 'Юг России',
  'middle_volga'         => 'Среднее Поволжье',

  'education'            => 'Бизнес-образование',
  'hi_tech'              => 'Высокие технологии',
  'realty'               => 'Недвижимость',
  'commercial_vehicles'  => 'Коммерческий траснпорт',
  'property_investments' => 'Инвестиции в недвижимость',
  'energy_resources'     => 'Топливо и энергетика',
  'law'                  => 'Юридический бизнес',
  'financial_markets'    => 'Финансовые рынки',
  'russia_investments'   => 'Инвестиции в Россию',
  'petersburg'           => 'Экономика Санкт-Петербурга',
  'telecom'              => 'Телекоммуникации',
  'logistics'            => 'Грузоперевозки и логистика',
  'commercial_property'  => 'Коммерческая недвижимость',
  'housing'              => 'Жилая недвижимость',
  'insurance'            => 'Страхование',
  'retail'               => 'Розничная торговля',
  'healthcare'           => 'Здравоохранение',
  'charity'              => 'Корпоративная благотворительность',
  'jewellery'            => 'Ювелирная промышленность',
  'results_petersburg'   => 'Итоги года в Санкт-Петербурге',
  'results_south'        => 'Юг России. Итоги года',
  'results_volga'        => 'Поволжье. Итоги года',
  'agribusiness'         => 'Агробизнес',
  'resume'               => 'Резюме',
  'forum'                => 'Форум'
}.each do |slug, title|
  create_category(
    slug: slug,
    title: title,
    parent_id: newspaper.id
  )
end

story = create_category(
  slug:      'story',
  title:     'Сюжет',
  position:  80,
  nested_categories: %w(single)
)

create_category(
  slug:         'forum1',
  title:        'Форум1',
  position:     10,
  parent_id:    story.id
)

authors.update(forbidden_combinations: [parts.id, rubrics.id, theme.id, storage.id, newspaper.id])
theme_cycles.update(forbidden_combinations: [parts.id, rubrics.id, storage.id, newspaper.id])
service_pages.update(forbidden_combinations: [parts.id, rubrics.id, theme.id, storage.id, newspaper.id])
links.update(forbidden_combinations: [domains.id, parts.id, rubrics.id, theme.id, storage.id, newspaper.id])
newsrelease.update(forbidden_combinations: [parts.id, rubrics.id, theme.id, storage.id, newspaper.id])

create_option(type: 'template', filename: 'small_banner',              position: 10)
create_option(type: 'template', filename: 'small_no_banner',           position: 20)
create_option(type: 'template', filename: 'medium_banner',             position: 30)
create_option(type: 'template', filename: 'medium_no_banner',          position: 40)
create_option(type: 'template', filename: 'lifestyle_big_banner',      position: 50)
create_option(type: 'template', filename: 'lifestyle_small_no_banner', position: 60)
create_option(type: 'template', filename: 'slider',                    position: 70)
create_option(type: 'template', filename: 'cycle_materials',           position: 80)
create_option(type: 'template', filename: 'cycle_columns',             position: 90)

[
  { relative_url: '/rubrics/business',   title: 'Бизнес' },
  { relative_url: '/rubrics/economics',  title: 'Экономика' },
  { relative_url: '/rubrics/opinion',    title: 'Мнения' },
  { relative_url: '/rubrics/politics',   title: 'Политика' },
  { relative_url: '/rubrics/finance',    title: 'Финансы' },
  { relative_url: '/rubrics/technology', title: 'Технологии' },
  { relative_url: '/rubrics/realty',     title: 'Недвижимость' },
  { relative_url: '/rubrics/management', title: 'Менеджмент' },
  { relative_url: '/rubrics/lifestyle',  title: 'Стиль жизни' },
  { relative_url: '/rubrics/auto',       title: 'Авто' }
].each_with_index do |menu_item, index|
  create_option(
    type:         'mobile_menu',
    title:        menu_item[:title],
    relative_url: menu_item[:relative_url],
    position:     (index + 1) * 10
  )
end

create_sandbox(
  base_date:       nil,
  category_ids:    [kinds, materials, domains, domain, parts, news].map(&:id),
  color:           '#D3E5F2',
  date_offset:     -86400,
  owner_ids:       [0],
  participant_ids: [],
  published:       nil,
  title:           'Новости за сутки'
)

create_sandbox(
  base_date:       nil,
  category_ids:    [kinds, materials, domains, domain, parts, articles].map(&:id),
  color:           '#D7CEF2',
  date_offset:     -302400,
  owner_ids:       [0],
  participant_ids: [],
  published:       nil,
  title:           'Статьи'
)

create_sandbox(
  base_date:       nil,
  category_ids:    [kinds, materials, domains, domain, parts, galleries].map(&:id),
  color:           '#C7F1D4',
  date_offset:     -604800,
  owner_ids:       [0],
  participant_ids: [],
  published:       nil,
  title:           'Галереи за неделю'
)

create_sandbox(
  category_ids:    [kinds, authors, domains, domain, authors].map(&:id),
  color:           '#FF99CC',
  owner_ids:       [0],
  participant_ids: [],
  title:           'Авторы'
)

create_sandbox(
  category_ids: [kinds, materials, domains, domain, parts, columns].map(&:id),
  color:        '#FF9900',
  date_offset:  -604800,
  owner_ids:    [0],
  title:        'Колонки за неделю'
)

create_sandbox(
  category_ids: [kinds, theme_cycles].map(&:id),
  color:        '#FF9900',
  date_offset:  -604800,
  owner_ids:    [0],
  title:        'Тематические циклы'
)

create_sandbox(
  category_ids: [kinds, newsrelease].map(&:id),
  color:        '#FF9900',
  date_offset:  -604800,
  owner_ids:    [0],
  title:        'Газетные выпуски'
)

create_sandbox(
  color:        '#FF9900',
  owner_ids:    [0],
  title:        'Вычитка',
  proofread:    false
)

main_news_list = create_list(
  slug:         'main-news',
  title:        'Главная — Новости',
  limit:        7,
  cols:         4,
  category_ids: [domains, domain, parts, news].map(&:id),
  blacklisted_category_ids: [trash_news.id]
)

main_top_list = create_list(
  slug:         'main-top',
  title:        'Главная — Топ',
  limit:        10,
  cols:         8,
  duration:     10.years,
  category_ids: [domains, domain, parts].map(&:id),
  blacklisted_category_ids: [news.id]
)

main_page_creator = build_page(
  title:    'Главная',
  slug:     'main',
  position: 0,
  list_ids: [main_news_list, main_top_list].map(&:id)
)

# [sub]rubrics and lists
{
  business: {
    title: 'Бизнес',
    list_template: 'big_banner',
    subrubrics: {
      energy: {
        title: 'ТЭК',
        list_template: 'small_no_banner'
      },
      industry: {
        title: 'Промышленность',
        list_template: 'small_no_banner'
      },
      transport: {
        title: 'Транспорт',
        list_template: 'small_no_banner'
      },
      agriculture: {
        title: 'Агропром',
        list_template: 'small_no_banner'
      },
      retail: {
        title: 'Торговля и услуги',
        list_template: 'small_no_banner'
      },
      sport: {
        title: 'Спорт',
        list_template: 'small_no_banner'
      }
    }
  },
  economics: {
    title: 'Экономика',
    list_template: 'medium_banner',
    subrubrics: {
      macro: {
        title: 'Макроэкономика и бюджет ',
        list_template: 'small_no_banner'
      },
      state_investments: {
        title: 'Госинвестиции и проекты',
        list_template: 'small_no_banner'
      },
      global: {
        title: 'Мировая экономика',
        list_template: 'small_no_banner'
      },
      taxes: {
        title: 'Налоги и сборы',
        list_template: 'small_no_banner'
      },
      regulations: {
        title: 'Правила',
        list_template: 'small_no_banner'
      }
    }
  },
  finance: {
    title: 'Финансы',
    list_template: 'big_banner',
    subrubrics: {
      banks: {
        title: 'Банки',
        list_template: 'small_no_banner'
      },
      markets: {
        title: 'Рынки',
        list_template: 'small_no_banner'
      },
      players: {
        title: 'Профучастники',
        list_template: 'small_no_banner'
      },
      insurance: {
        title: 'Страхование',
        list_template: 'small_no_banner'
      },
      personal_finance: {
        title: 'Персональные финансы',
        list_template: 'small_no_banner'
      }
    }
  },
  opinion: {
    title: 'Мнения',
    list_template: 'medium_no_banner'
  },
  politics: {
    title: 'Политика',
    list_template: 'medium_no_banner',
    subrubrics: {
      official: {
        title: 'Власть',
        list_template: 'small_no_banner'
      },
      democracy: {
        title: 'Демократия',
        list_template: 'small_no_banner'
      },
      international: {
        title: 'Международные отношения',
        list_template: 'small_no_banner'
      },
      security_law: {
        title: 'Безопасность и право',
        list_template: 'small_no_banner'
      },
      social: {
        title: 'Социальная политика',
        list_template: 'small_no_banner'
      },
      foreign: {
        title: 'Международная жизнь',
        list_template: 'small_no_banner'
      }
    }
  },
  technology: {
    title: 'Технологии',
    list_template: 'big_banner',
    subrubrics: {
      telecom: {
        title: 'Телекоммуникации',
        list_template: 'medium_no_banner'
      },
      internet: {
        title: 'Интернет и digital',
        list_template: 'medium_no_banner'
      },
      media: {
        title: 'Медиа',
        list_template: 'medium_no_banner'
      },
      it_business: {
        title: 'ИТ-бизнес',
        list_template: 'medium_no_banner'
      },
      personal_technologies: {
        title: 'Персональные технологии',
        list_template: 'medium_no_banner'
      },
      hi_tech: {
        title: 'Наукоемкие технологии',
        list_template: 'medium_no_banner'
      }
    }
  },
  realty: {
    title: 'Недвижимость',
    list_template: 'big_banner',
    subrubrics: {
      housing: {
        title: 'Жилая недвижимость',
        list_template: 'small_no_banner'
      },
      commercial_property: {
        title: 'Коммерческая недвижимость',
        list_template: 'small_no_banner'
      },
      infrastructure: {
        title: 'Стройки и инфраструктура',
        list_template: 'small_no_banner'
      },
      architecture: {
        title: 'Архитектура и дизайн',
        list_template: 'small_no_banner'
      },
      districts: {
        title: 'Место для жизни',
        list_template: 'small_no_banner'
      }
    }
  },
  auto: {
    title: 'Авто',
    list_template: 'big_banner',
    subrubrics: {
      auto_industry: {
        title: 'Автомобильная промышленность',
        list_template: 'small_no_banner'
      },
      cars: {
        title: 'Легковые автомобили',
        list_template: 'small_no_banner'
      },
      commercial_vehicles: {
        title: 'Коммерческие автомобили',
        list_template: 'small_no_banner'
      },
      car_design: {
        title: 'Дизайн и технологии',
        list_template: 'small_no_banner'
      },
      test_drive: {
        title: 'Тест-драйвы',
        list_template: 'small_no_banner'
      }
    }
  },
  management: {
    title: 'Менеджмент',
    list_template: 'medium_banner',
    subrubrics: {
      career: {
        title: 'Карьера',
        list_template: 'medium_no_banner'
      },
      management: {
        title: 'Менеджмент',
        list_template: 'medium_no_banner'
      },
      compensation: {
        title: 'Зарплаты и занятость',
        list_template: 'medium_no_banner'
      },
      entrepreneurship: {
        title: 'Предпринимательство',
        list_template: 'medium_no_banner'
      },
      education: {
        title: 'Бизнес-образование',
        list_template: 'medium_no_banner'
      }
    }
  },
  lifestyle: {
    title: 'Стиль жизни',
    list_template: 'big_banner',
    subrubrics: {
      leisure: {
        title: 'Досуг',
        list_template: 'small_no_banner'
      },
      culture: {
        title: 'Культура',
        list_template: 'small_no_banner'
      },
      luxury: {
        title: 'Люкс',
        list_template: 'small_no_banner'
      },
      brands_news: {
        title: 'Новости',
        list_template: 'small_no_banner'
      },
      interview: {
        title: 'Интервью',
        list_template: 'small_no_banner'
      },
      lifeline: {
        title: 'Линии жизни',
        list_template: 'small_no_banner'
      }
    }
  }
}.each.with_index do |(rubric_slug, rubric_attributes), rubric_index|
  rubric = create_category(
    slug:              rubric_slug,
    title:             rubric_attributes[:title],
    parent_id:         rubrics.id,
    position:          rubric_index * 10,
    nested_categories: %w(single)
  )
  
  rubric_top_list = create_list(
    slug:         "rubrics-#{rubric.slug}-top",
    title:        "Рубрика — #{rubric.title} — Топ",
    limit:        8,
    cols:         8,
    duration:     1.day,
    category_ids: [domains, domain, parts, rubrics, rubric].map(&:id),
    blacklisted_category_ids: [news.id],
    header:       rubric.title,
    relative_url: "/rubrics/#{rubric.slug}",
    template:     rubric_attributes[:list_template],
    exclude_list_ids: [main_top_list.id]
  )
  
  main_page_creator.object.list_ids << rubric_top_list.id
  
  rubric_news_list = create_list(
    slug:         "rubrics-#{rubric.slug}-news",
    title:        "Рубрика — #{rubric.title} — Новости",
    limit:        10,
    cols:         4,
    category_ids: [domains, domain, parts, news, rubrics, rubric].map(&:id)
  )
  
  subrubrics = create_category(
    slug:              :subrubrics,
    title:             'Подрубрики',
    parent_id:         rubric.id,
    nested_categories: %w(single)
  ) if rubric_attributes.key?(:subrubrics)
  
  # Создаем подрубрики и соответствующие списки
  subrubrics_lists = rubric_attributes.fetch(:subrubrics, []).each.with_index.map do |(subrubric_slug, subrubric_attributes), subrubric_index|
    subrubric = create_category(
      slug:              subrubric_slug,
      title:             subrubric_attributes[:title],
      parent_id:         subrubrics.id,
      position:          subrubric_index * 10,
      nested_categories: %w(single)
    )
    
    create_list(
      slug:         "rubrics-#{rubric.slug}-#{subrubric.slug}-top",
      title:        "Рубрика — #{rubric.title} — #{subrubric.title} — Топ",
      limit:        8,
      cols:         12,
      duration:     1.day,
      category_ids: [domains, domain, parts, rubrics, rubric, subrubrics, subrubric].map(&:id),
      blacklisted_category_ids: [news.id],
      header:       subrubric.title,
      relative_url: "/rubrics/#{rubric.slug}/#{subrubric.slug}",
      template:     subrubric_attributes[:list_template],
      exclude_list_ids: [rubric_top_list.id]
    )
  end
  
  create_page(
    title:    "Рубрика — #{rubric.title}",
    slug:     "rubrics-#{rubric_slug}",
    position: rubric_index.succ * 1_000,
    list_ids: [rubric_news_list, rubric_top_list, *subrubrics_lists].map(&:id)
  )
end

main_page_creator.save

# популярное за сутки
popular24_list = create_list(
  slug:         'popular24',
  title:        'Популярное за сутки',
  limit:        10,
  cols:         12,
  duration:     10.years,
  category_ids: [domains, domain, rubrics].map(&:id),
  header:       'Популярное за сутки',
  relative_url: '/popular24',
  template:     'popular24'
)

# исключения из популярного за сутки
excluded_popular24_list = create_list(
  slug:         'popular24-excludes',
  title:        'Исключения из списка «Популярное за сутки»',
  limit:        100,
  cols:         12,
  duration:     10.years,
  category_ids: [domains, domain, rubrics].map(&:id)
)

# создание служебной страницы со списком исключений
create_page(
  title:    'Служебные списки',
  slug:     'other-lists',
  position: 12000,
  list_ids: [popular24_list, excluded_popular24_list].map(&:id)
)

opinion = Category.by_path([rubrics.id]).find_by_slug(:opinion)

# Мнения
opinion_subrubrics = create_category(
  slug:              :subrubrics,
  title:             'Подрубрики',
  parent_id:         opinion.id,
  nested_categories: %w(single)
)

details = create_category(
  slug:      'details',
  title:     'Детали',
  parent_id: opinion_subrubrics.id,
  position:  10
)

analytics = create_category(
  slug:      'analytics',
  title:     'Аналитика',
  parent_id: opinion_subrubrics.id,
  position:  20
)

editorial = create_category(
  slug: 'editorial',
  title: 'От редакции',
  parent_id: opinion_subrubrics.id,
  position:  30
)

blogs = create_category(
  slug:      'blogs',
  title:     'Блоги профессионалов',
  parent_id: opinion_subrubrics.id,
  position:  40
)

detail_types = create_category(
  slug:              'sections',
  title:             'Типы ху-ху',
  parent_id:         details.id,
  nested_categories: %w(single)
)

person_of_the_week = create_category(
  slug:      'person_of_the_week',
  title:     'Человек недели',
  parent_id: detail_types.id,
  position:  10
)

quote_of_the_week = create_category(
  slug:      'quote_of_the_week',
  title:     'Цитата недели',
  parent_id: detail_types.id,
  position:  20
)

figure_of_the_week = create_category(
  slug:      'figure_of_the_week',
  title:     'Цифра недели',
  parent_id: detail_types.id,
  position:  30
)

company_of_the_week = create_category(
  slug:      'company_of_the_week',
  title:     'Компания недели',
  parent_id: detail_types.id,
  position:  40
)

object_of_the_week = create_category(
  slug:      'object_of_the_week',
  title:     'Вещь недели',
  parent_id: detail_types.id,
  position:  50
)

list_cycle_materials_top = create_list(
  slug:  'rubrics-opinion-theme_cycles-cycle_materials-top',
  title: 'Рубрика — Мнения — Тематические статьи',
  limit: 4,
  cols:  12,
  duration: 1.day,
  category_ids: [
    theme_cycles,
    domains,
    domain,
    theme,
    cycle_materials
  ].map(&:id),
  blacklisted_category_ids: [news.id],
  header:       'Тематические статьи',
  relative_url: '/rubrics/opinion/theme_cycles/cycle_materials',
  template:     'cycle_materials'
)

list_cycle_columns_top = create_list(
  slug:  'rubrics-opinion-theme_cycles-cycle_columns-top',
  title: 'Рубрика — Мнения — Тематические колонки',
  limit: 8,
  cols:  12,
  duration: 1.day,
  category_ids: [
    theme_cycles,
    domains,
    domain,
    theme,
    cycle_columns
  ].map(&:id),
  header:       'Тематические колонки',
  relative_url: '/rubrics/opinion/theme_cycles/cycle_columns',
  template:     'cycle_columns'
)

list_analytics_top = create_list(
  slug:  'rubrics-opinion-analytics-top',
  title: 'Рубрика — Мнения — Аналитика — Топ',
  limit: 8,
  cols:  12,
  duration: 1.day,
  category_ids: [
    domains,
    domain,
    parts,
    rubrics,
    opinion,
    opinion_subrubrics,
    analytics
  ].map(&:id),
  blacklisted_category_ids: [news.id],
  header:       'Аналитика',
  relative_url: '/rubrics/opinion/analytics',
  template:     'medium_banner'
)

list_opinion_blogs_top = create_list(
  slug:  'rubrics-opinion-blogs-top',
  title: 'Рубрика — Мнения — Блоги профессионалов',
  limit: 4,
  cols:  12,
  duration: 1.day,
  category_ids: [
    domains,
    domain,
    parts,
    blogs
  ].map(&:id),
  blacklisted_category_ids: [news.id],
  header:       'Блоги профессионалов',
  relative_url: '/rubrics/opinion/blogs',
  template:     'medium_no_banner'
)

page_rubrics_opinion = Page.where(slug: 'rubrics-opinion').first
list_rubrics_opinion_top = List.where(slug: 'rubrics-opinion-top').first

page_rubrics_opinion.list_ids = [
  list_rubrics_opinion_top,
  list_analytics_top,
  list_opinion_blogs_top,
  list_cycle_materials_top,
  list_cycle_columns_top
].map(&:id)

page_rubrics_opinion.save
