# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160610125445) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"
  enable_extension "pg_stat_statements"
  enable_extension "ltree"

  create_table "attachment_versions", force: true do |t|
    t.integer  "attachment_id", limit: 8,              null: false
    t.string   "slug",                                 null: false
    t.string   "filename",                             null: false
    t.hstore   "attacher_data",           default: {}, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "attachment_versions", ["attachment_id"], name: "index_attachment_versions_on_attachment_id", using: :btree

  create_table "attachments", force: true do |t|
    t.string   "type",       null: false
    t.string   "directory",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "boxes", force: true do |t|
    t.string   "type",                                  null: false
    t.hstore   "boxer_data",            default: {},    null: false
    t.integer  "path",        limit: 8, default: [],    null: false, array: true
    t.integer  "position",              default: 0,     null: false
    t.boolean  "enabled",               default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.json     "packed_box",            default: {},    null: false
    t.integer  "original_id", limit: 8
    t.boolean  "deleted",               default: false, null: false
  end

  add_index "boxes", ["original_id"], name: "index_boxes_on_original_id", using: :btree
  add_index "boxes", ["path"], name: "index_boxes_on_path", using: :gin
  add_index "boxes", ["position"], name: "index_boxes_on_position", using: :btree

  create_table "categories", force: true do |t|
    t.string   "slug",                                            null: false
    t.string   "title",                                           null: false
    t.integer  "position",                         default: 0,    null: false
    t.boolean  "enabled",                          default: true, null: false
    t.integer  "path",                   limit: 8, default: [],   null: false, array: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "setting_keys",                     default: [],   null: false, array: true
    t.string   "nested_categories",                default: [],   null: false, array: true
    t.integer  "forbidden_combinations", limit: 8, default: [],   null: false, array: true
    t.ltree    "slug_path",                                       null: false
  end

  add_index "categories", ["path", "slug"], name: "index_categories_on_path_and_slug", unique: true, using: :btree
  add_index "categories", ["slug_path"], name: "index_categories_on_slug_path", unique: true, using: :btree

  create_table "dependencies", force: true do |t|
    t.integer  "dependable_id",   limit: 8, null: false
    t.string   "dependable_type",           null: false
    t.integer  "dependent_id",    limit: 8, null: false
    t.string   "dependent_type",            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dependencies", ["dependable_id", "dependable_type", "dependent_id", "dependent_type"], name: "by_dependable", unique: true, using: :btree

  create_table "documents", force: true do |t|
    t.text     "title",                                         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.hstore   "header_data",                   default: {},    null: false
    t.integer  "category_ids",        limit: 8, default: [],    null: false, array: true
    t.hstore   "trigger_data",                  default: {},    null: false
    t.boolean  "published",                     default: false, null: false
    t.datetime "published_at"
    t.json     "milestones",                    default: {},    null: false
    t.hstore   "assigner_data",                 default: {},    null: false
    t.integer  "url_id",              limit: 8
    t.integer  "root_box_id",         limit: 8
    t.hstore   "illustrator_data",              default: {},    null: false
    t.integer  "participant_ids",     limit: 8, default: [],    null: false, array: true
    t.json     "packed_document",               default: {},    null: false
    t.json     "packed_taggings",               default: [],    null: false
    t.json     "packed_links",                  default: {},    null: false
    t.hstore   "sourcer_data",                  default: {},    null: false
    t.json     "packed_preview",                default: {},    null: false
    t.integer  "exclude_list_ids",    limit: 8, default: [],    null: false, array: true
    t.string   "cached_setting_keys",           default: [],    null: false, array: true
    t.hstore   "proofreader_data",              default: {},    null: false
    t.json     "view",                          default: {}
    t.hstore   "box_mover_data",                default: {},    null: false
  end

  add_index "documents", ["box_mover_data"], name: "index_documents_on_box_mover_data", using: :gin
  add_index "documents", ["category_ids"], name: "index_documents_on_category_ids", using: :gin
  add_index "documents", ["created_at"], name: "index_documents_on_created_at", using: :btree
  add_index "documents", ["proofreader_data"], name: "index_documents_on_proofreader_data", using: :gin
  add_index "documents", ["published", "updated_at"], name: "index_documents_on_published_and_updated_at", order: {"updated_at"=>:desc}, using: :btree
  add_index "documents", ["published_at"], name: "index_documents_on_published_at", order: {"published_at"=>:desc}, using: :btree
  add_index "documents", ["root_box_id"], name: "index_documents_on_root_box_id", unique: true, using: :btree
  add_index "documents", ["sourcer_data"], name: "index_documents_on_sourcer_data", using: :gin
  add_index "documents", ["updated_at", "category_ids"], name: "index_documents_on_updated_at_and_category_ids", using: :btree
  add_index "documents", ["updated_at"], name: "index_documents_on_updated_at", using: :btree

  create_table "editors", force: true do |t|
    t.string   "login",                                     null: false
    t.string   "email",                                     null: false
    t.string   "password_digest",                           null: false
    t.string   "first_name",                                null: false
    t.string   "last_name",                                 null: false
    t.boolean  "enabled",                   default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "group_ids",       limit: 8, default: [],    null: false, array: true
  end

  add_index "editors", ["email"], name: "index_editors_on_email", unique: true, using: :btree
  add_index "editors", ["login"], name: "index_editors_on_login", unique: true, using: :btree

  create_table "groups", force: true do |t|
    t.string   "title",                  null: false
    t.string   "slug",                   null: false
    t.integer  "position",   default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "links", force: true do |t|
    t.integer  "document_id",       limit: 8,             null: false
    t.string   "section",                                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "bound_document_id", limit: 8,             null: false
    t.integer  "position",                    default: 0, null: false
  end

  add_index "links", ["bound_document_id"], name: "index_links_on_bound_document_id", using: :btree
  add_index "links", ["document_id", "bound_document_id", "section"], name: "index_links_on_document_id_and_bound_document_id_and_section", unique: true, using: :btree
  add_index "links", ["document_id"], name: "index_links_on_document_id", using: :btree

  create_table "list_items", force: true do |t|
    t.integer  "list_id",             limit: 8,             null: false
    t.integer  "document_id",         limit: 8,             null: false
    t.integer  "top",                           default: 0, null: false
    t.datetime "effective_timestamp",                       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "list_items", ["document_id"], name: "index_list_items_on_document_id", using: :btree
  add_index "list_items", ["effective_timestamp"], name: "index_list_items_on_effective_timestamp", order: {"effective_timestamp"=>:desc}, using: :btree
  add_index "list_items", ["list_id", "document_id"], name: "index_list_items_on_list_id_and_document_id", unique: true, using: :btree
  add_index "list_items", ["list_id", "effective_timestamp"], name: "index_list_items_on_list_id_and_effective_timestamp", order: {"effective_timestamp"=>:desc}, using: :btree
  add_index "list_items", ["list_id"], name: "index_list_items_on_list_id", using: :btree

  create_table "lists", force: true do |t|
    t.string   "slug",                                            null: false
    t.integer  "category_ids",             limit: 8, default: [], null: false, array: true
    t.integer  "limit",                              default: 10, null: false
    t.string   "width"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title",                                           null: false
    t.hstore   "lister_data",                        default: {}, null: false
    t.integer  "cached_document_ids",      limit: 8, default: [], null: false, array: true
    t.integer  "exclude_list_ids",         limit: 8, default: [], null: false, array: true
    t.integer  "blacklisted_category_ids", limit: 8, default: [], null: false, array: true
    t.integer  "duration",                           default: 0,  null: false
  end

  add_index "lists", ["slug"], name: "index_lists_on_slug", unique: true, using: :btree

  create_table "options", force: true do |t|
    t.string   "type",                    null: false
    t.hstore   "value",      default: {}, null: false
    t.integer  "position",   default: 0,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "options", ["type"], name: "index_options_on_type", using: :btree

  create_table "pages", force: true do |t|
    t.string   "title",                                           null: false
    t.integer  "list_ids",                 limit: 8, default: [], null: false, array: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position",                           default: 0,  null: false
    t.string   "slug",                                            null: false
    t.integer  "category_ids",             limit: 8, default: [], null: false, array: true
    t.integer  "blacklisted_category_ids", limit: 8, default: [], null: false, array: true
  end

  add_index "pages", ["slug"], name: "index_pages_on_slug", unique: true, using: :btree
  add_index "pages", ["title"], name: "index_pages_on_title", unique: true, using: :btree

  create_table "sandboxes", force: true do |t|
    t.string   "title",                                  null: false
    t.integer  "category_ids",    limit: 8, default: [], null: false, array: true
    t.boolean  "published"
    t.integer  "participant_ids", limit: 8, default: [], null: false, array: true
    t.integer  "owner_ids",       limit: 8, default: [], null: false, array: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "base_date"
    t.integer  "date_offset",               default: 0,  null: false
    t.string   "color"
    t.boolean  "proofread"
  end

  add_index "sandboxes", ["owner_ids"], name: "index_sandboxes_on_owner_ids", using: :btree
  add_index "sandboxes", ["title"], name: "index_sandboxes_on_title", using: :btree

  create_table "sessions", force: true do |t|
    t.integer  "editor_id",     limit: 8, null: false
    t.string   "access_token",            null: false
    t.string   "refresh_token",           null: false
    t.integer  "expires_in",              null: false
    t.inet     "ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["access_token"], name: "index_sessions_on_access_token", using: :btree
  add_index "sessions", ["editor_id"], name: "index_sessions_on_editor_id", using: :btree

  create_table "snapshots", force: true do |t|
    t.string   "action",                 null: false
    t.json     "data",                   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "author_id",    limit: 8, null: false
    t.integer  "subject_id",   limit: 8, null: false
    t.string   "subject_type",           null: false
  end

  add_index "snapshots", ["subject_id"], name: "index_snapshots_on_subject_id", using: :btree

  create_table "taggings", force: true do |t|
    t.integer  "document_id", limit: 8,              null: false
    t.string   "type",                               null: false
    t.integer  "tag_ids",     limit: 8, default: [], null: false, array: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tags", force: true do |t|
    t.string   "title",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "urls", force: true do |t|
    t.integer  "document_id", limit: 8,                 null: false
    t.hstore   "url_data",              default: {},    null: false
    t.string   "rendered",                              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "fixed",                 default: false
  end

  add_index "urls", ["document_id"], name: "index_urls_on_document_id", using: :btree
  add_index "urls", ["rendered"], name: "index_urls_on_rendered", unique: true, using: :btree

end
