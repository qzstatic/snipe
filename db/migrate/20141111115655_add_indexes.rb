class AddIndexes < ActiveRecord::Migration
  def change
    add_index :categories, :type
    add_index :categories, [:path, :type]    
    add_index :boxes, :path, using: :gin
  end
end
