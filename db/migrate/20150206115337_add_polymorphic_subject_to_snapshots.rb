class AddPolymorphicSubjectToSnapshots < ActiveRecord::Migration
  def up
    change_table :snapshots do |t|
      t.integer :subject_id, limit: 8
      t.string :subject_type
      
      Snapshot.update_all("subject_id = document_id, subject_type = 'Document'")
      
      t.index  :subject_id
      t.remove :document_id
      
      change_column_null :snapshots, :subject_id,   false
      change_column_null :snapshots, :subject_type, false
    end
  end
  
  def down
    change_table :snapshots do |t|
      t.integer :document_id, limit: 8
      
      Snapshot.update_all('document_id = subject_id')
      
      t.remove :subject_id
      t.remove :subject_type
      
      change_column_null :snapshots, :document_id, false
      add_index :snapshots, :document_id
    end
  end
end
