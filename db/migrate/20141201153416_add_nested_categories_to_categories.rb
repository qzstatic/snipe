class AddNestedCategoriesToCategories < ActiveRecord::Migration
  def change
    change_table :categories do |t|
      t.string :nested_categories, array: true, null: false, default: []
    end
  end
end
