class AddLinkerDataToLists < ActiveRecord::Migration
  def change
    add_column :lists, :lister_data, :hstore, null: false, default: {}
  end
end
