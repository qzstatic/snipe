class AddIndexOnCategoriesToDocuments < ActiveRecord::Migration
  def change
    change_table :documents do |t|
      t.index :category_ids, using: :gin
    end
  end
end
