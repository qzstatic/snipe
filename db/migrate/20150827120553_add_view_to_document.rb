class AddViewToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :view, :json, default: {}
  end
end
