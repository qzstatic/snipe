class AddCachedSettingKeysToDocuments < ActiveRecord::Migration
  def change
    change_table :documents do |t|
      t.string :cached_setting_keys, array: true, null: false, default: []
    end
  end
end
