class AddFunctionalIndexOnPackedBox < ActiveRecord::Migration
  def change
    execute <<-SQL
      CREATE index index_on_boxes_first_path_element
      ON boxes (CAST(CASE array_length(path, 1) WHEN 1 THEN path[1] ELSE NULL END AS bigint));
    SQL
  end
end
