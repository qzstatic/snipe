class RenamePartToActionInSnapshots < ActiveRecord::Migration
  def up
    change_table :snapshots do |t|
      t.rename :part, :action
    end
  end
  
  def down
    change_table :snapshots do |t|
      t.rename :action, :part
    end
  end
end
