class IndexToLinks < ActiveRecord::Migration
  def change
    add_index :links, :bound_document_id
  end
end
