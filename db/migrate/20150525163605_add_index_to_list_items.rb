class AddIndexToListItems < ActiveRecord::Migration
  def change
    change_table :list_items do |t|
      t.index [:list_id, :effective_timestamp], order: { effective_timestamp: :desc }
    end
  end
end
