class AddDocumentIdsToList < ActiveRecord::Migration
  def change
    change_table :lists do |t|
      t.integer  :cached_document_ids, limit: 8, default: [], null: false, array: true
    end
  end
end
