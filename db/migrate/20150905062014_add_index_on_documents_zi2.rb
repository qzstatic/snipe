class AddIndexOnDocumentsZi2 < ActiveRecord::Migration
  def change
    add_index :documents, [:published, :updated_at], order: {updated_at: :desc}
  end
end
