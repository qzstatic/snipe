class AddForbiddenCombinationsToCategories < ActiveRecord::Migration
  def change
    change_table :categories do |t|
      t.integer :forbidden_combinations, limit: 8, array: true, null: false, default: []
    end
  end
end
