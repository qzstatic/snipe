class UseSequencer < ActiveRecord::Migration
  def up

=begin	  
    drop_table :attachment_versions
    drop_table :attachments
    drop_table :boxes
    drop_table :categories
    drop_table :dependencies
    drop_table :documents
    drop_table :editors
    drop_table :groups
    drop_table :links
    drop_table :list_items
    drop_table :lists
    drop_table :options
    drop_table :pages
    drop_table :sandboxes
    drop_table :sequences
    drop_table :sessions
    drop_table :taggings
    drop_table :tags
    drop_table :urls
    drop_table :zones
=end    
    
    create_table :attachment_versions, force: true do |t|
      t.integer :attachment_id, limit: 8, null: false
      t.string :slug, null: false
      t.string :filename, null: false
      t.hstore :attacher_data, default: {}, null: false
      t.datetime :created_at
      t.datetime :updated_at
      
      t.index :attachment_id
    end
    change_column :attachment_versions, :id, :bigint
    
    create_table :attachments, force: true do |t|
      t.string :type, null: false
      t.string :directory, null: false
      t.datetime :created_at
      t.datetime :updated_at
    end
    
    create_table :boxes, force: true do |t|
      t.string   :type,                                   null: false
      t.hstore   :boxer_data,             default: {},    null: false
      t.integer  :path,         limit: 8, default: [],    null: false, array: true
      t.integer  :position,               default: 0,     null: false
      t.boolean  :enabled,                default: false, null: false
      t.datetime :created_at
      t.datetime :updated_at
      t.json     :packed_box,             default: {},    null: false
      t.integer  :original_id,  limit: 8
      
      t.index :original_id
    end
    
    create_table :categories, force: true do |t|
      t.string   :type,                                   null: false
      t.string   :slug,                                   null: false
      t.string   :title,                                  null: false
      t.integer  :position,                default: 0,    null: false
      t.boolean  :enabled,                 default: true, null: false
      t.integer  :path,          limit: 8, default: [],   null: false, array: true
      t.json     :settings,                default: {},   null: false
      t.json     :view_settings,           default: {},   null: false
      t.datetime :created_at
      t.datetime :updated_at
      
      t.index [:path, :slug], unique: true
    end
    
    create_table :dependencies, force: true do |t|
      t.integer  :dependable_id,  limit: 8, null: false
      t.string   :dependable_type,           null: false
      t.integer  :dependent_id,   limit: 8, null: false
      t.string   :dependent_type,            null: false
      t.datetime :created_at
      t.datetime :updated_at
      
      fields = %i(dependable_id dependable_type dependent_id dependent_type)
      t.index fields, unique: true, name: 'by_dependable'
    end
    
    create_table :documents, force: true do |t|
      t.text     :title,                                      null: false
      t.datetime :created_at
      t.datetime :updated_at
      t.hstore   :header_data,                default: {},    null: false
      t.integer  :category_ids,    limit: 8, default: [],    null: false, array: true
      t.hstore   :trigger_data,               default: {},    null: false
      t.boolean  :published,                  default: false, null: false
      t.datetime :published_at
      t.json     :milestones,                 default: {},    null: false
      t.hstore   :assigner_data,              default: {},    null: false
      t.integer  :url_id,          limit: 8
      t.integer  :root_box_id,     limit: 8
      t.hstore   :illustrator_data,           default: {},    null: false
      t.integer  :participant_ids, limit: 8, default: [],    null: false, array: true
      t.json     :packed_document,            default: {},    null: false
      t.json     :packed_taggings,            default: [],    null: false
      t.json     :packed_links,               default: {},    null: false
      t.hstore   :sourcer_data,               default: {},    null: false
      
      t.index :root_box_id, unique: true
    end
    
    create_table :editors, force: true do |t|
      t.string   :login,                                     null: false
      t.string   :email,                                     null: false
      t.string   :password_digest,                           null: false
      t.string   :first_name,                                null: false
      t.string   :last_name,                                 null: false
      t.boolean  :enabled,                   default: false, null: false
      t.datetime :created_at
      t.datetime :updated_at
      t.integer  :group_ids,      limit: 8, default: [],    null: false, array: true
      
      t.index :email, unique: true
      t.index :login, unique: true
    end
    
    create_table :groups, force: true do |t|
      t.string   :title,                            null: false
      t.string   :slug,                             null: false
      t.integer  :position,             default: 0, null: false
      t.datetime :created_at
      t.datetime :updated_at
    end
    
    create_table :links, force: true do |t|
      t.integer  :document_id,       limit: 8,             null: false
      t.string   :section,                                  null: false
      t.datetime :created_at
      t.datetime :updated_at
      t.integer  :bound_document_id, limit: 8,             null: false
      t.integer  :position,                     default: 0, null: false
    end
    
    create_table :list_items, force: true do |t|
      t.integer  :list_id,            limit: 8,             null: false
      t.integer  :document_id,        limit: 8,             null: false
      t.integer  :position,                      default: 0, null: false
      t.integer  :top,                           default: 0, null: false
      t.datetime :effective_timestamp
      t.datetime :created_at
      t.datetime :updated_at
      
      t.index :document_id
      t.index [:list_id, :document_id], unique: true
      t.index :list_id
    end
    
    create_table "lists", force: true do |t|
      t.string   "slug",                                 null: false
      t.string   "type",                                 null: false
      t.integer  "category_ids", limit: 8, default: [], null: false, array: true
      t.integer  "limit",                   default: 10, null: false
      t.string   "width"
      t.datetime "created_at"
      t.datetime "updated_at"
      
      t.index [:slug, :category_ids], unique: true
    end
    
    create_table :options, force: true do |t|
      t.string   :type,                              null: false
      t.hstore   :value,                default: {}, null: false
      t.integer  :position,             default: 0,  null: false
      t.datetime :created_at
      t.datetime :updated_at
      
      t.index :type
    end
    
    create_table :pages, force: true do |t|
      t.string   :title,                             null: false
      t.integer  :list_ids,  limit: 8, default: [], null: false, array: true
      t.datetime :created_at
      t.datetime :updated_at
      
      t.index :title, unique: true
    end
    
    create_table :sandboxes, force: true do |t|
      t.string   :title,                                   null: false
      t.integer  :category_ids,    limit: 8, default: [], null: false, array: true
      t.boolean  :published
      t.integer  :participant_ids, limit: 8, default: [], null: false, array: true
      t.integer  :owner_ids,       limit: 8, default: [], null: false, array: true
      t.datetime :created_at
      t.datetime :updated_at
      t.datetime :base_date
      t.integer  :date_offset,                default: 0,  null: false
      t.string   :color
    end
    
    create_table :sessions, force: true do |t|
      t.integer  :editor_id,    limit: 8, null: false
      t.string   :access_token,            null: false
      t.string   :refresh_token,           null: false
      t.integer  :expires_in,              null: false
      t.inet     :ip
      t.datetime :created_at
      t.datetime :updated_at
      
      t.index :editor_id
    end
    
    create_table :taggings, force: true do |t|
      t.integer  :document_id, limit: 8,              null: false
      t.string   :type,                                null: false
      t.integer  :tag_ids,     limit: 8, default: [], null: false, array: true
      t.datetime :created_at
      t.datetime :updated_at
    end
    
    create_table :tags, force: true do |t|
      t.string   :title,                null: false
      t.datetime :created_at
      t.datetime :updated_at
    end
    
    create_table :urls, force: true do |t|
      t.integer  :document_id, limit: 8,                 null: false
      t.hstore   :url_data,               default: {},    null: false
      t.string   :rendered,                               null: false
      t.datetime :created_at
      t.datetime :updated_at
      t.boolean  :fixed,                  default: false
    end
  end
  
  def down
    drop_table :attachment_versions
    drop_table :attachments
    drop_table :boxes
    drop_table :categories
    drop_table :dependencies
    drop_table :documents
    drop_table :editors
    drop_table :groups
    drop_table :links
    drop_table :list_items
    drop_table :lists
    drop_table :options
    drop_table :pages
    drop_table :sandboxes
    drop_table :sessions
    drop_table :taggings
    drop_table :tags
    drop_table :urls
    
    create_table "attachment_versions", id: false, force: true do |t|
      t.integer  "gid",            limit: 8,              null: false
      t.integer  "attachment_gid", limit: 8,              null: false
      t.string   "slug",                                  null: false
      t.string   "filename",                              null: false
      t.hstore   "attacher_data",            default: {}, null: false
      t.integer  "zone_id",                               null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    add_index "attachment_versions", ["attachment_gid"], name: "index_attachment_versions_on_attachment_gid", using: :btree
    add_index "attachment_versions", ["gid"], name: "attachment_versions_pkey", unique: true, using: :btree
    add_index "attachment_versions", ["zone_id"], name: "index_attachment_versions_on_zone_id", using: :btree
    
    create_table "attachments", id: false, force: true do |t|
      t.integer  "gid",        limit: 8, null: false
      t.string   "type",                 null: false
      t.string   "directory",            null: false
      t.integer  "zone_id",              null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    add_index "attachments", ["gid"], name: "attachments_pkey", unique: true, using: :btree
    add_index "attachments", ["zone_id"], name: "index_attachments_on_zone_id", using: :btree
    
    create_table "boxes", id: false, force: true do |t|
      t.integer  "gid",          limit: 8,                 null: false
      t.string   "type",                                   null: false
      t.hstore   "boxer_data",             default: {},    null: false
      t.integer  "path",         limit: 8, default: [],    null: false, array: true
      t.integer  "position",               default: 0,     null: false
      t.boolean  "enabled",                default: false, null: false
      t.integer  "zone_id",                                null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.json     "packed_box",             default: {},    null: false
      t.integer  "original_gid", limit: 8
    end
    
    add_index "boxes", ["gid"], name: "boxes_pkey", unique: true, using: :btree
    add_index "boxes", ["original_gid"], name: "index_boxes_on_original_gid", using: :btree
    add_index "boxes", ["zone_id"], name: "index_boxes_on_zone_id", using: :btree
    
    create_table "categories", id: false, force: true do |t|
      t.integer  "gid",           limit: 8,                null: false
      t.string   "type",                                   null: false
      t.string   "slug",                                   null: false
      t.string   "title",                                  null: false
      t.integer  "position",                default: 0,    null: false
      t.boolean  "enabled",                 default: true, null: false
      t.integer  "path",          limit: 8, default: [],   null: false, array: true
      t.json     "settings",                default: {},   null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "zone_id",                                null: false
      t.json     "view_settings",           default: {},   null: false
    end
    
    add_index "categories", ["gid"], name: "categories_pkey", unique: true, using: :btree
    add_index "categories", ["path", "slug"], name: "index_categories_on_path_and_slug", unique: true, using: :btree
    add_index "categories", ["zone_id"], name: "index_categories_on_zone_id", using: :btree
    
    create_table "dependencies", id: false, force: true do |t|
      t.integer  "gid",             limit: 8, null: false
      t.integer  "dependable_gid",  limit: 8, null: false
      t.string   "dependable_type",           null: false
      t.integer  "dependent_gid",   limit: 8, null: false
      t.string   "dependent_type",            null: false
      t.integer  "zone_id",                   null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    add_index "dependencies", ["dependable_gid", "dependable_type", "dependent_gid", "dependent_type"], name: "by_dependable", unique: true, using: :btree
    add_index "dependencies", ["gid"], name: "dependencies_pkey", unique: true, using: :btree
    add_index "dependencies", ["zone_id"], name: "index_dependencies_on_zone_id", using: :btree
    
    create_table "documents", id: false, force: true do |t|
      t.integer  "gid",              limit: 8,                 null: false
      t.text     "title",                                      null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.hstore   "header_data",                default: {},    null: false
      t.integer  "category_gids",    limit: 8, default: [],    null: false, array: true
      t.integer  "zone_id",                                    null: false
      t.hstore   "trigger_data",               default: {},    null: false
      t.boolean  "published",                  default: false, null: false
      t.datetime "published_at"
      t.json     "milestones",                 default: {},    null: false
      t.hstore   "assigner_data",              default: {},    null: false
      t.integer  "url_gid",          limit: 8
      t.integer  "root_box_gid",     limit: 8
      t.hstore   "illustrator_data",           default: {},    null: false
      t.integer  "participant_gids", limit: 8, default: [],    null: false, array: true
      t.json     "packed_document",            default: {},    null: false
      t.json     "packed_taggings",            default: [],    null: false
      t.json     "packed_links",               default: {},    null: false
      t.hstore   "sourcer_data",               default: {},    null: false
    end
    
    add_index "documents", ["gid"], name: "documents_pkey", unique: true, using: :btree
    add_index "documents", ["root_box_gid"], name: "index_documents_on_root_box_gid", unique: true, using: :btree
    add_index "documents", ["zone_id"], name: "index_documents_on_zone_id", using: :btree
    
    create_table "editors", id: false, force: true do |t|
      t.integer  "gid",             limit: 8,                 null: false
      t.string   "login",                                     null: false
      t.string   "email",                                     null: false
      t.string   "password_digest",                           null: false
      t.string   "first_name",                                null: false
      t.string   "last_name",                                 null: false
      t.boolean  "enabled",                   default: false, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "zone_id",                                   null: false
      t.integer  "group_gids",      limit: 8, default: [],    null: false, array: true
    end
    
    add_index "editors", ["email"], name: "index_editors_on_email", unique: true, using: :btree
    add_index "editors", ["gid"], name: "editors_pkey", unique: true, using: :btree
    add_index "editors", ["login"], name: "index_editors_on_login", unique: true, using: :btree
    add_index "editors", ["zone_id"], name: "index_editors_on_zone_id", using: :btree
    
    create_table "groups", id: false, force: true do |t|
      t.integer  "gid",        limit: 8,             null: false
      t.string   "title",                            null: false
      t.string   "slug",                             null: false
      t.integer  "position",             default: 0, null: false
      t.integer  "zone_id",                          null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    add_index "groups", ["gid"], name: "groups_pkey", unique: true, using: :btree
    add_index "groups", ["zone_id"], name: "index_groups_on_zone_id", using: :btree
    
    create_table "links", id: false, force: true do |t|
      t.integer  "gid",                limit: 8,             null: false
      t.integer  "document_gid",       limit: 8,             null: false
      t.string   "section",                                  null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "zone_id",                                  null: false
      t.integer  "bound_document_gid", limit: 8,             null: false
      t.integer  "position",                     default: 0, null: false
    end
    
    add_index "links", ["gid"], name: "links_pkey", unique: true, using: :btree
    add_index "links", ["zone_id"], name: "index_links_on_zone_id", using: :btree
    
    create_table "list_items", id: false, force: true do |t|
      t.integer  "gid",                 limit: 8,             null: false
      t.integer  "list_gid",            limit: 8,             null: false
      t.integer  "document_gid",        limit: 8,             null: false
      t.integer  "position",                      default: 0, null: false
      t.integer  "top",                           default: 0, null: false
      t.datetime "effective_timestamp"
      t.integer  "zone_id",                                   null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    add_index "list_items", ["document_gid"], name: "index_list_items_on_document_gid", using: :btree
    add_index "list_items", ["gid"], name: "list_items_pkey", unique: true, using: :btree
    add_index "list_items", ["list_gid", "document_gid"], name: "index_list_items_on_list_gid_and_document_gid", unique: true, using: :btree
    add_index "list_items", ["list_gid"], name: "index_list_items_on_list_gid", using: :btree
    add_index "list_items", ["zone_id"], name: "index_list_items_on_zone_id", using: :btree
    
    create_table "lists", id: false, force: true do |t|
      t.integer  "gid",           limit: 8,              null: false
      t.string   "slug",                                 null: false
      t.string   "type",                                 null: false
      t.integer  "category_gids", limit: 8, default: [], null: false, array: true
      t.integer  "limit",                   default: 10, null: false
      t.string   "width"
      t.integer  "zone_id",                              null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    add_index "lists", ["gid"], name: "lists_pkey", unique: true, using: :btree
    add_index "lists", ["slug", "category_gids"], name: "index_lists_on_slug_and_category_gids", unique: true, using: :btree
    add_index "lists", ["zone_id"], name: "index_lists_on_zone_id", using: :btree
    
    create_table "options", id: false, force: true do |t|
      t.integer  "gid",        limit: 8,              null: false
      t.string   "type",                              null: false
      t.hstore   "value",                default: {}, null: false
      t.integer  "position",             default: 0,  null: false
      t.integer  "zone_id",                           null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    add_index "options", ["gid"], name: "options_pkey", unique: true, using: :btree
    add_index "options", ["type"], name: "index_options_on_type", using: :btree
    add_index "options", ["zone_id"], name: "index_options_on_zone_id", using: :btree
    
    create_table "pages", id: false, force: true do |t|
      t.integer  "gid",        limit: 8,              null: false
      t.string   "title",                             null: false
      t.integer  "list_gids",  limit: 8, default: [], null: false, array: true
      t.integer  "zone_id",                           null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    add_index "pages", ["gid"], name: "pages_pkey", unique: true, using: :btree
    add_index "pages", ["title"], name: "index_pages_on_title", unique: true, using: :btree
    add_index "pages", ["zone_id"], name: "index_pages_on_zone_id", using: :btree
    
    create_table "sandboxes", id: false, force: true do |t|
      t.integer  "gid",              limit: 8,              null: false
      t.string   "title",                                   null: false
      t.integer  "category_gids",    limit: 8, default: [], null: false, array: true
      t.boolean  "published"
      t.integer  "participant_gids", limit: 8, default: [], null: false, array: true
      t.integer  "owner_gids",       limit: 8, default: [], null: false, array: true
      t.integer  "zone_id",                                 null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.datetime "base_date"
      t.integer  "date_offset",                default: 0,  null: false
      t.string   "color"
    end
    
    add_index "sandboxes", ["gid"], name: "sandboxes_pkey", unique: true, using: :btree
    add_index "sandboxes", ["zone_id"], name: "index_sandboxes_on_zone_id", using: :btree
    
    create_table "sequences", id: false, force: true do |t|
      t.integer  "zone_id",                null: false
      t.string   "name",                   null: false
      t.integer  "count",      default: 0, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    add_index "sequences", ["zone_id", "name"], name: "index_sequences_on_zone_id_and_name", unique: true, using: :btree
    
    create_table "sessions", id: false, force: true do |t|
      t.integer  "gid",           limit: 8, null: false
      t.integer  "editor_gid",    limit: 8, null: false
      t.string   "access_token",            null: false
      t.string   "refresh_token",           null: false
      t.integer  "expires_in",              null: false
      t.inet     "ip"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "zone_id",                 null: false
    end
    
    add_index "sessions", ["editor_gid"], name: "index_sessions_on_editor_gid", using: :btree
    add_index "sessions", ["gid"], name: "sessions_pkey", unique: true, using: :btree
    add_index "sessions", ["zone_id"], name: "index_sessions_on_zone_id", using: :btree
    
    create_table "taggings", id: false, force: true do |t|
      t.integer  "gid",          limit: 8,              null: false
      t.integer  "document_gid", limit: 8,              null: false
      t.string   "type",                                null: false
      t.integer  "tag_gids",     limit: 8, default: [], null: false, array: true
      t.integer  "zone_id",                             null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    add_index "taggings", ["gid"], name: "taggings_pkey", unique: true, using: :btree
    add_index "taggings", ["zone_id"], name: "index_taggings_on_zone_id", using: :btree
    
    create_table "tags", id: false, force: true do |t|
      t.integer  "gid",        limit: 8, null: false
      t.string   "title",                null: false
      t.integer  "zone_id",              null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
    
    add_index "tags", ["gid"], name: "tags_pkey", unique: true, using: :btree
    add_index "tags", ["zone_id", "title"], name: "index_tags_on_zone_id_and_title", unique: true, using: :btree
    
    create_table "urls", id: false, force: true do |t|
      t.integer  "gid",          limit: 8,                 null: false
      t.integer  "document_gid", limit: 8,                 null: false
      t.hstore   "url_data",               default: {},    null: false
      t.string   "rendered",                               null: false
      t.integer  "zone_id",                                null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "fixed",                  default: false
    end
    
    add_index "urls", ["gid"], name: "urls_pkey", unique: true, using: :btree
    add_index "urls", ["zone_id"], name: "index_urls_on_zone_id", using: :btree
    
    create_table "zones", force: true do |t|
      t.string   "slug",       null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end
end
