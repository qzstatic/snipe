class AddIndexToUrls < ActiveRecord::Migration
  def change
    add_index :urls, :document_id
  end
end
