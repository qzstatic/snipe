class AddUniquenessOfDocumentsToLinks < ActiveRecord::Migration
  def change
    add_index :links, [:document_id, :bound_document_id, :section], unique: true
  end
end
