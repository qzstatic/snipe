class ExcludeListIds < ActiveRecord::Migration
  def change
    change_table :lists do |t|
      t.integer  :exclude_list_ids, limit: 8, default: [], null: false, array: true
    end
    
    List.find_each do |list|
      Hooks::Lists::CacheUpdate.new(list).save
    end
  end
end
