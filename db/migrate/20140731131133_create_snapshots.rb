class CreateSnapshots < ActiveRecord::Migration
  def change
    create_table :snapshots do |t|
      t.integer :document_id, limit: 8, null: false
      t.string :part, null: false
      t.json   :data, null: false
      
      t.timestamps
      
      t.index :document_id
    end
  end
end
