class RenameBlacklisted < ActiveRecord::Migration
  def change
    rename_column :lists, :blacklisted_categories_ids, :blacklisted_category_ids
  end
end
