class AddSlugPathToCategories < ActiveRecord::Migration
  def up
    enable_extension :ltree
    add_column :categories, :slug_path, :ltree
    
    Category.find_each do |category|
      category.recalculate_slug_path
      category.save
    end
    
    change_column_null :categories, :slug_path, false
    add_index :categories, :slug_path, unique: true
  end
  
  def down
    remove_column :categories, :slug_path, :ltree
    disable_extension :ltree
  end
end
