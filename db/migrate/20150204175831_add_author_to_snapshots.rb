class AddAuthorToSnapshots < ActiveRecord::Migration
  def up
    add_column :snapshots, :author_id, :integer, limit: 8
    Snapshot.update_all(author_id: Editor.first.id) if Editor.first.present?
    change_column_null :snapshots, :author_id, false
  end
  
  def down
    remove_column :snapshots, :author_id
  end
end
