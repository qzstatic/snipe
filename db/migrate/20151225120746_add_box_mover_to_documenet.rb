class AddBoxMoverToDocumenet < ActiveRecord::Migration
  def change
    change_table :documents do |t|
      t.hstore :box_mover_data, null: false, default: {}
      t.index  :box_mover_data, using: :gin
    end
  end
end
