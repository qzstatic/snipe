class ChangeListsIndex < ActiveRecord::Migration
  def up
    change_table :lists do |t|
      t.remove_index [:slug, :category_ids]
      t.index        :slug, unique: true
    end
  end
  
  def down
    change_table :lists do |t|
      t.remove_index :slug
      t.index        [:slug, :category_ids], unique: true
    end
  end
end
