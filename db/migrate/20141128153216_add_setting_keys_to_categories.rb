class AddSettingKeysToCategories < ActiveRecord::Migration
  def up
    change_table :categories do |t|
      t.remove :settings
      t.remove :view_settings
      t.string :setting_keys, array: true, null: false, default: []
    end
  end
  
  def down
    change_table :categories do |t|
      t.remove :setting_keys
      t.json   :settings,      default: {}, null: false
      t.json   :view_settings, default: {}, null: false
    end
  end
end
