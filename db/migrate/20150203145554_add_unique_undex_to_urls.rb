class AddUniqueUndexToUrls < ActiveRecord::Migration
  def change
    change_table :urls do |t|
      t.index :rendered, unique: true
    end
  end
end
