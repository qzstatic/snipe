class AlterPackedDocumentToJsonb < ActiveRecord::Migration
  def up
    change_column_default :documents, :packed_document, nil
    change_column :documents, :packed_document, 'jsonb USING CAST(packed_document AS jsonb)'
    change_column_default :documents, :packed_document, '{}'
    add_index :documents, :packed_document, using: :gin
  end

  def down
    remove_index :documents, column: :packed_document
    change_column_default :documents, :packed_document, nil
    change_column :documents, :packed_document, 'json USING CAST(packed_document AS json)'
    change_column_default :documents, :packed_document, {}
  end
end
