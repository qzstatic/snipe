class AddPackedPreviewToDocument < ActiveRecord::Migration
  def change
    change_table :documents do |t|
      t.json :packed_preview, null: false, default: {}
    end
  end
end
