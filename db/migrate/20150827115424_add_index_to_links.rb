class AddIndexToLinks < ActiveRecord::Migration
  def change
    add_index :links, :document_id
  end
end
