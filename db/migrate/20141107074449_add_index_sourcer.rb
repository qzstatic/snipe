class AddIndexSourcer < ActiveRecord::Migration
  def change
    add_index :documents, :sourcer_data, using: :gin
  end
end
