class AddIndexOnDocuments1 < ActiveRecord::Migration
  def change
    add_index :documents, :published_at, order: {published_at: 'DESC NULLS LAST'}  
  end
end
