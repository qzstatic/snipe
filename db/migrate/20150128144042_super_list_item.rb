class SuperListItem < ActiveRecord::Migration
  def up
    remove_column :list_items, :position
    ListItem.where(effective_timestamp: nil).update_all(effective_timestamp: Time.now)
    change_column :list_items, :effective_timestamp, :datetime, null: false, default: nil
    add_index :list_items, :effective_timestamp, order: {effective_timestamp: :desc}
    add_column :lists, :duration, :integer, null: false, default: 0
  end

  def down
    remove_column :lists, :duration
    
    change_column :list_items, :effective_timestamp, :datetime, null: true, default: nil 
    add_column :list_items, :position, :integer, null: false, default: 0
    remove_index :list_items, :effective_timestamp
  end
end
