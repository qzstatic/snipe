class SetDuration < ActiveRecord::Migration
  def up
    List.all.each do |list|
      duration = case list.slug
      when 'main-top', 'popular24'
        10.years
      when /\Arubrics-.*?-top\z/
        1.day
      end
      
      list.update(duration: duration) if duration
    end
  end
  
  def down
  end
end
