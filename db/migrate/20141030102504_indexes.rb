class Indexes < ActiveRecord::Migration
  def change
    add_index :documents, [:updated_at, :category_ids]
    add_index :sandboxes, [:owner_ids]
    add_index :sandboxes, [:title]
    add_index :sessions, [:access_token]
  end
end
