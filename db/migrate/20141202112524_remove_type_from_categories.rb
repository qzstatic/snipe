class RemoveTypeFromCategories < ActiveRecord::Migration
  def up
    change_table :categories do |t|
      t.remove :type
    end
  end
  
  def down
    change_table :categories do |t|
      t.string :type
      
      t.index [:path, :type]
      t.index :type
    end
  end
end
