class AddDeletedToBox < ActiveRecord::Migration
  def change
    add_column :boxes, :deleted, :boolean, null: false, default: false, index: true
  end
end
