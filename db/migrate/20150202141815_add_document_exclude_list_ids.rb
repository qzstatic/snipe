class AddDocumentExcludeListIds < ActiveRecord::Migration
  def change
    change_table :documents do |t|
      t.integer  :exclude_list_ids, limit: 8, default: [], null: false, array: true
    end
  end
end

