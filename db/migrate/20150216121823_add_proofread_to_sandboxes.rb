class AddProofreadToSandboxes < ActiveRecord::Migration
  def change
    add_column :sandboxes, :proofread, :boolean
  end
end
