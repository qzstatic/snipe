class AddProofreaderDataToDocuments < ActiveRecord::Migration
  def change
    change_table :documents do |t|
      t.hstore :proofreader_data, null: false, default: {}
      t.index  :proofreader_data, using: :gin
    end
  end
end
