class AddIndexOnPositionToBoxes < ActiveRecord::Migration
  def change
    change_table :boxes do |t|
      t.index :position
    end
  end
end
