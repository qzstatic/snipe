class AddBlacklistedCategoriesIdsToLists < ActiveRecord::Migration
  def change
    change_table :lists do |t|
      t.integer  :blacklisted_categories_ids, limit: 8, default: [], null: false, array: true
    end

    List.find_each do |list|
      list.blacklisted_categories_ids = list.category_ids.select { |id| id < 0 }.map(&:abs)
      list.category_ids = list.category_ids.select { |id| id > 0 }
      list.save
    end
  end
end
