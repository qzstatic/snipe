class AddCategoriesToPage < ActiveRecord::Migration
  def change
    change_table :pages do |t|
      t.integer :category_ids, limit: 8, default: [], null: false, array: true
      t.integer :blacklisted_category_ids, limit: 8, default: [], null: false, array: true
    end
  end
end
