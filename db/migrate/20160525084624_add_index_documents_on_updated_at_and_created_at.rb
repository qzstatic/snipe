class AddIndexDocumentsOnUpdatedAtAndCreatedAt < ActiveRecord::Migration
  def change
    add_index :documents, :created_at
    add_index :documents, :updated_at
  end
end
