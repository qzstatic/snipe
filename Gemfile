source 'https://rubygems.org'

# Ruby
ruby '2.1.5'

# Rails
gem 'rails'
gem 'rails-api'

# DB
gem 'pg'
gem 'bcrypt'

# Redis
gem 'redis'
gem 'redis-namespace', require: 'redis-namespace'

# Templates
gem 'active_model_serializers'

# Console
gem 'awesome_pry'

# Settings
gem 'settings', github: 'roomink/settings'

# Freezing objects
gem 'ice_nine', require: %w(ice_nine ice_nine/core_ext/object)

# Composition
gem 'anima'
gem 'concord'
gem 'procto'

# CLI
gem 'highline'

# CORS
gem 'rack-cors'

# Oj
gem 'oj'

# Diffs
gem 'diffy'

# DB cleaner
gem 'database_cleaner'

# HTTP requests (for bin/api and ImageConverter)
gem 'faraday', '~> 0.9.0'
gem 'faraday_middleware', '~> 0.9.1'
gem 'faraday_middleware-parse_oj', '~> 0.3.0'
gem 'net-http-persistent'

group :development do
  # Application preloader
  gem 'spring'
  gem 'spring-commands-rspec'
  
  # Guard
  gem 'guard-rspec'
  gem 'rb-fsevent'
  gem 'terminal-notifier'
  gem 'terminal-notifier-guard'
  
  # Annotations
  gem 'active_record-annotate'
  
  # Documentation
  gem 'guard-yard'
  gem 'apiaryio'
  
  # Puma
  gem 'puma'
end

group :development, :test do
  # RSpec
  gem 'rspec', '~> 3.2'
  gem 'rspec-mocks'
  gem 'rspec-rails'
end

group :test do
  # Factory girl
  gem 'factory_girl_rails', '~> 4.0'
end

group :production do
  # Unicorn
  gem 'unicorn'
end

# Monitoring
gem 'newrelic_rpm'

# Paginator
gem 'kaminari'

# Queue manager
gem 'resque'
