## Разворачивание системы

Ставим все необходимое:

``` sh
# rbenv/ruby-build
$ git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
$ git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
$ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.zshrc
$ echo 'eval "$(rbenv init -)"' >> ~/.zshrc
# перезапустить консоль

# PostgreSQL
$ brew install postgres
$ ln -sfv /usr/local/opt/postgresql/*.plist ~/Library/LaunchAgents
$ launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist
$ createdb $(whoami)
$ wget -O ~/.psqlrc https://raw.githubusercontent.com/7even/7erver/master/cookbooks/db/files/default/psqlrc

# Redis
$ brew install redis
$ ln -sfv /usr/local/opt/redis/*.plist ~/Library/LaunchAgents
$ launchctl load ~/Library/LaunchAgents/homebrew.mxcl.redis.plist

# Приложение
$ git clone git@github.com:roomink/snipe.git
$ cd snipe
$ rbenv install # ставим руби
$ bundle install # ставим гемы

# Pow
$ curl get.pow.cx | sh
$ gem install powder
$ rbenv rehash && powder link
$ echo 8808 > ~/.pow/yard # для документации на http://yard.dev
```

Правим локальные настройки:

``` yaml
# config/settings/development.local.yml
postgresql:
  username: # юзернейм в постгресе
# аналогично в config/settings/test.local.yml
```

Создаем базы в постгресе:

``` sh
$ rake db:create:all
$ rake db:dev:setup
$ rake db:test:setup
```

Вгружаем фейковые данные для разработки:

``` sh
$ rake db:populate
```

Система готова к работе.
