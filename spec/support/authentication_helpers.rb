module AuthenticationHelpers
  def sign_in(editor)
    editor_params = {
      login:    editor.login,
      password: editor.password
    }
    
    request = double(
      'Credentials authentication request',
      params: {
        editor: editor_params
      },
      ip: '0.0.0.0'
    )
    
    manager = SessionsManager::Credentials.new(request)
    manager.open_session
    
    @access_token  = manager.access_token.token
    @refresh_token = manager.refresh_token.token
  end
  
  def set_access_token_header
    request.headers[SessionsManager::AccessToken::TOKEN_HEADER] = @access_token
  end
  
  def set_content_type_header
    request.env['HTTP_ACCEPT'] = 'application/json'
  end
end
