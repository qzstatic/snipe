module DataHelpers
  def create_document(params = nil)
    params ||= {}
    default = { title: 'Halo!' }
    
    Savers::DocumentsCreator.new(acprm(default.merge(params)), current_editor).saved_object
  end
  
  def create_category(attributes = {})
    attributes = {
      slug: 'domain',
      title: 'vedomosti.ru'
    }.merge(attributes)
    
    Savers::CategoriesCreator.new(acprm(attributes), current_editor).saved_object
  end
  
  def create_url(document = nil)
    document ||= create_document
    default = { url: { rendered: 'http://ya.ru' } }
    handler = Handlers::Urls::RawCreator.new(Url.new, acprm(default), { document: document })
    handler.save
    handler.object
  end
  
  def current_editor
    @data_current_editor ||= Editor.create!(
      login: 'admin',
      email: 'admin@example.com',
      password: 'secret',
      first_name: 'Vasya',
      last_name: 'Pupkin',
      enabled: true,
      group_ids: []
    )
  end
  
  def acprm(attributes = nil)
    ActionController::Parameters.new(attributes)
  end
end
