module JsonHelpers
  def response_body
    unless response.body.blank?
      Oj.load(response.body)
    end
  end
end
