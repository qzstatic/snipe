require 'spec_helper'

describe Snapshot do
  let(:editor)  { create(:editor) }
  let(:article) { create(:article) }
  let(:parts)   { article.parent }
  
  let(:document) do
    Savers::DocumentsCreator.new(
      ActionController::Parameters.new(
        title: 'New document',
        category_ids: [parts.id, article.id],
        subtitle: 'Some subtitle'
      ),
      editor
    ).tap do |creator|
      creator.save
      creator.object.create_root_box
    end.object
  end
  
  before(:each) do
    @paragraph = Savers::BoxesCreator.new(
      ActionController::Parameters.new(
        type: 'paragraph',
        text: 'Hello',
        parent_id: document.root_box.id
      ),
      editor
    ).tap(&:save).object
    
    @link = Savers::LinksCreator.new(
      ActionController::Parameters.new(
        section:           'reference',
        bound_document_id: document.id
      ),
      editor,
      document
    ).tap(&:save).object
    
    @tagging = Savers::TaggingsCreator.new(
      ActionController::Parameters.new(type: 'Companies', tag_ids: [1, 2]),
      editor,
      document
    ).tap(&:save).object
    
    @url = Savers::UrlsCreator.new(
      ActionController::Parameters.new(
        domain:    'google.com',
        rubric:    'business',
        type:      'news',
        slug:      'putin',
        pseudo_id: '123',
        date:      '2014/07/17'
      ),
      editor,
      document
    ).tap(&:save).object
  end
  
  describe '#data=' do
    let(:snapshot) { document.snapshots.find_by!(action: 'document.created') }
    
    it 'ignores packed_* and timestamp attributes' do
      expect(snapshot.data).not_to have_key(:packed_document)
      expect(snapshot.data).not_to have_key(:created_at)
      expect(snapshot.data).not_to have_key(:updated_at)
    end
  end
  
  describe '.full_document_at' do
    context 'after all changes' do
      it 'returns an actual document' do
        data = Snapshot.full_document_at(Snapshot.last)
        
        expect(data[:title]).to eq(document.title)
        expect(data[:boxes].first[:type]).to eq('paragraph')
        expect(data[:links].first[:bound_document_id]).to eq(document.id)
        expect(data[:taggings].first[:type]).to eq('Companies')
        expect(data[:urls].first[:url_data][:domain]).to eq('google.com')
      end
    end
  end
end
