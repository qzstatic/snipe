require 'spec_helper'

describe Box do
  let(:domain)   { create(:domain) }
  let(:root_box) { create(:box) }
  
  before(:each) do
    @document = create(
      :document,
      category_ids: [domain.id],
      root_box_id: root_box.id
    )
    
    @paragraph = create(:box, type: :paragraph, parent_id: root_box.id)
  end
  
  describe '#document' do
    it "returns the root box's document" do
      expect(@paragraph.document).to eq(@document)
    end
  end
  
  describe '#settings' do
    it "returns the settings of box's document" do
      expect(@paragraph.settings).to eq(@document.settings)
    end
  end
end
