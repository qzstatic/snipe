require 'spec_helper'

describe Page do
  let(:domains)           { create(:domains) }
  let(:rubrics)           { create(:rubrics) }
  
  def create_document(params = {})
    params = {
      category_ids: [domains.id],
      published:    true,
      published_at: Time.now
    }.merge(params)
    
    create(:document, params)
  end
  
  
  describe '#matching_documents' do
    before(:each) do
      @page = create(:page, category_ids: [domains.id], blacklisted_category_ids: [rubrics.id])
      
      @first_document = create_document
      @second_document = create_document(category_ids: [domains.id, rubrics.id])
    end
    
    it 'returns items ordered by sort_criteria' do
      expect(@page.matching_documents).to include(@first_document)
      expect(@page.matching_documents).not_to include(@second_document)
    end
  end
end
