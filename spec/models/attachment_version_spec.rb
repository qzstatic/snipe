require 'spec_helper'

describe AttachmentVersion do
  subject { create(:attachment_version) }
  
  describe '#porcupined' do
    it 'processes the version with Attacher' do
      expect(Snipe::Modules::Attacher).to receive(:process).with(subject)
      subject.porcupined
    end
  end
  
  describe '#url' do
    it 'returns relative url for file' do
      expect(subject.url).to eq(Pathname.new('some/path/preview-123.jpg'))
    end
  end
end
