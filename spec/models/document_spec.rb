require 'spec_helper'

describe Document do
  let(:article) { create(:article) }
  let(:parts)   { article.parent }
  let(:rubrics) { create(:rubrics) }
  
  let(:document) do
    create(:document, category_ids: [parts, article].map(&:id))
  end
  
  describe '#categories' do
    it "returns this document's categories" do
      expect(document.categories).to eq([parts, article])
    end
  end
  
  describe '#recalculate_cached_setting_keys' do
    it 'recalculates the setting_keys cache' do
      expect {
        document.category_ids = [parts, article, rubrics].map(&:id)
        document.recalculate_cached_setting_keys
      }.to change { document.cached_setting_keys }.to(
        [parts, article, rubrics].flat_map(&:setting_keys)
      )
    end
  end
  
  describe '#recalculate_cached_setting_keys!' do
    before(:each) do
      document.category_ids = [parts, article, rubrics].map(&:id)
    end
    
    it 'updates the setting_keys cache in database' do
      document.recalculate_cached_setting_keys!
      expect(document.reload.cached_setting_keys).to eq(
        [parts, article, rubrics].flat_map(&:setting_keys)
      )
    end
    
    it "doesn't update the updated_at attribute" do
      expect {
        document.recalculate_cached_setting_keys!
      }.not_to change { document.reload.updated_at }
    end
  end
  
  describe '#settings' do
    it 'merges settings using the setting_keys cache' do
      expect(document.settings).to eq(
        document.categories.inject(Hash.new) do |hash, category|
          hash.deep_merge(category.settings)
        end
      )
    end
  end
  
  describe '#add_participant' do
    before(:each) do
      document.participant_ids = [1]
    end
    
    context 'with new participant' do
      it 'adds participant id to participants of document' do
        document.add_participant(2)
        
        expect(document.participant_ids).to eq([1, 2])
      end
    end
    
    context 'with existing participant' do
      it "doesn't add participant id" do
        document.add_participant(1)
        
        expect(document.participant_ids).to eq([1])
      end
    end
  end
  
  describe '#matching_lists' do
    it 'returns an array of matching lists' do
      list1 = create(:list, category_ids: [parts.id, article.id])
      list2 = create(:list, category_ids: [parts.id, rubrics.id])
      list3 = create(:list, category_ids: [parts.id], blacklisted_category_ids: [article.id])
      
      lists = document.matching_lists
      expect(lists).to include(list1)
      expect(lists).not_to include(list2)
      expect(lists).not_to include(list3)
    end
  end
end
