require 'spec_helper'

describe ListItem do
  describe '#set_effective_timestamp' do
    let(:document) { create(:document, published: true, published_at: Time.now) }
    let(:list) { create(:list) }
    let(:item) { ListItem.create!(list: list, document: document) }
  
    
    it 'sets the correct timestamp to item.effective_timestamp' do
      document = create(:document, published: true, published_at: Time.now)
      list = create(:list, duration: 1.hour)
      item = ListItem.create!(list: list, document: document, top: 3)

      item.update_effective_timestamp
      
      expect(item.effective_timestamp).to eq(document.published_at + item.top * list.duration)
    end
  end
end
