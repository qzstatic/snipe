require 'spec_helper'

describe Editor do
  let(:editor) { create(:editor) }
  
  describe '.authenticate' do
    context 'with correct login/password' do
      let(:editor_params) do
        {
          login: editor.login,
          password: editor.password
        }
      end
      
      it 'returns the editor' do
        found_editor = Editor.authenticate(editor_params)
        expect(found_editor).to eq(editor)
      end
    end
    
    context 'with correct email/password' do
      let(:editor_params) do
        {
          email: editor.email,
          password: editor.password
        }
      end
      
      it 'returns the editor' do
        found_editor = Editor.authenticate(editor_params)
        expect(found_editor).to eq(editor)
      end
    end
    
    context 'with wrong login/password' do
      let(:editor_params) do
        {
          login: editor.login,
          password: 'wrong'
        }
      end
      
      it 'returns false' do
        expect(Editor.authenticate(editor_params)).to be_falsy
      end
    end
    
    context 'with disabled editor' do
      let(:editor) { create(:editor, enabled: false) }
      
      let(:editor_params) do
        {
          login: editor.login,
          password: editor.password
        }
      end
      
      it 'returns false' do
        expect(Editor.authenticate(editor_params)).to be_falsy
      end
    end
  end
  
  describe '#groups' do
    let(:group)  { create(:admins) }
    let(:editor) { create(:editor, group_ids: [group.id]) }
    
    it "returns this editor's groups" do
      expect(editor.groups).to include(group)
    end
  end
end
