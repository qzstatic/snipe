require 'spec_helper'

describe List do
  let(:domain)            { create(:domain) }
  let(:rubrics)           { create(:rubrics) }
  let(:rubric)            { create(:rubric, parent: rubrics) }
  let(:required_category) { create(:rubric, slug: 'required', parent: rubrics) }
  
  def create_document(params = {})
    params = {
      category_ids: [domain.id, rubric.id],
      published:    true,
      published_at: Time.now
    }.merge(params)
    
    create(:document, params)
  end
  
  describe '#categories' do
    it "returns list's categories" do
      list = create(:list, category_ids: [domain.id, rubric.id])
      
      expect(list.categories).to include(domain)
      expect(list.categories).to include(rubric)
    end
  end
  
  describe 'before_destroy' do
    before(:each) do
      @first_list  = create(:list, slug: 'first')
      @second_list = create(:list, slug: 'second')
      
      @first_page  = create(
        :page,
        title: 'first',
        list_ids: [@first_list.id, @second_list.id]
      )
      @second_page = create(
        :page,
        title: 'second',
        list_ids: [@first_list.id, @second_list.id]
      )
    end
    
    it 'removes the list from all pages' do
      @first_list.destroy
      
      [@first_page, @second_page].each do |page|
        expect(page.reload.list_ids).to eq([@second_list.id])
      end
    end
  end
  
  describe 'associations' do
    before(:each) do
      @list = create(:list, category_ids: [domain.id], blacklisted_category_ids: [rubrics.id])
      
      @first_document = create(:document, published: true, published_at: 1.hour.ago, category_ids: [domain.id])
      Hooks::Documents::ListItems.new(@first_document).save
      
      @second_document = create(:document, published: true, published_at: Time.now, category_ids: [domain.id])
      Hooks::Documents::ListItems.new(@second_document).save
    end
    
    describe '#items' do
      it 'returns items ordered by sort_criteria' do
        expect(@list.items.first.document).to  eq(@second_document)
        expect(@list.items.second.document).to eq(@first_document)
      end
    end
    
    describe '#documents' do
      it 'returns documents ordered by their respective items sort_criteria' do
        expect(@list.documents.first).to eq(@second_document)
        expect(@list.documents.second).to eq(@first_document)
      end
      
      it 'returns only matching and published documents' do
        @third_document = create(:document, published: true, published_at: 3.hour.ago, category_ids: [rubrics.id])
        @fourth_document = create(:document, category_ids: [domain.id])
        
        expect(@list.matching_documents).not_to include(@third_document)
        expect(@list.matching_documents).not_to include(@fourth_document)
      end

      it 'return documents where list_id not present in exclude_list_ids' do
        @second_document.exclude_list_ids = [@list.id]
        @second_document.save

        expect(@list.matching_documents).not_to include(@second_document)
        expect(@list.matches?(@second_document)).to eq(false)
      end
    end
  end
end
