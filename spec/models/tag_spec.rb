require 'spec_helper'

describe Tag do
  before(:each) do
    @first_tag  = create(:tag)
    @second_tag = create(:tag, title: 'second_tag')
    
    @tagging = create(:tagging, tag_ids: [@first_tag.id])
  end
  
  describe '#taggings' do
    it 'returns taggings which contains this tag' do
      expect(@first_tag.taggings).to include(@tagging)
      expect(@second_tag.taggings).not_to include(@tagging)
    end
  end
  
  describe '#before_destroy' do
    context 'with taggings' do
      it 'fails' do
        expect {
          @first_tag.destroy
        }.not_to change { Tag.count }
      end
    end
    
    context 'without taggings' do
      it 'succeeds' do
        expect {
          @second_tag.destroy
        }.to change { Tag.count }.by(-1)
      end
    end
  end
end
