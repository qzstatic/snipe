require 'spec_helper'

describe Group do
  before(:each) do
    @admins = create(:admins)
    @guests = create(:guests)
    @admin  = create(:editor, group_ids: [@admins.id])
    @editor = create(:editor)
  end
  
  describe '#editors' do
    it 'returns all editors in this group' do
      expect(@admins.editors).to include(@admin)
      expect(@admins.editors).not_to include(@editor)
    end
  end
  
  describe '#before_destroy' do
    context 'with editors' do
      it 'fails' do
        expect {
          @admins.destroy
        }.not_to change { Group.count }
      end
    end
    
    context 'without editors' do
      it 'succeeds' do
        expect {
          @guests.destroy
        }.to change { Group.count }.by(-1)
      end
    end
  end
end
