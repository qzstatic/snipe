require 'spec_helper'

describe Sandbox do
  before(:each) do
    @admins = create(:admins)
    @guests = create(:guests)
    
    @first_admin  = create(:editor, group_ids: [@admins.id])
    @second_admin = create(:editor, group_ids: [@admins.id])
    @first_guest  = create(:editor, group_ids: [@guests.id])
    @second_guest = create(:editor, group_ids: [@guests.id])
    @nobody       = create(:editor, group_ids: [])
  end
  
  let(:params) do
    ActionController::Parameters.new(
      title:     'Последние документы за 24 часа',
      published: true,
      owner_ids: [
        @first_admin.id,
        @nobody.id,
        -@guests.id
      ]
    )
  end
  
  describe '.by_owner' do
    context 'with a private sandbox' do
      before(:each) do
        creator = Savers::SandboxesCreator.new(params, @first_admin)
        creator.save
        @sandbox = creator.object
      end
      
      context 'with an explicitly owning editor' do
        it 'returns the sandbox' do
          expect(Sandbox.by_owner(@first_admin.id)).to include(@sandbox)
          expect(Sandbox.by_owner(@nobody.id)).to include(@sandbox)
        end
      end
      
      context 'with an editor from owning group' do
        it 'returns the sandbox' do
          expect(Sandbox.by_owner(@first_guest.id)).to include(@sandbox)
          expect(Sandbox.by_owner(@second_guest.id)).to include(@sandbox)
        end
      end
      
      context 'with an outsider' do
        it "doesn't return the sandbox" do
          expect(Sandbox.by_owner(@second_admin.id)).not_to include(@sandbox)
        end
      end
    end
    
    context 'with a public sandbox' do
      before(:each) do
        params[:owner_ids] << 0
        creator = Savers::SandboxesCreator.new(params, @first_admin)
        creator.save
        @sandbox = creator.object
      end
      
      it 'returns the sandbox for any editor' do
        expect(Sandbox.by_owner(@first_admin.id)).to include(@sandbox)
        expect(Sandbox.by_owner(@second_admin.id)).to include(@sandbox)
      end
    end
  end
  
  describe '#documents' do
    before(:each) do
      domains          = create(:domains)
      @category        = create(:domain, parent: domains)
      @except_category = create(:domain, parent: domains)
      
      @matching_document = create(
        :document,
        category_ids:    [@category.id],
        participant_ids: [@first_admin.id],
        published:        true
      )
      
      @old_document = create(
        :document,
        category_ids:    [@category.id],
        participant_ids: [@first_admin.id],
        published:        true,
        updated_at:       1.day.ago
      )
      
      @blacklisted_document = create(
        :document,
        category_ids:    [@category.id, @except_category.id],
        participant_ids: [@first_admin.id],
        published:        true
      )
      
      @someones_document = create(
        :document,
        category_ids:    [@category.id],
        participant_ids: [@first_guest.id],
        published:        true
      )
      
      @uncategorized_document = create(
        :document,
        category_ids:    [],
        participant_ids: [@first_admin.id],
        published:        true
      )
      
      @unpublished_document = create(
        :document,
        category_ids:    [@category.id],
        participant_ids: [@first_admin.id]
      )
      
      params.merge!(
        base_date:       1.hour.ago,
        date_offset:     2.hours,
        participant_ids: [@first_admin.id],
        category_ids:    [@category.id, -@except_category.id]
      )
      
      @sandbox = Savers::SandboxesCreator.new(params, @first_admin).tap(&:save).object
    end
    
    it 'returns documents filtered by sandbox' do
      expect(@sandbox.documents).to     include(@matching_document)
      expect(@sandbox.documents).not_to include(@old_document)
      expect(@sandbox.documents).not_to include(@someones_document)
      expect(@sandbox.documents).not_to include(@uncategorized_document)
      expect(@sandbox.documents).not_to include(@unpublished_document)
      expect(@sandbox.documents).not_to include(@blacklisted_document)
    end
  end
  
  describe '#ids' do
    subject do
      Savers::SandboxesCreator.new(
        params.merge(
          participant_ids: [
            @first_admin.id,
            @nobody.id,
            -@guests.id
          ]
        ),
        @first_admin
      ).tap(&:save).object
    end
    
    it 'returns ids for querying on documents.participant_ids' do
      expect(subject.ids).to include(@first_admin.id)
      expect(subject.ids).to include(@first_guest.id)
      expect(subject.ids).to include(@second_guest.id)
      expect(subject.ids).to include(@nobody.id)
      
      expect(subject.ids).not_to include(@second_admin.id)
    end
  end
end
