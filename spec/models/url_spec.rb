require 'spec_helper'

describe Url do
  let(:category) { create(:domain) }
  let(:document) { create(:document, category_ids: [category.id]) }
  let(:url)      { create(:url, document_id: document.id) }
  
  describe '#settings' do
    it "returns url's settings from it's document" do
      expect(url.settings).to eq(document.settings)
    end
  end
end
