require 'spec_helper'

describe Nestable do
  before(:each) do
    @subrubrics = create(:subrubrics)
    @rubric     = @subrubrics.parent
    @rubrics    = @rubric.parent
  end
  
  context 'on a root object' do
    it 'leaves the path empty' do
      expect(@rubrics.path).to be_empty
    end
    
    describe '#descendants' do
      it "returns all object's descendants" do
        expect(@rubrics.descendants).to include(@rubric)
        expect(@rubrics.descendants).to include(@subrubrics)
        expect(@rubrics.descendants).not_to include(@rubrics)
      end
    end
    
    describe '#children' do
      it 'returns only direct children of the object' do
        expect(@rubrics.children).to include(@rubric)
        expect(@rubrics.children).not_to include(@subrubrics)
        expect(@rubrics.children).not_to include(@rubrics)
      end
    end
    
    describe '#ancestors' do
      it 'returns an empty array' do
        expect(@rubrics.ancestors).to eq([])
      end
    end
    
    describe '#root_ancestor' do
      it 'returns self' do
        expect(@rubrics.root_ancestor).to eq(@rubrics)
      end
    end
    
    describe '#parent' do
      it 'returns nil' do
        expect(@rubrics.parent).to be_nil
      end
    end
    
    describe '#root?' do
      it 'returns true' do
        expect(@rubrics).to be_root
      end
    end
    
    describe '#destroy' do
      it "destroys itself and it's children" do
        expect {
          @rubrics.destroy
        }.to change { Category.count }.by(-3)
      end
    end
  end
  
  context 'on a child object' do
    it "fills the path with ancestors' ids" do
      expect(@subrubrics.path).to eq([@rubrics.id, @rubric.id])
    end
    
    describe '#descendants' do
      it 'returns an empty array' do
        expect(@subrubrics.descendants).to eq([])
      end
    end
    
    describe '#children' do
      it 'returns an empty array' do
        expect(@subrubrics.children).to eq([])
      end
    end
    
    describe '#ancestors' do
      it "returns all object's ancestors" do
        expect(@subrubrics.ancestors).to eq([@rubrics, @rubric])
      end
    end
    
    describe '#root_ancestor' do
      it 'returns the root parent' do
        expect(@subrubrics.root_ancestor).to eq(@rubrics)
        expect(@rubric.root_ancestor).to eq(@rubrics)
      end
    end
    
    describe '#parent' do
      it 'returns the parent' do
        expect(@subrubrics.parent).to eq(@rubric)
        expect(@rubric.parent).to eq(@rubrics)
      end
    end
    
    describe '#root?' do
      it 'returns false' do
        expect(@rubric).not_to be_root
        expect(@subrubrics).not_to be_root
      end
    end
    
    describe '#destroy' do
      it 'just destroys itself' do
        expect {
          @subrubrics.destroy
        }.to change { Category.count }.by(-1)
      end
    end
  end
  
  describe '#depth' do
    it "returns the object's depth in the hierarchy" do
      expect(@rubrics.depth).to eq(1)
      expect(@rubric.depth).to eq(2)
      expect(@subrubrics.depth).to eq(3)
    end
  end
  
  describe '.by_depth' do
    let(:first_level) { Category.by_depth(1) }
    let(:second_level) { Category.by_depth(2) }
    let(:third_level) { Category.by_depth(3) }
    
    it 'returns objects by depth' do
      expect(first_level).to eq([@rubrics])
      expect(second_level).to eq([@rubric])
      expect(third_level).to eq([@subrubrics])
    end
  end
end
