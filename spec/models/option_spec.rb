require 'spec_helper'

describe Option do
  let(:option) do
    Option.create(
      type: 'source',
      value: {
        url:   'ya.ru',
        title: 'Yandex'
      }
    )
  end
  
  before(:each) do
    option
  end
  
  describe '.source' do
    it 'returns options with the type "source"' do
      expect(Option.source).to include(option)
    end
  end
  
  describe '.by_type' do
    it 'returns options with a specified type' do
      expect(Option.by_type(:source)).to include(option)
    end
  end
  
  describe '.grouped_by_type' do
    it 'returns options grouped by type' do
      expect(Option.grouped_by_type[:source]).to include(option)
    end
  end
  
  describe '#before_destroy' do
    let(:parts)    { create(:parts) }
    let(:document) { create(:document, category_ids: [parts.id]).porcupined }
    
    before(:each) do
      document.source_id = option.id
      document.save
    end
    
    it 'fails if document has this option in source' do
      expect {
        option.destroy
      }.not_to change { Option.count }
    end
  end
end
