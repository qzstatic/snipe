require 'spec_helper'

describe Category do
  before(:each) do
    @domain   = create(:domain)
    @rubric   = create(:rubric)
    @document = create(:document, category_ids: [@domain.id])
  end
  
  describe '#documents' do
    it 'returns all documents in this category' do
      expect(@domain.documents).to include(@document)
      expect(@rubric.documents).not_to include(@document)
    end
  end
  
  describe '#pseudo?' do
    context 'on pseudo categories' do
      it 'returns true' do
        expect(@domain.parent).to be_pseudo
      end
    end
    
    context 'on real categories' do
      it 'returns false' do
        expect(@domain).not_to be_pseudo
      end
    end
  end
  
  describe '#before_destroy' do
    context 'with documents' do
      it 'fails' do
        expect {
          @domain.destroy
        }.not_to change { Category.count }
      end
    end
    
    context 'without documents' do
      it 'succeeds' do
        expect {
          @rubric.destroy
        }.to change { Category.count }.by(-1)
      end
    end
  end
  
  describe '.available_setting_keys' do
    it 'returns all available setting keys' do
      keys = Category.available_setting_keys
      
      keys.each do |key|
        expect(key).to match(/[a-z]+\.[a-z]+/)
      end
    end
  end
  
  describe '#recalculate_slug_path' do
    it 'builds a slug path from ancestor slugs' do
      @rubric.recalculate_slug_path
      expect(@rubric.slug_path).to eq(
        [@rubric.parent.slug, @rubric.slug].map(&:to_s)
      )
    end
  end
  
  describe '#regenerate_setting_keys_cache' do
    before(:each) do
      documents = @domain.documents
      allow(@domain).to receive(:documents).and_return(documents)
    end
    
    it "regenerates the setting_keys cache in all category's documents" do
      expect(@domain.documents).to all(receive(:recalculate_cached_setting_keys!))
      @domain.regenerate_setting_keys_cache
    end
  end
end
