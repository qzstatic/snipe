require 'spec_helper'

describe Tagging do
  let(:first_tag)  { create(:tag) }
  let(:second_tag) { create(:tag) }
  
  describe '#tags' do
    context 'with tags' do
      let(:tagging) { create(:tagging, tag_ids: [first_tag.id, second_tag.id].reverse) }
      
      it 'returns tags in the correct order' do
        expect(tagging.tags).to eq([second_tag, first_tag])
      end
    end
    
    context 'without tags' do
      let(:tagging) { create(:tagging) }
      
      it 'returns empty relation' do
        expect(tagging.tags).to be_empty
      end
    end
  end
end
