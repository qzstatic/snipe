require 'spec_helper'

describe Link do
  let(:domain) { create(:domain) }
  let(:document) { create(:document, category_ids: [domain.id]) }
  let(:bound_document) { create(:document, category_ids: [domain.id]) }
  let(:link) { create(:link, document_id: document.id, bound_document_id: bound_document.id) }
  
  describe '#settings' do
    it "returns the settings of link's document" do
      expect(link.settings).to eq(document.settings)
    end
  end
end
