FactoryGirl.define do
  factory :tagging do
    document
    type 'Supertag'
    tag_ids []
  end
end
