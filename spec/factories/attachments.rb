FactoryGirl.define do
  factory :attachment do
    type 'image'
    directory 'some/path'
  end
end
