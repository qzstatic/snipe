FactoryGirl.define do
  factory :link do
    section :reference
    position 5
    
    document
    association :bound_document, factory: :document
  end
end
