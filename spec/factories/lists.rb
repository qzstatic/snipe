FactoryGirl.define do
  sequence :list_slug do |n|
    "list_slug_#{n}"
  end

  factory :list do
    slug  { generate(:list_slug) }
    title 'Список'
    limit 10
  end
end
