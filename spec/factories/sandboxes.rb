FactoryGirl.define do
  factory :sandbox do
    title 'Песочница'
    
    factory :last_week_sandbox do
      title 'Последняя неделя'
      date_offset -1.week
    end
    
    factory :fixed_month_sandbox do
      title 'Январь 2014'
      base_date Time.new(2014, 1)
      date_offset 1.month
    end
    
    factory :published_sandbox do
      published true
    end
  end
end
