FactoryGirl.define do
  sequence :slug do |n|
    "category_slug_#{n}"
  end
  
  factory :domains, class: 'Category' do
    slug :domains
    title 'Домены'
    slug_path %w(domains)
    setting_keys %w(attachments.common boxes.common)
  end
  
  factory :domain, class: 'Category' do
    slug
    title 'vedomosti.ru'
    slug_path { ['domains', slug] }
    
    association :parent, factory: :domains
  end
  
  factory :rubrics, class: 'Category' do
    slug :rubrics
    title 'Рубрики'
    slug_path %w(rubrics)
    setting_keys %w(lists.rubrics)
  end
  
  factory :rubric, class: 'Category' do
    slug :russia
    title 'Россия'
    slug_path { ['rubrics', slug] }
    
    association :parent, factory: :rubrics
  end
  
  factory :subrubrics, class: 'Category' do
    slug :subrubrics
    title 'Подрубрики'
    slug_path %w(rubrics russia subrubrics)
    
    association :parent, factory: :rubric
  end
  
  factory :subrubric, class: 'Category' do
    slug :life
    title 'Из жизни'
    slug_path %w(rubrics russia subrubrics life)
    
    association :parent, factory: :subrubrics
  end
  
  factory :parts, class: 'Category' do
    slug :parts
    title 'Разделы'
    slug_path %w(parts)
    setting_keys %w(urls.parts source.parts assignees.parts links.parts)
  end
  
  factory :news, class: 'Category' do
    slug :news
    title 'Новости'
    slug_path %w(parts news)
    setting_keys %w(boxes.news)
    
    association :parent, factory: :parts
  end
  
  factory :article, class: 'Category' do
    slug :article
    title 'Статьи'
    slug_path %w(parts article)
    setting_keys %w(boxes.article head.article title_image.article links.article)
    
    association :parent, factory: :parts
  end
  
  factory :column, class: 'Category' do
    slug :column
    title 'Колонки'
    slug_path %w(parts column)
    setting_keys %w(head.column)
    
    association :parent, factory: :parts
  end
  
  factory :kinds, class: 'Category' do
    slug :kinds
    title 'Типы документов'
    slug_path %w(kinds)
  end
  
  factory :links, class: 'Category' do
    slug :links
    title 'Ссылка'
    slug_path %w(kinds links)
    setting_keys %w(urls.outer_link source.outer_link)
    
    association :parent, factory: :kinds
  end
end
