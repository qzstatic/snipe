FactoryGirl.define do
  sequence :title do |n|
    "Tag #{n}"
  end
  
  factory :tag do
    title
  end
end
