FactoryGirl.define do
  sequence :email do |n|
    "email#{n}@factory.com"
  end
  
  sequence :login do |n|
    "username#{n}"
  end
  
  factory :editor do
    login
    email
    password    'secret'
    first_name  'Всеволод'
    last_name   'Ромашов'
    enabled     true
    group_ids []
  end
end
