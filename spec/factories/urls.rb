FactoryGirl.define do
  factory :url do
    url_data(
      domain:    'vedomosti.ru',
      rubric:    'finance',
      type:      'news',
      pseudo_id: '123',
      slug:      'visa'
    )
    rendered 'http://vedomosti.ru/finance/news/visa'
    document
  end
end
