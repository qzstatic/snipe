FactoryGirl.define do
  factory :box do
    type :root
    deleted false
    
    factory :box_copy do
      type :copy
      association :original, factory: :box
    end
  end
end
