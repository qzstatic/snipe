FactoryGirl.define do
  sequence :page_slug do |n|
    "page_slug_#{n}"
  end
  
  factory :page do
    title 'Страница'
    slug { generate(:page_slug) }
  end
end
