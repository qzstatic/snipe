FactoryGirl.define do
  factory :attachment_version do
    attachment
    slug 'preview'
    filename 'preview-123.jpg'
  end
end
