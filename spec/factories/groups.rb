FactoryGirl.define do
  factory :group do
    slug 'editors'
    title 'Редакторы'
    
    factory :admins do
      slug 'administrators'
      title 'Админы'
    end
    
    factory :guests do
      slug 'guests'
      title 'Гости'
    end
    
    factory :proofreaders do
      slug 'proofreaders'
      title 'Корректоры'
    end

    factory :web_editors do
      slug 'web_editors'
      title 'Выпуск'
    end
  end
end
