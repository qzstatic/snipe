require 'spec_helper'

describe BoxProxy do
  let(:editor)  { create(:editor) }
  let(:domain)  { create(:domain) }
  let(:domains) { domain.parent }
  
  let(:first_document) do
    create(:document, category_ids: [domains.id, domain.id])
  end
  
  let(:second_document) do
    create(:document, category_ids: [domains.id, domain.id])
  end
  
  before(:each) do
    first_document.create_root_box
    second_document.create_root_box
    
    @original = Savers::BoxesCreator.new(
      ActionController::Parameters.new(
        type:       'paragraph',
        body:       'First level paragraph',
        text_align: 'right',
        parent_id:  first_document.root_box.id,
        position:   1,
        enabled:    true
      ),
      editor
    ).saved_object
    
    Savers::BoxesCreator.new(
      ActionController::Parameters.new(
        type:       'paragraph',
        body:       'Second level paragraph',
        text_align: 'right',
        parent_id:  @original.id,
        position:   2,
        enabled:    false
      ),
      editor
    ).save
    
    @copy = Savers::BoxesCopier.new(
      ActionController::Parameters.new(
        parent_id: second_document.root_box.id
      ),
      editor,
      @original
    ).saved_object
  end
  
  context 'with original box' do
    subject { BoxProxy.from(@original) }
    
    describe 'fixed attributes' do
      it 'return the attributes of the box' do
        expect(subject.type).to     eq(@original.type)
        expect(subject.position).to eq(@original.position)
        expect(subject.enabled).to  eq(@original.enabled)
      end
    end
    
    describe 'dynamic attributes' do
      it 'return the attributes of the box' do
        expect(subject.body).to       eq(@original.body)
        expect(subject.text_align).to eq(@original.text_align)
      end
    end
    
    describe '#descendants' do
      it 'returns the descendants of the box' do
        expect(subject.descendants).to eq(@original.descendants)
      end
    end
    
    describe '#respond_to_missing?' do
      it 'returns true for all methods of the box' do
        expect(subject).to respond_to(:type)
        expect(subject).to respond_to(:body)
      end
      
      it 'returns false for unknown methods' do
        expect(subject).not_to respond_to(:unknown)
      end
    end
    
    describe '.with_descendants' do
      it "returns an array of proxies around the box and it's descendants" do
        boxes = BoxProxy.with_descendants(@original)
        
        expect(boxes.map(&:id)).to eq(
          [
            @original.id,
            @original.children.first.id
          ]
        )
      end
    end
  end
  
  context 'with a copy' do
    subject { BoxProxy.from(@copy) }
    
    describe '#type' do
      it 'returns the type of the original box' do
        expect(subject.type).to eq(@original.type)
      end
    end
    
    describe 'other fixed attributes' do
      it 'return the attributes of the copy' do
        expect(subject.position).to eq(@copy.position)
        expect(subject.enabled).to  eq(@copy.enabled)
      end
    end
    
    describe 'dynamic attributes' do
      it 'return the attributes of the original' do
        expect(subject.body).to       eq(@original.body)
        expect(subject.text_align).to eq(@original.text_align)
      end
    end
    
    describe '#descendants' do
      it 'returns the descendants of the original' do
        expect(subject.descendants).to eq(@original.descendants)
      end
    end
    
    describe '#respond_to_missing?' do
      it 'returns true for all methods of the box' do
        expect(subject).to respond_to(:type)
        expect(subject).to respond_to(:body)
      end
      
      it 'returns false for unknown methods' do
        expect(subject).not_to respond_to(:unknown)
      end
    end
    
    describe '.with_descendants' do
      it "returns an array of proxies around the original and it's descendants" do
        boxes = BoxProxy.with_descendants(@copy)
        
        expect(boxes.map(&:id)).to eq(
          [
            @copy.id,
            @original.children.first.id
          ]
        )
      end
      
      context 'with a parent of a copy' do
        it "returns an array with proxies around the copy, it's ancestors and descendants" do
          boxes = BoxProxy.with_descendants(second_document.root_box)
          
          expect(boxes.map(&:id)).to eq(
            [
              second_document.root_box.id,
              @copy.id,
              @original.children.first.id
            ]
          )
        end
      end
    end
  end
end
