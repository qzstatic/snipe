require 'spec_helper'

describe Participants do
  let(:zone) { create(:zone) }
  
  before(:each) do
    @editors = create(:group)
    @admins  = create(:admins)
    @guests  = create(:guests)
    
    @editor = create(:editor, group_ids: [@editors.id], login: 'editor')
    @admin  = create(:editor, group_ids: [@admins.id], login: 'admin')
    @guest  = create(:editor, group_ids: [@guests.id], login: 'guest')
  end
  
  subject do
    Participants.new(
      [@admin.id, @guest.id],
      [@editors.id, @guests.id, @editors.id]
    )
  end
  
  describe '#to_a' do
    it 'includes ids of all editors' do
      expect(subject.to_a).to include(@admin.id)
      expect(subject.to_a).to include(@guest.id)
    end
    
    it 'includes group_ids of all editors' do
      expect(subject.to_a).to include(-@admins.id)
      expect(subject.to_a).to include(-@guests.id)
    end
    
    it 'includes ids of explicitly specified groups' do
      expect(subject.to_a).to include(-@editors.id)
    end
    
    it "includes editors' ids of explicitly specified groups" do
      expect(subject.to_a).to include(@editor.id)
    end
    
    it 'includes zero' do
      expect(subject.to_a).to include(Participants::EVERYONE)
    end
  end
  
  describe '.divide' do
    it 'divides editor_ids from group_ids' do
      expect(Participants.divide([1, 2, -2, 3, -5])).to eq(
        {
          editors: [1, 2, 3],
          groups:  [2, 5]
        }
      )
    end
  end
end
