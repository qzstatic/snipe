require 'spec_helper'

describe FullDocumentSerializer do
  let(:document_settings) do
    {
      boxes: {
        root: {
          label:    'Корневой бокс',
          children: %w(paragraph)
        },
        paragraph: {
          label: 'Параграф',
          attributes: {
            kind: [
              'string',
              default: 'plain',
              label:   'Тип',
              type:    'select'
            ],
            body: [
              'string',
              label: 'Содержание',
              type:  'text'
            ],
            text_align: [
              'string',
              label: 'Выравнивание абзаца',
              type:  'align'
            ]
          }
        }
      },
      head: {
        attributes: {
          author: [
            'association',
            'document',
            ignored:  true,
            label:    'Автор',
            type:     'document',
            position: 0
          ],
          quote: [
            'string',
            label:    'Цитата',
            type:     'text',
            rows:     3,
            position: 10
          ],
          subtitle: [
            'string',
            label:    'Подзаголовок',
            type:     'text',
            rows:     3,
            position: 20
          ]
        }
      },
      links: {
        reference: {
          label:     'Ссылки по теме',
          filter:    %w(kind materials parts),
          not_found: %w(kind links)
        },
        authors: {
          label:  'Список авторов',
          filter: %w(kind authors)
        }
      },
      assignees: true
    }
  end
  
  let(:document) { create(:document) }
  
  before(:each) do
    allow(document).to receive(:settings).and_return(document_settings)
  end
  
  subject { described_class.new(document) }
  
  describe '#as_json' do
    it 'returns processed settings' do
      expect(subject.as_json[:settings]).to eq(
        boxes: {
          root: {
            label:    'Корневой бокс',
            children: %w(paragraph)
          },
          paragraph: {
            label: 'Параграф',
            attributes: [
              {
                name:    'kind',
                default: 'plain',
                label:   'Тип',
                type:    'select'
              },
              {
                name:  'body',
                label: 'Содержание',
                type:  'text'
              },
              {
                name:  'text_align',
                label: 'Выравнивание абзаца',
                type:  'align'
              }
            ]
          }
        },
        head: {
          attributes: [
            {
              name:    'author_id',
              ignored: true,
              label:   'Автор',
              type:    'document'
            },
            {
              name:  'quote',
              label: 'Цитата',
              type:  'text',
              rows:  3
            },
            {
              name:  'subtitle',
              label: 'Подзаголовок',
              type:  'text',
              rows:  3
            }
          ]
        },
        links: document_settings[:links],
        assignees: true
      )
    end
  end
end
