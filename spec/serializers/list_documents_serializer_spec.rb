require 'spec_helper'

describe ListDocumentsSerializer do
  let(:first_document)  { create(:document, published: true, published_at: 1.hour.ago) }
  let(:second_document) { create(:document, published: true, published_at: 2.hour.ago) }
  
  before(:each) do
    @list = create(:list)
    Hooks::Documents::ListItems.new(first_document).save
    Hooks::Documents::ListItems.new(second_document).save
  end
  
  subject { ListDocumentsSerializer.new(@list) }
  
  describe '#as_json' do
    it 'returns documents along with their list items' do
      data = subject.as_json.map(&:deep_symbolize_keys)
      
      expect(data.first[:id]).to eq(first_document.id)
      expect(data.first[:list_item][:id]).to eq(@list.items.first.id)
      
      expect(data.second[:id]).to eq(second_document.id)
      expect(data.second[:list_item][:id]).to eq(@list.items.second.id)
    end
  end
end
