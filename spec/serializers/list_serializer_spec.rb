require 'spec_helper'

describe ListSerializer do
  let(:list) { create(:list, category_ids: [1,2,3], blacklisted_category_ids: [4,5,6]) }
 
  subject { ListSerializer.new(list) }
  
  describe '#as_json' do
    it 'returns category_ids without blacklisted_category_ids' do
      data = subject.as_json

      expect(data[:category_ids]).to eq([1, 2, 3])
      expect(data[:blacklisted_category_ids]).to eq([4, 5, 6])
    end
  end
end
