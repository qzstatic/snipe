require 'spec_helper'

describe BoxSerializer do
  let(:editor)   { create(:editor) }
  let(:article)  { create(:article) }
  let(:document) { create(:document, category_ids: [article.id]) }
  let(:root_box) { document.create_root_box; document.root_box }
  
  context 'with a usual box' do
    let(:box) { create(:box, parent_id: root_box.id) }
    
    subject { described_class.new(BoxProxy.from(box)) }
    
    describe '#as_json' do
      it 'returns id, type, enabled, position and dynamic attributes' do
        expect(subject.as_json[:id]).to eq(box.id)
        expect(subject.as_json[:type]).to eq(box.type)
        expect(subject.as_json[:enabled]).to eq(box.enabled)
        expect(subject.as_json[:position]).to eq(box.position)
      end
    end
  end
  
  context 'with a copy' do
    let(:box) { create(:box_copy, parent_id: root_box.id) }
    
    before(:each) do
      box.original.parent_id = root_box
      box.original.type = 'paragraph'
      
      box.original.porcupined.tap do |porcupine|
        porcupine.body = 'I am a box.'
      end
      
      box.original.save
    end
    
    subject { described_class.new(BoxProxy.from(box)) }
    
    describe '#as_json' do
      it 'returns enabled and position from the copy' do
        expect(subject.as_json[:enabled]).to eq(box.enabled)
        expect(subject.as_json[:position]).to eq(box.position)
      end
      
      it 'returns type and dynamic attributes from the original' do
        expect(subject.as_json[:type]).to eq(box.original.type)
        expect(subject.as_json['body']).to eq(box.original.porcupined.body)
      end
      
      it 'returns readonly: true' do
        expect(subject.as_json[:readonly]).to eq(true)
      end
    end
  end
end
