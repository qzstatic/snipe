require 'spec_helper'

describe BaseSerializer do
  let(:serializer) { serializer_class.new(object) }
  
  describe '#id' do
    let(:serializer_class) do
      Class.new(BaseSerializer) do
        attributes :name
      end
    end
    
    let(:object) do
      Struct.new(:id, :name) do
        include ActiveModel::Serialization
      end.new(1, 'John')
    end
    
    it "returns the object's id" do
      expect(serializer.id).to eq(object.id)
    end
  end
end
