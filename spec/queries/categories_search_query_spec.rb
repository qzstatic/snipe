require 'spec_helper'

describe CategoriesSearchQuery do
  describe '#search' do
    before(:each) do
      @domain = create(:domain)
      @rubric = create(:rubric)
    end
    
    it 'returns all categories with a given type' do
      expect(subject.search('domains')).to eq([@domain])
      expect(subject.search('domains')).not_to eq([@rubric])
      
      expect(subject.search('rubrics')).to eq([@rubric])
      expect(subject.search('rubrics')).not_to eq([@domain])
    end
  end
end
