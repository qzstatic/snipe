require 'spec_helper'

describe UrlsSearchQuery do
  let(:urls) { Url.all }
  
  before(:each) do
    [
      'http://ya.ru/politics/news/33610271',
      'http://ya.ru/newspaper/article/757531'
    ].each { |url| create(:url, rendered: url) }
    
    subject { UrlsSearchQuery.new(urls) }
  end
  
  describe '#find_by_url' do
    context 'with url of existing url' do
      it 'returns found url' do
        expect(subject.find_by_url('http://ya.ru/politics/news/33610271')).to eq(Url.first)
      end
    end
    
    context 'with new url' do
      it 'raises ActiveRecord::RecordNotFound' do
        expect do
          subject.find_by_url('http://ya.ru/')
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
