require 'spec_helper'

describe DocumentsSearchQuery do
  let(:documents) { Document.all }
  let(:domain)    { create(:domain, slug: 'yandex.ru') }
  let(:rubric)    { create(:rubric, slug: 'usa') }
  let(:news)      { create(:news) }
  
  before(:each) do
    create(:document, title: 'Владимир Путин',    category_ids: [domain.id, rubric.id, news.id])
    create(:document, title: 'Алексей Навальный', category_ids: [domain.id, rubric.id])
    create(:document, title: 'Путин',             category_ids: [domain.id, rubric.id])
  end
  
  subject { DocumentsSearchQuery.new(documents) }
  
  describe '#search' do
    let(:query) { 'путин' }
    let(:category_slugs) { %w(yandex.ru usa).join(',') }
    
    it 'returns matching documents' do
      expect(subject.search(query, category_slugs: category_slugs).count).to eq(2)
    end
    
    context 'with category_ids' do
      let(:category_ids) { [domain.id, rubric.id, -news.id].join(',') }
      
      it 'returns matching documents' do
        expect(subject.search(query, category_ids: category_ids).count).to eq(1)
      end
    end
  end
  
  describe '#fresh' do
    context 'with time in past' do
      let(:timestamp) { 1.hour.to_i }
      
      it 'returns matching documents' do
        expect(subject.fresh(timestamp).count).to eq(3)
      end
    end

    context 'with time in future' do
      let(:timestamp) { (Time.now + 1.hour).to_i }
      
      it 'returns nothing' do
        expect(subject.fresh(timestamp).count).to eq(0)
      end
    end
  end
end
