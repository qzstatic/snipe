require 'spec_helper'

describe TagsSearchQuery do
  let(:tags) { Tag.all }
  
  before(:each) do
    [
      'Путин',
      'Владимир Путин',
      'Путин Владимир',
      'Владимир Владимирович'
    ].each { |title| create(:tag, title: title) }
    
    subject { TagsSearchQuery.new(tags) }
  end
  
  describe '#search' do
    it 'returns matching tags' do
      expect(subject.search('путин').count).to eq(3)
      expect(subject.search('владимирович').count).to eq(1)
      expect(subject.search(' путин ').count).to eq(3)
      expect(subject.search('медведев').count).to eq(0)
    end
  end
  
  describe '#find_by_title' do
    context 'with title of existing tag' do
      it 'returns found tag' do
        expect(subject.find_by_title('Путин')).to eq(Tag.first)
      end
    end
    
    context 'with new title' do
      it 'raises ActiveRecord::RecordNotFound' do
        expect do
          subject.find_by_title('путин')
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
