require 'spec_helper'

describe V1::BoxesController do
  let(:editor) { create(:editor) }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    @box = create(:box)
    create(:document, root_box_id: @box.id, cached_setting_keys: %w(box_moving))
  end

  describe 'block functionality' do
    it 'included Blocker::Controller' do
      expect(subject.class.included_modules).to include(Blocker::Controller)
    end
    it 'has route to methods' do
      expect(:put => '/v1/boxes/33/block').to route_to({'format' =>:json, 'action' => 'block', 'object' =>:box, 'controller' => 'v1/boxes', 'id' => '33'})
    end
  end

  describe 'GET :show' do
    it 'renders the requested box' do
      get :show, id: @box.id
      
      expect(response_body[:id]).to eq(@box.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:box_attributes) { ActionController::Parameters.new(type: 'paragraph', parent_id: @box.id) }

      it 'creates and returns a new box' do
        post :create, attributes: box_attributes
        
        expect(response).to be_success
        expect(response_body[:type]).to eq(box_attributes[:type].to_s)
      end
    end
    
    context 'with invalid attributes' do
      let(:box_attributes) do
        { type: nil, parent_id: @box.id }
      end
      
      it "renders 422 with url's errors" do
        post :create, attributes: box_attributes
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end

  describe 'POST :multi' do
    context 'with valid attributes' do
      let(:boxes_attributes) {
        [
          ActionController::Parameters.new(type: 'paragraph', parent_id: @box.id),
          ActionController::Parameters.new(type: 'paragraph', parent_id: @box.id)
        ]
      }
      
      it 'creates and returns a list new boxes' do
        expect {
          post :multi, attributes: boxes_attributes
        }.to change { Box.count }.by(2)
        
        expect(response).to be_success
        expect(response_body.size).to eq(2)
      end
    end
    
    context 'with invalid attributes' do
      let(:boxes_attributes) do
        [ 
          { type: nil, parent_id: @box.id },
          ActionController::Parameters.new(type: 'paragraph', parent_id: @box.id) 
        ]
      end
      
      it 'return error and json for given params' do
        expect {
          post :multi, attributes: boxes_attributes
        }.to change { Box.count }.by(1)

        expect(response).to be_success
        expect(response_body.size).to eq(2)
      end
    end
  end
  
  describe 'PATCH :update' do
    it 'updates an existing box' do
      expect {
        patch :update, id: @box.id, attributes: { enabled: true, updated_at: @box.updated_at.to_s }
      }.to change { Box.find(@box.id).enabled? }
      
      expect(response).to be_success
    end
  end
  
  describe 'POST :copy' do
    it 'makes a copy of an existing box' do
      post :copy, id: @box.id, attributes: { parent_id: @box.id }
      copy = Box.last
      
      expect(copy).to be_copy
      expect(response_body[:id]).to eq(copy.id)
    end
  end
  
  describe 'DELETE :destroy' do
    it 'mark box as deleted' do
      expect {
        delete :destroy, id: @box.id
      }.to change { @box.reload.deleted }.from(false).to(true)
      
      expect(response).to be_success
    end
  end
  context 'box moving' do
    let(:box) { create(:box, type: 'paragraph', parent_id: @box.id) }
    it 'move the box' do
      post :move, id: box.id, attributes: { position: 123, moved_at: box.document.updated_at.to_s }
      box.reload
      pp response.body
      expect(response).to be_success
      expect(box.position).to eq(123)
    end
    it 'multiple move boxes' do
      post :multi_move, attributes: { boxes: [{ id: box.id, position: 123 }],moved_at: box.document.updated_at.to_s }
      box.reload
      pp response.body
      expect(response).to be_success
      expect(box.position).to eq(123)
    end
  end
end
