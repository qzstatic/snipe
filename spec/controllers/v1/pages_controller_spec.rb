require 'spec_helper'

describe V1::PagesController do
  let(:editor) { create(:editor) }
  let(:domains) { create(:domains) }
  let(:rubrics) { create(:rubrics) }
  
  def create_published_document(params = {})
    params = {
      category_ids: [domains.id],
      published:    true,
      published_at: Time.now
    }.merge(params)
    
    create(:document, params)
  end
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    @page = create(:page)
  end
  
  describe 'GET :index' do
    it 'renders all pages' do
      get :index
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested page' do
      get :show, id: @page.id
      
      expect(response_body[:id]).to eq(@page.id)
    end
  end
  
  describe 'GET :last_documents' do
    before(:each) do
      @page.category_ids = [domains.id]
      @page.blacklisted_category_ids = [rubrics.id]
      @page.save
      
      @first_document = create_published_document
      @second_document = create_published_document(category_ids: [domains.id, rubrics.id])
    end
    
    it 'renders documents filtered by page filter' do
      get :last_documents, id: @page.id
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:page_attributes) { attributes_for(:page, title: 'New title') }
      
      it 'creates and returns a new page' do
        post :create, attributes: page_attributes
        
        expect(response).to be_success
        expect(response_body[:title]).to eq(page_attributes[:title])
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with tag's errors" do
        post :create, attributes: { title: nil }
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'PATCH :update' do
    context 'with valid attributes' do
      it 'updates an existing page' do
        expect {
          patch :update, id: @page.id, attributes: { title: 'new title' }
        }.to change { Page.find(@page.id).title }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with tag's errors" do
        patch :update, id: @page.id, attributes: { title: nil }
        
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'DELETE :destroy' do
    it 'deletes the tag' do
      expect {
        delete :destroy, id: @page.id
      }.to change { Page.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
