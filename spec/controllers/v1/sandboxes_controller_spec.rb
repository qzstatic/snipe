require 'spec_helper'

describe V1::SandboxesController do
  let(:admin) { create(:editor) }
  let(:guest) { create(:editor) }
  
  before(:each) do
    sign_in(admin)
    set_access_token_header
    set_content_type_header
    
    @sandbox = create(:last_week_sandbox, owner_ids: [admin.id])
  end
  
  describe 'GET :index' do
    before(:each) do
      create(:fixed_month_sandbox, owner_ids: [guest.id, 0])
    end
    
    context 'for an admin' do
      it 'renders all sandboxes for editor included in owners' do
        get :index
        
        expect(response_body.count).to eq(2)
      end
    end
    
    context 'for a guest' do
      before(:each) do
        sign_in(guest)
        set_access_token_header
      end
      
      it 'renders a public sandbox' do
        get :index
        
        expect(response_body.count).to eq(1)
      end
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested sandbox' do
      get :show, id: @sandbox.id
      
      expect(response_body[:id]).to eq(@sandbox.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:sandbox_attributes) { attributes_for(:fixed_month_sandbox) }
      
      it 'creates and returns a new sandbox' do
        post :create, attributes: sandbox_attributes
        
        expect(response).to be_success
        expect(response_body[:title]).to eq(sandbox_attributes[:title])
      end
    end
    
    context 'with invalid attributes' do
      let(:sandbox_attributes) { attributes_for(:fixed_month_sandbox, title: nil) }
      
      it "renders 422 with sandbox's errors" do
        post :create, attributes: sandbox_attributes
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'PATCH :update' do
    context 'with valid attributes' do
      it 'updates an existing sandbox' do
        expect {
          patch :update, id: @sandbox.id, attributes: { title: 'new title' }
        }.to change { Sandbox.find(@sandbox.id).title }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with sandbox's errors" do
        patch :update, id: @sandbox.id, attributes: { title: nil }
        
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'DELETE :destroy' do
    it 'deletes the sandbox' do
      expect {
        delete :destroy, id: @sandbox.id
      }.to change { Sandbox.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
