require 'spec_helper'

describe V1::UrlsController do
  let(:editor) { create(:editor) }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    category = create(:parts)
    @document = create(:document, category_ids: [category.id])
    @url = create(:url, document_id: @document.id)
  end
  
  describe 'GET :index' do
    it 'renders all urls' do
      get :index, document_id: @document.id
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested url' do
      get :show, id: @url.id
      
      expect(response_body[:id]).to eq(@url.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:url_data) do
        {
          domain:    'google.com',
          rubric:    'politics',
          type:      'news',
          pseudo_id: '123',
          slug:      'putin'
        }
      end
      
      it 'creates and returns a new url' do
        post :create, document_id: @document.id, attributes: url_data
        
        expect(response).to be_success
        expect(response_body[:domain]).to eq(url_data[:domain])
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with url's errors" do
        post :create, document_id: @document.id, attributes: { domain: '' }
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'POST :raw_create' do
    it 'creates and returns a new url' do
      raw_url_data = { rendered: 'http://yandex.ru/' }
      post :raw_create, document_id: @document.id, attributes: raw_url_data
      
      expect(response).to be_success
      expect(response_body[:rendered]).to eq(raw_url_data[:rendered])
      expect(response_body[:fixed]).to be_truthy
    end
  end
  
  describe 'GET :find_by_url' do
    context 'with url of some existing url' do
      it 'renders the url' do
        get :find_by_url, url: 'http://vedomosti.ru/finance/news/visa'
        
        expect(response).to be_success
        expect(response_body[:rendered]).to eq(@url.rendered)
      end
    end
    
    context 'with unknown url' do
      it "complains that record can't be found" do
        expect {
          get :find_by_url, url: 'http://google.com/'
        }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    
    context 'without the url' do
      it 'complains on missing argument' do
        expect {
          get :find_by_url
        }.to raise_error(ActionController::ParameterMissing)
      end
    end
  end
  
  describe 'PATCH :update' do
    context 'with valid attributes' do
      it 'updates an existing url' do
        expect {
          patch :update, id: @url.id, attributes: { domain: 'yandex.com' }
        }.to change { Url.find(@url.id).porcupined.domain }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with url's errors" do
        patch :update, id: @url.id, attributes: { domain: '' }
        
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'DELETE :destroy' do
    it 'deletes the url' do
      expect {
        delete :destroy, id: @url.id
      }.to change { Url.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
