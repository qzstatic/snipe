require 'spec_helper'

describe V1::DocumentPartsController do
  let(:editor) { create(:editor) }
  let(:last_modified) { Time.now }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    @document = create(:document,
      cached_setting_keys: %w(source.common),
      sourcer_data: {
        source_title: 'meduza.io',
        updated_at:   last_modified
      }
    )
  end
  
  describe "GET 'show'" do
    it 'renders the requested document part' do
      get :show, document_id: @document.id, part: 'sourcer_data'
      
      expect(response).to be_success
      expect(response_body[:data]).to eq(source_title: 'meduza.io')
      expect(response_body[:meta]).to eq(last_modified: last_modified.to_s)
    end
  end
  
  describe "GET 'show_title'" do
    it 'renders the requested document title' do
      get :show_title, document_id: @document.id
      
      expect(response).to be_success
      expect(response_body[:data]).to eq(title: @document.title)
    end
  end
  
  describe "PATCH 'update'" do
    context 'with a fresh last_modified' do
      it 'updates the requested document part' do
        expect {
          patch :update,
            document_id: @document.id,
            part: 'sourcer_data',
            data: { source_title:  'tjournal.ru' },
            meta: { last_modified: last_modified }
        }.to change { @document.reload.sourcer_data }
        
        expect(response).to be_success
        expect(response_body).to eq(meta: { last_modified: Time.now.to_s })
      end
    end
    
    context 'with a stale last_modified' do
      it 'responds with 412 Precondition Failed' do
        patch :update,
          document_id: @document.id,
          part: 'sourcer_data',
          data: { source_title:  'tjournal.ru' },
          meta: { last_modified: last_modified - 5.minutes }
        
        expect(response.status).to eq(412)
      end
    end
  end
  
  describe "PATCH 'update_title'" do
    it "updates the document's title" do
      expect {
        patch :update_title, document_id: @document.id, data: { title: 'New title' }
      }.to change { @document.reload.title }.from('Hello world!').to('New title')
      
      expect(response).to be_success
    end
  end
end
