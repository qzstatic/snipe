require 'spec_helper'

describe V1::ApplicationController do
  controller(V1::ApplicationController) do
    def index
      render json: { success: true }
    end
  end
  
  describe '#authorize!' do
    context 'with an authorized request' do
      let(:editor) { create(:editor) }
      let(:token)  { Token.generate }
      
      before(:each) do
        sign_in(editor)
        set_access_token_header
      end
      
      it 'renders { success: true }' do
        get :index
        expect(response_body).to eq(success: true)
      end
      
      it 'authorizes the editor' do
        get :index
        expect(assigns(:sessions_manager).editor).to eq(editor)
      end
      
      context 'and a disabled editor' do
        before(:each) do
          editor.update(enabled: false)
        end
        
        it 'renders 401 Unauthorized' do
          get :index
          expect(response.status).to eq(401)
        end
      end
    end
    
    context 'with a non-authorized request' do
      it 'renders 401 Unauthorized' do
        get :index
        expect(response.status).to eq(401)
      end
    end
  end
end
