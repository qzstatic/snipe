require 'spec_helper'

describe V1::GroupsController do
  let(:admins) { create(:admins) }
  let(:admin) { create(:editor, group_ids: [admins.id]) }
  let(:editor) { create(:editor) }
  
  before(:each) do
    sign_in(admin)
    set_access_token_header
    set_content_type_header
    
    @group = create(:guests)
  end
  
  describe 'GET :index' do
    it 'renders all groups' do
      get :index
      expect(response_body.count).to eq(2)
    end
  end
  
  describe 'GET :by_editor' do
    context 'without editor in groups' do
      it 'renders no groups' do
        get :by_editor, editor_id: editor.id
        expect(response_body.count).to eq(0)
      end
    end
    
    context 'with editor in groups' do
      before(:each) do
        editor.update(group_ids: [@group.id])
      end
      
      it 'renders groups which contains specified editor' do
        get :by_editor, editor_id: editor.id
        expect(response_body.count).to eq(1)
      end
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested group' do
      get :show, id: @group.id
      expect(response_body[:id]).to eq(@group.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:group_attributes) { attributes_for(:web_editors) }
      
      it 'creates and returns a new group' do
        post :create, attributes: group_attributes
        
        expect(response).to be_success
        expect(response_body[:title]).to eq(group_attributes[:title])
      end
    end
    
    context 'with invalid attributes' do
      let(:group_attributes) { attributes_for(:admins, title: nil) }
      
      it "renders 422 with group's errors" do
        post :create, attributes: group_attributes
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'PATCH :update' do
    context 'with valid attributes' do
      it 'updates an existing group' do
        expect {
          patch :update, id: @group.id, attributes: { title: 'new title' }
        }.to change { Group.find(@group.id).title }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with group's errors" do
        patch :update, id: @group.id, attributes: { title: nil }
        
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'DELETE :destroy' do
    it 'deletes the group' do
      expect {
        delete :destroy, id: @group.id
      }.to change { Group.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
