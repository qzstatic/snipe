require 'spec_helper'

describe V1::Search::DocumentsController do
  let(:editor) { create(:editor) }
  let(:links)  { create(:links) }
  let(:now)    { Time.now.round(0) }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    %w(One Two Three).each { |title| create(:document, title: title) }
  end

  describe 'GET :search' do
    context 'when documents can be found' do
      let(:timestamp) { 1.hour.to_i }
      
      it 'returns the found documents' do
        get :fresh, timestamp: timestamp
        
        expect(response).to be_success
        expect(response_body.count).to eq(3)
      end
    end
    
    context "when documents can't be found" do
      let(:timestamp) { (Time.now + 1.hour).to_i }
      
      it 'returns nothing' do
        get :fresh, timestamp: timestamp
        
        expect(response).to be_success
        expect(response_body.count).to eq(0)
      end
    end
  end
  
  describe 'GET :index' do
    context 'when documents can be found at first page' do
      let(:page) { 1 }
      
      it 'returns the found documents' do
        get :index, page: page
        
        expect(response).to be_success
        expect(response_body.count).to eq(3)
      end
    end
    
    context "when documents number too small for second page" do
      let(:page) { 2 }
      
      it 'returns nothing' do
        get :index, page: page
        
        expect(response).to be_success
        expect(response_body.count).to eq(0)
      end
    end
  end
end
