require 'spec_helper'

describe V1::AttachmentVersionsController do
  let(:editor) { create(:editor) }
  
  def create_version(attachment, slug = 'preview')
    create(
      :attachment_version,
      slug: slug,
      attachment_id: attachment.id
    )
  end
  
  before(:each) do
    @attachment = create(:attachment)
    
    @preview = create_version(@attachment)
    @small = create_version(@attachment, 'small')
    
    sign_in(editor)
    set_access_token_header
    set_content_type_header
  end
  
  describe 'GET :index' do
    it "renders all attachment's versions" do
      get :index, attachment_id: @attachment.id
      
      expect(response).to be_success
      expect(response_body.count).to eq(2)
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested version' do
      get :show, id: @preview.id
      
      expect(response).to be_success
      expect(response_body[:id]).to eq(@preview.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:version_attributes) { attributes_for(:attachment_version) }
      
      it 'creates and returns a new version' do
        post :create, attachment_id: @attachment.id, attributes: version_attributes
        
        expect(response).to be_success
        expect(response_body[:slug]).to eq('preview')
        expect(response_body[:url]).to eq('some/path/preview-123.jpg')
      end
    end
    
    context 'with invalid attributes' do
      let(:version_attributes) { attributes_for(:attachment_version, slug: nil) }
      
      it "renders 422 with version's errors" do
        post :create, attachment_id: @attachment.id, attributes: version_attributes
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'PATCH :update' do
    before(:each) do
      @version = create_version(@attachment, 'big')
    end
    
    context 'with valid attributes' do
      it 'updates an existing version' do
        expect {
          patch :update, id: @version.id, attributes: { slug: 'medium' }
        }.to change { AttachmentVersion.find(@version.id).slug }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with version's errors" do
        patch :update, id: @version.id, attributes: { slug: nil }
        
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'DELETE :destroy' do
    before(:each) do
      @version = create_version(@attachment, 'big')
    end
    
    it 'deletes the version' do
      expect {
        delete :destroy, id: @version.id
      }.to change { AttachmentVersion.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
