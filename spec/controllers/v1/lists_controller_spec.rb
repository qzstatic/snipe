require 'spec_helper'

describe V1::ListsController do
  let(:editor) { create(:editor) }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    @list = create(:list)
    @document = create(:document, published: true, published_at: Time.now)
    Hooks::Documents::ListItems.new(@document).save
    @document.reload
  end
  
  describe 'GET :index' do
    it 'renders all lists' do
      get :index
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested list' do
      get :show, id: @list.id
      
      expect(response_body[:id]).to eq(@list.id)
    end
  end
  
  describe 'GET :documents' do
    it 'renders documents filtered by list' do
      get :documents, id: @list.id
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end

  describe 'GET :item' do
    it 'return ListItem' do
      get :item, id: @list.id, document_id: @document.id
      
      expect(response_body[:document_id]).to eq(@document.id)
    end

    it 'render 404 if list_item not found' do
      expect {
        get :item, id: @list.id, document_id: 123
      }.to raise_error ActiveRecord::RecordNotFound
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:list_attributes) { attributes_for(:list) }
      
      it 'creates and returns a new list' do
        post :create, attributes: list_attributes
        
        expect(response).to be_success
        expect(response_body[:slug]).to eq(list_attributes[:slug])
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with list's errors" do
        post :create, attributes: { slug: nil }
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'PATCH :update' do
    context 'with valid attributes' do
      it 'updates an existing list' do
        expect {
          patch :update, id: @list.id, attributes: { slug: 'top10' }
        }.to change { List.find(@list.id).slug }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with list's errors" do
        patch :update, id: @list.id, attributes: { slug: nil }
        
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'DELETE :destroy' do
    it "deletes the list" do
      expect {
        delete :destroy, id: @list.id
      }.to change { List.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
