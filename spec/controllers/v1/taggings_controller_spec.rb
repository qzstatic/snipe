require 'spec_helper'

describe V1::TaggingsController do
  let(:editor) { create(:editor) }
  let(:domain) { create(:domain) }
  
  let(:document) { create(:document, category_ids: [domain.id]) }
  let(:tag)      { create(:tag) }
  
  before(:each) do
    allow(document).to receive(:settings).and_return(
      taggings: {
        allowed_types: %w(people companies places events)
      }
    )
    
    allow(Document).to receive(:find).and_return(document)
  end
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    document.taggings.create(type: 'people')
  end
  
  let(:tagging) { document.taggings.first }
  
  describe 'GET :index' do
    it 'renders all taggings' do
      get :index, document_id: document.id
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested tagging' do
      get :show, id: tagging.id
      expect(response_body[:id]).to eq(tagging.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:tagging_attributes) { attributes_for(:tagging) }
      
      it 'creates and returns a new tagging' do
        post :create, document_id: document.id, attributes: tagging_attributes
        
        expect(response).to be_success
        expect(response_body[:document_id]).to eq(document.id)
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with tagging's errors" do
        post :create, document_id: document.id, attributes: { type: nil }
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'PATCH :update' do
    context 'with valid attributes' do
      it 'updates an existing tagging' do
        expect {
          patch :update, id: tagging.id, attributes: { type: 'new type' }
        }.to change { Tagging.find(tagging.id).type }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with tagging's errors" do
        patch :update, id: tagging.id, attributes: { type: nil }
        
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'DELETE :destroy' do
    it 'deletes the tagging' do
      expect {
        delete :destroy, id: tagging.id
      }.to change { Tagging.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
