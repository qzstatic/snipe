require 'spec_helper'

describe V1::ListItemsController do
  let(:editor) { create(:editor) }
  let(:list)   { create(:list) }
  
  let(:first_document)  { create(:document, published: true, published_at: Time.now) }
  let(:second_document) { create(:document, published: true, published_at: Time.now) }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    @list_item = ListItem.create!(
      list:     list,
      document: first_document,
      top:      2
    )
  end
  
  describe 'GET :index' do
    it 'renders all items of the requested list' do
      get :index, list_id: list.id
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested list item' do
      get :show, id: @list_item.id
      
      expect(response_body[:id]).to eq(@list_item.id)
    end
  end
  
  describe 'POST :create' do
    let(:item_attributes) do
      @list_item.attributes.with_indifferent_access.merge(
        document_id: second_document.id
      )
    end
    
    it 'creates and returns a new item' do
      post :create, list_id: list.id, attributes: item_attributes
      
      expect(response).to be_success
      expect(response_body[:top]).to eq(item_attributes[:top])
    end
  end
  
  describe 'PATCH :update' do
    it 'updates an existing item' do
      expect {
        patch :update, id: @list_item.id, attributes: { top: 3 }
      }.to change { ListItem.find(@list_item.id).top }
      
      expect(response).to be_success
    end
  end
  
  describe 'DELETE :destroy' do
    it "deletes the item" do
      expect {
        delete :destroy, id: @list_item.id
      }.to change { ListItem.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
