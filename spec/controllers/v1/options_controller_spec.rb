require 'spec_helper'

describe V1::OptionsController do
  let(:editor) { create(:editor) }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    params = ActionController::Parameters.new(
      type:  'source',
      title: 'Yandex',
      url:   'ya.ru'
    )
    @option = Savers::OptionsCreator.new(params, editor).saved_object
  end
  
  describe 'GET :index' do
    it 'renders all options' do
      get :index
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end
  
  describe 'GET :types' do
    it 'renders attributes for different types of options' do
      get :types
      
      expect(response).to be_success
      expect(response_body).to include(
        name:       'Источники',
        type:       'source',
        attributes: %w(title url)
      )
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested option' do
      get :show, id: @option.id
      
      expect(response_body[:id]).to eq(@option.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:option_attributes) { { type: 'source', title: 'Google', url: 'google.com' } }
      
      it 'creates and returns a new option' do
        post :create, attributes: option_attributes
        
        expect(response).to be_success
        expect(response_body[:title]).to eq('Google')
      end
    end
    
    context 'with invalid attributes' do
      context 'with wrong type' do
        it "renders 422 with option's errors" do
          post :create, attributes: { type: 'source1' }
          
          expect(response).to be_unprocessable
          expect(response_body).not_to be_empty
        end
      end
      
      context 'without value' do
        it "renders 422 with option's errors" do
          post :create, attributes: { type: 'source' }
          
          expect(response).to be_unprocessable
          expect(response_body).not_to be_empty
        end
      end
    end
  end
  
  describe 'PATCH :update' do
    context 'with valid attributes' do
      it 'updates an existing option' do
        expect {
          patch :update, id: @option.id, attributes: { title: 'Google' }
        }.to change { Option.find(@option.id).porcupined.title }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with option's errors" do
        patch :update, id: @option.id, attributes: { type: 'lol' }
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'DELETE :destroy' do
    it "deletes the option" do
      expect {
        delete :destroy, id: @option.id
      }.to change { Option.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
