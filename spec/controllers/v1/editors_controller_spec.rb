require 'spec_helper'

describe V1::EditorsController do
  # let(:editor) { create(:editor) }
  let(:admins) { create(:admins) }
  let(:editor) { create(:editor, group_ids: [admins.id]) }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
  end
  
  describe 'GET :index' do
    it 'renders all editors' do
      get :index
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested editor' do
      get :show, id: editor.id
      
      expect(response_body[:id]).to eq(editor.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:editor_attributes) { attributes_for(:editor) }
      
      it 'creates and returns a new editor' do
        post :create, attributes: editor_attributes
        
        expect(response).to be_success
        expect(response_body[:login]).to eq(editor_attributes[:login])
      end
    end
    
    context 'with invalid attributes' do
      let(:editor_attributes) { attributes_for(:editor, login: nil) }
      
      it "renders 422 with editor's errors" do
        post :create, attributes: editor_attributes
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'PATCH :update' do
    before(:each) do
      @editor = create(:editor)
    end
    
    context 'with valid attributes' do
      it 'updates an existing editor' do
        expect {
          patch :update, id: @editor.id, attributes: { login: 'putin' }
        }.to change { Editor.find(@editor.id).login }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with editor's errors" do
        patch :update, id: @editor.id, attributes: { login: nil }
        
        expect(response).to be_unprocessable
      end
    end
    
    context 'with adding to group' do
      it 'updates an existing editor' do
        expect {
          patch :update, id: @editor.id, attributes: { group_ids: [admins.id] }
        }.to change { Editor.find(@editor.id).group_ids }
        
        expect(response).to be_success
      end
    end
  end
  
  describe 'DELETE :destroy' do
    before(:each) do
      @editor = create(:editor)
    end
    
    it 'deletes the editor' do
      expect {
        delete :destroy, id: @editor.id
      }.to change { Editor.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
