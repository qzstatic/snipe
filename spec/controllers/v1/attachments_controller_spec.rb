require 'spec_helper'

describe V1::AttachmentsController do
  def create_version(attachment, slug)
    create(
      :attachment_version,
      slug: slug,
      attachment_id: attachment.id
    )
  end
  
  let(:editor) { create(:editor) }

  let(:attachment) { @at ||= create(:attachment) }
  let(:small) { create_version(attachment, 'medium') }
  let(:small_updated_attributes) {
    {
      id: small.id,
      filename: 'medium-12ga.png'
    }
  }
 
  let(:attachment_params) {
    ActionController::Parameters.new(
      type:      'image',
      directory: 'some/dir',
      versions: {
        original: {
          height:   60,
          mime:     'image',
          url:      '/image/2014/6f/w015i/original-15h1.png',
          filename: 'original-15h1.png',
          size:     3464,
          width:    53
        },
        pic: {
          height:   90,
          mime:     'image',
          url:      '/image/2014/6f/w015i/pic-15h1.png',
          filename: 'pic-15h1.png',
          size:     7648,
          width:    120
        }
      }
    )
  }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    params = ActionController::Parameters.new(
      type:      'image',
      directory: 'some/dir'
    )
    creator = Savers::AttachmentsCreator.new(params, editor)
    creator.save
    @attachment = creator.object
  end
  
  describe 'GET :show' do
    it 'renders the requested attachment' do
      get :show, id: @attachment.id
      
      expect(response).to be_success
      expect(response_body[:id]).to eq(@attachment.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:attachment_attributes) { attachment_params }
      
      it 'creates and returns a new attachment' do
        post :create, attributes: attachment_attributes
        
        expect(response).to be_success
        expect(response_body[:title]).to eq(attachment_attributes[:title]) # ???
      end
    end
    
    context 'with invalid attributes' do
      let(:attachment_attributes) { attributes_for(:attachment, type: nil) }
      
      it "renders 422 with attachment's errors" do
        post :create, attributes: attachment_attributes
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'PATCH :update' do
    context 'with valid attributes' do
      it 'updates an existing attachment' do
        params = attachment_params
        params[:versions].delete(:original)
        params[:versions][:medium] = small_updated_attributes
        
        patch :update, id: attachment.id, attributes: params
        
        expect(attachment.versions.count).to eq(2)
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with attachment's errors" do
        patch :update, id: @attachment.id, attributes: { directory: nil }
        
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'DELETE :destroy' do
    it 'deletes the version' do
      expect {
        delete :destroy, id: @attachment.id
      }.to change { Attachment.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
