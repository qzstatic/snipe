require 'spec_helper'

describe V1::TagsController do
  let(:editor) { create(:editor) }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    @document   = create(:document)
    @first_tag  = create(:tag, title: 'СССР')
    @second_tag = create(:tag, title: 'Китай')
    @tagging    = create(:tagging, tag_ids: [@first_tag.id], document: @document)
    
    [
      'Путин',
      'Владимир Путин',
      'Путин Владимир',
      'Владимир Владимирович'
    ].each { |title| create(:tag, title: title) }
  end
  
  describe 'GET :index' do
    it "renders all tagging's tags" do
      get :index, tagging_id: @tagging.id
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested tag' do
      get :show, id: @first_tag.id
      
      expect(response_body[:id]).to eq(@first_tag.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:tag_attributes) { attributes_for(:tag, title: 'New tag') }
      
      it 'creates and returns a new tag' do
        post :create, attributes: tag_attributes
        
        expect(response).to be_success
        expect(response_body[:title]).to eq(tag_attributes[:title])
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with tag's errors" do
        post :create, attributes: { title: nil }
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'GET :search' do
    context 'when tags can be found' do
      it 'returns the found tags' do
        get :search, search_string: 'путин'
        
        expect(response).to be_success
        expect(response_body.count).to eq(3)
      end
    end
      
    context "when tags can't be found" do
      it 'returns nothing' do
        get :search, search_string: 'медведев'
        
        expect(response).to be_success
        expect(response_body.count).to eq(0)
      end
    end
    
    context 'without the search_string' do
      it 'complains on missing argument' do
        expect {
          get :search
        }.to raise_error(ActionController::ParameterMissing)
      end
    end
  end
  
  describe 'GET :find_by_title' do
    context 'with title of some existing tag' do
      it 'renders the tag' do
        get :find_by_title, title: 'Путин'
        
        expect(response).to be_success
        expect(response_body[:title]).to eq('Путин')
      end
    end
    
    context 'with unknown title' do
      it "complains that record can't be found" do
        expect {
          get :find_by_title, title: 'Навальный'
        }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    
    context 'without the title' do
      it 'complains on missing argument' do
        expect {
          get :find_by_title
        }.to raise_error(ActionController::ParameterMissing)
      end
    end
  end
  
  describe 'PATCH :update' do
    context 'with valid attributes' do
      it 'updates an existing tag' do
        expect {
          patch :update, id: @first_tag.id, attributes: { title: 'new title' }
        }.to change { Tag.find(@first_tag.id).title }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with tag's errors" do
        patch :update, id: @first_tag.id, attributes: { title: nil }
        
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'DELETE :destroy' do
    context 'without taggings' do
      it 'deletes the tag' do
        expect {
          delete :destroy, id: @second_tag.id
        }.to change { Tag.count }.by(-1)
        
        expect(response).to be_success
      end
    end
    
    context 'with taggings' do
      it "doesn't delete the tag" do
        expect {
          delete :destroy, id: @first_tag.id
        }.not_to change { Tag.count }
        
        expect(response).to be_success
      end
    end
  end
end
