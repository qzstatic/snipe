require 'spec_helper'

describe V1::SnapshotsController do
  let(:editor) { create(:editor) }
  
  let(:document) do
    Savers::DocumentsCreator.new(
      ActionController::Parameters.new(
        title: 'New document'
      ),
      editor
    ).saved_object
  end
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
  end
  
  describe "GET 'show'" do
    let(:snapshot) { document.snapshots.first }
    
    it 'returns http success' do
      get :show, id: snapshot.id
      
      expect(response).to be_success
      expect(response_body[:id]).to eq(document.id)
      expect(response_body[:title]).to eq(document.title)
    end
  end
  
  describe "GET 'for_document'" do
    it 'returns http success' do
      get :for_document, document_id: document.id
      
      expect(response).to be_success
      expect(response_body.first[:id]).to eq(document.snapshots.first.id)
    end
  end
  
  describe "GET 'for_sandbox'" do
    let(:sandbox) do
      Savers::SandboxesCreator.new(
        ActionController::Parameters.new(
          title: 'Последние документы за 24 часа'
        ),
        editor
      ).saved_object
    end
    
    it 'returns http success' do
      get :for_sandbox, sandbox_id: sandbox.id
      
      expect(response).to be_success
      expect(response_body.first[:id]).to eq(sandbox.snapshots.first.id)
    end
  end
  
  describe "GET 'for_list'" do
    let(:list) do
      Savers::ListsCreator.new(
        ActionController::Parameters.new(
          slug:  'some_list',
          title: 'List',
          limit: 7
        ),
        editor
      ).saved_object
    end
    
    it 'returns http success' do
      get :for_list, list_id: list.id
      
      expect(response).to be_success
      expect(response_body.first[:id]).to eq(list.snapshots.first.id)
    end
  end
  
  describe "GET 'for_page'" do
    let(:page) do
      Savers::PagesCreator.new(
        ActionController::Parameters.new(
          title: 'Page',
          slug:  'some_page'
        ),
        editor
      ).saved_object
    end
    
    it 'returns http success' do
      get :for_page, page_id: page.id
      
      expect(response).to be_success
      expect(response_body.first[:id]).to eq(page.snapshots.first.id)
    end
  end
end
