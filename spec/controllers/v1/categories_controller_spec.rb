require 'spec_helper'

describe V1::CategoriesController do
  let(:editor) { create(:editor) }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    @domain = create(:domain)
    @rubric = create(:rubric)
  end
  
  describe 'GET :index' do
    it 'renders all categories' do
      get :index
      
      expect(response_body.count).to eq(4)
    end
  end
  
  describe 'GET :by_parent' do
    it 'renders all categories with a given type' do
      get :by_parent, slug: 'domains'
      
      expect(response_body.count).to eq(1)
      expect(response_body.first[:name]).to eq(@domain.title)
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested category' do
      get :show, id: @domain.id
      expect(response_body[:id]).to eq(@domain.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:category_attributes) { attributes_for(:domain) }
      
      it 'creates and returns a new category' do
        post :create, attributes: category_attributes
        
        expect(response).to be_success
        expect(response_body[:title]).to eq(category_attributes[:title])
      end
    end
    
    context 'with invalid attributes' do
      let(:category_attributes) { attributes_for(:domain, title: nil) }
      
      it "renders 422 with category's errors" do
        post :create, attributes: category_attributes
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'PATCH :update' do
    context 'with valid attributes' do
      it 'updates an existing category' do
        expect {
          patch :update, id: @domain.id, attributes: { title: 'new title' }
        }.to change { Category.find(@domain.id).title }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with category's errors" do
        patch :update, id: @domain.id, attributes: { title: nil }
        
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'DELETE :destroy' do
    it 'deletes the category' do
      expect {
        delete :destroy, id: @domain.parent_id
      }.to change { Category.count }.by(-2)
      
      expect(response).to be_success
    end
  end
end
