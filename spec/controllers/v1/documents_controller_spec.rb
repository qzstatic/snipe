require 'spec_helper'

describe V1::DocumentsController do
  let(:editor) { create(:editor) }
  let(:links) { create(:links) }
  let(:now) { Time.now.round(0) }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    @document = create(:document, published: true, published_at: Time.now)
    @sandbox = create(:published_sandbox)
  end

  describe 'GET :index' do
    it 'renders all documents' do
      get :index
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end
  
  describe 'GET :by_sandbox' do
    it 'renders documents filtered by sandbox' do
      get :by_sandbox, sandbox_id: @sandbox.id
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested document' do
      get :show, id: @document.id
      
      expect(response_body[:id]).to eq(@document.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:document_attributes) { attributes_for(:document) }
      
      it 'creates and returns a new document' do
        post :create, attributes: document_attributes
        
        expect(response).to be_success
        expect(response_body[:title]).to eq(document_attributes[:title])
      end
    end
    
    context 'with invalid attributes' do
      let(:document_attributes) { attributes_for(:document, title: nil) }
      
      it "renders 422 with document's errors" do
        post :create, attributes: document_attributes
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'POST :fast' do
    context 'with valid attributes' do
      let(:document_attributes) {
        ActionController::Parameters.new(
          title: 'Fast create document',
          category_ids: [links.id],
          published_at: now,
          url: 'http://opa.opa'
        )
      }
      
      it 'creates and returns a new document' do
        post :fast, attributes: document_attributes
        
        expect(response).to be_success
        expect(response_body[:title]).to eq(document_attributes[:title])
        expect(response_body[:published]).to eq(true)
        expect(response_body[:published_at]).to eq(document_attributes[:published_at].iso8601(3))
      end
    end
    
    context 'with invalid attributes' do
      let(:document_attributes) {
        ActionController::Parameters.new(
          title: nil,
          category_ids: [links.id]
        )
      }
      
      it "renders 422 with document's errors" do
        post :fast, attributes: document_attributes
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'GET :search' do
    let(:domain) { create(:domain, slug: 'google.com') }
    let(:rubric) { create(:rubric, slug: 'europe') }
    
    before(:each) do
      [
        'Владимир Путин',
        'Путин Владимир',
        'Владимир Владимирович'
      ].each { |title| create(:document, title: title) }
      
      create(:document, title: 'Путин', category_ids: [domain.id])
    end
    
    context 'when documents can be found' do
      let(:query) { 'путин' }
      let(:category_slugs) { %w(google.com usa).join(',') }
      
      it 'returns the found documents' do
        get :search, query: query, category_slugs: category_slugs
        
        expect(response).to be_success
        expect(response_body.count).to eq(1)
      end
    end
    
    context "when documents can't be found" do
      let(:query) { 'медведев' }
      
      it 'returns nothing' do
        get :search, query: query
        
        expect(response).to be_success
        expect(response_body.count).to eq(0)
      end
    end
    
    context 'without the search_string' do
      it 'complains on missing argument' do
        expect {
          get :search
        }.to raise_error(ActionController::ParameterMissing)
      end
    end
  end
  
  describe 'PATCH :update' do
    context 'with valid attributes' do
      it 'updates an existing document' do
        expect {
          patch :update, id: @document.id, attributes: { title: 'new title' }
        }.to change { Document.find(@document.id).title }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with document's errors" do
        patch :update, id: @document.id, attributes: { title: nil }
        
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'PATCH :update_preview' do
    let(:pipeliner) { double('Pipeliner') }
    
    it 'processes the document with DocumentsPipeliner' do
      expect(Savers::DocumentsPipeliner).to receive(:new).with(document: @document).and_return(pipeliner)
      expect(pipeliner).to receive(:process).with(preview: true)
      
      patch :update_preview, id: @document.id
    end
  end
  
  describe 'GET :root_box' do
    context 'with a document with root box' do
      before(:each) do
        @document.create_root_box
      end
      
      it 'renders the root box' do
        get :root_box, id: @document.id
        
        expect(response_body[:id]).to eq(@document.root_box.id)
      end
    end
    
    context 'with an empty document' do
      it 'creates a root box and renders it' do
        get :root_box, id: @document.id
        
        expect(@document.reload.root_box_id).not_to be_nil
        expect(response_body[:id]).to eq(@document.root_box.id)
      end
    end
  end
  
  describe 'GET :matching_lists' do
    let(:domains) { create(:domains) }

    before(:each) do
      @document.category_ids = [domains.id]
      @document.save
      @list = create(:list, category_ids: [domains.id])
    end

    it 'return matching lists' do
      get :matching_lists, id: @document.id
      
      expect(response_body[0][:id]).to eq(@list.id)
    end
  end

  describe 'DELETE :destroy' do
    before(:each) do
      pending 'Disabled'
    end
    
    it 'deletes the document' do
      expect {
        delete :destroy, id: @document.id
      }.to change { Document.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
