require 'spec_helper'

describe V1::LinksController do
  let(:editor) { create(:editor) }
  let(:domain) { create(:domain) }
  let(:bound_document) { create(:document, category_ids: [domain.id]) }
    
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header
    
    @document = create(:document)
    @link = create(:link, document_id: @document.id, bound_document_id: bound_document.id)
  end
  
  describe 'GET :index' do
    it "renders all document's links" do
      get :index, document_id: @document.id
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end
  
  describe 'GET :show' do
    it 'renders the requested link' do
      get :show, id: @link.id
      
      expect(response).to be_success
      expect(response_body[:id]).to eq(@link.id)
    end
  end
  
  describe 'POST :create' do
    context 'with valid attributes' do
      let(:link_attributes) { attributes_for(:link) }
      
      it 'creates and returns a new link' do
        # create another document
        bound_document1 = create(:document, category_ids: [domain.id])
        link_attributes[:bound_document_id] = bound_document1.id

        post :create, document_id: @document.id, attributes: link_attributes
        
        expect(response).to be_success
        expect(response_body[:section]).to eq(link_attributes[:section].to_s)
      end
    end
    
    context 'with invalid attributes' do
      let(:link_attributes) { attributes_for(:link, section: nil) }
      
      it "renders 422 with link's errors" do
        post :create, document_id: @document.id, attributes: link_attributes
        
        expect(response).to be_unprocessable
        expect(response_body).not_to be_empty
      end
    end
  end
  
  describe 'PATCH :update' do
    context 'with valid attributes' do
      it 'updates an existing link' do
        expect {
          patch :update, id: @link.id, attributes: { position: 23 }
        }.to change { Link.find(@link.id).position }
        
        expect(response).to be_success
      end
    end
    
    context 'with invalid attributes' do
      it "renders 422 with link's errors" do
        patch :update, id: @link.id, attributes: { section: nil }
        
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'DELETE :destroy' do
    it 'deletes the link' do
      expect {
        delete :destroy, id: @link.id
      }.to change { Link.count }.by(-1)
      
      expect(response).to be_success
    end
  end
end
