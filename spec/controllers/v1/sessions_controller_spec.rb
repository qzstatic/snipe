require 'spec_helper'

describe V1::SessionsController do
  let(:editor) { create(:editor) }
  
  describe 'POST :create' do
    context 'with valid credentials' do
      it 'returns authentication info' do
        get :create, editor: {
          login:    editor.login,
          password: editor.password
        }
        
        expect(response_body[:editor_id]).to eq(editor.id)
      end
    end
    
    context 'with invalid credentials' do
      it 'returns 422 Unprocessable Entity' do
        get :create, editor: { login: 'wrong' }
        expect(response).to be_unprocessable
      end
    end
  end
  
  describe 'PATCH :refresh' do
    before(:each) do
      sign_in(editor)
    end
    
    context 'with valid refresh_token' do
      it 'returns a new token' do
        patch :refresh, refresh_token: @refresh_token
        
        expect(response_body[:access_token]).to be_a(String)
        expect(response_body[:access_token]).not_to eq(@access_token)
        expect(response_body[:refresh_token]).to be_a(String)
        expect(response_body[:refresh_token]).not_to eq(@refresh_token)
      end
    end
    
    context 'with invalid refresh_token' do
      it 'renders 404 Not Found' do
        patch :refresh, refresh_token: 'wrong'
        expect(response.status).to eq(404)
      end
    end
  end
  
  describe 'DELETE :destroy' do
    before(:each) do
      sign_in(editor)
      set_access_token_header
    end
    
    it 'deletes the tokens' do
      expect(Token.redis).to receive(:del).twice
      delete :destroy
      expect(response).to be_success
    end
  end
end
