require 'spec_helper_v2'

describe V2::DocumentsController do
  let(:editor) { current_editor }
  
  before(:each) do
    sign_in(editor)
    set_access_token_header
    set_content_type_header

    @document = create_document
  end
  
  describe 'GET :index' do
    it 'renders all documents' do
      get :index
      
      expect(response).to be_success
      expect(response_body.count).to eq(1)
    end
  end

  describe 'POST :fast' do
    let(:domain) { create_category }
    let(:now) { Time.now.round(0) }
    let(:params) do
      ActionController::Parameters.new(
        document: {
          title: 'Halo!', 
          rightcol: 'sub title', 
          category_ids: [domain.id]
        },
        url: {
          rendered: 'https://www.google.ru/'
        },
        publisher: {
          published_at: now
        }
      )
    end
    let(:editor) { current_editor }

    it 'creates and returns a new document' do
      post :fast, attributes: params
      
      expect(response).to be_success
      expect(response_body[:title]).to eq(params[:document][:title])
      expect(response_body[:title]).to eq(params[:document][:title])
      expect(response_body[:published]).to eq(true)
      expect(response_body[:published_at]).to eq(params[:publisher][:published_at].iso8601(3))
    end

    it 'generate error with invalid params' do
      params[:document].delete(:title)
      post :fast, attributes: params

      expect(response).to be_unprocessable
      expect(response_body).not_to be_empty
    end
  end
end
