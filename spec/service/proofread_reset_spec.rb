require 'spec_helper'

describe ProofreadReset do
  let(:editor)       { create(:editor) }
  let(:proofreaders) { create(:proofreaders) }
  let(:category)     { create(:news, setting_keys: %w(proofreading.common)) }
  
  let(:document) do
    create(
      :document,
      category_ids: [category.id],
      proofreader_data: {
        proofread: true
      },
      milestones: {
        proofread: {
          at: Time.now.to_i,
          by: editor.id
        }
      }
    ).porcupined
  end
  
  subject { described_class.new(document, editor) }
  
  describe '#reset' do
    it 'sets proofread to false' do
      subject.reset
      
      expect(document.proofread).to be_falsy
    end
    
    it 'removes the `proofread` milestone' do
      subject.reset
      
      expect(document.milestones).not_to have_key(:proofread)
    end
    
    context 'by a proofreader' do
      before(:each) do
        editor.update(group_ids: [proofreaders.id])
      end
      
      it 'leaves the document as proofread' do
        subject.reset
        
        expect(document.proofread).to be_truthy
      end
      
      it 'leaves the milestones intact' do
        expect {
          subject.reset
        }.not_to change { document.milestones }
      end
    end
  end
end
