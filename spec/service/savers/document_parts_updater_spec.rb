require 'spec_helper'

describe Savers::DocumentPartsUpdater do
  let(:editor)        { create(:editor) }
  let(:last_modified) { 5.minutes.ago }
  
  let(:document) do
    create(:document,
      cached_setting_keys: %w(source.common),
      sourcer_data: { updated_at: last_modified }
    )
  end
  
  let(:params) do
    ActionController::Parameters.new(
      source_title: 'Meduza',
      source_url:   'meduza.io'
    )
  end
  
  subject do
    described_class.new(document, params, editor, part: 'sourcer_data', meta: meta)
  end
  
  describe '#save' do
    context 'with a fresh request' do
      let(:meta) do
        { last_modified: last_modified.to_s }
      end
      
      it 'updates the document part' do
        expect {
          subject.save
        }.to change { document.reload.sourcer_data }
      end
    end
    
    context 'with an object without updated_at' do
      let(:document) do
        create(:document, cached_setting_keys: %w(source.common))
      end
      
      let(:meta) { Hash.new }
      
      it 'updates the document part' do
        expect {
          subject.save
        }.to change { document.reload.sourcer_data }
      end
    end
    
    context 'with a stale request' do
      let(:meta) do
        { last_modified: 3.hours.ago.to_s }
      end
      
      it 'does not update the document part' do
        expect {
          subject.save
        }.not_to change { document.reload.sourcer_data }
      end
      
      let(:meta) do
        { last_modified: Time.now.to_s }
      end
      
      it 'does not update the document part' do
        expect {
          subject.save
        }.not_to change { document.reload.sourcer_data }
      end
    end
  end
end
