require 'spec_helper'

describe Savers::EditorsUpdater do
  let(:params) do
    ActionController::Parameters.new(attributes_for(:editor, login: 'azazaz'))
  end
  
  let(:editor) { create(:editor) }
  let(:object) { create(:editor) }
  subject { Savers::EditorsUpdater.new(object, params, editor) }
  
  describe '#save' do
    it 'updates the editor with the given params' do
      subject.save
      expect(subject.object.login).to eq('azazaz')
    end
  end
end
