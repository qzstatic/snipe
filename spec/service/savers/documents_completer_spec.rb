require 'spec_helper'

describe Savers::DocumentsCompleter do
  let(:editor)  { create(:editor) }
  let(:domain)  { create(:domain) }
  let(:domains) { domain.parent }
  let(:article) { create(:article) }
  let(:parts)   { article.parent }
  let(:now)     { Time.now }
  
  let(:document) do
    create(:document, category_ids: [domains, domain, parts, article].map(&:id))
  end
  
  before(:each) do
    allow(Time).to receive(:now).and_return(now)
  end
  
  describe '#save' do
    subject { Savers::DocumentsCompleter.new(document, editor) }
    
    context 'with a valid document' do
      before(:each) do
        params = ActionController::Parameters.new(editor_id: editor.id)
        Savers::DocumentsAssigner.new(document, params, editor).save
        
        subject.save
      end
      
      it 'creates a snapshot' do
        expect(subject.object.snapshots.count).to eq(2)
        
        snapshot = subject.object.snapshots.last
        expect(snapshot.action).to eq('document.completed')
        expect(snapshot.data[:id]).to eq(subject.object.id)
      end
      
      context 'with an editor assigned to this document' do
        it "sets document's assignee_status to finished" do
          expect(subject.object.assignee_status).to eq('finished')
        end
        
        it 'adds the respective record to document.milestones' do
          expect(subject.object.milestones[:completed]).to eq(
            at: now.to_i,
            by: editor.id
          )
        end
        
        it 'adds the editor to participants' do
          expect(subject.object.participant_ids).to include(editor.id)
        end
      end
      
      context 'with another editor' do
        let(:other_editor) { create(:editor) }
        
        subject { Savers::DocumentsCompleter.new(document, other_editor) }
        
        before(:each) do
          subject.save
        end
        
        it "doesn't touch the document" do
          expect(subject.object.assignee_status).to eq('enabled')
          expect(subject.object.milestones).not_to have_key('completed')
        end
      end
    end
    
    context 'with an inapplicable document' do
      let(:document) { create(:document) }
      
      it "doesn't touch the document" do
        expect(subject.object.assigner_data).to be_empty
      end
    end
  end
end
