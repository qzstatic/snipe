require 'spec_helper'

describe Savers::EditorsCreator do
  let(:params) do
    ActionController::Parameters.new(attributes_for(:editor))
  end
  
  let(:editor) { create(:editor) }
  
  describe '#save' do
    subject { Savers::EditorsCreator.new(params, editor) }
    
    it 'creates a new editor with passed in params' do
      subject.save
      expect(subject.object).to be_persisted
      expect(subject.object.login).to eq(params[:login])
    end
  end
end
