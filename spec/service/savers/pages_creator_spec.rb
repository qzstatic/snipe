require 'spec_helper'

describe Savers::PagesCreator do
  let(:editor) { create(:editor) }
  let(:list)   { create(:list) }
  
  let(:params) do
    ActionController::Parameters.new(
      title: 'A new page',
      slug: 'page',
      position: 100,
      list_ids: [list.id]
    )
  end
  
  subject { Savers::PagesCreator.new(params, editor) }
  
  describe '#save' do
    before(:each) do
      subject.save
    end
    
    it 'creates a new page with the given parameters' do
      expect(subject.object).to be_persisted
      expect(subject.object.title).to eq(params[:title])
      expect(subject.object.position).to eq(params[:position])
      expect(subject.object.lists).to include(list)
    end
    
    it 'creates a snapshot' do
      expect(subject.object.snapshots.count).to eq(1)
      expect(subject.object.snapshots.first.action).to eq('page.created')
    end
  end
end
