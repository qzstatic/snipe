require 'spec_helper'

describe Savers::DocumentsUpdater do
  let(:editor)   { create(:editor) }
  let(:domain)   { create(:domain) }
  let(:domains)  { domain.parent }
  let(:article)  { create(:article) }
  let(:parts)    { article.parent }
  let(:document) { create(:document, category_ids: [domain.id]) }
  let(:list)     { create(:list) }
  
  let(:params) do
    ActionController::Parameters.new(
      title:        'New title',
      subtitle:     'Subtitle',
      category_ids: [domains, domain, parts, article].map(&:id),
      exclude_list_ids: [list.id]
    )
  end
  
  subject { Savers::DocumentsUpdater.new(document, params, editor) }
  
  describe '#save' do
    before(:each) do
      subject.save
    end
    
    it 'updates the document with both fixed and dynamic attributes' do
      expect(subject.object.title).to eq('New title')
      expect(subject.object.subtitle).to eq('Subtitle')
    end
        
    it 'adds the editor to participants' do
      expect(subject.object.participant_ids).to include(editor.id)
    end
    
    it 'saves exclude_list_ids' do
      expect(subject.object.exclude_list_ids).to include(list.id)
    end
    
    it 'creates a snapshot' do
      expect(subject.object.snapshots.count).to eq(1)
      
      snapshot = subject.object.snapshots.first
      expect(snapshot.action).to eq('document.updated')
      expect(snapshot.data[:id]).to eq(subject.object.id)
    end

    it 'update doc.view with part' do
      expect(subject.object.view['part']).to eq(article.slug.to_s.singularize)
    end
    
    context 'on a published document' do
      before(:each) do
        document.update(published: true, published_at: Time.now)
        @list = create(:list)
      end
      
      it 'update list_items' do
        expect(document.list_items).to eq([])
        subject.save
        
        expect(document.reload.list_items.first).to eq(@list.items.first)
      end
    end
  end
end
