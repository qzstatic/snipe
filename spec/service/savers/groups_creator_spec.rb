require 'spec_helper'

describe Savers::GroupsCreator do
  let(:editor) { create(:editor) }
  
  let(:params) do
    ActionController::Parameters.new(
      slug:      'editors',
      title:     'Редакторы',
      position:  10,
    )
  end
  
  subject { Savers::GroupsCreator.new(params, editor) }
  
  describe '#save' do
    before(:each) do
      subject.save
    end
    
    it 'creates a new group with passed in params' do
      expect(subject.object).to be_persisted
      expect(subject.object.slug).to eq(params[:slug])
      expect(subject.object.title).to eq(params[:title])
      expect(subject.object.position).to eq(params[:position])
    end
  end
end
