require 'spec_helper'

describe Savers::AttachmentsMultiCreator do
  let(:editor) { create(:editor) }
  
  let(:params) do
    ActionController::Parameters.new(
      type:      'image',
      directory: 'some/dir',
      versions: {
        original: {
          height:   60,
          mime:     'image',
          url:      '/image/2014/6f/w015i/original-15h1.png',
          filename: 'original-15h1.png',
          size:     3464,
          width:    53
        },
        pic: {
          height:   90,
          mime:     'image',
          url:      '/image/2014/6f/w015i/pic-15h1.png',
          filename: 'pic-15h1.png',
          size:     7648,
          width:    120
        }
      }
    )
  end
  
  subject { Savers::AttachmentsMultiCreator.new(params, editor) }
  
  describe '#save' do
    it 'creates a new attachment with versions' do
      expect {
        subject.save
      }.to change { Attachment.count }.by(1)
      expect(Attachment.last.versions.count).to eq(2)
    end
  end
end
