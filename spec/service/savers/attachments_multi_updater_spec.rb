require 'spec_helper'

describe Savers::AttachmentsMultiUpdater do
  def create_version(attachment, slug)
    create(
      :attachment_version,
      slug: slug,
      attachment_id: attachment.id
    )
  end

  let(:editor)     { create(:editor) }
  let(:attachment) { create(:attachment) }
  let(:small) { create_version(attachment, 'medium') }
  let(:small_updated_attributes) {
    {
      filename: 'medium-12ga.png'
    }
  }
  
  let(:params) do
    ActionController::Parameters.new(
      type: 'image',
      directory: '/path/to/directory'
    )
  end

  let(:params) do
    ActionController::Parameters.new(
      type:      'image',
      directory: 'some/dir2',
      versions: {
        pic: {
          height:   90,
          mime:     'image',
          url:      '/image/2014/6f/w015i/pic-15h1.png',
          filename: 'pic-15h1.png',
          size:     7648,
          width:    120
        },
        medium: small_updated_attributes
      }
    )
  end
  
  subject { Savers::AttachmentsMultiUpdater.new(attachment, params, editor) }
  
  describe '#save' do
    it 'updates the attachment with parameters' do
      subject.save
      
      expect(subject.object.type).to eq(params[:type])
      expect(subject.object.directory).to eq(params[:directory])
    end
  end
end
