require 'spec_helper'

describe Savers::DocumentsPublisher do
  let(:editor) { create(:editor) }
  let(:parts)  { create(:parts) }
  
  let(:bound_document) do
    create(:document, category_ids: [parts.id], published: true, published_at: Time.now)
  end
  
  let(:yesterday) { 1.day.ago }
  let(:now)       { Time.now }
  let(:tomorrow)  { 1.day.from_now }
  
  before(:each) do
    allow(Time).to receive(:now).and_return(now)
  end
  
  subject { Savers::DocumentsPublisher.new(document, params, editor) }
  
  describe '#save' do
    context 'without parameters' do
      let(:params) { ActionController::Parameters.new }
      
      context 'on an unpublished document' do
        let(:document) { create(:document, category_ids: [parts.id]) }
        
        before(:each) do
          url = Savers::UrlsCreator.new(
            ActionController::Parameters.new(
              domain:    'ya.ru',
              rubric:    'politics',
              type:      'news',
              date:      Date.today,
              pseudo_id: '123',
              slug:      'putin'
            ),
            editor,
            document
          ).tap(&:save).object
          document.update_column(:url_id, url.id)
          
          Savers::LinksCreator.new(
            ActionController::Parameters.new(
              section: 'reference',
              bound_document_id: bound_document.id
            ),
            editor,
            document
          ).save
          
          document.create_root_box
          Savers::BoxesCreator.new(
            ActionController::Parameters.new(
              type:      'paragraph',
              text:      'Some text',
              parent_id: document.root_box.id
            ),
            editor
          ).save
          
          @list = Savers::ListsCreator.new(
            ActionController::Parameters.new(
              slug:     'top7',
              title:    'Топ 7',
              duration: 1.day,
              limit:    7
            ),
            editor
          ).saved_object
          
          subject.save
        end
        
        it 'publishes the document and sets published_at to current time' do
          expect(subject.object).to be_published
          expect(subject.object.published_at).to eq(now)
          
          expect(subject.object.milestones[:published]).to eq(
            at: now.to_i,
            by: editor.id
          )
        end
        
        it 'creates a snapshot' do
          expect(subject.object.snapshots.count).to eq(1)
          
          snapshot = subject.object.snapshots.last
          expect(snapshot.action).to eq('document.published')
          expect(snapshot.data[:id]).to eq(subject.object.id)
        end
        
        it 'enables all boxes' do
          expect(subject.object.boxes).to all(be_enabled)
        end
        
        it 'fixes the urls' do
          expect(subject.object.urls(true).first).to be_fixed
        end
        
        it 'adds the editor to participants' do
          expect(subject.object.participant_ids).to include(editor.id)
        end
        
        it 'processes the document with all 4 pipelines' do
          expect(subject.object.packed_document).not_to be_empty
          expect(subject.object.packed_links).not_to be_empty
          expect(subject.object.root_box.children.first.packed_box).not_to be_empty
        end
        
        it 'updates effective timestamps of list items' do
          item = subject.object.list_items(true).first
          expect(item.effective_timestamp).to eq(document.published_at)
        end
        
        it 'creates list_item' do
          expect(subject.object.list_items.first).to eq(@list.reload.items.first)
        end
        
        it 'updates list cache' do
          expect(@list.reload.cached_document_ids).to eq([subject.object.id])
        end
      end
      
      context 'on a published document' do
        let(:document) do
          create(:document, published: true, published_at: yesterday)
        end
        
        before(:each) do
          document.create_root_box
          
          Savers::BoxesCreator.new(
            ActionController::Parameters.new(
              type: 'paragraph',
              parent_id: document.root_box.id
            ),
            editor
          ).save
          
          Savers::BoxesCreator.new(
            ActionController::Parameters.new(
              type: 'inset_image',
              parent_id: document.root_box.id
            ),
            editor
          ).save
        end
        
        it "doesn't change the document" do
          subject.save
          
          expect(subject.object).to be_published
          expect(subject.object.published_at).to eq(yesterday)
        end
        
        it 'enables only paragraph boxes' do
          expect(subject.object.boxes.first).to be_enabled
          expect(subject.object.boxes.last).not_to be_enabled
        end
      end
    end
    
    context 'with a published_at parameter' do
      let(:params) { ActionController::Parameters.new(published_at: tomorrow) }
      
      context 'on an unpublished document' do
        let(:document) { create(:document) }
        
        it 'assigns the new published_at date' do
          subject.save
          
          expect(subject.object).to be_published
          expect(subject.object.published_at).to eq(tomorrow)
        end
      end
      
      context 'on a published document' do
        let(:document) do
          create(:document, published: true, published_at: yesterday)
        end
        
        it 'updates published_at' do
          subject.save
          
          expect(subject.object).to be_published
          expect(subject.object.published_at).to eq(tomorrow)
        end
        
        context 'with a nil published_at parameter' do
          let(:params) { ActionController::Parameters.new(published_at: nil) }
          
          it "doesn't update published_at" do
            subject.save
            
            expect(subject.object).to be_published
            expect(subject.object.published_at).to eq(yesterday)
          end
        end
      end
    end
  end
end
