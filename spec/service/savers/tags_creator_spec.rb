require 'spec_helper'

describe Savers::TagsCreator do
  let(:editor) { create(:editor) }
  
  let(:params) do
    ActionController::Parameters.new(
      title: 'New tag'
    )
  end
  
  subject { Savers::TagsCreator.new(params, editor) }
  
  describe '#save' do
    before(:each) do
      subject.save
    end
    
    it 'creates a new tag with passed in params' do
      expect(subject.object).to be_persisted
      expect(subject.object.title).to eq(params[:title])
    end
  end
end
