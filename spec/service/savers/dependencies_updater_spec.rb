require 'spec_helper'

describe Savers::DependenciesUpdater do
  let(:document)           { create(:document) }
  let(:dependent_document) { create(:document).tap(&:create_root_box) }
  
  subject { described_class.new(document) }
  
  context 'with a dependent link' do
    let(:link) { create(:link, document_id: dependent_document.id) }
    
    before(:each) do
      Dependency.create! do |dependency|
        dependency.dependable = document
        dependency.dependent  = link
      end
    end
    
    it 'updates it' do
      pipeline = double('Pipeline')
      expect(Pipeline::Links).to receive(:new).with(dependent_document.porcupined).and_return(pipeline)
      expect(pipeline).to receive(:process)
      
      subject.call
    end
  end
  
  context 'with a dependent box' do
    let(:box) { create(:box, parent_id: dependent_document.root_box_id) }
    
    before(:each) do
      Dependency.create! do |dependency|
        dependency.dependable = document
        dependency.dependent  = box
      end
    end
    
    it 'updates it' do
      pipeline = double('Pipeline')
      expect(Pipeline::Box).to receive(:new).with(box.porcupined).and_return(pipeline)
      expect(pipeline).to receive(:process)
      
      subject.call
    end
  end
end
