require 'spec_helper'

describe Savers::DocumentsAssigner do
  let(:editor)          { create(:editor) }
  let(:assigned_editor) { create(:editor) }
  let(:parts)           { create(:parts) }
  let(:now)             { Time.now }
  
  before(:each) do
    allow(Time).to receive(:now).and_return(now)
  end
  
  subject { Savers::DocumentsAssigner.new(document, params, editor) }
  
  describe '#save' do
    before(:each) do
      subject.save
    end
    
    context 'with valid parameters' do
      let(:params) do
        ActionController::Parameters.new(editor_id: assigned_editor.id)
      end
      
      context 'and an applicable document' do
        let(:document) do
          create(:document, category_ids: [parts.id])
        end
        
        it "sets document's assignee_status to enabled" do
          expect(subject.object.assignee_status).to eq('enabled')
        end
        
        it "sets document's assigned_editor_id to assigned editor's id" do
          expect(subject.object.assigned_editor_id).to eq(assigned_editor.id)
        end
        
        it 'adds the respective record to document.milestones' do
          expect(subject.object.milestones[:assigned]).to eq(
            at: now.to_i,
            by: assigned_editor.id
          )
        end
        
        it 'adds the assigned editor to participants' do
          expect(subject.object.participant_ids).to include(assigned_editor.id)
        end
        
        it 'creates a snapshot' do
          expect(subject.object.snapshots.count).to eq(1)
          
          snapshot = subject.object.snapshots.first
          expect(snapshot.action).to eq('document.assigned')
          expect(snapshot.data[:id]).to eq(subject.object.id)
        end
      end
      
      context 'and an inapplicable document' do
        let(:document) { create(:document) }
        
        it "doesn't touch the document" do
          expect(subject.object.assigner_data).to be_empty
        end
      end
    end
    
    context 'with invalid parameters' do
      let(:params) { ActionController::Parameters.new }
      let(:document) { create(:document) }
      
      it "doesn't touch the document" do
        expect(subject.object.assigner_data).to be_empty
      end
    end
  end
end
