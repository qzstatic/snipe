require 'spec_helper'

describe Savers::BoxesRestorer do
  let(:editor)     { create(:editor) }
  let(:document)   { create(:document) }
  let(:box)        { create(:box, deleted: true ) }
  
  before :each do
    create(:document, root_box_id: box.id, cached_setting_keys: %w(boxes.news))
  end
  
  describe '#save' do
    subject { Savers::BoxesRestorer.new(box, editor) }
    
    it 'updates the box' do
      expect { subject.save }.to change { box.deleted }.from(true).to(false)
    end
  end
end
