require 'spec_helper'

describe Savers::BoxesUpdater do
  let(:editor)     { create(:editor) }
  let(:document)   { create(:document) }
  let(:box)        { create(:box) }
  let(:updated_at) { box.updated_at.to_s }
  
  before :each do
    create(:document, root_box_id: box.id, cached_setting_keys: %w(boxes.news))
  end
  
  let(:params) do
    ActionController::Parameters.new(enabled: true)
  end
  
  describe '#save' do
    subject { Savers::BoxesUpdater.new(box, params, editor, updated_at: updated_at) }
    
    it 'updates the box' do
      subject.save
      
      expect(subject.object).to be_enabled
      expect(subject.object.changes).to be_empty
    end
    
    context 'with a child box' do
      before(:each) do
        document.create_root_box
        
        @box = Savers::BoxesCreator.new(
          ActionController::Parameters.new(
            type:      'paragraph',
            parent_id: document.root_box.id
          ),
          editor
        ).tap(&:save).object
      end
      
      it 'creates a snapshot with a tree of boxes' do
        updater = Savers::BoxesUpdater.new(@box, params, editor, updated_at: updated_at)
        
        expect {
          updater.save
        }.to change { Snapshot.count }.by(1)
        
        expect(@box.snapshots.last.action).to eq('box.updated')
      end
    end
    
    it 'adds the editor to participants' do
      subject.save
      subject.object.reload
      expect(subject.object.document.participant_ids).to include(editor.id)
    end
    
    context 'with a paragraph with bad characters' do
      before(:each) do
        @paragraph = create(:box, type: 'paragraph', parent_id: box.id)
      end
      
      let(:params) do
        ActionController::Parameters.new(body: "Т\u0003екст")
      end
      
      subject { Savers::BoxesUpdater.new(@paragraph, params, editor, updated_at: updated_at) }
      
      it 'writes the box with a cleaned up body' do
        subject.save
        
        expect(subject.object.body).to eq('Текст')
      end
    end
    
    context 'with expired updated_at' do
      let(:updated_at) { 1.day.ago.to_s }
      it "don't updates the box" do
        subject.save
        subject.object.reload
        expect(subject.object).not_to be_enabled
      end
    end
  end
end
