require 'spec_helper'

describe Savers::PagesUpdater do
  let(:editor)   { create(:editor) }
  let(:old_list) { create(:list) }
  let(:new_list) { create(:list) }
  
  let(:page) do
    Savers::PagesCreator.new(
      ActionController::Parameters.new(
        title: 'A new page',
        slug: 'page',
        position: 100,
        list_ids: [old_list.id]
      ),
      editor
    ).tap(&:save).object
  end
  
  let(:params) do
    ActionController::Parameters.new(
      title: 'New title',
      position: 200,
      list_ids: [
        old_list.id,
        new_list.id
      ]
    )
  end
  
  subject { Savers::PagesUpdater.new(page, params, editor) }
  
  describe '#save' do
    before(:each) do
      subject.save
    end
    
    it 'updates parameters in the given page' do
      expect(subject.object).to be_persisted
      expect(subject.object.title).to eq(params[:title])
      expect(subject.object.position).to eq(params[:position])
      expect(subject.object.lists).to include(old_list)
      expect(subject.object.lists).to include(new_list)
    end
    
    it 'creates a snapshot' do
      expect(subject.object.snapshots.count).to eq(2)
      expect(subject.object.snapshots.last.action).to eq('page.updated')
    end
  end
end
