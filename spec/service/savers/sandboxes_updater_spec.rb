require 'spec_helper'

describe Savers::SandboxesUpdater do
  let(:params) do
    ActionController::Parameters.new(
      title:       'Последние 48 часов',
      base_date:   nil,
      date_offset: -2.days
    )
  end
  
  let(:editor)  { create(:editor) }
  let(:sandbox) { create(:fixed_month_sandbox) }
  
  subject { Savers::SandboxesUpdater.new(sandbox, params, editor) }
  
  describe '#save' do
    before(:each) do
      subject.save
    end
    
    it 'updates the sandbox with the given params' do
      expect(subject.object.title).to       eq(params[:title])
      expect(subject.object.base_date).to   eq(params[:base_date])
      expect(subject.object.date_offset).to eq(params[:date_offset])
    end
    
    it 'creates a snapshot' do
      expect(subject.object.snapshots.count).to eq(1)
      expect(subject.object.snapshots.first.action).to eq('sandbox.updated')
    end
  end
end
