require 'spec_helper'

describe Savers::RawUrlsCreator do
  let(:editor)   { create(:editor) }
  let(:category) { create(:domain) }
  let(:document) { create(:document, category_ids: [category.id]) }
  
  let(:params) do
    ActionController::Parameters.new(rendered: 'https://www.google.ru/')
  end
  
  describe '#save' do
    subject { Savers::RawUrlsCreator.new(params, editor, document) }
    
    it 'creates a new url with raw URL' do
      subject.save
      
      expect(subject.object).to be_persisted
      expect(subject.object.rendered).to eq('https://www.google.ru/')
      expect(subject.object.fixed).to be_truthy
      expect(subject.object.document).to eq(document)
    end
  end
end
