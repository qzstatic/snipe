require 'spec_helper'

describe Savers::AttachmentVersionsUpdater do
  def create_version(attachment, slug)
    create(
      :attachment_version,
      slug: slug,
      attachment_id: attachment.id
    )
  end
  
  let(:editor)  { create(:editor) }
  let(:attachment) { create(:attachment) }
  let(:small) { create_version(attachment, 'medium') }
  
  let(:params) do
    ActionController::Parameters.new(
      slug:     'preview',
      filename: 'preview-123.jpg'
    )
  end
  
  subject { Savers::AttachmentVersionsUpdater.new(small, params, editor) }
  
  describe '#save' do
    it 'updates the version' do
      subject.save
      
      expect(subject.object.slug).to eq(params[:slug])
      expect(subject.object.filename).to eq(params[:filename])
    end
  end
end
