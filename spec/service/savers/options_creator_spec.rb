require 'spec_helper'

describe Savers::OptionsCreator do
  let(:editor) { create(:editor) }
  
  let(:params) do
    ActionController::Parameters.new(
      type:  'source',
      title: 'Yandex',
      url:   'ya.ru'
    )
  end
  
  subject { Savers::OptionsCreator.new(params, editor) }
  
  describe '#save' do
    it 'creates new option' do
      subject.save
      expect(subject.object).to       be_persisted
      expect(subject.object.title).to eq(params[:title])
      expect(subject.object.url).to   eq(params[:url])
      expect(subject.object.type).to  eq('source')
    end
  end
end
