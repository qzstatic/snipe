require 'spec_helper'

describe Savers::SandboxesCreator do
  let(:editor) { create(:editor) }
  
  let(:params) do
    ActionController::Parameters.new(title: 'Последние документы за 24 часа')
  end
  
  subject { Savers::SandboxesCreator.new(params, editor) }
  
  describe '#save' do
    before(:each) do
      subject.save
    end
    
    it 'creates a new sandbox with passed in params' do
      expect(subject.object).to be_persisted
      expect(subject.object.title).to eq(params[:title])
    end
    
    
    it "adds the editor to sandbox's owners" do
      expect(subject.object.owner_ids).to include(editor.id)
    end
    
    it 'creates a snapshot' do
      expect(subject.object.snapshots.count).to eq(1)
      expect(subject.object.snapshots.first.action).to eq('sandbox.created')
    end
  end
end
