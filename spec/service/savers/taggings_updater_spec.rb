require 'spec_helper'

describe Savers::TaggingsUpdater do
  let(:editor)  { create(:editor) }
  let(:new_tag) { create(:tag, title: 'New tag') }
  
  let(:params) do
    ActionController::Parameters.new(
      tag_ids: [new_tag.id]
    )
  end
  
  subject { Savers::TaggingsUpdater.new(tagging, params, editor) }
  
  describe '#save' do
    context 'without tags' do
      let(:tagging) { create(:tagging) }
      
      before(:each) do
        subject.save
      end
      
      it 'updates the tagging with parameters' do
        expect(subject.object.tags).to include(new_tag)
      end
      
      it 'creates a snapshot' do
        expect(subject.object.snapshots.count).to eq(1)
        expect(subject.object.snapshots.last.action).to eq('tagging.updated')
      end
    end
    
    context 'with tags' do
      let(:old_tag)  { create(:tag) }
      let(:tagging)  { create(:tagging, tag_ids: [old_tag.id]) }
      
      it 'updates the tagging with parameters' do
        subject.save
        
        expect(subject.object.tags).not_to include(old_tag)
        expect(subject.object.tags).to     include(new_tag)
      end
    end
  end
end
