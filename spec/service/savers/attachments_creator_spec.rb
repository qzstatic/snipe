require 'spec_helper'

describe Savers::AttachmentsCreator do
  let(:editor) { create(:editor) }
  
  let(:params) do
    ActionController::Parameters.new(
      type:      'image',
      directory: 'some/dir'
    )
  end
  
  subject { Savers::AttachmentsCreator.new(params, editor) }
  
  describe '#save' do
    before(:each) do
      subject.save
    end
    
    it 'creates a new attachment with passed in params' do
      expect(subject.object).to be_persisted
      expect(subject.object.type).to eq('image')
      expect(subject.object.directory).to eq('some/dir')
    end
  end
end
