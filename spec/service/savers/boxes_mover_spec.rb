require 'spec_helper'

describe Savers::BoxesMover do
  let(:editor)   { create(:editor) }
  let(:root_box) { create(:box) }
  let(:document) { create(:document, root_box_id: root_box.id, cached_setting_keys: %w(box_moving)).porcupined }
  let(:box) {
    Savers::BoxesCreator.new(
      ActionController::Parameters.new(
          type:      'paragraph',
          parent_id: document.root_box.id
      ),
      editor
    ).tap(&:save).object
  }
  let(:box_updated_at) { box.updated_at }

  describe '#save' do
    it 'change position' do
      mover = Savers::BoxesMover.new(box, 234, editor, document.updated_at)
      expect {
        if mover.save
          box.reload
          document.reload
        end
      }.to change { Snapshot.count }.by(2)
      expect(box.position).to eq(234)
      expect(box.updated_at).to eq(box_updated_at)
      expect(mover.object.changes).to be_empty
      expect(document.box_move_editor_id).to eq(editor.id)
      expect(box.snapshots.last.action).to eq('box.moved')
      expect(document.snapshots.last.action).to eq('box.moved')
    end
  end

end
