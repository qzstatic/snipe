require 'spec_helper'

describe Savers::ListItemsUpdater do
  # let(:web_editors) { create(:web_editors) }
  # let(:editor)    { create(:editor, group_ids:[web_editors.id]) }  
  let(:editor)    { create(:editor) }  
  let(:document)  { create(:document,title: 'f1') }
  let(:document2) { create(:document,title: 'f2') }
  
  before(:each) do
    @list = create(:list, duration: 1.day)
    
    Savers::DocumentsPublisher.new(document, ActionController::Parameters.new(published_at: 2.hour.ago), editor).save
    Savers::DocumentsPublisher.new(document2, ActionController::Parameters.new(published_at: 1.hour.ago), editor).save
    @list.reload
    @item = document.list_items.where(list_id: @list.id).first
  end
  
  let(:params) { ActionController::Parameters.new(top: 1) }
  
  describe '#save' do
    subject { Savers::ListItemsUpdater.new(@item, params, editor) }
    
    it 'updates the item attributes' do
      subject.save
      
      expect(subject.object.top).to eq(params[:top])
    end
    
    it 'updates the effective timestamp' do
      expect(subject.object.effective_timestamp).to eq(document.published_at + params[:top].days)
    end
    
    it 'updates list cache' do
      expect(@list.cached_document_ids).to eq([document2.id, document.id])
      subject.save
      @list.reload
      expect(@list.cached_document_ids).to eq([document.id, document2.id])
    end
    
    it 'creates a snapshot' do
      subject.save
      
      expect(subject.object.snapshots.count).to eq(1)
      expect(subject.object.snapshots.first.action).to eq('list_item.updated')
    end
  end
end
