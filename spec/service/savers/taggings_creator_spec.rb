require 'spec_helper'

describe Savers::TaggingsCreator do
  let(:editor)   { create(:editor) }
  let(:document) { create(:document) }
  let(:tag)      { create(:tag) }
  
  let(:params) do
    ActionController::Parameters.new(
      type: 'New type',
      tag_ids: [tag.id]
    )
  end
  
  subject { Savers::TaggingsCreator.new(params, editor, document) }
  
  describe '#save' do
    before(:each) do
      subject.save
    end
    
    it 'creates a new tagging with parameters' do
      expect(subject.object).to be_persisted
      expect(subject.object.document).to eq(document)
      expect(subject.object.type).to eq('New type')
      expect(subject.object.tags).to include(tag)
    end
    
    it 'adds the editor to participants' do
      subject.object.reload
      expect(subject.object.document.participant_ids).to include(editor.id)
    end
    
    it 'creates a snapshot' do
      expect(subject.object.snapshots.count).to eq(1)
      expect(subject.object.snapshots.last.action).to eq('tagging.created')
    end
  end
end
