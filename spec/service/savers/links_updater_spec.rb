require 'spec_helper'

describe Savers::LinksUpdater do
  let(:editor) { create(:editor) }
  let(:domain) { create(:domain) }
  let(:document) { create(:document, category_ids: [domain.id]) }
  let(:bound_document) { create(:document, category_ids: [domain.id]) }
  let(:link) { create(:link, document_id: document.id, bound_document_id: bound_document.id) }
  
  let(:params) do
    ActionController::Parameters.new(section: :author, position: 15)
  end
  
  subject { Savers::LinksUpdater.new(link, params, editor) }
  
  describe '#save' do
    it 'updates the link with the given params' do
      subject.save
      
      expect(subject.object.section).to eq(:author)
      expect(subject.object.position).to eq(15)
    end
    
    it "creates a snapshot with all document's links" do
      expect {
        subject.save
      }.to change { Snapshot.count }.by(1)
      
      expect(subject.object.snapshots.last.action).to eq('link.updated')
    end
  end
end
