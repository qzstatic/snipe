require 'spec_helper'

describe Savers::UrlsUpdater do
  let(:editor)   { create(:editor) }
  let(:category) { create(:parts) }
  let(:document) { create(:document, category_ids: [category.id]) }
  let(:url)      { create(:url, document_id: document.id) }
  
  let(:params) do
    ActionController::Parameters.new(
      domain: 'yandex.ru',
      rubric: 'politics',
      type:   'articles',
      slug:   'navalny',
      date:   '2014/07/17'
    )
  end
  
  describe '#save' do
    subject { Savers::UrlsUpdater.new(url, params, editor) }
    
    it 'updates url with passed in params' do
      subject.save
      
      expect(subject.object.domain).to eq('yandex.ru')
      expect(subject.object.type).to eq('articles')
      expect(subject.object.slug).to eq('navalny')
      expect(subject.object.document).to eq(document)
      expect(subject.object.changes).to be_empty
    end
    
    it 'adds the editor to participants' do
      subject.save
      subject.object.reload
      expect(subject.object.document.participant_ids).to include(editor.id)
    end
    
    it "creates a snapshot with all document's urls" do
      expect {
        subject.save
      }.to change { Snapshot.count }.by(1)
      
      expect(subject.object.snapshots.last.action).to eq('url.updated')
    end
    
    context 'with a fixed url' do
      before(:each) do
        url.fix
      end
      
      it "doesn't touch the url" do
        expect {
          subject.save
        }.not_to change { url.porcupined.domain }
      end
      
      it "doesn't create any snapshots" do
        expect {
          subject.save
        }.not_to change { Snapshot.count }
      end
    end
  end
end
