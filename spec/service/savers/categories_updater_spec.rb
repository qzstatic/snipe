require 'spec_helper'

describe Savers::CategoriesUpdater do
  let(:editor) { create(:editor) }
  let(:category) { create(:rubrics) }
  
  subject { Savers::CategoriesUpdater.new(category, params, editor) }
  
  let(:params) do
    ActionController::Parameters.new(slug: 'new_rubrics', position: 1)
  end
  
  describe '#save' do
    it 'updates the category with the given params' do
      subject.save
      
      expect(subject.object.slug).to eq(params[:slug])
      expect(subject.object.position).to eq(params[:position])
    end
    
    it "recalculates slug path in category and all of it's descendants" do
      expect {
        subject.save
      }.to change { Category.find(category.id).slug_path }.from(%w(rubrics)).to(%w(new_rubrics))
    end
    
    it "recalculates slug path in category's descendants" do
      rubric = create(:rubric, parent_id: subject.object.id)
      allow(category).to receive(:descendants).and_return([rubric])
      
      expect {
        subject.save
      }.to change { rubric.reload.slug_path }.from(%w(rubrics russia)).to(%w(new_rubrics russia))
    end
    
    it 'rejects empty strings in setting_keys' do
      params[:setting_keys] = ['abc', '']
      subject.save
      
      expect(subject.object.setting_keys).to eq(%w(abc))
    end
    
    context 'when setting_keys are changed' do
      before(:each) do
        params[:setting_keys] = %w(boxes.common)
      end
      
      it 'regenerates the setting_keys cache' do
        expect(subject.object).to receive(:async_regenerate_setting_keys_cache)
        subject.save
      end
    end
    
    context 'when setting_keys are not changed' do
      it "doesn't regenerate the setting_keys cache" do
        expect(subject.object).not_to receive(:async_regenerate_setting_keys_cache)
        subject.save
      end
    end
  end
end
