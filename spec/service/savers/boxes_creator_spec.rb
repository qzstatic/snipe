require 'spec_helper'

describe Savers::BoxesCreator do
  let(:editor)   { create(:editor) }
  let(:document) { create(:document, cached_setting_keys: %w(boxes.news)) }
  
  let(:root_box) do
    create(:box).tap do |box|
      document.update(root_box_id: box.id)
    end
  end
  
  let(:params) do
    ActionController::Parameters.new(type: 'gallery', parent_id: root_box.id)
  end
  
  describe '#save' do
    subject { Savers::BoxesCreator.new(params, editor) }
    
    it 'creates a new box' do
      subject.save
      
      expect(subject.object).to be_persisted
      expect(subject.object.type).to eq('gallery')
    end
    
    it 'adds the editor to participants' do
      subject.save
      subject.object.reload
      expect(subject.object.document.participant_ids).to include(editor.id)
    end
    
    it 'creates a snapshot with attributes of the box' do
      expect {
        subject.save
      }.to change { Snapshot.count }.by(1)
      
      expect(Snapshot.first.action).to eq('box.created')
      expect(Snapshot.first.data[:type]).to eq('gallery')
    end
    
    context 'with a paragraph with bad characters' do
      let(:params) do
        ActionController::Parameters.new(type: 'paragraph', body: "Т\u0003екст", parent_id: root_box.id)
      end
      
      it 'writes the box with a cleaned up body' do
        subject.save
        
        expect(subject.object.body).to eq('Текст')
      end
    end
  end
end
