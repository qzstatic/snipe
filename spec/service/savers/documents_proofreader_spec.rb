require 'spec_helper'

describe Savers::DocumentsProofreader do
  let(:editor) { create(:editor) }
  let(:now)    { Time.now }
  
  before(:each) do
    allow(Time).to receive(:now).and_return(now)
  end
  
  subject { Savers::DocumentsProofreader.new(document, editor) }
  
  describe '#save' do
    context 'with valid parameters' do
      context 'and an applicable document' do
        let(:document) { create(:document) }
        
        before(:each) do
          allow(document).to receive(:settings).and_return(
            proofreading: {
              on: true
            }
          )
          
          subject.save
        end
        
        it "sets document's assignee_status to enabled" do
          expect(subject.object.proofread).to be_truthy
        end
        
        it 'adds the respective record to document.milestones' do
          expect(subject.object.milestones[:proofread]).to eq(
            at: now.to_i,
            by: editor.id
          )
        end
        
        it 'adds the assigned editor to participants' do
          expect(subject.object.participant_ids).to include(editor.id)
        end
        
        it 'creates a snapshot' do
          expect(subject.object.snapshots.count).to eq(1)
          
          snapshot = subject.object.snapshots.first
          expect(snapshot.action).to eq('document.proofread')
          expect(snapshot.data[:id]).to eq(subject.object.id)
        end
      end
      
      context 'and an inapplicable document' do
        let(:document) { create(:document) }
        
        it "doesn't touch the document" do
          subject.save
          expect(subject.object.proofreader_data).to be_empty
        end
      end
    end
    
    context 'with invalid parameters' do
      let(:document) { create(:document) }
      
      it "doesn't touch the document" do
        subject.save
        expect(subject.object.proofreader_data).to be_empty
      end
    end
  end
end
