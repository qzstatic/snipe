require 'spec_helper'

describe Savers::ListsUpdater do
  let(:editor) { create(:editor) }
  let(:list)   { create(:list, duration: 1.hour) }
  let(:params) { ActionController::Parameters.new(slug: 'top5', duration: 2.hours, cols: 5) }
  
  subject { Savers::ListsUpdater.new(list, params, editor) }
  
  describe '#save' do
    it 'updates the list' do
      prm = ActionController::Parameters.new(slug: 'top5', duration: 2.hours, cols: 5)
      saver = Savers::ListsUpdater.new(list, prm, editor)
      
      saver.save
      saver.object.reload
      
      expect(saver.object.slug).to eq('top5')
      expect(saver.object.width).to eq('5')
      expect(saver.object.duration).to eq(2.hours)
    end
    
    let(:params) { ActionController::Parameters.new(slug: 'top5', cols: 5, category_ids: [1, 2, 3, -4, -5], blacklisted_category_ids: [6]) }
    
    it 'saves blacklisted_category_ids' do
      subject.save
      
      expect(subject.object.category_ids).to eq([1, 2, 3])
      expect(subject.object.blacklisted_category_ids).to eq([6, 4, 5])
    end
    
    it 'creates a snapshot' do
      subject.save
      
      expect(subject.object.snapshots.count).to eq(1)
      expect(subject.object.snapshots.first.action).to eq('list.updated')
    end
  end
end
