require 'spec_helper'

describe Savers::AttachmentsUpdater do
  let(:editor)     { create(:editor) }
  let(:attachment) { create(:attachment) }
  
  let(:params) do
    ActionController::Parameters.new(
      type: 'image',
      directory: '/path/to/directory'
    )
  end
  
  subject { Savers::AttachmentsUpdater.new(attachment, params, editor) }
  
  describe '#save' do
    it 'updates the attachment with parameters' do
      subject.save
      
      expect(subject.object.type).to eq(params[:type])
      expect(subject.object.directory).to eq(params[:directory])
    end
  end
end
