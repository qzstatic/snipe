require 'spec_helper'

describe Savers::TagsUpdater do
  let(:editor) { create(:editor) }
  let(:tag)    { create(:tag) }
  
  let(:params) do
    ActionController::Parameters.new(
      title: 'New title'
    )
  end
  
  subject { Savers::TagsUpdater.new(tag, params, editor) }
  
  describe '#save' do
    it 'updates the tag with parameters' do
      subject.save
      
      expect(subject.object.title).to eq('New title')
    end
  end
end
