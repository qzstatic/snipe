require 'spec_helper'

describe Savers::Params do
  subject { Savers::Params.new(a: 1, b: 2) }
  
  describe '#extract' do
    it 'deletes the specified keys' do
      subject.extract(:a)
      expect(subject.key?(:a)).to be_falsy
    end
    
    it 'returns the specified keys along with their values' do
      expect(subject.extract(:a)).to eq(a: 1)
    end
    
    it 'ignores missing keys' do
      expect(subject.extract(:c)).to eq({})
    end
  end
end
