require 'spec_helper'

describe Savers::AttachmentVersionsCreator do
  let(:editor) { create(:editor) }
  let(:attachment) { create(:attachment) }
  
  let(:params) do
    ActionController::Parameters.new(
      slug:     'preview',
      filename: 'preview-123.jpg'
    )
  end
  
  subject { Savers::AttachmentVersionsCreator.new(params, editor, attachment) }
  
  describe '#save' do
    before(:each) do
      subject.save
    end
    
    it 'creates a new version with parameters' do
      expect(subject.object).to be_persisted
      expect(subject.object.attachment).to eq(attachment)
      expect(subject.object.slug).to eq('preview')
      expect(subject.object.filename).to eq('preview-123.jpg')
    end
  end
end
