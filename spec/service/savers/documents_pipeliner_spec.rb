require 'spec_helper'

describe Savers::DocumentsPipeliner do
  let(:document) { create(:document) }
  
  subject { described_class.new(document: document) }
  
  describe '#process' do
    context 'without arguments' do
      it 'processes the document with all 4 pipelines' do
        described_class::PIPELINES.each do |pipeline_class|
          pipeline = double('Pipeline')
          expect(pipeline_class).to receive(:new).with(document).and_return(pipeline)
          expect(pipeline).to receive(:process)
        end
        
        subject.process
      end
    end
    
    context 'with preview: true' do
      let(:document_data) do
        { id: 1, title: 'New doc' }
      end
      
      let(:taggings_data) do
        [
          { type: 'people', tag_ids: [1, 2] }
        ]
      end
      
      let(:links_data) do
        {
          reference: [
            { bound_document: {}, position: 1 }
          ]
        }
      end
      
      let(:boxes_data) do
        {
          123 => { type: 'paragraph', body: 'Some text' }
        }
      end
      
      before(:each) do
        document_pipeline = double('Document pipeline')
        allow(Pipeline::Document).to receive(:new).with(document).and_return(document_pipeline)
        allow(document_pipeline).to receive(:serialize).and_return(document_data)
        
        taggings_pipeline = double('Taggings pipeline')
        allow(Pipeline::Taggings).to receive(:new).with(document).and_return(taggings_pipeline)
        allow(taggings_pipeline).to receive(:serialize).and_return(taggings_data)
        
        links_pipeline = double('Links pipeline')
        allow(Pipeline::Links).to receive(:new).with(document).and_return(links_pipeline)
        allow(links_pipeline).to receive(:serialize).and_return(links_data)
        
        boxes_pipeline = double('Boxes pipeline')
        allow(Pipeline::Boxes).to receive(:new).with(document).and_return(boxes_pipeline)
        allow(boxes_pipeline).to receive(:serialize).and_return(boxes_data)
      end
      
      it 'serializes the document data and writes it to packed_preview' do
        subject.process(preview: true)
        
        expect(document.packed_preview).to eq(
          document_data.merge(
            taggings: taggings_data,
            links:    links_data,
            boxes:    boxes_data.values
          ).deep_stringify_keys
        )
      end
    end
  end
end
