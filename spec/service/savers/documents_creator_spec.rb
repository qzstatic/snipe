require 'spec_helper'

describe Savers::DocumentsCreator do
  let(:editor)  { create(:editor) }
  let(:article) { create(:article) }
  let(:now)     { Time.now }
  
  before(:each) do
    allow(Time).to receive(:now).and_return(now)
  end
  
  let(:params) do
    ActionController::Parameters.new(
      title:        'New document',
      subtitle:     'New subtitle',
      category_ids: [article.id]
    )
  end
  
  subject { Savers::DocumentsCreator.new(params, editor) }
  
  describe '#save' do
    before(:each) do
      subject.save
    end
    
    it 'creates a new document with both fixed and dynamic attributes' do
      expect(subject.object).to be_persisted
      expect(subject.object.title).to eq(params[:title])
      expect(subject.object.subtitle).to eq(params[:subtitle])
    end
    
    it 'converts category_ids to category_ids' do
      expect(subject.object.category_ids).to include(article.id)
    end
    
    it 'correctly assigns the :created milestone' do
      expect(subject.object.milestones[:created]).to eq(
        at: now.to_i,
        by: editor.id
      )
    end
    
    it 'adds the editor to participants' do
      expect(subject.object.participant_ids).to include(editor.id)
    end
    
    it 'creates a snapshot' do
      expect(subject.object.snapshots.count).to eq(1)
      
      snapshot = subject.object.snapshots.first
      expect(snapshot.action).to eq('document.created')
      expect(snapshot.data[:id]).to eq(subject.object.id)
    end
  end
end
