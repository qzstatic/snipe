require 'spec_helper'

describe Savers::UrlsCreator do
  let(:editor)   { create(:editor) }
  let(:category) { create(:parts) }
  let(:document) { create(:document, category_ids: [category.id]) }
  
  let(:params) do
    ActionController::Parameters.new(
      domain:    'google.com',
      rubric:    'politics',
      type:      'news',
      date:      '2014/07/17',
      pseudo_id: '123',
      slug:      'putin'
    )
  end
  
  describe '#save' do
    subject { Savers::UrlsCreator.new(params, editor, document) }
    
    before(:each) do
      subject.save
    end
    
    it 'creates a new url with passed in params' do
      expect(subject.object).to be_persisted
      expect(subject.object.domain).to eq('google.com')
      expect(subject.object.type).to eq('news')
      expect(subject.object.slug).to eq('putin')
      expect(subject.object.date).to eq(Date.new(2014, 7, 17))
      expect(subject.object.document).to eq(document)
    end
    
    it 'adds the editor to participants' do
      subject.object.reload
      expect(subject.object.document.participant_ids).to include(editor.id)
    end
    
    it "creates a snapshot with all document's urls" do
      expect {
        subject.save
      }.to change { Snapshot.count }.by(1)
      
      expect(subject.object.snapshots.last.action).to eq('url.created')
    end
  end
end
