require 'spec_helper'

describe Savers::DocumentsFastCreator do
  let(:editor) { create(:editor) }
  let(:kinds)  { create(:kinds) }
  let(:links)  { create(:links, parent: kinds) }
  let(:now)    { Time.now }
  
  before(:each) do
    allow(Time).to receive(:now).and_return(now)
    
    params = ActionController::Parameters.new(
      type:  'source',
      title: 'Yandex',
      url:   'ya.ru'
    )
    @source = Savers::OptionsCreator.new(params, editor).saved_object
  end
  
  describe '#save' do
    context 'with valid attributes' do
      let(:params) do
        ActionController::Parameters.new(
          title: 'New document',
          category_ids: [links.id],
          url: 'http://outer.link/',
          source_id: @source.id,
          source_url: @source.url,
          source_title: @source.title
        )
      end
      
      let(:params_slugs) do
        ActionController::Parameters.new(
          title: 'New document',
          category_slugs: %w(kinds links),
          url: 'http://outer.link/'
        )
      end
      
      subject { Savers::DocumentsFastCreator.new(params, editor) }
      
      it 'creates and publishes a new document with url' do
        expect(subject.save).to eq(true)
        
        expect(subject.object.url.rendered).to eq(params[:url])
        expect(subject.object.url).to be_fixed
        expect(subject.object).to be_published
        expect(subject.object.source_id).to eq(@source.id)
        expect(subject.object.source_url).to eq(@source.url)
        expect(subject.object.source_title).to eq(@source.title)
      end
      
      it 'creates and publishes a new document with url and category slugs' do
        params[:category_slugs] = %w(kinds links)
        params.delete(:category_ids)
        subject.save
        
        expect(subject.object.url.rendered).to eq(params[:url])
        expect(subject.object.url).to be_fixed
        expect(subject.object).to be_published
        expect(subject.object.category_ids.size).to eq(2)
      end
    end
    
    context 'with invalid attributes' do
      let(:params) do
        ActionController::Parameters.new(
          title: 'New document',
          category_ids: [links.id]
        )
      end
      
      subject { Savers::DocumentsFastCreator.new(params, editor) }
      
      it 'failed creates without url' do
        expect(subject.save).to eq(false)
      end
    end
  end
end
