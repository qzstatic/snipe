require 'spec_helper'

describe Savers::ListItemsCreator do
  let(:editor)   { create(:editor) }
  let(:list)     { create(:list, duration: 1.day) }
  let(:domain)   { create(:domain) }
  let(:rubrics)  { create(:rubrics) }
  let(:document) { create(:document, category_ids: [domain.id,rubrics.id]) }
  
  let(:params) do
    ActionController::Parameters.new(
      document_id: document.id,
      top: 2
    )
  end
  
  before(:each) do
    Savers::DocumentsPublisher.new(document, {}, editor).save
  end
  
  describe '#save' do
    subject { Savers::ListItemsCreator.new(params, editor, list) }
    
    it 'creates a new list item in the specified list' do
      subject.save
      
      expect(subject.object.list).to eq(list)
      expect(subject.object.document).to eq(document)
      expect(subject.object.top).to eq(params[:top])
    end
    
    it 'updates the effective timestamp' do
      subject.save
      
      expect(subject.object.effective_timestamp).to eq(document.published_at + params[:top].days)
    end
    
    it 'return error if document not matching' do
      list.category_ids = [domain.id]
      list.blacklisted_category_ids = [rubrics.id]
      list.save
      
      subject.save
      
      expect(subject.errors).not_to be_nil
    end
    
    it 'creates a snapshot' do
      subject.save
      
      expect(subject.object.snapshots.count).to eq(1)
      expect(subject.object.snapshots.first.action).to eq('list_item.created')
    end
  end
end
