require 'spec_helper'

describe Savers::BoxesCopier do
  let(:editor) { create(:editor) }
  let(:original) { create(:box) }
  let(:root_box) { create(:box) }
  let(:document) { create(:document, root_box_id: root_box.id) }
  
  let(:params) do
    ActionController::Parameters.new(
      enabled:   true,
      parent_id: root_box.id
    )
  end
  
  describe '#save' do
    subject { Savers::BoxesCopier.new(params, editor, original) }
    
    it 'creates a copy of the provided original box' do
      subject.save
      
      expect(subject.object).to be_persisted
      expect(subject.object).to be_enabled
      expect(subject.object.parent).to eq(root_box)
      expect(subject.object.original).to eq(original)
    end
  end
end
