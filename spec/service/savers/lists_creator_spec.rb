require 'spec_helper'

describe Savers::ListsCreator do
  let(:editor) { create(:editor) }
  
  let(:params) do
    ActionController::Parameters.new(
      slug:     'top7',
      title:    'Топ 7',
      limit:    7,
      cols:     3,
      duration: 1.hour
    )
  end
  
  describe '#save' do
    subject { Savers::ListsCreator.new(params, editor) }
    
    before(:each) do
      subject.save
    end
    
    it 'creates a new list with given params' do
      expect(subject.object).to be_persisted
      expect(subject.object.slug).to eq(params[:slug])
      expect(subject.object.limit).to eq(7)
      expect(subject.object.width).to eq(3)
      expect(subject.object.duration).to eq(1.hour)
    end
    
    it 'creates a snapshot' do
      expect(subject.object.snapshots.count).to eq(1)
      expect(subject.object.snapshots.first.action).to eq('list.created')
    end
  end
end
