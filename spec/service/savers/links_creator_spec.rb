require 'spec_helper'

describe Savers::LinksCreator do
  let(:editor) { create(:editor) }
  let(:domain) { create(:domain) }
  let(:document) { create(:document, category_ids: [domain.id]) }
  let(:bound_document) { create(:document, category_ids: [domain.id]) }
  
  let(:params) do
    ActionController::Parameters.new(
      section: :reference,
      bound_document_id: bound_document.id,
      position: 5
    )
  end
  
  describe '#save' do
    subject { Savers::LinksCreator.new(params, editor, document) }
    
    it 'creates a new link with passed in params' do
      expect {
        subject.save
      }.to change { Dependency.count }.by(1)
      
      expect(subject.object).to be_persisted
      expect(subject.object.section).to eq(:reference)
      expect(subject.object.document).to eq(document)
      expect(subject.object.bound_document).to eq(bound_document)
    end
    
    it 'adds the editor to participants' do
      subject.save
      subject.object.reload
      expect(subject.object.document.participant_ids).to include(editor.id)
    end
    
    it "creates a snapshot with all document's links" do
      expect {
        subject.save
      }.to change { Snapshot.count }.by(1)
      
      expect(subject.object.snapshots.last.action).to eq('link.created')
    end
  end
end
