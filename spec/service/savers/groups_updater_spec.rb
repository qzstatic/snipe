require 'spec_helper'

describe Savers::GroupsUpdater do
  let(:params) do
    ActionController::Parameters.new(
      slug:    'guests',
      title:   'Гости',
      position: 20
    )
  end
  
  let(:editor) { create(:editor) }
  let(:group)  { create(:admins) }
  
  subject { Savers::GroupsUpdater.new(group, params, editor) }
  
  describe '#save' do
    it 'updates the group with the given params' do
      subject.save
      expect(subject.object.slug).to eq(params[:slug])
      expect(subject.object.title).to eq(params[:title])
      expect(subject.object.position).to eq(params[:position])
    end
  end
end
