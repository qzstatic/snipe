require 'spec_helper'

describe Savers::OptionsUpdater do
  let(:editor) { create(:editor) }
  before(:each) do
    params = ActionController::Parameters.new(
      type:  'source',
      title: 'Yandex',
      url:   'ya.ru'
    )
    @option = Savers::OptionsCreator.new(params, editor).saved_object
  end
  
  let(:params) do
    ActionController::Parameters.new(
      type:  'source',
      title: 'Google',
      url:   'google.com'
    )
  end
  
  subject { Savers::OptionsUpdater.new(@option, params, editor) }
  
  describe '#save' do
    it 'updates option' do
      subject.save
      expect(subject.object.title).to eq(params[:title])
      expect(subject.object.url).to   eq(params[:url])
    end
  end
end
