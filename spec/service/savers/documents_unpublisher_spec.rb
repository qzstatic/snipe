require 'spec_helper'

describe Savers::DocumentsUnpublisher do
  let(:editor) { create(:editor) }
  
  before(:each) do
    @document = Savers::DocumentsCreator.new(
      ActionController::Parameters.new(
        title: 'New document'
      ),
      editor
    ).saved_object
    
    @list = Savers::ListsCreator.new(
      ActionController::Parameters.new(
        slug:  'top7',
        title: 'Топ 7',
        limit: 7
      ),
      editor
    ).saved_object
    
    Savers::DocumentsPublisher.new(
      @document,
      ActionController::Parameters.new,
      editor
    ).save
    
    @document.reload
  end
  
  subject do
    Savers::DocumentsUnpublisher.new(
      @document,
      ActionController::Parameters.new,
      editor
    )
  end
  
  describe '#save' do
    it 'unpublishes the document' do
      subject.save
      
      expect(@document).not_to be_published
      expect(@document.milestones).not_to have_key(:published)
    end
    
    it 'adds the editor to participants' do
      expect(subject.object.participant_ids).to include(editor.id)
    end
    
    it 'updates the dependencies' do
      expect(Savers::DependenciesUpdater).to receive(:call).with(@document)
      subject.save
    end
    
    it 'removes all list items of the document' do
      expect {
        subject.save
      }.to change { ListItem.count }.by(-1)
    end
    
    it 'creates a snapshot' do
      subject.save
      
      snapshot = subject.object.snapshots.last
      expect(snapshot.action).to eq('document.unpublished')
      expect(snapshot.data[:id]).to eq(subject.object.id)
    end
    
    it 'removes the document from list cache' do
      expect {
        subject.save
      }.to change { @list.reload.cached_document_ids }.from([@document.id]).to([])
    end
  end
end
