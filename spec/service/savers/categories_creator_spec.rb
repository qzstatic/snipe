require 'spec_helper'

describe Savers::CategoriesCreator do
  let(:editor)  { create(:editor) }
  let(:domains) { create(:domains) }
  
  let(:params) do
    ActionController::Parameters.new(
      slug:      'vedomosti.ru',
      title:     'Vedomosti',
      position:  0,
      parent_id: domains.id
    )
  end
  
  subject { Savers::CategoriesCreator.new(params, editor) }
  
  describe '#save' do
    it 'creates a new category with passed in params' do
      subject.save
      
      expect(subject.object).to be_persisted
      
      expect(subject.object.slug).to eq(params[:slug])
      expect(subject.object.title).to eq(params[:title])
      expect(subject.object.position).to eq(params[:position])
      expect(subject.object.path).to eq([domains.id])
    end
    
    it 'calculates slug path' do
      subject.save
      
      expect(subject.object.slug_path).not_to be_empty
    end
    
    it 'rejects empty strings in setting_keys' do
      params[:setting_keys] = ['abc', '']
      subject.save
      
      expect(subject.object.setting_keys).to eq(%w(abc))
    end
  end
end
