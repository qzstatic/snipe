require 'spec_helper'

describe SessionsManager::RefreshToken do
  let(:editor) { create(:editor) }
  
  let(:credentials_manager) do
    credentials_request = double(
      'Credentials request',
      params: {
        editor: {
          login:    editor.login,
          password: editor.password
        }
      },
      ip: '0.0.0.0'
    )
    
    SessionsManager::Credentials.new(credentials_request).tap(&:open_session)
  end
  
  let(:valid_request) do
    double('HTTP request', params: {
      refresh_token: credentials_manager.refresh_token.token
    })
  end
  
  let(:invalid_request) do
    double('HTTP request', params: {
      refresh_token: 'wrong_token'
    })
  end
  
  describe '#authenticate!' do
    context 'with a valid token' do
      let(:manager) { SessionsManager::RefreshToken.new(valid_request) }
      
      it 'assigns the correct editor to @editor' do
        expect(manager.editor).to eq(editor)
      end
    end
    
    context 'with an invalid token' do
      let(:manager) { SessionsManager::RefreshToken.new(invalid_request) }
      
      it 'assigns nil to @editor' do
        expect(manager.editor).to be_nil
      end
    end
  end
end
