require 'spec_helper'

describe SessionsManager::Credentials do
  let(:editor) { create(:editor) }
  
  let(:valid_editor_params) do
    {
      login:    editor.login,
      password: editor.password
    }
  end
  
  let(:invalid_editor_params) do
    valid_editor_params.merge(password: 'wrong')
  end
  
  let(:valid_request) do
    double('HTTP request', params: { editor: valid_editor_params })
  end
  
  let(:invalid_request) do
    double('HTTP request', params: { editor: invalid_editor_params })
  end
  
  describe '#authenticate!' do
    context 'with a valid request' do
      let(:manager) { SessionsManager::Credentials.new(valid_request) }
      
      it 'assigns the correct editor to @editor' do
        expect(manager.editor).to eq(editor)
      end
    end
    
    context 'with an invalid request' do
      let(:manager) { SessionsManager::Credentials.new(invalid_request) }
      
      it 'assigns nil to @editor' do
        expect(manager.editor).to be_nil
      end
    end
  end
end
