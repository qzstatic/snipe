require 'spec_helper'

describe SessionsManager::AccessToken do
  let(:editor) { create(:editor) }
  
  before(:each) do
    sign_in(editor)
  end
  
  let(:valid_request) do
    double('HTTP request', headers: {
      SessionsManager::AccessToken::TOKEN_HEADER => @access_token
    })
  end
  
  let(:invalid_request) do
    double('HTTP request', headers: {
      SessionsManager::AccessToken::TOKEN_HEADER => 'wrong_token'
    })
  end
  
  describe '#authenticate!' do
    context 'with a valid request' do
      let(:manager) { SessionsManager::AccessToken.new(valid_request) }
      
      it 'assigns the correct editor to @editor' do
        expect(manager.editor).to eq(editor)
      end
    end
    
    context 'with an invalid request' do
      let(:manager) { SessionsManager::AccessToken.new(invalid_request) }
      
      it 'assigns nil to @editor' do
        expect(manager.editor).to be_nil
      end
    end
  end
end
