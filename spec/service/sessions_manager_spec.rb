require 'spec_helper'

describe SessionsManager do
  before(:each) do
    Token.redis.flushdb
    allow_any_instance_of(SessionsManager).to receive(:authenticate!)
  end
  
  let(:editor)  { create(:editor) }
  let(:request) { double('HTTP request', ip: '0.0.0.0') }
  let(:manager) { SessionsManager.new(request) }
  
  context 'with invalid request' do
    describe '#valid?' do
      it 'returns false' do
        expect(manager).not_to be_valid
      end
    end
  end
  
  context 'with valid request' do
    before(:each) do
      allow(manager).to receive(:editor).and_return(editor)
    end
    
    describe '#valid?' do
      it 'returns true' do
        expect(manager).to be_valid
      end
    end
    
    describe '#tokens_data' do
      before(:each) do
        manager.open_session
      end
      
      it 'returns the tokens data' do
        expect(manager.tokens_data).to eq(
          access_token:  manager.access_token.token,
          editor_id:     editor.id,
          expires_in:    manager.access_token.ttl,
          refresh_token: manager.refresh_token.token
        )
      end
    end
    
    describe '#open_session' do
      it 'creates tokens' do
        manager.open_session
        expect(Token.redis.keys('tokens:editors:access:*').count).to eq(1)
        expect(Token.redis.keys('tokens:editors:refresh:*').count).to eq(1)
      end
      
      it 'creates a session' do
        expect {
          manager.open_session
        }.to change { Session.count }.by(1)
      end
    end
    
    describe '#close_session' do
      before(:each) do
        manager.open_session
      end
      
      it 'deletes tokens from redis' do
        manager.close_session
        
        expect(Token.redis.keys('tokens:editors:access:*')).to be_empty
        expect(Token.redis.keys('tokens:editors:refresh:*')).to be_empty
      end
      
      it 'nullifies @access_token and @refresh_token' do
        manager.close_session
        
        expect(manager.access_token).to be_nil
        expect(manager.refresh_token).to be_nil
      end
      
      context 'without an access token in redis' do
        before(:each) do
          manager.access_token.destroy!
          manager.instance_variable_set(:@access_token, nil)
        end
        
        it 'deletes the refresh token' do
          manager.close_session
          expect(Token.redis.keys('tokens:editors:refresh:*')).to be_empty
        end
      end
    end
    
    describe '#refresh_session' do
      let(:manager) { SessionsManager.new(request) }
      
      it 'closes the current session and opens a new one' do
        expect(manager).to receive(:close_session)
        expect(manager).to receive(:open_session)
        
        manager.refresh_session
      end
    end
  end
end
