require 'spec_helper'

describe Tree do
  before(:each) do
    @rubrics    = create(:rubrics)
    @rubric     = create(:rubric,     parent: @rubrics)
    @subrubrics = create(:subrubrics, parent: @rubric)
    @subrubric  = create(:subrubric,  parent: @subrubrics)
  end
  
  subject { Tree.new(Category.all, CategorySerializer) }
  
  describe '#arrange' do
    it 'indexes the records by their parent_id' do
      expect(subject.objects).to eq(
        nil            => [@rubrics],
        @rubrics.id    => [@rubric],
        @rubric.id     => [@subrubrics],
        @subrubrics.id => [@subrubric]
      )
    end
  end
  
  describe '#as_json' do
    it "returns a sorted tree of objects' attributes" do
      tree = subject.as_json
      
      expect(tree.first[:id]).to eq(@rubrics.id)
      expect(tree.first[:children].first[:id]).to eq(@rubric.id)
      expect(tree.first[:children].first[:children].first[:id]).to eq(@subrubrics.id)
      expect(tree.first[:children].first[:children].first[:children].first[:id]).to eq(@subrubric.id)
    end
    
    context 'with a non-root tree' do
      subject { Tree.new([@subrubrics, @subrubric], CategorySerializer, @rubric.id) }
      
      it 'returns a sorted tree starting from the topmost element' do
        tree = subject.as_json
        
        expect(tree.first[:id]).to eq(@subrubrics.id)
        expect(tree.first[:children].first[:id]).to eq(@subrubric.id)
      end
    end
  end
end
