require 'spec_helper_v2'

describe Handlers::Documents::Creator do
  let(:article) do
    create_category(setting_keys: ['head.article'])
  end
  
  let(:params) do
    ActionController::Parameters.new(
      document: {
        title:        'Halo!',
        subtitle:     'Subtitle',
        category_ids: [article.id]
      }
    )
  end
  
  let(:editor) { current_editor }
  let(:now)    { Time.now }
  
  before(:each) do
    allow(Time).to receive(:now).and_return(now)
  end
  
  subject do
    Handlers::Documents::Creator.new(Document.new, params, editor: editor)
  end
  
  describe "#save" do
    before(:each) { subject.save }
    
    it 'creates a new document' do
      expect(subject.object).to be_persisted
      expect(subject.object.title).to eq(params[:document][:title])
      expect(subject.object.subtitle).to eq(params[:document][:subtitle])
      expect(subject.object.category_ids).to include(article.id)
    end
    
    it 'correctly assigns the :created milestone' do
      expect(subject.object.milestones[:created]).to eq(
        at: now.to_i,
        by: editor.id
      )
    end
  end
  
  describe '#converts_params' do
    before(:each) do
      params[:document][:category_ids] = [123]
      params[:document][:category_slugs] = [article.slug]
    end
    
    it 'converts category slugs to ids' do
      expect(subject.object.category_ids).to eq([123, article.id])
    end
  end
end
