require 'spec_helper_v2'

describe Handlers::Documents::Publisher do
  let(:document) { create_document }
  let(:now) { Time.now }
  let(:params) do
    ActionController::Parameters.new(
      publisher: {
        published_at: now
      }
    )
  end
  let(:editor) { current_editor }
  
  before(:each) do
    allow(Time).to receive(:now).and_return(now)
  end
  
  subject { Handlers::Documents::Publisher.new(document, params, {editor: editor}) }
  
  describe "#save" do
    before(:each) { subject.save }
    it 'publishes the document and sets published_at to current time' do
      expect(subject.object).to be_published
      expect(subject.object.published_at).to eq(now) 
    end
    
    it 'correctly assigns the :published milestone' do
      expect(subject.object.milestones[:published]).to eq(
        at: now.to_i,
        by: editor.id
      )
    end
  end
end
