require 'spec_helper_v2'

describe Handlers::Urls::RawCreator do
  let(:document) { create_document }
  let(:params) do
    ActionController::Parameters.new(
      url: {
        rendered: 'https://www.google.ru/'
      }
    )
  end
  
  subject { Handlers::Urls::RawCreator.new(Url.new, params, {document: document}) }
  
  describe "#save" do
    before(:each) { subject.save }
    
    it 'create a new url' do
      expect(subject.object).to be_persisted
      expect(subject.object.rendered).to eq(params[:url][:rendered])
      expect(subject.object.document).to eq(document)
    end
  end
end
