require 'spec_helper'

describe Hooks::Lists::CacheUpdate do
  let(:domains) { create(:domains) }
  let(:list)    { create(:list, limit: 5, category_ids: [domains.id]) }
  
  it 'fill document_ids after initialize' do
    doc_ids = []
    15.times do |i|
      doc_ids << list.items.create(
        document_id: create(:document, published: true, published_at: i.hour.ago, category_ids: [domains.id]).id,
        effective_timestamp: i.hour.ago
      ).document_id
    end

    hook = Hooks::Lists::CacheUpdate.new(list)
    expect(list.cached_document_ids).to eq(doc_ids[0..4])
  end
end
