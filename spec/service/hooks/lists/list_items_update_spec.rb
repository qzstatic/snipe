require 'spec_helper'

describe Hooks::Lists::ListItemsUpdate do
  let(:domains) { create(:domains) }
  let(:rubrics) { create(:rubrics) }
  let(:parts)   { create(:parts) }


  describe '#save' do
    context 'check assosiations' do
      before(:each) do
        @list = create(:list, limit: 3, category_ids: [domains.id], blacklisted_category_ids: [rubrics.id], duration: 1.hour)
        @documents = []
        5.times.each do |i|
          d = create(:document, published: true, published_at: i.hour.ago, category_ids: [parts.id, domains.id])
          Hooks::Documents::ListItems.new(d).save
          @documents << d
        end

        @list_items = []
        5.times.each do |i|
          d = create(:document, published: true, published_at: (i+5).hour.ago, category_ids: [domains.id])
          Hooks::Documents::ListItems.new(d).save
          @documents << d
          @list_items << d.list_items.first
        end
        
        5.times.each do |i|
          d = create(:document, published: true, published_at: (i+10).hour.ago, category_ids: [rubrics.id, domains.id])
          Hooks::Documents::ListItems.new(d).save
          @documents << d
        end

        @list.blacklisted_category_ids = [parts.id]
        @list.save

        Hooks::Lists::ListItemsUpdate.new(@list).save
        @list.reload
      end
      
      it 'remove list_items unmatching documents' do
        expect(@list.documents[0..4]).not_to eq(@documents[0..4])
      end

      it 'saved list_items on old matching documents' do
        expect(@list.items[0..4]).to eq(@list_items)
      end
      
      it 'create list_items on new matching documents' do
        expect(@list.documents[5..9]).to eq(@documents[10..14])
      end
    end
    
    context 'correct effective_timestamp' do
      before(:each) do
        @list = create(:list, limit: 3, duration: 0)
        3.times.each do |i|
          d = create(:document, published: true, published_at: i.hour.ago)
          Hooks::Documents::ListItems.new(d).save
          ListItem.update_all(top: 1)
        end

      end

      it 'update list_item effective_timestamp' do
        @list.update(duration: 1.hour)
        Hooks::Lists::ListItemsUpdate.new(@list).save
        @list.reload

        @list.items.each do |item|
          expect(item.effective_timestamp - item.document.published_at).to eq(1.hour)
        end
      end
    end
  end

end
