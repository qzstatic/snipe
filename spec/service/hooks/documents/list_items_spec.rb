require 'spec_helper'

describe Hooks::Documents::ListItems do
  let(:domains) { create(:domains) }
  let(:rubrics) { create(:rubrics) }
  let(:parts)   { create(:parts) }

  let(:document) { create(:document, category_ids: [domains.id], published: true, published_at: Time.now) }

  it 'create list_items for matching lists' do
    list = create(:list, category_ids: [domains.id])
    list_parts = create(:list, category_ids: [parts.id])
    
    Hooks::Documents::ListItems.new(document).save
    expect(list.reload.documents).to include(document)
    expect(list_parts.reload.documents).not_to include(document)
  end

  it 'drop list_items on unmatching lists' do
    list_domains = create(:list, slug: 'ldomains', category_ids: [domains.id])
    list_parts   = create(:list, slug: 'lparts',   category_ids: [parts.id])
    Hooks::Documents::ListItems.new(document).save

    document.update_column(:category_ids, [rubrics.id, parts.id])
    document.reload
    Hooks::Documents::ListItems.new(document).save
    document.reload

    expect(document.lists).not_to include(list_domains)
    expect(document.lists).to include(list_parts)
  end

  it 'drop list_items on exclude_list_ids' do
    list = create(:list, category_ids: [domains.id])
    document.update(exclude_list_ids: [list.id])
    Hooks::Documents::ListItems.new(document).save
    document.reload

    expect(document.lists).not_to include(list)
  end

  it 'create list_items for only published documents' do
    list = create(:list, category_ids: [domains.id])
    doc = create(:document, category_ids: [domains.id])
    Hooks::Documents::ListItems.new(doc).save

    expect(list.reload.documents).not_to include(doc)
  end

  it 'remove list_item if document unpublished' do
    list = create(:list, category_ids: [domains.id])
    Hooks::Documents::ListItems.new(document).save
    
    document.update_column(:published, false)
    document.reload

    Hooks::Documents::ListItems.new(document).save
    expect(list.reload.documents).not_to include(document)
  end

  describe 'update effective_timestamp' do
    before(:each) do
      @list = create(:list, category_ids: [domains.id], duration: 10.years)
      Hooks::Documents::ListItems.new(document).save
    end

    it 'on existing list_item' do
      document.published_at = 3.hours.ago
      document.save
      list_item = ListItem.find_by(document_id: document.id, list_id: @list.id)
      list_item.update(top: 3)

      Hooks::Documents::ListItems.new(document).save
      expect(list_item.reload.effective_timestamp).to eq(document.published_at + list_item.top * @list.duration)
    end
  end
end
