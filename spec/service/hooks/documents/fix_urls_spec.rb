require 'spec_helper_v2'

describe Hooks::Documents::FixUrls do
  let(:document) { create_document }
  let(:url) {create_url(document)}
  
  subject { Hooks::Documents::FixUrls.new(document, {}) }
  
  describe "#save" do
    before(:each) do
      create_url(document)
      subject.save 
    end
    
    it 'fixes the urls' do
      expect(subject.object.urls(true).first).to be_fixed
    end
  end
end
