require 'spec_helper_v2'

describe Hooks::Documents::UrlAppender do
  let(:document) { create_document }
  let(:url) {create_url(document)}
  
  subject { Hooks::Documents::UrlAppender.new(document, {url: url}) }
  
  describe "#save" do
    before(:each) { subject.save }
    
    it 'save url_id attr' do
      expect(subject.object.url_id).to eq(url.id)
    end
  end
end
