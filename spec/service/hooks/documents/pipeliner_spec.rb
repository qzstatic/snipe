require 'spec_helper_v2'

describe Hooks::Documents::Pipeliner do
  let(:document) { create_document }
  
  subject { Hooks::Documents::Pipeliner.new(document, {}) }
  
  describe "#save" do
    before(:each) do
      subject.save 
    end
    
    it 'process pipeline' do
      expect(subject.object.packed_document).not_to be_empty
    end
  end
end
