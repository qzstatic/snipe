require 'spec_helper'

describe Hooks::Documents::ListCache do
  let(:editor)  { create(:editor) }
  let(:domains) { create(:domains) }
  let(:parts)   { create(:parts) }
  let(:rubrics) { create(:rubrics) }

  let(:document) { create(:document, category_ids: [parts.id], published: true, published_at: Time.now) }

  before(:each) do
    @list_1 = create(:list, slug: 'list-parts',   category_ids: [parts.id])
    @list_2 = create(:list, slug: 'list-rubrics', category_ids: [rubrics.id])
    @list_3 = create(:list, slug: 'list-domains', category_ids: [domains.id], blacklisted_category_ids: [rubrics.id])

    Hooks::Documents::ListItems.new(document).save
    Hooks::Documents::ListCache.new(document).save
  end

  it 'update list cache after publish document' do
    expect(@list_1.reload.cached_document_ids).to include(document.id)
  end

  it 'update cache in unmatching list and new matching list' do
    
    document.category_ids = [rubrics.id, domains.id]
    document.save
    Hooks::Documents::ListItems.new(document).save
    Hooks::Documents::ListCache.new(document).save
    
    expect(@list_1.reload.cached_document_ids).not_to include(document.id)
    expect(@list_2.reload.cached_document_ids).to include(document.id)
    expect(@list_3.reload.cached_document_ids).not_to include(document.id)
  end
end
