require 'spec_helper_v2'

describe Hooks::Documents::AddParticipant do
  let(:document) { create_document }
  
  subject { Hooks::Documents::AddParticipant.new(document, {editor: current_editor}) }
  
  describe "#save" do
    before(:each) { subject.save }
    
    it 'adds the editor to participants' do
      expect(subject.object.participant_ids).to include(current_editor.id)
    end
  end
end
