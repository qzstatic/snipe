require 'spec_helper_v2'

describe UseCases::Documents::FastCreator do
  let(:domain) { create_category(setting_keys: %w(head.article)) }
  let(:now)    { Time.now }
  
  let(:params) do
    ActionController::Parameters.new(
      document: {
        title: 'Halo!',
        subtitle: 'Subtitle',
        category_ids: [domain.id]
      },
      url: {
        rendered: 'https://www.google.ru/'
      },
      publisher: {
        published_at: now
      }
    )
  end
  
  let(:editor) { current_editor }
  
  describe '#process' do
    subject { UseCases::Documents::FastCreator.new(params, editor) }
    
    context 'with valid params' do
      before(:each) do
        allow(Time).to receive(:now).and_return(now)
        subject.process
      end
      
      it 'process handler document creator' do
        expect(subject.document.title).to eq(params[:document][:title])
        expect(subject.document.subtitle).to eq(params[:document][:subtitle])
        expect(subject.document.category_ids).to include(domain.id)
        expect(subject.document.milestones[:created][:by]).to eq(editor.id)
      end
      
      it 'process handler url raw_creator' do
        expect(subject.url.rendered).to eq(params[:url][:rendered])
      end
      
      it 'process hook url appender' do
        expect(subject.document.url_id).to eq(subject.url.id)
      end
      
      it 'process handler document publisher' do
        expect(subject.document).to be_published
        expect(subject.document.published_at).to eq(now)
      end
      
      it 'process hook fix urls' do
        expect(subject.document.urls(true).first).to be_fixed
      end
      
      it 'process hook pipeliner' do
        expect(subject.document.packed_document).not_to be_empty
      end
      
      it 'process hook add participant' do
        expect(subject.document.participant_ids).to include(current_editor.id)
      end
    end
    
    context 'with invalid params' do
      before(:each) do
        params[:document].delete(:title)
      end
      
      it 'without document title' do
        subject.process
        
        expect(subject.errors.as_json[:document]).not_to be_empty
      end
    end
  end
end
