require 'spec_helper'

class FakeController < V1::ApplicationController
end

describe Blocker::Controller, type: :controller do
  controller(FakeController) do
    include Blocker::Controller
    blocker_methods_for { Box.find(params[:id]) }
  end

  before do
    routes.draw do
      resources :fake do
        member do
          put 'block'
          put 'unblock'
        end
      end
    end
  end

  before(:each) do
    Snipe::Redis.connection.flushdb
  end

  let(:editor_first) { create(:editor) }
  let(:editor_second) { create(:editor) }
  let(:box) { create(:box) }

  context 'blocking box' do
    it 'block with old updated_at' do
      sign_in(editor_first)
      set_access_token_header
      set_content_type_header
      put :block, id: box.id, object: :box, attributes: { updated_at: box.updated_at - 1.minute }
      expect(response).to have_http_status(412)
    end

    it 'block' do
      sign_in(editor_first)
      set_access_token_header
      set_content_type_header

      put :block, id: box.id, object: :box, attributes: { updated_at: box.updated_at }
      expect(response).to have_http_status(200)
    end
    it 'already blocked' do
      sign_in(editor_first)
      set_access_token_header
      set_content_type_header

      blocker = Blocker::Object.new(box)
      blocker.block!(editor_first)

      put :block, id: box.id, object: :box, attributes: { updated_at: box.updated_at }
      expect(response).to have_http_status(423)

      result = Oj.load(response.body)
      expect(result[:editor_id]).to eql(blocker.editor_id)
      expect(Time.zone.parse(result[:blocked_at]).to_i).to eql(blocker.blocked_at.to_i)
    end

    it 'block by second editor' do
      sign_in(editor_second)
      set_access_token_header
      set_content_type_header

      blocker = Blocker::Object.new(box)
      blocker.block!(editor_first)

      put :block, id: box.id, object: :box, attributes: { updated_at: box.updated_at }
      expect(response).to have_http_status(423)

      result = Oj.load(response.body)
      pp result
      expect(result[:editor_id]).to eql(blocker.editor_id)
      expect(Time.zone.parse(result[:blocked_at]).to_i).to eql(blocker.blocked_at.to_i)
    end
  end
  context 'blocked object' do
    it 'unblock' do
      blocker = Blocker::Object.new(box)
      blocker.block!(editor_first)

      sign_in(editor_first)
      set_access_token_header
      set_content_type_header

      put :unblock, id: box.id, object: :box
      expect(response).to have_http_status(200)
    end

    it 'already unblock' do
      sign_in(editor_first)
      set_access_token_header
      set_content_type_header
      put :unblock, id: box.id, object: :box
      expect(response).to have_http_status(200)
    end

    it 'unblock by second editor' do
      blocker = Blocker::Object.new(box)
      blocker.block!(editor_first)

      sign_in(editor_second)
      set_access_token_header
      set_content_type_header

      put :unblock, id: box.id, object: :box
      expect(response).to have_http_status(403)
    end
  end
end
