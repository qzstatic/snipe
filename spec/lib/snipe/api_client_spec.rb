require 'spec_helper'

describe Snipe::APIClient do
  let(:editor) { create(:editor) }
  
  let(:settings) do
    Hashie::Mash.new(
      editor: {
        login:    editor.login,
        password: editor.password
      },
      domains: {
        default: 'http://echo.httpkit.com',
        snipe: 'http://snipe.dev'
      }
    )
  end
  
  let(:api) { Snipe::APIClient.new(settings, :default) }
  
  it 'sends all arguments as parameters' do
    response = api.get('/', editor_id: 123)
    expect(response.body.path.params.editor_id).to eq('123')
  end
  
  context 'when authenticated' do
    let(:token) { 'some_token' }
    
    before(:each) do
      allow(api).to receive(:authenticated?).and_return(true)
      allow(api).to receive(:access_token).and_return(token)
    end
    
    it 'sends a X-Access-Token header' do
      response = api.get('/')
      expect(response.body.headers['x-access-token']).to eq(token)
    end
  end
  
  describe '#browse' do
    let(:response) do
      double('API response', body: 'hello world')
    end
    
    before(:each) do
      allow(api).to receive(:system)
    end
    
    it 'writes response body to file' do
      api.browse(response)
      expect(File.read(Snipe::APIClient::TMP_PATH)).to eq('hello world')
    end
    
    it 'opens response body in the browser' do
      expect(api).to receive(:system).with("open #{Snipe::APIClient::TMP_PATH}")
      api.browse(response)
    end
  end
end
