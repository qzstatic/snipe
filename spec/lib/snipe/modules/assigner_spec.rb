require 'spec_helper'

describe Snipe::Modules::Assigner do
  describe '.process' do
    context 'with an invalid object' do
      let(:object) { Object.new }
      
      it 'returns the object unmodified' do
        expect(subject.process(object, true)).to eq(object)
      end
    end
    
    context 'with a valid object' do
      let(:object) { Struct.new(:assigner_data).new(Hash.new) }
      
      context 'and disabled module' do
        let(:settings) { false }
        
        it 'returns the object unmodified' do
          expect(subject.process(object, settings)).to eq(object)
        end
      end
      
      context 'and enabled module' do
        let(:settings) { true }
        
        it 'adds accessors for assigned_editor_id and assignee_status' do
          porcupine = subject.process(object, settings)
          
          expect(porcupine).to respond_to(:assigned_editor_id)
          expect(porcupine).to respond_to(:assignee_status)
        end
      end
    end
  end
end
