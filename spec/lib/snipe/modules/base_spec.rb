require 'spec_helper'

describe Snipe::Modules::Base do
  let(:snipe_module) do
    Module.new do
      extend Snipe::Modules::Base
    end
  end
  
  describe '#register' do
    it 'adds the module to the registry' do
      expect(Snipe::Modules).to receive(:register).with(:head, snipe_module)
      
      snipe_module.class_eval do
        register :head
      end
    end
  end
end
