require 'spec_helper'

describe Snipe::Modules::Proofreader do
  describe '.process' do
    let(:settings) do
      { on: true }
    end
    
    context 'with an invalid object' do
      let(:object) { Object.new }
      
      it 'returns the object unmodified' do
        expect(subject.process(object, settings)).to eq(object)
      end
    end
    
    context 'with a valid object' do
      let(:object) { Struct.new(:proofreader_data).new(Hash.new) }
      
      it 'returns a corresponding porcupine' do
        porcupine = subject.process(object, settings)
        
        expect(porcupine).to respond_to(:dynamic_attributes_names)
        expect(porcupine.dynamic_attributes_names).to include(:proofread)
      end
    end
  end
end
