require 'spec_helper'

describe Snipe::Modules::Resolver do
  let(:article) { create(:article) }
  let(:document) do
    create(
      :document,
      category_ids: [article.id],
      header_data: {
        subtitle: 'Subtitle'
      }
    )
  end
  
  describe '#porcupined' do
    it 'returns a porcupined object' do
      expect(document.porcupined.subtitle).to eq('Subtitle')
    end
  end
end
