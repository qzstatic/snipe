require 'spec_helper'

describe Snipe::Modules::Boxer do
  let(:root)       { create(:box, type: :root) }
  let(:paragraph)  { create(:box, type: :paragraph) }
  let(:inset_link) { create(:box, type: :inset_link) }
  
  let(:settings) do
    {
      root: {},
      paragraph: {
        attributes: {
          kind: [:string, default: 'plain'],
          text: [:string]
        }
      },
      inset: {
        attributes: {
          title: [:string],
          text:  [:string],
          align: [:string]
        }
      },
      quote: {
        attributes: {
          text:   [:string],
          author: [:string]
        }
      },
      inset_link: {
        attributes: {
          bound_document: [:association, :document]
        }
      }
    }
  end
  
  describe '.process' do
    context 'with an invalid object' do
      let(:object) { Object.new }
      
      it 'returns the object unmodified' do
        expect(subject.process(object, settings)).to eq(object)
      end
    end
    
    context 'with a box of unknown type' do
      let(:box) { create(:box, type: :unknown) }
      
      it 'returns the object unmodified' do
        expect(subject.process(box, settings)).to eq(box)
      end
    end
    
    context 'with a valid box' do
      it 'creates accessors for fields' do
        porcupine = subject.process(paragraph, settings)
        
        expect(porcupine).to respond_to(:dynamic_attributes_names)
        expect(porcupine.dynamic_attributes_names).to eq([:kind, :text])
      end
    end
    
    context 'with box associated with documents' do
      let(:document) { create(:document) }
      
      it 'puts a dependency creation callback on document associations' do
        porcupine = subject.process(inset_link, settings)
        
        porcupine.bound_document_id = document.id
        
        expect {
          porcupine.save
        }.to change { Dependency.count }.by(1)
      end
    end
  end
end
