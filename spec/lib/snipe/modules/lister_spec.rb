require 'spec_helper'

describe Snipe::Modules::Lister do
  describe '.process' do
    let(:settings) do
      {
        attributes: {
          shark_title: :string,
          relative_url: :string,
          template: [:association, :option]
        }
      }
    end
    
    context 'with an invalid object' do
      let(:object) { Object.new }
      
      it 'returns the object unmodified' do
        expect(subject.process(object, settings)).to eq(object)
      end
    end
    
    context 'with a valid object' do
      let(:object) { Struct.new(:lister_data).new(Hash.new) }
      
      it 'returns a corresponding porcupine' do
        porcupine = subject.process(object, settings)
        
        expect(porcupine).to respond_to(:dynamic_attributes_names)
        expect(porcupine.dynamic_attributes_names).to include(:shark_title)
      end
    end
  end
end
