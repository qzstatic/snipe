require 'spec_helper'

describe Snipe::Modules::Sourcer do
  describe '.process' do
    let(:settings) do
      {
        attributes: {
          source_title: :string,
          source_url:   :string,
          source_id:   :integer
        }
      }
    end
    
    context 'with a valid object' do
      let(:object) { Struct.new(:sourcer_data).new(Hash.new) }
      
      it 'returns a corresponding porcupine' do
        porcupine = subject.process(object, settings)
        
        expect(porcupine).to respond_to(:dynamic_attributes_names)
        expect(porcupine.dynamic_attributes_names).to include(:source_title)
        expect(porcupine.dynamic_attributes_names).to include(:source_url)
        expect(porcupine.dynamic_attributes_names).to include(:source_id)
      end
    end
    
    context 'with an invalid object' do
      let(:object) { Object.new }
      
      it 'returns the object unmodified' do
        expect(subject.process(object, settings)).to eq(object)
      end
    end
  end
end
