require 'spec_helper'

describe Snipe::Modules::UrlRenderer do
  describe '.process' do
    let(:settings) do
      {
        attributes: {
          domain: :string,
          type:   :string,
          slug:   :string,
          staff:  :string,
          date:   :date
        },
        mask: 'http://:domain/:type/:date/:slug'
      }
    end
    
    context 'with an invalid object' do
      let(:object) { Object.new }
      
      it 'returns the object unmodified' do
        expect(subject.process(object, settings)).to eq(object)
      end
    end
    
    context 'with a valid object' do
      let(:object) { build(:url) }
      
      it 'returns a corresponding porcupine' do
        porcupine = subject.process(object, settings)
        
        expect(porcupine).to respond_to(:dynamic_attributes_names)
        expect(porcupine.dynamic_attributes_names).to include(:domain)
      end
      
      describe '#render' do
        context 'with valid object' do
          let(:porcupine) do
            subject.process(object, settings).tap do |porcupine|
              porcupine.domain = 'vedomosti.ru'
              porcupine.type   = 'news'
              porcupine.slug   = 'putin'
              porcupine.date   = Date.new(2014, 7, 17)
            end
          end
          
          it 'assigns the rendered string to `rendered`' do
            porcupine.render
            expect(porcupine.rendered).to eq('http://vedomosti.ru/news/2014/07/17/putin')
          end

          it 'escape rendered url' do
            porcupine.slug = 'Привет'
            porcupine.render
            expect(porcupine.rendered).to eq('http://vedomosti.ru/news/2014/07/17/%D0%9F%D1%80%D0%B8%D0%B2%D0%B5%D1%82')
          end
        end
        
        context 'with invalid object' do
          let(:porcupine) do
            subject.process(object, settings).tap do |porcupine|
              porcupine.domain = 'vedomosti.ru'
              porcupine.type   = ''
              porcupine.slug   = ''
            end
          end
          
          it 'assigns nil to `rendered`' do
            porcupine.render
            expect(porcupine.rendered).to be_nil
          end
        end
      end
    end
  end
end
