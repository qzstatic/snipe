require 'spec_helper'

describe Snipe::Modules::BoxMover do
  describe '.process' do
    context 'with an invalid object' do
      let(:object) { Object.new }
      it 'returns the object unmodified' do
        expect(subject.process(object,nil)).to eq(object)
      end
    end
    context 'with a valid object' do
      let(:object) { Struct.new(:box_mover_data).new(Hash.new) }
      it 'returns a corresponding porcupine' do
        porcupine = subject.process(object, nil)
        expect(porcupine).to respond_to(:dynamic_attributes_names)
        expect(porcupine.dynamic_attributes_names).to include(:box_move_editor_id)
      end
    end
  end
end
