require 'spec_helper'

describe Snipe::Modules::Attacher do
  describe '.process' do
    context 'with an invalid object' do
      let(:object) { Object.new }
      
      it 'returns the object unmodified' do
        expect(subject.process(object)).to eq(object)
      end
    end
    
    context 'with an object of unknown type' do
      let(:object) { Struct.new(:type, :attacher_data).new('text', Hash.new) }
      
      it 'returns a porcupine without attributes' do
        porcupine = subject.process(object)
        
        expect(porcupine).to respond_to(:dynamic_attributes_names)
        expect(porcupine.dynamic_attributes_names).to be_empty
      end
    end
    
    context 'with a valid object' do
      let(:object) { Struct.new(:type, :attacher_data).new('image', Hash.new) }
      
      it 'adds accessors for assigned_editor_id and assignee_status' do
        porcupine = subject.process(object)
        
        expect(porcupine).to respond_to(:dynamic_attributes_names)
        expect(porcupine.dynamic_attributes_names).to include(:width)
        expect(porcupine.dynamic_attributes_names).to include(:height)
      end
    end
  end
end
