require 'spec_helper'

describe Snipe::Modules do
  describe '.register' do
    it 'adds the object to the registry' do
      subject.register(:key, :value)
      expect(subject.send(:registry)[:key]).to eq(:value)
    end
  end
  
  describe '.fetch' do
    before(:each) do
      subject.register(:key, :value)
    end
    
    context 'with an existing key' do
      it 'returns the value' do
        expect(subject.fetch(:key)).to eq(:value)
      end
    end
    
    context 'with an absent key' do
      it 'returns nil' do
        expect(subject.fetch(:unknown_key)).to eq(nil)
      end
    end
  end
  
  after(:each) do
    subject.unregister(:key)
  end
end
