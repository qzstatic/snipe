require 'spec_helper'

describe Snipe::Redis do
  before(:each) do
    Snipe::Redis.disconnect!
  end
  
  describe '.connect!' do
    it 'makes a connection to redis' do
      subject.connect!
      
      expect(subject.connection).to be_a(Redis)
      expect(subject.connection.client.host).to eq(Settings.redis.host)
      expect(subject.connection.client.port).to eq(Settings.redis.port)
      expect(subject.connection.client.db).to eq(Settings.redis.db)
    end
  end
  
  describe '.disconnect!' do
    before(:each) do
      subject.connect!
    end
    
    it 'deletes the connection' do
      subject.disconnect!
      expect(subject.instance_variable_get(:@connection)).to be_nil
    end
  end
  
  describe '.connection' do
    context 'with a nil connection' do
      it 'calls .connect! and returns the connection' do
        expect(subject.connection).to be_a(Redis)
      end
    end
  end
  
  after(:each) do
    Snipe::Redis.connect!
  end
end
