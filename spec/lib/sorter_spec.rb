require 'spec_helper'

describe Sorter do
  let(:sorted_values) { %w(people companies places events) }
  
  let(:taggings) do
    [
      build(:tagging, type: 'places'),
      build(:tagging, type: 'companies'),
      build(:tagging, type: 'events'),
      build(:tagging, type: 'people')
    ]
  end
  
  subject { Sorter.new(:type, sorted_values) }
  
  describe '#sort' do
    it 'sorts objects with a given order of attribute values' do
      sorted_taggings = subject.sort(taggings)
      expect(sorted_taggings.map(&:type)).to eq(sorted_values)
    end
  end
end
