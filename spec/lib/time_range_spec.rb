require 'spec_helper'

describe TimeRange do
  describe '#range' do
    context 'without a base' do
      context 'with a positive offset' do
        subject { TimeRange.from(nil, 1.day) }
        
        it 'returns a range beginning at current time' do
          expect(subject.first).to be_within(0.05).of(Time.now)
          expect(subject.last).to  be_within(0.05).of(1.day.from_now)
        end
      end
      
      context 'with a negative offset' do
        subject { TimeRange.from(nil, -1.day) }
        
        it 'returns a range ending at current time' do
          expect(subject.first).to be_within(0.05).of(1.day.ago)
          expect(subject.last).to  be_within(0.05).of(Time.now)
        end
      end
    end
    
    context 'with a base' do
      let(:base) { 2.days.ago }
      
      context 'with a positive offset' do
        subject { TimeRange.from(base, 1.day) }
        
        it 'returns a range beginning at base' do
          expect(subject.first).to eq(base)
          expect(subject.last).to  eq(base + 1.day)
        end
      end
      
      context 'with a negative offset' do
        subject { TimeRange.from(base, -1.day) }
        
        it 'returns a range beginning at base' do
          expect(subject.first).to eq(base - 1.day)
          expect(subject.last).to  eq(base)
        end
      end
    end
  end
  
  describe '#empty?' do
    context 'with a positive offset' do
      subject { TimeRange.from(nil, 1.day) }
      
      it 'returns false' do
        expect(subject).not_to be_empty
      end
    end
    
    context 'with a negative offset' do
      subject { TimeRange.from(nil, -1.day) }
      
      it 'returns false' do
        expect(subject).not_to be_empty
      end
    end
    
    context 'with a zero offset' do
      subject { TimeRange.from(nil, 0) }
      
      it 'returns true' do
        expect(subject).to be_empty
      end
    end
  end
end
