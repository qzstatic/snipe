require 'spec_helper'

describe Porcupine do
  let(:klass) { Struct.new(:age, :names, :address) }
  let(:city_id) { 1 }
  
  let(:person) do
    klass.new(
      35,
      {
        first_name: 'John',
        last_name: 'Smith'
      },
      {
        city_id: city_id,
        street: 'Arbat',
        building: 5
      }
    )
  end
  
  let(:names_attributes) do
    {
      first_name: [:string],
      middle_name: [:string, default: 'Jr.'],
      last_name: [:string]
    }
  end
  
  let(:address_attributes) do
    {
      city: [:association, :city],
      street: [:string],
      building: [:integer]
    }
  end
  
  let(:porcupine) { Porcupine.new(person, :names, names_attributes) }
  let(:outer_porcupine) { Porcupine.new(porcupine, :address, address_attributes) }
  
  describe 'reader methods' do
    it 'return values from the meta attribute' do
      expect(porcupine.first_name).to eq('John')
      expect(outer_porcupine.last_name).to eq('Smith')
      expect(outer_porcupine.city_id).to eq(city_id)
    end
    
    it 'respect default values' do
      expect(porcupine.middle_name).to eq('Jr.')
    end
  end
  
  describe 'checker methods' do
    it 'checks if the attribute is present' do
      expect(porcupine.first_name?).to eq(true)
      expect(outer_porcupine.city_id?).to eq(true)
    end
  end
  
  describe 'writer methods' do
    it 'write the attribute' do
      porcupine.first_name = 'Zaur'
      expect(porcupine.first_name).to eq('Zaur')
      
      outer_porcupine.last_name = 'Henry'
      expect(outer_porcupine.last_name).to eq('Henry')
      outer_porcupine.street = 'Broadway'
      expect(outer_porcupine.street).to eq('Broadway')
      
      outer_porcupine.city_id = city_id
      expect(outer_porcupine.city_id).to eq(city_id)
    end
    
    it "doesn't mark attribute as changed if new value equals old value" do
      porcupine.first_name = 'John'
      expect(porcupine.send(:changed_attributes)).to be_empty
      
      porcupine.first_name = 'Zaur'
      expect(porcupine.send(:changed_attributes)).not_to be_empty
    end
  end
  
  describe '#dynamic_attributes_names' do
    it "returns all attributes' names" do
      expect(porcupine.dynamic_attributes_names).to eq(names_attributes.keys)
    end
    
    context 'with a second porcupine' do
      it 'returns attributes from both porcupines' do
        names = outer_porcupine.dynamic_attributes_names
        expect(names).to eq(names_attributes.keys + [:city_id, :street, :building])
      end
    end
  end
  
  describe '#dynamic_attributes_values' do
    it 'returns a HashWithIndifferentAccess of all dynamic attributes' do
      values = {
        first_name:  'John',
        middle_name: 'Jr.',
        last_name:   'Smith',
        city_id:     city_id,
        street:      'Arbat',
        building:    5
      }.with_indifferent_access
      
      expect(outer_porcupine.dynamic_attributes_values).to eq(values)
    end
    
    context 'with a private porcupine and false parameter' do
      let(:private_porcupine) do
        Porcupine.new(porcupine, :address, address_attributes, public_attributes: false)
      end
      
      it 'returns only public attributes' do
        values = {
          first_name:  'John',
          middle_name: 'Jr.',
          last_name:   'Smith'
        }.with_indifferent_access
        
        expect(private_porcupine.dynamic_attributes_values(false)).to eq(values)
      end
    end
  end
  
  describe '#assign_attributes' do
    let(:document) { create(:document) }
    let(:porcupine) { Porcupine.new(document, :header_data, announce: :string) }
    
    let(:raw_attributes) do
      ActionController::Parameters.new(
        title: 'New title',
        announce: 'New announce'
      )
    end
    
    let(:permitted_attributes) { raw_attributes.permit(:title, :announce) }
    
    context 'with permitted attributes' do
      it 'assigns both fixed and dynamic attributes' do
        porcupine.assign_attributes(permitted_attributes)
        
        expect(porcupine.title).to eq('New title')
        expect(porcupine.announce).to eq('New announce')
      end
    end
    
    context 'with forbidden attributes' do
      it 'raises an ActiveModel::ForbiddenAttributesError' do
        expect {
          porcupine.assign_attributes(raw_attributes)
        }.to raise_error(ActiveModel::ForbiddenAttributesError)
      end
    end
  end
  
  describe '#method_missing' do
    it "delegates all unknown methods to it's object" do
      expect(porcupine.age).to eq(person.age)
    end
  end
  
  describe '#respond_to_missing?' do
    context 'when object responds to the method' do
      it 'returns true' do
        expect(porcupine.method(:age)).to be_a(Method)
      end
    end
    
    context "when object doesn't respond to the method" do
      it 'returns false' do
        expect {
          porcupine.method(:wrong)
        }.to raise_error(NameError)
      end
    end
  end
end
