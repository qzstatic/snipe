require 'spec_helper'

describe Scrubber do
  describe '.scrub' do
    it 'returns scrubbed string' do
      expect(Scrubber.scrub('  word1  word2 ')).to eq('word1 word2')
    end
  end
end
