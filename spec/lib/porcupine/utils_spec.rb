require 'spec_helper'

describe Porcupine::Utils do
  describe '.typecast' do
    context 'with a :string type' do
      it 'typecasts the value to string' do
        expect(subject.typecast(1, :string)).to eq('1')
        expect(subject.typecast(:a, :string)).to eq('a')
        expect(subject.typecast('b', :string)).to eq('b')
      end
    end
    
    context 'with an :integer type' do
      it 'typecasts the value to number' do
        expect(subject.typecast(1, :integer)).to eq(1)
        expect(subject.typecast('1', :integer)).to eq(1)
        expect(subject.typecast('a1', :integer)).to eq(0)
      end
    end
    
    context 'with a :boolean type' do
      it 'typecasts the value to true or false' do
        expect(subject.typecast(true, :boolean)).to eq(true)
        expect(subject.typecast('true', :boolean)).to eq(true)
        expect(subject.typecast(false, :boolean)).to eq(false)
        expect(subject.typecast('false', :boolean)).to eq(false)
        expect(subject.typecast(1, :boolean)).to eq(true)
      end
    end
    
    context 'with a :date type' do
      it 'converts date object to string' do
        source = '2014/07/17'
        result = Date.new(2014, 7, 17)
        
        expect(subject.typecast(source, :date)).to eq(result)
      end
    end
  end
  
  describe '.default_for' do
    context 'for :string type' do
      it 'returns an empty string' do
        expect(subject.default_for(:string)).to eq('')
      end
    end
    
    context 'for :integer' do
      it 'returns zero' do
        expect(subject.default_for(:integer)).to eq(0)
      end
    end
    
    context 'for :boolean' do
      it 'returns false' do
        expect(subject.default_for(:boolean)).to eq(false)
      end
    end
  end
  
  describe '.parse_attributes_params' do
    let(:attributes_params) do
      {
        announce: [:string],
        top: [:integer, default: 3],
        image: [:association, :attachment],
        bound_document: [:association, :document, ignored: true]
      }
    end
    
    it 'creates Attribute instances' do
      attributes = subject.parse_attributes_params(attributes_params)
      
      expect(attributes).to eq([
        Porcupine::Attribute.new(:announce, :string),
        Porcupine::Attribute.new(:top, :integer, default: 3),
        Porcupine::Association.new(:image, :attachment),
        Porcupine::Association.new(:bound_document, :document, ignored: true)
      ])
    end
  end
end
