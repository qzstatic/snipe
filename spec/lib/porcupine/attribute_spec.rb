require 'spec_helper'

describe Porcupine::Attribute do
  let(:top)      { Porcupine::Attribute.new(:top, :integer, default: 3) }
  let(:announce) { Porcupine::Attribute.new(:announce, :string) }
  let(:twitter)  { Porcupine::Attribute.new(:twitter, 'boolean') }
  
  describe '#initialize' do
    it 'parses the attribute params' do
      expect(top.name).to eq(:top)
      expect(top.default_value).to eq(3)
    end
    
    it 'gets a default value from Utils.default_for if not specified' do
      expect(announce.default_value).to eq('')
    end
  end
  
  describe '#stored_name' do
    it 'returns the attribute name' do
      expect(top.stored_name).to eq(top.name)
    end
  end
  
  describe '#public_name' do
    it 'returns the attribute name' do
      expect(top.public_name).to eq(top.name)
    end
  end
  
  describe '#typecast_value' do
    it 'returns the value typecasted to the attribute type' do
      expect(top.typecast_value('1')).to eq(1)
      expect(announce.typecast_value(1)).to eq('1')
      
      expect(twitter.typecast_value('true')).to be_truthy
      expect(twitter.typecast_value(nil)).to be_falsy
    end
  end
end
