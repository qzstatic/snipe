require 'spec_helper'

describe Porcupine::Data do
  let(:document) { create(:document, header_data: { announce: 'Announce' }) }
  subject { Porcupine::Data.new(document, :header_data) }
  
  describe '#initialize' do
    let(:object) { Object.new }
    
    it 'fails on an incompatible object' do
      expect {
        Porcupine::Data.new(object, :some_attribute)
      }.to raise_error(ArgumentError)
    end
  end
  
  describe '#read' do
    it 'returns meta attribute data as a HashWithIndifferentAccess' do
      expect(subject.read[:announce]).to eq('Announce')
      expect(subject.read['announce']).to eq('Announce')
    end
  end
  
  describe '#write' do
    it 'correctly writes the new data to the meta attribute' do
      subject.write(announce: 'New announce')
      
      expect(subject.read[:announce]).to eq('New announce')
      expect(document.changes[:header_data].first[:announce]).to eq('Announce')
      expect(document.changes[:header_data].last[:announce]).to eq('New announce')
    end
  end
end
