require 'spec_helper'

describe Porcupine::Association do
  let(:association) { Porcupine::Association.new(:bound_document, :type) }
  
  describe '#default_value' do
    it 'returns 0' do
      expect(association.default_value).to eq(0)
    end
  end
  
  describe '#stored_name' do
    it 'returns the attribute name with `_id`' do
      expect(association.stored_name).to eq(:bound_document_id)
    end
  end
  
  describe '#public_name' do
    it 'returns the attribute name with `_id`' do
      expect(association.public_name).to eq(:bound_document_id)
    end
  end
  
  describe '#typecast_value' do
    it 'typecasts the value to integer' do
      expect(association.typecast_value('1')).to eq(1)
    end
  end
end
