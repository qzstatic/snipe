require 'spec_helper'

describe Pipeline::Boxes do
  let(:editor) { create(:editor) }
  let(:news)   { create(:news) }
  
  let(:document) do
    create(:document, category_ids: [news.id])
  end
  
  before(:each) do
    document.create_root_box
    
    @paragraph = Savers::BoxesCreator.new(
      ActionController::Parameters.new(
        type: 'paragraph',
        body: 'First level paragraph',
        text_align: 'right',
        parent_id: document.root_box.id
      ),
      editor
    ).saved_object
    
    quote_image = Savers::AttachmentsCreator.new(
      ActionController::Parameters.new(
        type:      'image',
        directory: 'some/dir'
      ),
      editor
    ).saved_object
    
    Savers::AttachmentVersionsCreator.new(
      ActionController::Parameters.new(
        slug:     'original',
        filename: 'original-123abc.jpg',
        width:    800,
        height:   600,
        size:     100.kilobytes
      ),
      editor,
      quote_image
    ).save
    
    @quote = Savers::BoxesCreator.new(
      ActionController::Parameters.new(
        type:      'quote',
        body:      'Quote body',
        name:      'Quote name',
        job:       'Quote job',
        image_id:  quote_image.id,
        parent_id: document.root_box.id
      ),
      editor
    ).saved_object
    
    Savers::BoxesCreator.new(
      ActionController::Parameters.new(
        type:       'paragraph',
        body:       'Paragraph inside quote',
        text_align: 'right',
        parent_id:  @quote.id
      ),
      editor
    ).save
  end
  
  subject { Pipeline::Boxes.new(document) }
  
  describe '#process' do
    before(:each) do
      subject.process
    end
    
    it 'updates each first-level box with packed_box' do
      expect(@paragraph.reload.packed_box).to eq(
        id:         @paragraph.id,
        type:       'paragraph',
        kind:       'plain',
        body:       'First level paragraph',
        text_align: 'right',
        created_at: '',
        scribble_post_id:     0,
        scribble_post_author: ''
      )
      
      expect(@quote.reload.packed_box).to eq(
        id:     @quote.id,
        type:   'quote',
        body:   'Quote body',
        name:   'Quote name',
        job:    'Quote job',
        image:  Pipeline::Attachment.new(@quote.image).serialize,
        created_at: '',
        children: [
          {
            id:         @quote.children.first.id,
            type:       'paragraph',
            kind:       'plain',
            body:       'Paragraph inside quote',
            text_align: 'right',
            created_at: '',
            scribble_post_id:     0,
            scribble_post_author: ''
          }
        ]
      )
    end
  end
end
