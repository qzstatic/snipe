require 'spec_helper'

describe Pipeline::Links do
  let(:editor) { create(:editor) }
  let(:domain) { create(:domain) }
  
  let(:document) do
    create(:document, category_ids: [domain.id])
  end
  
  let(:referenced_document) do
    create(:document, published: true, packed_document: { document: 'data' })
  end
  
  let(:draft_document) { create(:document) }
  
  let(:author) do
    create(:document, published: true, packed_document: { author: 'data' })
  end
  
  before(:each) do
    Savers::LinksCreator.new(
      ActionController::Parameters.new(
        section:           :reference,
        bound_document_id: referenced_document.id,
        position: 5
      ),
      editor,
      document
    ).save
    
    Savers::LinksCreator.new(
      ActionController::Parameters.new(
        section:           :reference,
        bound_document_id: draft_document.id,
        position: 10
      ),
      editor,
      document
    ).save
       
    Savers::LinksCreator.new(
      ActionController::Parameters.new(
        section:           :author,
        bound_document_id: author.id,
        position: 5
      ),
      editor,
      document
    ).save
  end
  
  subject { Pipeline::Links.new(document) }
  
  describe '#process' do
    it 'stores all links data in packed_links' do
      subject.process
      
      expect(document.packed_links).to eq(
        {
          reference: [
            {
              bound_document: { document: 'data' },
              position: 5
            }
          ],
          author: [
            {
              bound_document: { author: 'data' },
              position: 5
            }
          ]
        }
      )
    end
  end
end
