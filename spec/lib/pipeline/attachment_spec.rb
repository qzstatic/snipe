require 'spec_helper'

describe Pipeline::Attachment do
  let(:editor) { create(:editor) }
  
  before(:each) do
    @title_image = Savers::AttachmentsCreator.new(
      ActionController::Parameters.new(
        type:      'image',
        directory: 'some/dir'
      ),
      editor
    ).tap(&:save).object
    
    Savers::AttachmentVersionsCreator.new(
      ActionController::Parameters.new(
        slug:     'original',
        filename: 'original-123abc.jpg',
        width:    800,
        height:   600,
        size:     50.kilobytes
      ),
      editor,
      @title_image
    ).save
  end
  
  subject { Pipeline::Attachment.new(@title_image) }
  
  describe '#serialize' do
    it "returns full denormalized data for the attachment's versions" do
      expect(subject.serialize).to eq(
        original: {
          url:    'some/dir/original-123abc.jpg',
          width:  800,
          height: 600,
          size:   50.kilobytes
        }
      )
    end
  end
end
