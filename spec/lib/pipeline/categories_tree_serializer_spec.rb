require 'spec_helper'

describe Pipeline::CategoriesTreeSerializer do
  let(:category_class) do
    Struct.new(:id, :slug, :title, :position, :nested_categories, :path) do
      def real?
        path.count.odd?
      end
    end
  end
  
  let(:categories) do
    [
      [1,  'domains',      'Домены',       0, %w(single),          []],
      [2,  'vedomosti.ru', 'vedomosti.ru', 0, [],                  [1]],
      [3,  'parts',        'Разделы',      1, %w(required single), []],
      [4,  'news',         'Новости',      0, [],                  [3]],
      [5,  'rubrics',      'Рубрики',      0, %w(required),        []],
      [6,  'russia',       'Россия',       0, %w(single),          [5]],
      [7,  'world',        'Мир',          1, %w(single),          [5]],
      [8,  'subrubrics',   'Подрубрики',   0, %w(required),        [5, 7]],
      [9,  'politics',     'Политика',     0, [],                  [5, 7, 8]]
    ].map { |category_attributes| category_class.new(*category_attributes) }
  end
  
  subject { described_class.new(categories) }
  
  describe '#as_json' do
    it 'returns correct structure' do
      expect(subject.as_json).to eq(
        domains: {
          title: 'vedomosti.ru',
          slug: 'vedomosti.ru',
          position: 0,
        },
        parts: {
          title: 'Новости',
          slug: 'news',
          position: 0
        },
        rubrics: [
          {
            title: 'Россия',
            slug: 'russia',
            position: 0
          },
          {
            title: 'Мир',
            slug: 'world',
            position: 1,
            subrubrics: [
              {
                title: 'Политика',
                slug: 'politics',
                position: 0
              }
            ]
          }
        ]
      )
    end
    
    context 'with invalid hierarchy' do
      before(:each) do
        categories.pop
      end
      
      it 'raises an ArgumentError' do
        expect {
          subject.as_json
        }.to raise_error(ArgumentError)
      end
    end
  end
end
