require 'spec_helper'

describe Pipeline::Document do
  let(:editor) { create(:editor) }
  
  let(:domain) { create(:domain) }
  let(:column) { create(:column) }
  let(:parts)  { column.parent }
  
  let(:author) { create(:document, category_ids: [domain.id], published: true) }
  
  before(:each) do
    title_image = Savers::AttachmentsCreator.new(
      ActionController::Parameters.new(
        type:      'image',
        directory: 'some/dir'
      ),
      editor
    ).saved_object
    
    Savers::AttachmentVersionsCreator.new(
      ActionController::Parameters.new(
        slug:     'original',
        filename: 'original-123abc.jpg',
        width:    800,
        height:   600,
        size:     100.kilobytes
      ),
      editor,
      title_image
    ).save
    
    @document = Savers::DocumentsCreator.new(
      ActionController::Parameters.new(
        title:        'New document',
        subtitle:     'Some subtitle',
        category_ids: [domain, parts, column].map(&:id),
        source_title: 'Yandex',
        source_url:   'ya.ru',
        quote_body:   'Quote',
        author_id:    author.id
      ),
      editor
    ).saved_object
    
    Savers::UrlsCreator.new(
      ActionController::Parameters.new(
        domain:    'vedomosti.ru',
        rubric:    'business',
        type:      'news',
        date:      Date.today,
        pseudo_id: '123',
        slug:      'war'
      ),
      editor,
      @document
    ).save
    @document.update(url_id: Url.last.id)
    
    Savers::DocumentsPublisher.new(@document, {}, editor).save
    
    @dependent = Savers::DocumentsCreator.new(
      ActionController::Parameters.new(
        title: 'Dependent document',
        category_ids: [domain.id]
      ),
      editor
    ).saved_object
    
    link = Savers::LinksCreator.new(
      ActionController::Parameters.new(
        section: :reference,
        type: :inner,
        bound_document_id: @document.id
      ),
      editor,
      @dependent
    ).saved_object
    
    other_doc_params = ActionController::Parameters.new(bound_document_id: @document.id)
    Savers::DocumentsUpdater.new(author, other_doc_params, editor).save
    
    [@document, author].each(&:reload)
  end
  
  subject { Pipeline::Document.new(@document) }
  
  describe '#process' do
    it "stores a full hash of document's attributes in packed_document" do
      subject.process
      
      expect(@document.packed_document).to eq(
        id:           @document.id,
        title:        'New document',
        subtitle:     'Some subtitle',
        published_at: @document.published_at.strftime('%Y-%m-%dT%H:%M:%S.%L%:z'),
        url:          @document.url.rendered,
        categories:   Pipeline::CategoriesTreeSerializer.new(@document.categories).as_json,
        source_title: 'Yandex',
        source_url:   'ya.ru',
        quote_body:   'Quote',
        author:       Pipeline::Document.new(author.porcupined).serialize_as_nested.deep_symbolize_keys
      )
    end
    
    it 'updates the dependent documents' do
      expect(subject).to receive(:async_update_dependencies)
      subject.process
    end
  end
end
