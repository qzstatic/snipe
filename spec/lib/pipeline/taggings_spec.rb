require 'spec_helper'

describe Pipeline::Taggings do
  let(:editor)   { create(:editor) }
  let(:domain)   { create(:domain) }
  let(:document) { create(:document, category_ids: [domain.id]) }
  
  before(:each) do
    tags = 3.times.map do |i|
      saver = Savers::TagsCreator.new(
        ActionController::Parameters.new(
          title: "Tag#{i + 1}"
        ),
        editor
      )
      saver.saved_object
    end
    
    Savers::TaggingsCreator.new(
      ActionController::Parameters.new(
        type: 'events',
        tag_ids: [tags[0].id, tags[1].id]
      ),
      editor,
      document
    ).save
    
    Savers::TaggingsCreator.new(
      ActionController::Parameters.new(
        type: 'companies',
        tag_ids: [tags[1].id, tags[2].id]
      ),
      editor,
      document
    ).save
    
    Savers::TaggingsCreator.new(
      ActionController::Parameters.new(
        type: 'people',
        tag_ids: [tags[2].id]
      ),
      editor,
      document
    ).save
    
    allow(document).to receive(:settings).and_return(
      taggings: {
        allowed_types: %w(events people companies places)
      }
    )
  end
  
  subject { Pipeline::Taggings.new(document) }
  
  describe '#process' do
    it 'stores all taggings data in packed_taggings' do
      subject.process
      
      expect(document.packed_taggings).to eq(
        [
          {
            type: 'events',
            tags: %w(Tag1 Tag2)
          },
          {
            type: 'people',
            tags: %w(Tag3)
          },
          {
            type: 'companies',
            tags: %w(Tag2 Tag3)
          }
        ]
      )
    end
  end
end
