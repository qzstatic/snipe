require 'spec_helper'

describe Pipeline::Box do
  let(:editor)   { create(:editor) }
  let(:news)     { create(:news) }
  let(:document) { create(:document, category_ids: [news.id]).tap(&:create_root_box) }
  
  before(:each) do
    @box = Savers::BoxesCreator.new(
      ActionController::Parameters.new(
        type:       'paragraph',
        body:       'First level paragraph',
        text_align: 'right',
        parent_id:  document.root_box.id
      ),
      editor
    ).saved_object
    
    Savers::BoxesCreator.new(
      ActionController::Parameters.new(
        type:       'paragraph',
        body:       'Second level paragraph',
        text_align: 'left',
        parent_id:  @box.id
      ),
      editor
    ).saved_object
  end
  
  subject { described_class.new(@box) }
  
  describe '#serialize' do
    it 'returns a serialized form of the box' do
      expect(subject.serialize.deep_symbolize_keys).to eq(
        id:         @box.id,
        type:       'paragraph',
        kind:       'plain',
        body:       'First level paragraph',
        text_align: 'right',
        created_at: '',
        scribble_post_id:     0,
        scribble_post_author: '',
        children: [
          id:         @box.children.first.id,
          type:       'paragraph',
          kind:       'plain',
          body:       'Second level paragraph',
          text_align: 'left',
          created_at: '',
          scribble_post_id:     0,
          scribble_post_author: ''
        ]
      )
    end
  end
end
