require 'spec_helper'

describe Blocker do
  before(:each) do
    Snipe::Redis.connection.flushdb
  end

  let(:box) { create(:box, id: 123) }
  let(:editor) { create(:editor) }
  let(:editor_second) { create(:editor) }

  context :namespace do
    before :each do
      Snipe::Redis.connection.set('blockers:boxes:123', 'object')
    end
    it :namespace_for_class do
      expect(Blocker.namespace_for_class(box.class).get(Blocker.key_for(box))).to eql('object')
    end
    it :key_for do
      expect(Blocker.key_for(box)).to eql('123')
      expect(Blocker.key_for(box, :title)).to eql('123/title')
      expect(Blocker.key_for(nil, :title)).to eql('*/title')
    end
  end

  context 'block/unblock' do
    it :object do
      blocker = Blocker::Object.new(box)
      expect(blocker.block!(editor)).to be_truthy
      expect(Snipe::Redis.connection.keys).to include('blockers:boxes:123')
      expect(blocker.unblock!(editor_second)).to be_falsey
      expect(Snipe::Redis.connection.keys).to include('blockers:boxes:123')
      expect(blocker.unblock!(editor)).to be_truthy
      expect(Snipe::Redis.connection.keys).to be_empty
    end
    it :attribute do
      blocker = Blocker::Attribute.new(box, :title)
      expect(blocker.block!(editor)).to be_truthy
      expect(Snipe::Redis.connection.keys).to include('blockers:boxes:123/title')
      expect(blocker.unblock!(editor_second)).to be_falsey
      expect(Snipe::Redis.connection.keys).to include('blockers:boxes:123/title')
      expect(blocker.unblock!(editor)).to be_truthy
      expect(Snipe::Redis.connection.keys).to be_empty
    end
  end
end
