require 'spec_helper'

describe Token do
  describe '.generate' do
    it 'generates unique tokens' do
      tokens = 10.times.map { Token.generate }
      expect(tokens.uniq.count).to eq(10)
    end
  end
end
