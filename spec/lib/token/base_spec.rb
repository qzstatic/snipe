require 'spec_helper'

describe Token::Base do
  let(:subclass) do
    Class.new(Token::Base) do
      path do |var|
        "namespace:#{var}"
      end
    end
  end
  
  describe '.path' do
    it 'saves the given block in @path_proc' do
      path = subclass.path.call(123)
      expect(path).to eq('namespace:123')
    end
  end
  
  describe '.create' do
    before(:each) do
      token = '12345abc'
      allow(Token).to receive(:generate).and_return(token)
    end
    
    it 'saves the passed in value to redis' do
      subclass.create(123)
      value = Snipe::Redis.connection.get('namespace:12345abc')
      expect(value).to eq('123')
    end
    
    it 'returns a class instance' do
      token = subclass.create(123)
      expect(token).to be_a(subclass)
    end
  end
  
  describe '.find' do
    let(:token) { subclass.create(123) }
    
    context 'with an existing token' do
      it 'finds the value in redis' do
        found_token = subclass.find(token.token)
        expect(found_token).to eq(token)
      end
    end
    
    context 'with a non-existing token' do
      it 'returns nil' do
        expect(subclass.find('wrong')).to be_nil
      end
    end
  end
  
  describe '#destroy!' do
    let(:token) { subclass.create(123) }
    
    it 'deletes the key from redis' do
      expect(subclass.find(token.token)).not_to be_nil
      token.destroy!
      expect(subclass.find(token.token)).to be_nil
    end
  end
end
