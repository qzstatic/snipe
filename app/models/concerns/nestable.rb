# A mixin for objects with hierarchical structure.
#
# @note Objects using `Nestable` must have a primary key `id` and an array field `path`.
module Nestable
  extend ActiveSupport::Concern
  
  # Returns all object's descendants.
  #
  # @return [ActiveRecord::Relation] descendants
  #
  def descendants
    self.class.descendants_of(id)
  end
  
  # Returns direct children of the object.
  #
  # @return [ActiveRecord::Relation] children
  #
  def children
    descendants.by_depth(depth + 1)
  end
  
  # Returns all object's ancestors.
  #
  # @return [ActiveRecord::Relation] ancestors
  #
  def ancestors
    self.class.where(id: path)
  end
  
  # Returns the root ancestor.
  #
  # @return [ActiveRecord::Base] root ancestor
  #
  def root_ancestor
    if root?
      self
    else
      self.class.find(root_ancestor_id)
    end
  end
  
  # Returns id of the root ancestor.
  #
  # @return [Fixnum] id
  #
  def root_ancestor_id
    if root?
      id
    else
      path.first
    end
  end
  
  # Returns the object's parent.
  #
  # @return [ActiveRecord::Base] parent
  # @return [nil] if the object is root
  #
  def parent
    self.class.find(parent_id) unless root?
  end
  
  # Returns the object's depth in the hierarchy.
  #
  # @return [Fixnum] depth
  #
  def depth
    path.length + 1
  end
  
  # Returns the object parent's id.
  #
  # @return [Fixnum] parent's id
  #
  def parent_id
    path.last
  end
  
  # Checks if the object is at the root level of hierarchy (e.g. has no parent).
  #
  # @return [true] if object is at the root level
  # @return [false] otherwise
  #
  def root?
    path.empty?
  end
  
  # Assigns this object a new parent object using it's id.
  #
  # @param [Fixnum, Bignum] new_parent_id new parent's id
  #
  # @return [undefined]
  #
  def parent_id=(new_parent_id)
    self.parent = self.class.find(new_parent_id)
  end
  
  # Assigns this object a new parent object.
  #
  # @param [ActiveRecord::Base] new_parent a new parent object
  #
  # @return [undefined]
  #
  def parent=(new_parent)
    self.path = new_parent.path + [new_parent.id]
  end
  
  included do
    scope :descendants_of, -> (parent_id) do
      where('path @> ARRAY[?]::bigint[]', parent_id)
    end
    
    scope :by_depth, -> (depth) do
      where('COALESCE(array_length(path, 1), 0) = ?', depth - 1)
    end
    
    before_destroy do |object|
      children.each(&:destroy)
    end
  end
end
