# ```
# create_table :tags, force: true do |t|
#   t.string   :title,      null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
# ```

class Tag < ActiveRecord::Base
  validates :title, presence: true
  validates :title, uniqueness: true
  
  # Returns taggings which contain this tag.
  #
  # @return [<Tagging>] taggings
  #
  def taggings
    Tagging.where('tag_ids @> ARRAY[?]::bigint[]', id)
  end
  
  before_destroy do |tag|
    fail ActiveRecord::Rollback if tag.taggings.present?
  end
end
