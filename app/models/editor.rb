# ```
# create_table :editors, force: true do |t|
#   t.string   :login,                                     null: false
#   t.string   :email,                                     null: false
#   t.string   :password_digest,                           null: false
#   t.string   :first_name,                                null: false
#   t.string   :last_name,                                 null: false
#   t.boolean  :enabled,                   default: false, null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.integer  :group_ids,       limit: 8, default: [],    null: false, array: true
# end
#
# add_index :editors, [:email], name: :index_editors_on_email, unique: true, using: :btree
# add_index :editors, [:login], name: :index_editors_on_login, unique: true, using: :btree
# ```

class Editor < ActiveRecord::Base
  has_secure_password validations: false
  
  validates :login, :email, :first_name, :last_name, presence: true
  validates :password, presence: true, on: :create
  validates :login, :email, uniqueness: true
  
  # Returns editor's full name.
  #
  # @return [String] full name
  #
  def full_name
    [first_name, last_name].compact.join(' ')
  end
  
  # Returns all groups the editor belongs to.
  #
  # @return [<Group>] groups
  #
  def groups
    if group_ids.blank?
      Group.none
    else
      Group.where(id: group_ids)
    end
  end
  
  class << self
    # Authenticates an editor by login and password or by email and password.
    #
    # @param [String] login editor's login
    # @param [String] email editor's email
    # @param [String] password editor's password
    #
    # @return [Editor] if editor was found
    # @return [nil] if editor wasn't found
    #
    def authenticate(login: nil, email: nil, password: nil)
      if editor = find_editor_by(login: login, email: email)
        editor.enabled? && editor.authenticate(password)
      end
    end
    
  private
    def find_editor_by(login: nil, email: nil)
      if login
        find_by(login: login)
      elsif email
        find_by(email: email)
      end
    end
  end
end
