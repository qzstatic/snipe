# ```
# create_table :options, force: true do |t|
#   t.string   :type,                    null: false
#   t.hstore   :value,      default: {}, null: false
#   t.integer  :position,   default: 0,  null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :options, [:type], name: :index_options_on_type, using: :btree
# ```

class Option < ActiveRecord::Base
  # Dynamic settings grouped by type.
  SETTINGS = {
    source: {
      name: 'Источники',
      attributes: {
        title: :string,
        url:   :string
      }
    },
    template: {
      name: 'Шаблоны',
      attributes: {
        filename: :string
      }
    },
    mobile_menu: {
      name: 'Мобильное меню v1',
      attributes: {
        title:        :string,
        template:     :string,
        relative_url: :string
      }
    },
    mobile_menu_v2: {
      name: 'Мобильное меню v2',
      attributes: {
        title:        :string,
        template:     :string,
        relative_url: :string
      }
    }
  }.with_indifferent_access
  
  validates :type, :value, presence: true
  validates :type, inclusion: { in: SETTINGS.keys }
  
  validate  :url_must_be_unique
  
  self.inheritance_column = :sti_disabled
  
  scope :order_by_position, -> { order(:position) }
  scope :by_type, -> (type) { where(type: type) }
  
  SETTINGS.keys.each do |type|
    scope type, -> { by_type(type) }
  end
  
  class << self
    # Returns options grouped by type.
    #
    # @return [{String => ActiveRecord::Relation}]
    #
    def grouped_by_type
      SETTINGS.keys.each_with_object(Hash.new) do |type, hash|
        hash[type.to_sym] = by_type(type)
      end
    end
  end
  
  # Return associated documents
  #
  # @return [<ActiveRecord::Relation>]
  #
  def documents
    Document.where("sourcer_data @> 'source_id=>?'", id)
  end
  
  # Returns a porcupine with an option if it's type is present in settings.
  #
  # @return [Porcupine, Option]
  #
  def porcupined
    if settings = SETTINGS[type]
      Porcupine.new(self, :value, settings[:attributes])
    else
      self
    end
  end
  
  before_destroy do |option|
    if option.type == 'source' && option.documents.count > 0
      fail ActiveRecord::Rollback
    end
  end
  
private
  def url_must_be_unique
    errors.add(:value, :taken) if self.class.where("value -> 'url' = ?", value[:url]).where.not(id: id).any?
  end
end
