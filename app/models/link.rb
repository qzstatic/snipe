# ```
# create_table :links, force: true do |t|
#   t.integer  :document_id,       limit: 8,             null: false
#   t.string   :section,                                 null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.integer  :bound_document_id, limit: 8,             null: false
#   t.integer  :position,                    default: 0, null: false
# end
#
# add_index :links, [:bound_document_id], name: :index_links_on_bound_document_id, using: :btree
# add_index :links, [:document_id, :bound_document_id, :section], name: :index_links_on_document_id_and_bound_document_id_and_section, unique: true, using: :btree
# add_index :links, [:document_id], name: :index_links_on_document_id, using: :btree
# ```

class Link < ActiveRecord::Base
  belongs_to :document, foreign_key: :document_id
  belongs_to :bound_document, class_name: 'Document', foreign_key: :bound_document_id
  
  has_many :dependencies, -> { where(dependent_type: 'Link') }, foreign_key: :dependent_id, dependent: :destroy
  has_many :snapshots, as: :subject
  
  validates :document_id, :bound_document_id, :section, presence: true
  
  # Gets the settings from the parent document.
  #
  # @return [Hash] settings
  #
  def settings
    document.settings
  end
end
