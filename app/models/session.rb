# ```
# create_table :sessions, force: true do |t|
#   t.integer  :editor_id,     limit: 8, null: false
#   t.string   :access_token,            null: false
#   t.string   :refresh_token,           null: false
#   t.integer  :expires_in,              null: false
#   t.inet     :ip
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :sessions, [:access_token], name: :index_sessions_on_access_token, using: :btree
# add_index :sessions, [:editor_id], name: :index_sessions_on_editor_id, using: :btree
# ```

class Session < ActiveRecord::Base
  
  validates :editor_id, :expires_in, presence: true
  validates :access_token, :refresh_token, presence: true
  
  belongs_to :editor, foreign_key: :editor_id
end
