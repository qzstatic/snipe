# ```
# Could not dump table :documents because of following StandardError
#   Unknown type 'jsonb' for column 'packed_document'
# ```

class Document < ActiveRecord::Base
  has_many :links, -> { order(position: :asc) }, foreign_key: :document_id, dependent: :destroy
  has_many :taggings, foreign_key: :document_id, dependent: :destroy
  has_many :urls, foreign_key: :document_id, dependent: :destroy
  has_many :list_items, foreign_key: :document_id, dependent: :destroy
  has_many :lists, through: :list_items

  has_many :snapshots, as: :subject

  has_many :dependencies, foreign_key: :dependable_id, as: :dependable

  has_many :bound_links, foreign_key: :bound_document_id, class_name: 'Link'
  has_many :bound_documents, through: :bound_links, source: :document

  belongs_to :url, foreign_key: :url_id
  belongs_to :root_box, foreign_key: :root_box_id, class_name: 'Box'

  validates :title, presence: true
  validates :published, inclusion: { in: [true, false] }
  validates :root_box_id, uniqueness: true, allow_nil: true

  serialize :packed_document, HashSerializer

  include Snipe::Modules::Resolver

  scope :with_category_ids, -> (ids) do
    if ids.blank?
      all
    else
      where('category_ids @> ARRAY[?]::bigint[]', ids)
    end
  end

  scope :without_category_ids, -> (ids) do
    if ids.blank?
      all
    else
      where('NOT (category_ids && ARRAY[?]::bigint[])', ids)
    end
  end

  scope :published, -> { where(published: true) }

  paginates_per 60

  # Returns all categories the document belongs to (caching the result).
  #
  # @return [<Category>] categories
  #
  def categories
    @categories ||= if category_ids.blank?
                      Category.none
                    else
                      categories = Category.where(id: category_ids)
                      Sorter.new(:id, category_ids).sort(categories)
                    end
  end

  # Assigns a new value to `category_ids` and flushes the categories cache.
  #
  # @param new_category_ids [<Fixnum>] new category_ids
  #
  # @return [undefined]
  #
  def category_ids=(new_category_ids)
    @categories = nil
    super
    recalculate_cached_setting_keys
    set_view_part
  end

  # Returns combined settings from all document's categories.
  #
  # @return [Hash] settings
  #
  def settings
    cached_setting_keys.inject(Hash.new) do |hash, key|
      module_name, type = key.split('.')
      settings          = Settings.modules.underbang_reader(module_name).fetch(type, {})
      hash.deep_merge(module_name.to_sym => settings)
    end
  end

  # Returns settings for a module with a given synonym.
  #
  # @param module_synonym [String] synonym of the module
  #
  # @return [Hash] settings
  #
  def settings_for_module(module_synonym)
    if module_name = Settings.modules.synonyms[module_synonym]
      settings[module_name.to_sym].deep_dup
    else
      {}
    end
  end

  # Recalculates the `setting_keys` cache from document's categories.
  #
  # @return [undefined]
  #
  def recalculate_cached_setting_keys
    self.cached_setting_keys = categories.flat_map(&:setting_keys)
  end

  # Updates the `setting_keys` cache directly in the database.
  #
  # @return [undefined]
  #
  def recalculate_cached_setting_keys!
    update_column(:cached_setting_keys, categories.flat_map(&:setting_keys))
  end

  # Updates the `view.part`
  def set_view_part
    if self.part
      self.view['part'] = self.part.slug.singularize
      self.view_will_change!
    end
  end

  # Updates the `view.part` in database
  def set_view_part!
    if part = self.part
      v         = self.view
      v['part'] = part.slug.singularize
      update_column(:view, v)
    end
  end

  # Returns all snapshots of the document and it's associated objects
  # (boxes, links, taggings and urls) ordered descending by id
  # (ignoring snapshots with old data format).
  #
  # @return [<Snapshot>] snapshots
  #
  def all_snapshots
    [boxes, links, taggings, urls].inject(snapshots) do |all_snapshots, objects|
      all_snapshots + objects.includes(:snapshots).flat_map(&:snapshots)
    end.sort do |snapshot1, snapshot2|
      snapshot2.id <=> snapshot1.id
    end.select do |snapshot|
      snapshot.action =~ /^[a-z_]+\.[a-z_]+$/
    end
  end

  # Returns milestones as a hash with symbol keys.
  #
  # @return [Hash] milestones
  #
  def milestones
    super.deep_symbolize_keys
  end

  # Returns packed document data as a hash with symbol keys.
  #
  # @return [Hash] document data
  #
  def packed_document
    super.deep_symbolize_keys
  end

  # Returns packed taggings data as an array of hashes with symbol keys.
  #
  # @return [Array<Hash>] taggings data
  #
  def packed_taggings
    super.map(&:deep_symbolize_keys)
  end

  # Returns packed links data as a hash with symbol keys.
  #
  # @return [Hash] links data
  #
  def packed_links
    super.deep_symbolize_keys
  end

  # Creates a root box and links this document to it.
  #
  # @return [undefined]
  #
  def create_root_box
    super(type: :root, enabled: true)
    save
  end

  # Returns all boxes of the document (except the root box).
  #
  # @return [ActiveRecord::Relation] boxes
  #
  def boxes
    Box.descendants_of(root_box_id)
  end

  # Adds participant id (editor or group) to participants.
  #
  # @param [Fixnum] id for adding to participants
  #
  # @return [<Fixnum>] participants ids
  #
  def add_participant(id)
    self.participant_ids |= [id]
  end

  # Checks if the document is illustrated.
  #
  # @return [true] if the document is illustrated
  # @return [false] otherwise
  #
  def illustrated?
    illustrator_data['image_id'].to_i > 0
  end

  # Returns part of this document.
  #
  # @return [Category] document's part
  # @return [nil]
  # 
  def part
    Category.where(id: category_ids).where("slug_path ~ 'parts.*{1}'").first
  end

  # Returns all lists matching the list's filters.
  #
  # @return [<List>] lists
  #
  def matching_lists
    List.matching(category_ids)
  end

  # Returns all lists where document is stored in the cache.
  #
  # @return [<List>] lists
  #
  def cached_lists
    List.where('cached_document_ids @> ARRAY[?]::bigint[]', id)
  end

  # Returns bound documents for section
  #
  # @returns [ActiveRecord::Relation] documents
  #
  def bound_documents_by_section(section)
    bound_documents.where(links: { section: section })
  end

  # Check if the document is payment required
  #
  # @return [true] if the document is payment required
  # @return [false] otherwise
  #
  def locked?
    header_data['pay_required'].to_s == 'true' && !lock_expired?
  end

  # Check if payment requiring expired
  #
  # @return [true] if pay_required_expre < now()
  # @return [false] otherwise
  #
  def lock_expired?
    header_data.key?('pay_required_expire') && begin
      header_data['pay_required_expire'].blank? || Time.parse(header_data['pay_required_expire']) < Time.now
    rescue Exception
      false
    end
  end
end
