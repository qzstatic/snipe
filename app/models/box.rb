# ```
# create_table :boxes, force: true do |t|
#   t.string   :type,                                  null: false
#   t.hstore   :boxer_data,            default: {},    null: false
#   t.integer  :path,        limit: 8, default: [],    null: false, array: true
#   t.integer  :position,              default: 0,     null: false
#   t.boolean  :enabled,               default: false, null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.json     :packed_box,            default: {},    null: false
#   t.integer  :original_id, limit: 8
#   t.boolean  :deleted,               default: false, null: false
# end
#
# add_index :boxes, [:original_id], name: :index_boxes_on_original_id, using: :btree
# add_index :boxes, [:path], name: :index_boxes_on_path, using: :gin
# add_index :boxes, [:position], name: :index_boxes_on_position, using: :btree
# ```

class Box < ActiveRecord::Base
  include Nestable
  include Snipe::Modules::Resolver
  include Blocker::Model
  
  self.inheritance_column = :sti_disabled
  
  validates :type, presence: true
  
  belongs_to :original, class_name: 'Box', foreign_key: :original_id
  has_many :dependencies, -> { where(dependent_type: 'Box') }, foreign_key: :dependent_id, dependent: :destroy
  has_many :snapshots, as: :subject, dependent: :destroy
  
  scope :enabled, -> { where(enabled: true) }
  scope :deleted, -> { where(deleted: true) }
  scope :not_deleted, -> { where(deleted: false) }
  
  # Returns the settings from the associated document.
  #
  # @return [Hash] settings
  #
  def settings
    if document
      document.settings
    else
      {}
    end
  end
  
  # Returns the associated document from the root box.
  #
  # @return [Document] document if found
  # @return [nil] otherwise
  #
  def document
    @document ||= Document.find_by(root_box_id: root_ancestor_id)
  end
  
  # Assigns a new value to `document`.
  #
  # @param new_document [Document] a new document
  #
  # @return [undefined]
  #
  def document=(new_document)
    @document = new_document
  end
  
  # Returns packed box data as a hash with symbol keys.
  #
  # @return [Hash] box data
  #
  def packed_box
    super.deep_symbolize_keys
  end

  # Returns children boxes
  #
  # @return [Array] of child boxes
  #
  def children
    Box.not_deleted.where('path @> ARRAY[?]', path << id)
  end

  # Checks if this box is just a copy.
  #
  # @return [true] if box is a copy
  # @return [false] otherwise
  #
  def copy?
    original_id.present?
  end

  # Returns attachment
  #
  # @return [Attachment] of box
  def attachment
    begin
      case type.to_sym
      when :gallery_image
        has_image? && Attachment.find(boxer_data['image_id'])
      else
        nil
      end
    rescue ActiveRecord::RecordNotFound
      nil
    end
  end

  # Checks if this box has images(Attachments).
  #
  # @return [true] if box is a copy
  # @return [false] otherwise
  #
  def has_image?
    boxer_data['image_id'].to_i > 0
  end
end
