# ```
# create_table :lists, force: true do |t|
#   t.string   :slug,                                            null: false
#   t.integer  :category_ids,             limit: 8, default: [], null: false, array: true
#   t.integer  :limit,                              default: 10, null: false
#   t.string   :width
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.string   :title,                                           null: false
#   t.hstore   :lister_data,                        default: {}, null: false
#   t.integer  :cached_document_ids,      limit: 8, default: [], null: false, array: true
#   t.integer  :exclude_list_ids,         limit: 8, default: [], null: false, array: true
#   t.integer  :blacklisted_category_ids, limit: 8, default: [], null: false, array: true
#   t.integer  :duration,                           default: 0,  null: false
# end
#
# add_index :lists, [:slug], name: :index_lists_on_slug, unique: true, using: :btree
# ```

class List < ActiveRecord::Base
  include Snipe::Modules::Resolver
  
  has_many :items, -> { order(effective_timestamp: :desc) }, class_name: 'ListItem', foreign_key: :list_id, dependent: :destroy
  has_many :documents, through: :items
  
  has_many :snapshots, as: :subject
  
  validates :slug, :title, :limit, presence: true
  validates :limit, numericality: { greater_than_or_equal_to: 1 }
  validates :slug, uniqueness: true
  
  scope :by_category_ids, -> (category_ids) do
    if category_ids.empty?
      where("category_ids = '{}'")
    else
      where('category_ids = ARRAY[?]::bigint[]', category_ids)
    end
  end
  
  scope :matching, -> (category_ids) do
    if category_ids.empty?
      all
    else
      where('category_ids <@ ARRAY[?]::bigint[]', category_ids)
        .where.not('blacklisted_category_ids && ARRAY[?]::bigint[]', category_ids)
    end
  end
  
  # Returns all documents matching the list's filters.
  #
  # @return [<Document>] documents
  #
  def matching_documents
    documents = Document.with_category_ids(category_ids)
    documents = documents.without_category_ids(blacklisted_category_ids)
    documents = documents.where(published: true)
    documents = documents.where.not('exclude_list_ids && ARRAY[?]::bigint[]', id)
    documents = documents.order('published_at DESC NULLS LAST')
    documents = documents.order(id: :desc)
    
    documents
  end
  
  # Returns all documents cached for this list.
  #
  # @return [<Document>] cached documents
  #
  def cached_documents
    Document.where(id: cached_document_ids)
  end
  
  # Returns lists excluded by current list.
  #
  # @return [<List>] excluded lists
  #
  def excluded_lists
    List.where(id: exclude_list_ids)
  end
  
  # Checks if a given document matches the list's filters.
  #
  # @return [true] if the document matches
  # @return [false] otherwise
  #
  def matches?(document)
    (category_ids & document.category_ids == category_ids) &&
    (blacklisted_category_ids & document.category_ids).empty? &&
    !document.exclude_list_ids.include?(id)
  end
  
  # Returns all categories the list belongs to.
  #
  # @return [<Category>] categories
  #
  def categories
    if category_ids.blank?
      Category.none
    else
      categories = Category.where(id: category_ids)
      Sorter.new(:id, category_ids).sort(categories)
    end
  end
  
  # Returns combined settings from all list's categories.
  #
  # @return [Hash] settings
  #
  def settings
    categories.inject(Hash.new) do |hash, category|
      hash.deep_merge(category.settings)
    end.deep_symbolize_keys
  end
  
  before_destroy do |list|
    pages.each do |page|
      new_list_ids = page.list_ids.dup
      new_list_ids.delete(list.id)
      
      page.update(list_ids: new_list_ids)
    end
  end
  
  # Returns all pages which include this list.
  #
  # @return [<Page>] pages
  #
  def pages
    Page.where('list_ids @> ARRAY[?]::bigint[]', id)
  end
  
private
  class << self
    # Returns a serializer to use for lists serialization.
    #
    # @return [Class] serializer
    #
    def active_model_serializer
      'ListSerializer'.constantize
    end
  end
end
