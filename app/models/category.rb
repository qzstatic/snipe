# ```
# create_table :categories, force: true do |t|
#   t.string   :slug,                                            null: false
#   t.string   :title,                                           null: false
#   t.integer  :position,                         default: 0,    null: false
#   t.boolean  :enabled,                          default: true, null: false
#   t.integer  :path,                   limit: 8, default: [],   null: false, array: true
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.string   :setting_keys,                     default: [],   null: false, array: true
#   t.string   :nested_categories,                default: [],   null: false, array: true
#   t.integer  :forbidden_combinations, limit: 8, default: [],   null: false, array: true
#   t.ltree    :slug_path,                                       null: false
# end
#
# add_index :categories, [:path, :slug], name: :index_categories_on_path_and_slug, unique: true, using: :btree
# add_index :categories, [:slug_path], name: :index_categories_on_slug_path, unique: true, using: :btree
# ```

class Category < ActiveRecord::Base
  include Nestable
  
  self.inheritance_column = :sti_disabled
  
  validates :slug, :title, :slug_path, presence: true
  validate :slug_should_be_unique
  
  scope :by_path, -> (path) do
    if path.empty?
      where("path = '{}'")
    else
      where('path = ARRAY[?]::bigint[]', path)
    end
  end
  
  scope :pseudo, -> do
    where('mod(array_length(path, 1), 2) = 0 OR array_length(path, 1) IS NULL')
  end
  
  scope :real, -> do
    where('mod(array_length(path, 1), 2) = 1')
  end
  
  # Returns all documents in this category.
  #
  # @return [<Document>] documents
  #
  def documents
    Document.where('category_ids @> ARRAY[?]::bigint[]', id)
  end
  
  # Returns settings for this category.
  #
  # @return [Hash] settings
  #
  def settings
    setting_keys.uniq.inject(Hash.new) do |hash, key|
      module_name, type = key.split('.')
      settings = Settings.modules.underbang_reader(module_name).fetch(type, {})
      hash.merge(module_name.to_sym => settings)
    end
  end
  
  # Returns slug path as an array of slug strings.
  #
  # @return [<String>] slugs
  #
  def slug_path
    super && super.split('.')
  end
  
  # Assigns a new slug path from given slugs.
  #
  # @param [<String>] new_slug_path
  #
  # @return [undefined]
  #
  def slug_path=(new_slug_path)
    super(new_slug_path.join('.'))
  end
  
  # Assembles the slug path with ancestor slugs and category's own slug.
  #
  # @return [unknown]
  #
  def recalculate_slug_path
    ancestor_slugs = Sorter.new(:id, path).sort(ancestors).map(&:slug)
    slugs = ancestor_slugs << slug
    self.slug_path = slugs.map { |slug| slug.to_s.tr('-.', '_') }
  end
  
  # Regenerates the setting_keys cache in all category's documents.
  #
  # @return [undefined]
  #
  # @note Very long execution time, using this method in web interactions will
  #   result in a request timeout.
  def regenerate_setting_keys_cache
    documents.reverse_each do |document|
      document.recalculate_cached_setting_keys!
    end
  end
  
  # Enqueues a cache regeneration task in resque.
  #
  # @return [undefined]
  #
  def async_regenerate_setting_keys_cache
    Resque.enqueue(SettingsCacheUpdater, id)
  end
  
  class << self
    # Returns all available setting keys.
    #
    # @return [<String>] available keys
    #
    def available_setting_keys
      Settings.modules.flat_map do |module_name, module_settings|
        module_settings.each_key.map do |set_name|
          "#{module_name}.#{set_name}"
        end
      end
    end
  end
  
  # Checks if the category is pseudo.
  #
  # @return [true] if the category is pseudo
  # @return [false] otherwise
  #
  def pseudo?
    path.count.even?
  end
  
  # Checks if the category is real.
  #
  # @return [true] if the category is real
  # @return [false] otherwise
  #
  def real?
    !pseudo?
  end
  
  before_destroy do |category|
    fail ActiveRecord::Rollback if category.documents.present?
  end
  
private
  def same_categories
    categories = Category.by_path(path).where(slug: slug)
    categories = categories.where.not(id: id) if persisted?
    
    categories
  end
  
  def slug_should_be_unique
    errors.add(:slug, :taken) if same_categories.present?
  end
end
