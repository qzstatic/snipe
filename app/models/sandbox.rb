# ```
# create_table :sandboxes, force: true do |t|
#   t.string   :title,                                  null: false
#   t.integer  :category_ids,    limit: 8, default: [], null: false, array: true
#   t.boolean  :published
#   t.integer  :participant_ids, limit: 8, default: [], null: false, array: true
#   t.integer  :owner_ids,       limit: 8, default: [], null: false, array: true
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.datetime :base_date
#   t.integer  :date_offset,               default: 0,  null: false
#   t.string   :color
#   t.boolean  :proofread
# end
#
# add_index :sandboxes, [:owner_ids], name: :index_sandboxes_on_owner_ids, using: :btree
# add_index :sandboxes, [:title], name: :index_sandboxes_on_title, using: :btree
# ```

class Sandbox < ActiveRecord::Base
  validates :title, presence: true
  
  has_many :snapshots, as: :subject
  
  scope :by_owner, -> (id) do
    ids = Participants.new([id]).to_a
    where('owner_ids && ARRAY[?]::bigint[]', ids)
  end
  
  # Returns the {TimeRange} for documents filtering.
  #
  # @return [TimeRange] range
  #
  def date_range
    TimeRange.from(base_date, date_offset)
  end
  
  # Returns documents filtered by sandbox parameters.
  #
  # @return [<Document>] documents filtered by sandbox parameters
  #
  def documents
    documents = Document
    
    unless date_range.empty?
      documents = documents.where(updated_at: date_range.range)
    end
    
    unless participant_ids.empty?
      documents = documents.where('participant_ids && ARRAY[?]::bigint[]', ids)
    end
    
    unless category_ids.empty?
      documents = documents.with_category_ids(required_categories_ids)
      documents = documents.without_category_ids(blacklisted_category_ids)
    end
    
    unless published.nil?
      documents = documents.where(published: published)
    end
    
    unless proofread.nil?
      documents = documents.where("proofreader_data -> 'proofread' = ?", proofread.to_s)
    end
    
    documents
  end
  
  # Returns ids for querying all documents matching on participant_ids.
  #
  # @return [<Fixnum>] ids
  #
  def ids
    Participants.from_participant_ids(participant_ids).to_a
  end
  
  # Returns ids for required categories.
  #
  # @return [<Fixnum>] ids
  #
  def required_categories_ids
    category_ids.select { |id| id > 0 }
  end
  
  # Returns ids for blacklisted categories.
  #
  # @return [<Fixnum>] ids
  #
  def blacklisted_category_ids
    category_ids.select { |id| id < 0 }.map(&:abs)
  end
end
