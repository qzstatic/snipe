# ```
# create_table :taggings, force: true do |t|
#   t.integer  :document_id, limit: 8,              null: false
#   t.string   :type,                               null: false
#   t.integer  :tag_ids,     limit: 8, default: [], null: false, array: true
#   t.datetime :created_at
#   t.datetime :updated_at
# end
# ```

class Tagging < ActiveRecord::Base
  belongs_to :document, foreign_key: :document_id
  has_many :snapshots, as: :subject
  
  validates :document_id, :type, presence: true
  
  self.inheritance_column = :sti_disabled
  
  # Returns all tags of tagging.
  #
  # @return [<Tag>] tags
  #
  def tags
    if tag_ids.blank?
      Tag.none
    else
      tags = Tag.where(id: tag_ids)
      Sorter.new(:id, tag_ids).sort(tags)
    end
  end
end
