# ```
# create_table :snapshots, force: true do |t|
#   t.string   :action,                 null: false
#   t.json     :data,                   null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.integer  :author_id,    limit: 8, null: false
#   t.integer  :subject_id,   limit: 8, null: false
#   t.string   :subject_type,           null: false
# end
#
# add_index :snapshots, [:subject_id], name: :index_snapshots_on_subject_id, using: :btree
# ```

class Snapshot < ActiveRecord::Base
  belongs_to :subject, polymorphic: true
  belongs_to :author, class_name: 'Editor'
  
  validates :subject_id, :subject_type, :author_id, :action, :data, presence: true
  
  # Returns `data` with symbolized keys.
  #
  # @return [Hash] data
  #
  def data
    raw_data = super
    
    if raw_data.respond_to?(:deep_symbolize_keys)
      raw_data.deep_symbolize_keys
    elsif raw_data.respond_to?(:map)
      raw_data.map(&:deep_symbolize_keys)
    end
  end
  
  # Assigns new data without several service attributes.
  #
  # @param new_data [Hash] new data
  #
  # @return [undefined]
  #
  def data=(new_data)
    processed_data = new_data.reject do |key, value|
      key.to_s =~ /^(packed_[a-z]+|(crea|upda)ted_at|path|original_id)$/
    end
    
    super(processed_data)
  end
  
  # Returns a document of snapshot or a parent document of snapshot's subject.
  #
  # @return [Document, nil]
  #
  def document
    case action.split('.').first
    when 'document'
      subject
    when 'box', 'link', 'tagging', 'url'
      subject.document
    end
  end
  
private
  def porcupine(object)
    if object.respond_to?(:porcupined)
      object.porcupined
    else
      object
    end
  end
  
  class << self
    # Returns a dump of document's data at the moment of given snapshot's creation.
    #
    # @param snapshot [Snapshot] snapshot
    #
    # @return [Hash] document data
    #
    def full_document_at(snapshot)
      doc = snapshot.document
      association_types = %i(boxes links taggings urls)
      
      snapshots = { document: doc.snapshots.last_before(snapshot) }
      
      association_types.each do |assoc_type|
        snapshots[assoc_type] = doc.public_send(assoc_type).map do |object|
          object.snapshots.last_before(snapshot)
        end.compact
      end
      
      association_types.each_with_object(snapshots[:document].data) do |assoc_type, data|
        data[assoc_type] = snapshots[assoc_type].map(&:data)
      end
    end
    
    # Returns last snapshot created before the given snapshot.
    # May return the given snapshot itself.
    #
    # @param snapshot [Snapshot] a snapshot to match against
    #
    # @return [Snapshot, nil]
    #
    def last_before(snapshot)
      where('id <= ?', snapshot.id).last
    end
  end
end
