# ```
# create_table :attachments, force: true do |t|
#   t.string   :type,       null: false
#   t.string   :directory,  null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
# ```

class Attachment < ActiveRecord::Base
  has_many :versions, class_name: 'AttachmentVersion', foreign_key: :attachment_id, dependent: :destroy
  
  validates :type, :directory, presence: true
  
  self.inheritance_column = :sti_disabled
end
