# ```
# create_table :list_items, force: true do |t|
#   t.integer  :list_id,             limit: 8,             null: false
#   t.integer  :document_id,         limit: 8,             null: false
#   t.integer  :top,                           default: 0, null: false
#   t.datetime :effective_timestamp,                       null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :list_items, [:document_id], name: :index_list_items_on_document_id, using: :btree
# add_index :list_items, [:effective_timestamp], name: :index_list_items_on_effective_timestamp, order: {:effective_timestamp=>:desc}, using: :btree
# add_index :list_items, [:list_id, :document_id], name: :index_list_items_on_list_id_and_document_id, unique: true, using: :btree
# add_index :list_items, [:list_id, :effective_timestamp], name: :index_list_items_on_list_id_and_effective_timestamp, order: {:effective_timestamp=>:desc}, using: :btree
# add_index :list_items, [:list_id], name: :index_list_items_on_list_id, using: :btree
# ```

class ListItem < ActiveRecord::Base
  belongs_to :list,     foreign_key: :list_id
  belongs_to :document, foreign_key: :document_id
  
  has_many :snapshots, as: :subject
  
  validates :list_id, :document_id, :effective_timestamp, presence: true
  validates :list_id, uniqueness: { scope: :document_id }
  validate :document_valid?, on: :create
  
  before_validation do |item|
    item.effective_timestamp ||= Time.now
  end
  
  # Sets the actual effective timestamp calculating it from
  # document's `published_at` and item's `top`.
  #
  # @return [undefined]
  #
  def set_effective_timestamp
    if document.present? && document.published?
      self.effective_timestamp = document.published_at + top * list.duration
    end
  end
  
  # Calls `#set_effective_timestamp` and saves the item.
  #
  # @return [undefined]
  #
  def update_effective_timestamp
    set_effective_timestamp
    save
  end
  
private
  def document_valid?
    message = "Document doesn't match."
    errors.add(:document_id, message) unless list.matches?(document)
  end
end
