# ```
# create_table :attachment_versions, force: true do |t|
#   t.integer  :attachment_id, limit: 8,              null: false
#   t.string   :slug,                                 null: false
#   t.string   :filename,                             null: false
#   t.hstore   :attacher_data,           default: {}, null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :attachment_versions, [:attachment_id], name: :index_attachment_versions_on_attachment_id, using: :btree
# ```

class AttachmentVersion < ActiveRecord::Base
  belongs_to :attachment
  
  validates :attachment_id, :slug, :filename, presence: true
  
  # Returns the attachment type.
  #
  # @return [String] attachment type
  #
  def type
    attachment.type
  end
  
  # Returns relative url for file.
  #
  # @return [String] relative url
  #
  def url
    Pathname.new(attachment.directory).join(filename)
  end
  
  # Wraps the object in a porcupine with accessors for media information.
  #
  # @return [Porcupine] the porcupine
  #
  def porcupined
    Snipe::Modules::Attacher.process(self)
  end
end
