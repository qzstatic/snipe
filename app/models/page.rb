# ```
# create_table :pages, force: true do |t|
#   t.string   :title,                                           null: false
#   t.integer  :list_ids,                 limit: 8, default: [], null: false, array: true
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.integer  :position,                           default: 0,  null: false
#   t.string   :slug,                                            null: false
#   t.integer  :category_ids,             limit: 8, default: [], null: false, array: true
#   t.integer  :blacklisted_category_ids, limit: 8, default: [], null: false, array: true
# end
#
# add_index :pages, [:slug], name: :index_pages_on_slug, unique: true, using: :btree
# add_index :pages, [:title], name: :index_pages_on_title, unique: true, using: :btree
# ```

class Page < ActiveRecord::Base
  has_many :snapshots, as: :subject
  
  validates :title, :slug, presence: true, uniqueness: true
  
  # Returns all lists of this page.
  #
  # @return [<List>] lists
  #
  def lists
    List.where(id: list_ids)
  end

  # Returns all documents matching the page's filters.
  #
  # @return [<Document>] documents
  #
  def matching_documents
    documents = Document.with_category_ids(category_ids)
    documents = documents.without_category_ids(blacklisted_category_ids)
    documents = documents.where(published: true)
    documents = documents.order('published_at DESC NULLS LAST')
    
    documents
  end
end
