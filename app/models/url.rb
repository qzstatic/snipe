# ```
# create_table :urls, force: true do |t|
#   t.integer  :document_id, limit: 8,                 null: false
#   t.hstore   :url_data,              default: {},    null: false
#   t.string   :rendered,                              null: false
#   t.datetime :created_at
#   t.datetime :updated_at
#   t.boolean  :fixed,                 default: false
# end
#
# add_index :urls, [:document_id], name: :index_urls_on_document_id, using: :btree
# add_index :urls, [:rendered], name: :index_urls_on_rendered, unique: true, using: :btree
# ```

class Url < ActiveRecord::Base
  include Snipe::Modules::Resolver
  
  belongs_to :document, foreign_key: :document_id
  has_many :snapshots, as: :subject
  
  validates :document_id, :rendered, presence: true
  validates :rendered, uniqueness: true
  
  # Gets the settings from the parent document.
  #
  # @return [Hash] settings
  #
  def settings
    document.settings
  end
  
  # Fixes URL as public.
  #
  # @return [undefined]
  #
  def fix
    self.fixed = true
  end
end
