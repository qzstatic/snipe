# ```
# create_table :dependencies, force: true do |t|
#   t.integer  :dependable_id,   limit: 8, null: false
#   t.string   :dependable_type,           null: false
#   t.integer  :dependent_id,    limit: 8, null: false
#   t.string   :dependent_type,            null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
#
# add_index :dependencies, [:dependable_id, :dependable_type, :dependent_id, :dependent_type], name: :by_dependable, unique: true, using: :btree
# ```

class Dependency < ActiveRecord::Base
  belongs_to :dependable, foreign_key: :dependable_id, polymorphic: true
  belongs_to :dependent,  foreign_key: :dependent_id,  polymorphic: true
  
  validates :dependable_id, uniqueness: {
    scope: %w(dependable_type dependent_id dependent_type)
  }
end
