# ```
# create_table :groups, force: true do |t|
#   t.string   :title,                  null: false
#   t.string   :slug,                   null: false
#   t.integer  :position,   default: 0, null: false
#   t.datetime :created_at
#   t.datetime :updated_at
# end
# ```

class Group < ActiveRecord::Base
  validates :title, :slug, presence: true, uniqueness: true
  
  # Returns all editors in this group.
  #
  # @return [<Editor>] editors
  #
  def editors
    Editor.where('group_ids @> ARRAY[?::bigint]', id)
  end
  
  before_destroy do |group|
    fail ActiveRecord::Rollback if group.editors.present?
  end
end
