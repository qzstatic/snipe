# Query object for documents search.
class DocumentsSearchQuery
  # Returns the source relation.
  #
  # @return [ActiveRecord::Relation]
  #
  attr_reader :relation
  
  # Documents search result count.
  RESULT_COUNT = 20
  
  # Initializes the query object with a given relation.
  #
  # @param [ActiveRecord::Relation] relation source relation of Document.
  #
  def initialize(relation = Document.all)
    @relation = relation
  end
  
  # Returns all documents matching the given search string.
  #
  # @param [String] query the search query
  # @param [String] category_slugs comma separated category_slugs
  # @param [String] category_ids comma separated required and blacklisted categories' ids
  #
  # @return [<Document>] matching documents
  #
  def search(query, category_slugs: '', category_ids: '')
    slugs = category_slugs.split(',')
    ids = category_ids.split(',').map(&:to_i)
    
    result = filter_by_title(relation, query)
    result = filter_by_category_slugs(result, slugs)
    result = filter_by_category_ids(result, ids)
    result.limit(RESULT_COUNT)
  end
  
  # Returns ids of documents created after specified timestamp.
  #
  # @param [Fixnum] timestamp time after which the documents were created
  #
  # @return [<Fixnum>] ids of matching documents
  #
  def fresh(timestamp = 1.hour.ago.to_i)
    relation.where('updated_at > ?', Time.at(timestamp.to_i)).pluck(:id)
  end
  
private
  def filter_by_title(relation, title)
    relation.where('title ILIKE ?', "%#{Scrubber.scrub(title)}%")
  end
  
  def filter_by_category_slugs(relation, slugs)
    relation.with_category_ids(category_ids_by_slugs(slugs))
  end
  
  def filter_by_category_ids(relation, ids)
    required    = ids.select { |id| id > 0 }
    blacklisted = ids.select { |id| id < 0 }.map(&:abs)
    
    if ids.empty?
      relation
    else
      relation.with_category_ids(required).without_category_ids(blacklisted)
    end
  end
  
  def category_ids_by_slugs(slugs)
    Category.where(slug: slugs).pluck(:id)
  end
end
