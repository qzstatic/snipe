# Query object for tags search.
class TagsSearchQuery
  # Returns the source relation.
  #
  # @return [ActiveRecord::Relation]
  #
  attr_reader :relation
  
  # Initializes the query object with a given relation.
  #
  # @param [ActiveRecord::Relation] relation source relation of Tag.
  #
  def initialize(relation = Tag.all)
    @relation = relation
  end
  
  # Returns all tags matching the given search string.
  #
  # @param [String] search_string the search string
  #
  # @return [<Tag>] matching tags
  #
  def search(search_string)
    relation.where('title ILIKE ?', "%#{Scrubber.scrub(search_string)}%")
  end
  
  # Tries to find a tag with the given title.
  #
  # @param [String] title tag title
  #
  # @return [Tag] found tag
  #
  # @raise [ActiveRecord::RecordNotFound] if there are no tags with a given title
  #
  def find_by_title(title)
    relation.find_by!(title: Scrubber.scrub(title))
  end
end
