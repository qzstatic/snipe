# Query object for getting documents published ad a specified period.
class DocumentsByPeriodQuery
  attr_reader :relation
  private :relation
  
  # Initializes the query object with a given relation.
  #
  # @param relation [ActiveRecord::Relation] source relation of Document.
  #
  def initialize(relation = Document.all)
    @relation = relation
  end
  
  # Returns all documents published on a given period.
  #
  # @param period [Range<Date>] the period
  #
  # @return [<Document>] matching documents
  #
  def search(period)
    start_time = period.first.to_time
    end_time   = period.last.to_time + 1.day
    
    relation
      .where(published_at: start_time...end_time)
      .with_category_ids(included_categories.pluck(:id))
      .without_category_ids(excluded_categories.pluck(:id))
      .includes(:url)
  end
  
private
  def included_categories
    Category.where(slug_path: Settings.documents_by_period_categories.has)
  end
  
  def excluded_categories
    Category.where(slug_path: Settings.documents_by_period_categories.has_not)
  end
end
