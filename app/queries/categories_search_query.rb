# A query object for searching categories by type and parents.
class CategoriesSearchQuery
  # Returns all matching categories.
  #
  # @param [String] parent_slug category's parent slug
  #
  def search(parent_slug)
    parent = Category.pseudo.find_by(slug: parent_slug)
    parent.children
  end
end
