# Query object for urls search.
class UrlsSearchQuery
  # Returns the source relation.
  #
  # @return [ActiveRecord::Relation]
  #
  attr_reader :relation
  
  # Initializes the query object with a given relation.
  #
  # @param [ActiveRecord::Relation] relation source relation of Url.
  #
  def initialize(relation = Url.all)
    @relation = relation
  end
  
  # Tries to find a Url with the given rendered url.
  #
  # @param [String] rendered url string
  #
  # @return [Url] found url
  #
  # @raise [ActiveRecord::RecordNotFound] if there are no Urls with a given url
  #
  def find_by_url(rendered)
    relation.find_by!(rendered: Scrubber.scrub(rendered))
  end
end
