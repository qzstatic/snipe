# A controller for documents management.
class V2::DocumentsController < V2::ApplicationController
  # Lists last 100 documents.
  #
  # `GET /v2/documents`
  #
  def index
    respond_with Document.last(100), each_serializer: V2::Serializers::Documents::Listed
  end

  # Creates and publish a new document with url.
  #
  # `POST /v2/documents/fast`
  #
  def fast
    if fast_creator.process
      respond_with :v1, fast_creator.document, serializer: V2::Serializers::Documents::Listed
    else
      render json: fast_creator.errors, status: :unprocessable_entity
    end
  end
  
private
  def fast_creator
    @fast_creator ||= UseCases::Documents::FastCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
end
