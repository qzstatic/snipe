# Base controller for api version 2.
class V2::ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include ActionController::ImplicitRender
  
  respond_to :json
  
  # Reads and writes the sessions manager.
  #
  # @return [SessionsManager] sessions manager
  #
  attr_reader :sessions_manager
  
  before_action :authorize!
  
private
  def attributes
    params.require(:attributes)
  end
  
  def authorize!
    @sessions_manager = SessionsManager::AccessToken.new(request)
    head :unauthorized unless @sessions_manager.valid?
  end
end
