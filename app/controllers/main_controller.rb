# A controller for load balancer.
class MainController < ActionController::Base
  # Returns :ok
  #
  def check_alive
    head :ok
  end
end
