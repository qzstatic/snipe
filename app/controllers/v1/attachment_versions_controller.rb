# A controller for attachment versions management.
class V1::AttachmentVersionsController < V1::ApplicationController
  before_action :find_attachment, only: %i(index create)
  before_action :find_version, only: %i(show update destroy)
  
  # Shows all versions of the requested attachment.
  #
  # `GET /v1/attachments/:attachment_id/versions`
  #
  def index
    respond_with @attachment.versions.map(&:porcupined)
  end
  
  # Shows a particular version.
  #
  # `GET /v1/versions/:id`
  #
  def show
    respond_with @version
  end
  
  # Creates a new version with the given parameters.
  #
  # `POST /v1/attachments/:attachment_id/versions`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Updates an existing version with the given parameters.
  #
  # `PATCH /v1/versions/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys a version.
  #
  # `DELETE /v1/versions/:id`
  #
  def destroy
    @version.destroy
    head :ok
  end
  
private
  def find_attachment
    @attachment ||= Attachment.find(params[:attachment_id])
  end
  
  def find_version
    @version ||= AttachmentVersion.find(params[:id]).porcupined
  end
  
  def creator
    @creator ||= Savers::AttachmentVersionsCreator.new(
      attributes,
      sessions_manager.editor,
      @attachment
    )
  end
  
  def updater
    @updater ||= Savers::AttachmentVersionsUpdater.new(
      @version,
      attributes,
      sessions_manager.editor
    )
  end
end
