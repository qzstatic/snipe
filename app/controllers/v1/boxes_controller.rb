# A controller for boxes management.
class V1::BoxesController < V1::ApplicationController
  include Blocker::Controller
  blocker_methods_for { Box.find(params[:id]) }
  before_action -> { verify_access_to_categories(box.document.category_ids) }, only: %i(update copy destroy move restore)

  # Move single box inside document.
  #
  # `POST /v1/boxes/:id/move`
  #
  def move
    status = box_mover.save ? :ok : :not_acceptable
    render json: { moved_at: box_mover.object.document.updated_at }, status: status
  end

  # Move some boxes inside document.
  #
  # `POST /v1/boxes/multi_move`
  #
  def multi_move
    moved_at = Document.transaction do
      raise ActiveRecord::Rollback if box_movers.select{ |mover| !mover.save }.any?
      box_movers.last.object.document.updated_at
    end
    render json: { moved_at: moved_at }, status: moved_at ? :ok : :not_acceptable
  end

  # Lists all blocked document's boxes.
  #
  # `GET /v1/documents/:document_id/blocked_boxes`
  #
  def blocked
    result = Box.by_blocked_models.where(id: boxes.map(&:id)).map{|record| Blocker::Object.new(record) }
    result = result.map{|record| {id: record.object.id, editor_id: record.editor_id, blocked_at: record.blocked_at} }
    render json: result
  end

  # Lists all document's boxes as a tree.
  #
  # `GET /v1/documents/:document_id/boxes`
  #
  def index
    respond_with Tree.new(boxes, BoxSerializer)
  end
  
  # Shows a particular box.
  #
  # `GET /v1/boxes/:id`
  #
  def show
    respond_with box_proxy, serializer: BoxFullSerializer
  end
  
  # Creates a new box with the given parameters.
  #
  # `POST /v1/boxes`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Creates a new boxes with the given parameters.
  #
  # `POST /v1/boxes/multi`
  #
  def multi
    response = creators.map do |creator|
      if creator.save
        BoxSerializer.new(creator.object).as_json
      else
        creator.errors
      end
    end
    
    render json: response
  end
  
  # Updates an existing box with the given parameters.
  #
  # `PATCH /v1/boxes/:id`
  #
  def update
    if updater.stale_request?
      updater.object.reload
      snapshot = updater.object.snapshots.order(created_at: :desc).first
      render(
        json: {
          box:      BoxSerializer.new(updater.object),
          snapshot: SnapshotSerializer.new(snapshot)
        },
        status: :precondition_failed
      )
    elsif updater.save
      updater.object.reload
      render json: updater.object, serializer: BoxSerializer
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Makes a copy of an existing box with the given parameters.
  #
  # `POST /v1/boxes/:id/copy`
  #
  def copy
    if copier.save
      respond_with :v1, copier.object
    else
      render json: copier.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys a box.
  #
  # `DELETE /v1/boxes/:id`
  #
  def destroy
    if destroyer.save
      respond_with :v1, destroyer.object
    else
      render json: destroyer.errors, status: :unprocessable_entity
    end
  end

  # Restores deleted box.
  #
  # `PATCH /v1/boxes/:id/restore`
  def restore
    if restorer.save
      respond_with :v1, restorer.object
    else
      render json: restorer.errors, status: :unprocessable_entity
    end
  end
  
private
  def document
    @document ||= Document.find(params[:document_id])
  end
  
  def boxes
    if document.root_box.present?
      BoxProxy.with_descendants(document.root_box, deleted)
    else
      []
    end
  end
  
  def box_proxy
    BoxProxy.from(box)
  end
  
  def box
    Box.find(params[:id]).porcupined
  end
  
  def creator
    @creator ||= Savers::BoxesCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
  
  def updater
    @updater ||= Savers::BoxesUpdater.new(
      box,
      attributes,
      sessions_manager.editor,
      updated_at: attributes[:updated_at]
    )
  end
  
  def copier
    @copier ||= Savers::BoxesCopier.new(
      attributes,
      sessions_manager.editor,
      box
    )
  end

  def destroyer
    @destroyer ||= Savers::BoxesDestroyer.new(
        box,
        sessions_manager.editor
    )
  end

  def restorer
    @restorer ||= Savers::BoxesRestorer.new(
        box,
        sessions_manager.editor
    )
  end

  def box_mover
    @box_mover ||= Savers::BoxesMover.new(box, attributes.require(:position), sessions_manager.editor, attributes[:moved_at])
  end

  def box_movers
    return @box_movers if defined? @box_movers
    boxes_params = attributes.require(:boxes)
    boxes = Box.find(boxes_params.map{|params| params.require(:id)}).group_by(&:id)
    @box_movers ||= boxes_params.map do |box_mover_params|
      Savers::BoxesMover.new(boxes[box_mover_params.require(:id).to_i].first, box_mover_params.require(:position), sessions_manager.editor, attributes[:moved_at])
    end
  end

  def creators
    attributes.map do |box_params|
      Savers::BoxesCreator.new(box_params, sessions_manager.editor)
    end
  end

  def deleted
    params.fetch(:deleted, false)
  end
end
