# A controller for attachments management.
class V1::AttachmentsController < V1::ApplicationController
  before_action :find_attachment, only: %i(show update destroy)
  
  # Shows a particular attachment.
  #
  # `GET /v1/attachments/:id`
  #
  def show
    respond_with @attachment
  end
  
  # Creates a new attachment with the given parameters.
  #
  # `POST /v1/attachments`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Updates an existing attachment with the given parameters.
  #
  # `PATCH /v1/attachments/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys an attachment.
  #
  # `DELETE /v1/attachments/:id`
  #
  def destroy
    @attachment.destroy
    head :ok
  end
  
private
  def find_attachment
    @attachment = Attachment.find(params[:id])
  end
  
  def creator
    @creator ||= Savers::AttachmentsMultiCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
  
  def updater
    @updater ||= Savers::AttachmentsMultiUpdater.new(
      @attachment,
      attributes,
      sessions_manager.editor
    )
  end
end
