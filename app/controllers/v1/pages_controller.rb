# A controller for pages management.
class V1::PagesController < V1::ApplicationController
  before_action :require_admin, only: %i(destroy)
  before_action -> { requires_roles(:administrators, :web_editors) }, only: %i(create)

  # Lists pages.
  #
  # `GET /v1/pages`
  #
  def index
    respond_with pages
  end
  
  # Shows a particular page.
  #
  # `GET /v1/pages/:id`
  #
  def show
    respond_with page
  end
  
  # Pages last documents filtered by the specified list.
  #
  # `GET /v1/pages/:id/last_documents`
  #
  def last_documents
    respond_with page.matching_documents.includes(:url).limit(30), each_serializer: DocumentListItemSerializer
  end
  
  # Creates a new page with the given parameters.
  #
  # `POST /v1/pages`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Updates an existing page with the given parameters.
  #
  # `PATCH /v1/pages/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys a page.
  #
  # `DELETE /v1/pages/:id`
  #
  def destroy
    page.destroy
    head :ok
  end
  
private
  def pages
    Page.order(:position)
  end
  
  def page
    Page.find(params[:id])
  end
  
  def creator
    @creator ||= Savers::PagesCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
  
  def updater
    @updater ||= Savers::PagesUpdater.new(
      page,
      attributes,
      sessions_manager.editor
    )
  end
end
