# A controller for taggings management.
class V1::TaggingsController < V1::ApplicationController
  before_action :find_document, only: %i(index create)
  before_action :find_tagging, except: %i(index create)
  
  # Lists document's taggings.
  #
  # `GET /v1/documents/:document_id/taggings`
  #
  def index
    head :bad_request and return unless @document.settings[:taggings]
    
    sorter = Sorter.new(:type, @document.settings[:taggings][:allowed_types])
    respond_with sorter.sort(@document.taggings)
  end
  
  # Shows a particular tagging.
  #
  # `GET /v1/taggings/:id`
  #
  def show
    respond_with @tagging
  end
  
  # Creates a new tagging with the given parameters for document.
  #
  # `POST /v1/documents/:document_id/taggings`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Updates an existing tagging with the given parameters.
  #
  # `PATCH /v1/taggings/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys a tagging.
  #
  # `DELETE /v1/taggings/:id`
  #
  def destroy
    @tagging.destroy
    head :ok
  end
  
private
  def find_document
    @document = Document.find(params[:document_id])
  end
  
  def find_tagging
    @tagging = Tagging.find(params[:id])
  end
  
  def creator
    @creator ||= Savers::TaggingsCreator.new(
      attributes,
      sessions_manager.editor,
      @document
    )
  end
  
  def updater
    @updater ||= Savers::TaggingsUpdater.new(
      @tagging,
      attributes,
      sessions_manager.editor
    )
  end
end
