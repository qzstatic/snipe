# A controller for partial document updates.
class V1::DocumentPartsController < V1::ApplicationController
  before_action -> { verify_access_to_categories(document.category_ids) }, only: %i(update update_title)

  # Available document parts.
  PARTS = [
    :header_data,
    :trigger_data,
    :assigner_data,
    :illustrator_data,
    :sourcer_data,
    :proofreader_data
  ].freeze
  
  # Renders the requested document part.
  #
  # `GET /v1/documents/:document_id/:part`
  #
  def show
    if PARTS.include?(params[:part].to_sym)
      respond_with DocumentPartSerializer.new(document, params[:part])
    else
      head :not_found
    end
  end
  
  # Renders the document's title.
  #
  # `GET /v1/documents/:document_id/title`
  #
  def show_title
    respond_with DocumentTitleSerializer.new(document)
  end
  
  # Updates the requested document part.
  #
  # `PATCH /v1/documents/:document_id/:part`
  #
  def update
    if !PARTS.include?(params[:part].to_sym)
      head :not_found
    elsif updater.stale_request?
      head :precondition_failed
    elsif updater.save
      render json: BaseDocumentPartSerializer.new(updater.object, params[:part])
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Updates the document's title.
  #
  # `PATCH /v1/documents/:document_id/title`
  #
  def update_title
    if title_updater.save
      head :ok
    else
      render json: title_updater.errors, status: :unprocessable_entity
    end
  end
  
private
  def document
    @document ||= Document.find(params[:document_id])
  end
  
  def updater
    @updater ||= Savers::DocumentPartsUpdater.new(
      document,
      params[:data],
      sessions_manager.editor,
      part: params[:part],
      meta: params.fetch(:meta, {})
    )
  end
  
  def title_updater
    @title_updater ||= Savers::DocumentsUpdater.new(
      document,
      params[:data].slice(:title),
      sessions_manager.editor
    )
  end
end
