# A controller for tags management.
class V1::TagsController < V1::ApplicationController
  before_action :find_tagging, only: %i(index)
  before_action :find_tag, only: %i(show update destroy)
  
  # Lists tags of requested tagging.
  #
  # `GET /v1/taggings/:tagging_id/tags`
  #
  def index
    respond_with @tagging.tags
  end
  
  # Shows a particular tag.
  #
  # `GET /v1/tags/:id`
  #
  def show
    respond_with @tag
  end
  
  # Creates a new tag with the given parameters.
  #
  # `POST /v1/tags`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Searches for tags with titles matching the given query.
  #
  # `GET /v1/tags/search`
  #
  def search
    respond_with TagsSearchQuery.new(tags).search(params.require(:search_string))
  end
  
  # Tries to find a tag by the given title.
  #
  # `GET /v1/tags/find_by_title`
  #
  def find_by_title
    respond_with TagsSearchQuery.new(tags).find_by_title(params.require(:title))
  end
  
  # Updates an existing tag with the given parameters.
  #
  # `PATCH /v1/tags/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys a tag.
  #
  # `DELETE /v1/tags/:id`
  #
  def destroy
    @tag.destroy
    head :ok
  end
  
private
  def find_tagging
    @tagging = Tagging.find(params[:tagging_id])
  end
  
  def find_tag
    @tag = Tag.find(params[:id])
  end
  
  def creator
    @creator ||= Savers::TagsCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
  
  def updater
    @updater ||= Savers::TagsUpdater.new(
      @tag,
      attributes,
      sessions_manager.editor
    )
  end
  
  def tags
    Tag.all
  end
end
