# A controller for sandboxes management.
class V1::SandboxesController < V1::ApplicationController
  before_action :find_sandbox, only: %i(show update destroy)
  
  # Lists all sandboxes.
  #
  # `GET /v1/sandboxes`
  #
  def index
    respond_with Sandbox.by_owner(sessions_manager.editor.id).order(:title)
  end
  
  # Shows a particular sandbox.
  #
  # `GET /v1/sandboxes/:id`
  #
  def show
    respond_with @sandbox
  end
  
  # Creates a new sandbox with the given parameters.
  #
  # `POST /v1/sandboxes`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Updates an existing sandbox with the given parameters.
  #
  # `PATCH /v1/sandboxes/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys a sandbox.
  #
  # `DELETE /v1/sandboxes/:id`
  #
  def destroy
    @sandbox.destroy
    head :ok
  end
  
private
  def find_sandbox
    @sandbox = Sandbox.find(params[:id])
  end
  
  def creator
    @creator ||= Savers::SandboxesCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
  
  def updater
    @updater ||= Savers::SandboxesUpdater.new(
      @sandbox,
      attributes,
      sessions_manager.editor
    )
  end
end
