# A controller for categories management.
class V1::CategoriesController < V1::ApplicationController
  before_action :require_admin, only: %i(create update destroy)
  before_action :find_category, only: %i(show update destroy)
  
  # Lists all categories.
  #
  # `GET /v1/categories`
  #
  def index
    categories = Category.order(:id)
    render json: categories, each_serializer: FullCategorySerializer
  end
  
  # Lists all categories with their children.
  #
  # `GET /v1/categories/with_children`
  #
  def index_with_children
    categories = Category.order(:id)
    respond_with CategoriesWithChildrenSerializer.new(categories)
  end
  
  # Renders all categories in a tree.
  #
  # `GET /v1/categories/tree`
  #
  def tree
    respond_with Tree.new(Category.all, CategorySerializer)
  end
  
  # Renders children of particular pseudo-category.
  #
  # `GET /v1/categories/:slug`
  #
  def by_parent
    categories = CategoriesSearchQuery.new.search(params.require(:slug))
    
    respond_with categories, each_serializer: CategoryDataSourceSerializer
  end
  
  # Renders all available setting keys.
  #
  # `GET /v1/categories/available_setting_keys`
  #
  def available_setting_keys
    respond_with Category.available_setting_keys
  end
  
  # Shows a particular category.
  #
  # `GET /v1/categories/:id`
  #
  def show
    respond_with @category
  end
  
  # Creates a new category with the given parameters.
  #
  # `POST /v1/categories`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Updates an existing category with the given parameters.
  #
  # `PATCH /v1/categories/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys a category.
  #
  # `DELETE /v1/categories/:id`
  #
  def destroy
    @category.destroy
    head :ok
  end
  
private
  def find_category
    @category = Category.find(params[:id])
  end
  
  def creator
    @creator ||= Savers::CategoriesCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
  
  def updater
    @updater ||= Savers::CategoriesUpdater.new(
      @category,
      attributes,
      sessions_manager.editor
    )
  end
end
