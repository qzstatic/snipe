# A controller for urls management.
class V1::UrlsController < V1::ApplicationController
  before_action :find_document, only: %i(index create raw_create)
  before_action :find_url, only: %i(show update destroy)
  before_action -> { verify_access_to_categories(find_document.category_ids) }, only: %i(create raw_create)
  
  # Lists urls of document.
  #
  # `GET /v1/documents/:document_id/urls`
  #
  def index
    respond_with @document.urls.map(&:porcupined)
  end
  
  # Shows a particular url.
  #
  # `GET /v1/urls/:id`
  #
  def show
    respond_with @url
  end
  
  # Creates a new url with the given parameters.
  #
  # `POST /v1/documents/:document_id/urls`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Creates a new url with the raw URL.
  #
  # `POST /v1/documents/:document_id/urls/raw`
  #
  def raw_create
    if raw_creator.save
      respond_with :v1, raw_creator.object
    else
      render json: raw_creator.errors, status: :unprocessable_entity
    end
  end
  
  # Searches for urls matching the given query.
  #
  # `GET /v1/urls/find_by_url`
  #
  def find_by_url
    url = UrlsSearchQuery.new(urls).find_by_url(params.require(:url))
    respond_with url, serializer: FullUrlSerializer
  end
  
  # Updates an existing url with the given parameters.
  #
  # `PATCH /v1/urls/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys a url.
  #
  # `DELETE /v1/urls/:id`
  #
  def destroy
    @url.destroy
    head :ok
  end
  
private
  def find_document
    @document = Document.find(params[:document_id])
  end
  
  def find_url
    @url = Url.find(params[:id]).porcupined
  end
  
  def creator
    @creator ||= Savers::UrlsCreator.new(
      attributes,
      sessions_manager.editor,
      @document
    )
  end
  
  def raw_creator
    @raw_creator ||= Savers::RawUrlsCreator.new(
      attributes,
      sessions_manager.editor,
      @document
    )
  end
  
  def updater
    @updater ||= Savers::UrlsUpdater.new(
      @url,
      attributes,
      sessions_manager.editor
    )
  end
  
  def urls
    Url.all
  end
end
