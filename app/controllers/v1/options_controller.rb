# A controller for options management.
class V1::OptionsController < V1::ApplicationController
  # Renders all options.
  #
  # `GET /v1/options`
  #
  # @todo В продакшене необходимо сделать ограничение на вывод или вообще убрать index.
  #
  def index
    respond_with Option.all.map(&:porcupined)
  end
  
  # Renders all attributes for options of different types.
  #
  # `GET /v1/options/types`
  #
  def types
    respond_with OptionTypesSerializer.new(Option::SETTINGS).as_json
  end
  
  # Renders all options of a specified type.
  #
  # `GET /v1/options/:type`
  #
  def by_type
    respond_with Option.by_type(params[:type]).order_by_position.map(&:porcupined)
  end
  
  # Shows a particular option.
  #
  # `GET /v1/options/:id`
  #
  def show
    respond_with option.porcupined
  end
  
  # Creates a new option with the given parameters.
  #
  # `POST /v1/options`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Updates an existing option with the given parameters.
  #
  # `PATCH /v1/options/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys a option.
  #
  # `DELETE /v1/options/:id`
  #
  def destroy
    option.destroy
    head :ok
  end
  
private
  def option
    @option ||= Option.find(params[:id])
  end
  
  def creator
    @creator ||= Savers::OptionsCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
  
  def updater
    @updater ||= Savers::OptionsUpdater.new(
      option,
      attributes,
      sessions_manager.editor
    )
  end
end
