# A controller for documents management.
class V1::DocumentsController < V1::ApplicationController
  before_action -> { verify_access_to_categories(current_category_ids) }, only: %i(create fast show update assign complete proofread update_preview publish unpublish)

  # Lists last 100 documents.
  #
  # `GET /v1/documents`
  #
  def index
    respond_with Document.last(100).map(&:porcupined)
  end
  
  # Lists first 200 documents filtered by specified sandbox.
  #
  # `GET /v1/sandboxes/:sandbox_id/documents`
  #
  def by_sandbox
    documents = sandbox.documents
    documents = documents.order(:published) if sandbox.published.nil?
    documents = documents.order(sort_column => sort_direction)
    documents = documents.limit(200)
    
    respond_with documents.map(&:porcupined)
  end

  # Lists documents by its categories
  #
  # `GET /v1/documents/categorized`
  #
  def categorized
    limit = (params[:limit] || 200).to_i
    documents = Document.with_category_ids(category_ids)
    respond_with documents.limit(limit).offset(params[:offset].to_i).map(&:porcupined)
  end

  # Shows a particular document.
  #
  # `GET /v1/documents/:id`
  #
  def show
    respond_with document, serializer: FullDocumentSerializer
  end
  
  # Creates a new document with the given parameters.
  #
  # `POST /v1/documents`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Creates and publish a new document with url.
  #
  # `POST /v1/documents/fast`
  #
  def fast
    if fast_creator.save
      respond_with :v1, fast_creator.object, serializer: ListedDocumentSerializer
    else
      render json: fast_creator.errors, status: :unprocessable_entity
    end
  end
  
  # Searches for documents with titles matching the given query.
  #
  # `GET /v1/documents/search`
  #
  def search
    query_object = DocumentsSearchQuery.new
    respond_with query_object.search(query, category_slugs: category_slugs, category_ids: category_ids)
  end

  # Renders all documents published on a given period.
  #
  # `GET /v1/documents/by_period`
  #
  def by_period
    query_object = DocumentsByPeriodQuery.new
    documents = query_object.search(Date.parse(params.fetch(:start_date))..Date.parse(params.fetch(:end_date)))
    respond_with documents, each_serializer: DocumentsByPeriodSerializer
  end
  
  # Updates an existing document with the given parameters.
  #
  # `PATCH /v1/documents/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Assigns the specified editor to the document.
  #
  # `PATCH /v1/documents/:id/assign`
  #
  def assign
    if assigner.save
      head :ok
    else
      render json: assigner.errors, status: :unprocessable_entity
    end
  end
  
  # Marks the document as completed.
  #
  # `PATCH /v1/documents/:id/complete`
  #
  def complete
    if completer.save
      head :ok
    else
      render json: completer.errors, status: :unprocessable_entity
    end
  end
  
  # Marks the document as proofread.
  #
  # `PATCH /v1/documents/:id/proofread`
  #
  def proofread
    if proofreader.save
      head :ok
    else
      render json: proofreader.errors, status: :unprocessable_entity
    end
  end
  
  # Creates a preview for document.
  #
  # `PATCH /v1/documents/:id/preview`
  #
  def update_preview
    Savers::DocumentsPipeliner.new(document: document).process(preview: true)
    
    head :ok
  end
  
  # Returns a preview for document without saving `packed_preview`.
  #
  # `GET /v1/documents/:id/preview`
  #
  def preview
    preview = Savers::DocumentsPipeliner.new(document: document).packed_preview
    
    render json: preview.merge(published: document.published)
  end
  
  # Publishes the document (optionally setting the `published_at` attribute).
  #
  # `PATCH /v1/documents/:id/publish`
  #
  def publish
    if publisher.save
      head :ok
    else
      render json: publisher.errors, status: :unprocessable_entity
    end
  end
  
  # Unpublishes the document.
  #
  # `PATCH /v1/documents/:id/unpublish`
  #
  def unpublish
    if unpublisher.save
      head :ok
    else
      render json: unpublisher.errors, status: :unprocessable_entity
    end
  end
  
  # Shows the document's root box creating it if nesessary.
  #
  # `GET /v1/documents/:id/root_box`
  #
  def root_box
    document.create_root_box unless document.root_box
    
    respond_with document.root_box
  end
  
  # Renders documents bound to the requested document.
  #
  # `GET /v1/documents/:id/bound`
  #
  def bound
    render json: document.bound_documents.order(published_at: :desc).limit(200)
  end
  
  # Renders porcupined documents bound to the requested document.
  #
  # `GET /v1/documents/:id/full_bound`
  #
  def full_bound
    render json: document.bound_documents.order(published_at: :desc).limit(200).map(&:porcupined)
  end

  # Renders documents bound to the requested document in section.
  #
  # `GET /v1/documents/:id/bound_section/:section`
  #
  def bound_section
    limit = params[:limit].to_i
    limit = 200 unless limit.present?
    render json: document.bound_documents_by_section(params[:section]).order(published_at: :desc).limit(limit)
  end
  
  # Renders porcupined documents bound to the requested document in section.
  #
  # `GET /v1/documents/:id/full_bound_section/:section`
  #
  def full_bound_section
    limit = params[:limit].to_i
    limit = 200 unless limit.present?
    render json: document.bound_documents_by_section(params[:section]).order(published_at: :desc).limit(limit).map(&:porcupined)
  end
  
  # Renders matching lists for the requested document.
  #
  # `GET /v1/documents/:id/matching_lists`
  #
  def matching_lists
    render json: document.matching_lists
  end
  
  # # Destroys a document.
  # #
  # # `DELETE /v1/documents/:id`
  # #
  # def destroy
  #   @document.destroy
  #   head :ok
  # end
  
private
  def query
    params.require(:query)
  end
  
  def category_slugs
    params.fetch(:category_slugs, '')
  end
  
  def category_ids
    params.fetch(:category_ids, '')
  end
  
  def document
    @document ||= Document.find(params[:id]).porcupined
  end
  
  def creator
    @creator ||= Savers::DocumentsCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
  
  def fast_creator
    @fast_creator ||= Savers::DocumentsFastCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
  
  def updater
    @updater ||= Savers::DocumentsUpdater.new(
      document,
      attributes,
      sessions_manager.editor
    )
  end
  
  def assigner
    @assigner ||= Savers::DocumentsAssigner.new(
      document,
      attributes,
      sessions_manager.editor
    )
  end
  
  def completer
    @completer ||= Savers::DocumentsCompleter.new(
      document,
      sessions_manager.editor
    )
  end
  
  def proofreader
    @proofreader ||= Savers::DocumentsProofreader.new(
      document,
      sessions_manager.editor
    )
  end
  
  def publisher
    @publisher ||= Savers::DocumentsPublisher.new(
      document,
      params.fetch(:attributes, Hash.new),
      sessions_manager.editor
    )
  end
  
  def unpublisher
    @unpublisher ||= Savers::DocumentsUnpublisher.new(
      document,
      params.fetch(:attributes, Hash.new),
      sessions_manager.editor
    )
  end
  
  def sandbox
    Sandbox.find(params[:sandbox_id])
  end
  
  def list
    List.find_by(params[:list_id])
  end

  def current_category_ids
    params.try(:[], :attributes).try(:[], :category_ids) || document.category_ids || []
  end

  def sort_column
    %w(created_at updated_at published_at).include?(params[:sort_column]) ? params[:sort_column] : :updated_at
  end

  def sort_direction
    %w(asc desc).include?(params[:sort_direction]) ? params[:sort_direction].to_sym : :desc
  end
end
