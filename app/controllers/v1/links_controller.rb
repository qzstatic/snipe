# A controller for links management.
class V1::LinksController < V1::ApplicationController
  before_action :find_document, only: %i(index create)
  before_action :find_link, except: %i(index create)
  
  # Lists the requested document's links.
  #
  # `GET /v1/documents/:document_id/links`
  #
  def index
    respond_with @document.links.order(:position)
  end
  
  # Shows a particular link.
  #
  # `GET /v1/links/:id`
  #
  def show
    respond_with @link
  end
  
  # Creates a new link with the given parameters for the requested document.
  #
  # `POST /v1/documents/:document_id/links`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Updates an existing link with the given parameters.
  #
  # `PATCH /v1/links/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys a link.
  #
  # `DELETE /v1/links/:id`
  #
  def destroy
    @link.destroy
    head :ok
  end
  
private
  def find_document
    @document = Document.find(params[:document_id])
  end
  
  def find_link
    @link = Link.find(params[:id])
  end
  
  def creator
    @creator ||= Savers::LinksCreator.new(
      attributes,
      sessions_manager.editor,
      @document
    )
  end
  
  def updater
    @updater ||= Savers::LinksUpdater.new(
      @link,
      attributes,
      sessions_manager.editor
    )
  end
end
