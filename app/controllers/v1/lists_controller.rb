# A controller for lists management.
class V1::ListsController < V1::ApplicationController
  before_action -> { verify_access_to_categories(category_ids) }, only: %i(create update destroy)
  before_action -> { requires_roles(:administrators, :web_editors) }, only: %i(create)

  # Renders all lists.
  #
  # `GET /v1/lists`
  #
  def index
    respond_with List.all.map(&:porcupined)
  end
  
  # Shows a particular list.
  #
  # `GET /v1/lists/:id`
  #
  def show
    respond_with list
  end
  
  # Lists documents filtered by the specified list.
  #
  # `GET /v1/lists/:id/documents`
  #
  def documents
    respond_with list, serializer: ListDocumentsSerializer
  end
  
  # Lists documents cached for the specified list.
  #
  # `GET /v1/lists/:id/cached_documents`
  #
  def cached_documents
    respond_with list, serializer: ListDocumentsCacheSerializer
  end
  
  # Tries to find & render a list item for specified `list_id` and `document_id`.
  #
  # `GET /v1/lists/:id/item`
  #
  def item
    respond_with ListItem.find_by!(
      list_id:     params[:id],
      document_id: params[:document_id]
    )
  end
  
  # Creates a new list with the given parameters.
  #
  # `POST /v1/lists`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Updates an existing list with the given parameters.
  #
  # `PATCH /v1/lists/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys a list.
  #
  # `DELETE /v1/lists/:id`
  #
  def destroy
    list.destroy
    head :ok
  end
  
private
  def list
    @list ||= List.find(params[:id]).porcupined
  end
  
  def creator
    @creator ||= Savers::ListsCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
  
  def updater
    @updater ||= Savers::ListsUpdater.new(
      list,
      attributes,
      sessions_manager.editor
    )
  end

  def category_ids
    params.try(:[], :attributes).try(:[], :category_ids) || list.category_ids || []
  end
end
