# A controller for logging in and out.
class V1::SessionsController < V1::ApplicationController
  skip_before_action :authorize!, only: %i(create refresh)
  
  # Authenticates the editor and creates all tokens.
  #
  # `POST /v1/access_token`
  #
  def create
    @sessions_manager = SessionsManager::Credentials.new(request)
    
    if @sessions_manager.valid?
      @sessions_manager.open_session
      render json: @sessions_manager.tokens_data
    else
      render json: { error: 'Invalid credentials' }, status: :unprocessable_entity
    end
  end
  
  # Renders the current session information.
  #
  # `GET /v1/session`
  #
  def show
    respond_with @sessions_manager, serializer: SessionsManagerSerializer
  end
  
  # Refreshes the access_token given the refresh_token.
  #
  # `PATCH /v1/access_token`
  #
  def refresh
    @sessions_manager = SessionsManager::RefreshToken.new(request)
    
    if @sessions_manager.valid?
      @sessions_manager.refresh_session
      render json: @sessions_manager.tokens_data
    else
      head :not_found
    end
  end
  
  # Closes the session deleting all tokens.
  #
  # `DELETE /v1/access_token`
  #
  def destroy
    sessions_manager.close_session
    head :ok
  end
end
