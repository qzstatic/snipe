# A controller for list items management.
class V1::ListItemsController < V1::ApplicationController
  # Renders all items of the requested list.
  #
  # `GET /v1/lists/:list_id/items`
  #
  def index
    if list.respond_to?(:items)
      respond_with list.items
    else
      head :not_found
    end
  end
  
  # Shows a particular list item.
  #
  # `GET /v1/list_items/:id`
  #
  def show
    respond_with item
  end
  
  # Creates a new list item with the given parameters.
  #
  # `POST /v1/lists/:list_id/items`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Updates an existing item with the given parameters.
  #
  # `PATCH /v1/list_items/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys an item.
  #
  # `DELETE /v1/list_items/:id`
  #
  def destroy
    item.destroy
    head :ok
  end
  
private
  def list
    @list ||= List.find(params[:list_id])
  end
  
  def item
    @list_item ||= ListItem.find(params[:id])
  end
  
  def creator
    @creator ||= Savers::ListItemsCreator.new(
      attributes,
      sessions_manager.editor,
      list
    )
  end
  
  def updater
    @updater ||= Savers::ListItemsUpdater.new(
      item,
      attributes,
      sessions_manager.editor
    )
  end
end
