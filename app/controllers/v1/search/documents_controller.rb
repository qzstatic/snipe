# A controller for documents search.
class V1::Search::DocumentsController < V1::ApplicationController
  # Lists ids of documents filtered by updated_at after specified timestamp.
  #
  # `GET /v1/search/documents/fresh/:timestamp`
  #
  def fresh
    query_object = DocumentsSearchQuery.new
    respond_with query_object.fresh(params[:timestamp])
  end
  
  # Lists ids of documents by page and ordered by updated_at.
  #
  # `GET /v1/search/documents/index/:page`
  #
  def index
    respond_with Document.with_category_ids(params[:category_ids]).order(updated_at: :desc).page(params[:page]).pluck(:id)
  end
end
