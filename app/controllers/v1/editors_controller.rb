# A controller for editors management.
class V1::EditorsController < V1::ApplicationController
  before_action :require_admin, except: %i(index)
  before_action :find_editor, only: %i(show update destroy)
  
  # Lists all editors.
  #
  # `GET /v1/editors`
  #
  def index
    respond_with Editor.order(:id)
  end
  
  # Shows a particular editor.
  #
  # `GET /v1/editors/:id`
  #
  def show
    respond_with @editor
  end
  
  # Creates a new editor with the given parameters.
  #
  # `POST /v1/editors`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Updates an existing editor with the given parameters.
  #
  # `PATCH /v1/editors/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys an editor.
  #
  # `DELETE /v1/editors/:id`
  #
  def destroy
    @editor.destroy
    head :ok
  end
  
private
  def find_editor
    @editor = Editor.find(params[:id])
  end
  
  def creator
    @creator ||= Savers::EditorsCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
  
  def updater
    @updater ||= Savers::EditorsUpdater.new(
      @editor,
      attributes,
      sessions_manager.editor
    )
  end
end
