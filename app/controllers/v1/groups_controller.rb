# A controller for groups management.
class V1::GroupsController < V1::ApplicationController
  before_action :require_admin, except: %i(index)
  before_action :find_group, only: %i(show update destroy)
  
  # Lists all groups.
  #
  # `GET /v1/groups`
  #
  def index
    respond_with Group.order(:position)
  end
  
  # Renders groups filtered by editor.
  #
  # `GET /v1/groups/by_editor/:editor_id`
  #
  def by_editor
    respond_with Editor.find(params[:editor_id]).groups
  end
  
  # Shows a particular group.
  #
  # `GET /v1/groups/:id`
  #
  def show
    respond_with @group
  end
  
  # Creates a new group with the given parameters.
  #
  # `POST /v1/groups`
  #
  def create
    if creator.save
      respond_with :v1, creator.object
    else
      render json: creator.errors, status: :unprocessable_entity
    end
  end
  
  # Updates an existing group with the given parameters.
  #
  # `PATCH /v1/groups/:id`
  #
  def update
    if updater.save
      head :ok
    else
      render json: updater.errors, status: :unprocessable_entity
    end
  end
  
  # Destroys a group.
  #
  # `DELETE /v1/groups/:id`
  #
  def destroy
    @group.destroy
    head :ok
  end
  
private
  def find_group
    @group = Group.find(params[:id])
  end
  
  def creator
    @creator ||= Savers::GroupsCreator.new(
      attributes,
      sessions_manager.editor
    )
  end
  
  def updater
    @updater ||= Savers::GroupsUpdater.new(
      @group,
      attributes,
      sessions_manager.editor
    )
  end
end
