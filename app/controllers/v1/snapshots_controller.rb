# A controller for snapshots management.
class V1::SnapshotsController < V1::ApplicationController
  # Renders the requested snapshot's document state at the moment of snapshot's creation.
  #
  # `GET /v1/snapshots/:id`
  #
  def show
    respond_with Snapshot.full_document_at(snapshot)
  end
  
  # Lists all snapshots of a requested document.
  #
  # `GET /v1/documents/:document_id/snapshots`
  #
  def for_document
    respond_with Document.find(params[:document_id]).all_snapshots
  end
  
  # Renders the diff between two document's states identified by snapshots.
  #
  # `GET /v1/snapshots/:old_id..:new_id`
  #
  def diff
    old_snapshot = Snapshot.find(params[:old_id])
    new_snapshot = Snapshot.find(params[:new_id])
    
    render text: Differ.new(old_snapshot, new_snapshot).diff
  end
  
  # Lists all snapshots of a requested sandbox.
  #
  # `GET /v1/sandboxes/:sandbox_id/snapshots`
  #
  def for_sandbox
    respond_with Sandbox.find(params[:sandbox_id]).snapshots.order(id: :desc)
  end
  
  # Lists all snapshots of a requested list.
  #
  # `GET /v1/lists/:list_id/snapshots`
  #
  def for_list
    respond_with List.find(params[:list_id]).snapshots.order(id: :desc)
  end
  
  # Lists all snapshots of a requested page.
  #
  # `GET /v1/pages/:page_id/snapshots`
  #
  def for_page
    respond_with Page.find(params[:page_id]).snapshots.order(id: :desc)
  end
  
private
  def snapshot
    Snapshot.find(params[:id])
  end
end
