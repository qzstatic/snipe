# Base controller.
class V1::ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include ActionController::ImplicitRender
  
  respond_to :json
  
  # Reads and writes the sessions manager.
  #
  # @return [SessionsManager] sessions manager
  #
  attr_reader :sessions_manager
  
  before_action :authorize!
  
private
  def attributes
    params.require(:attributes)
  end
  
  def authorize!
    @sessions_manager = SessionsManager::AccessToken.new(request)
    head :unauthorized unless @sessions_manager.valid?
  end

  def require_admin
    head :forbidden unless administrator?
  end

  %w(administrators web_editors proofreaders newspaper_editors photo_editors video_editors commers subscription).each do |group|
    define_method("#{group.singularize}?") { user_group_slugs.include?(group) }
  end

  def verify_access_to_categories(category_ids)
    return if administrator?
    head(:forbidden) if user_group_slugs.blank? && request_type?('delete', 'post', 'patch', 'put')
    category_slugs = Category.where(id: category_ids).select(:slug).map(&:slug)
    if (%w(articles advert_articles) - category_slugs).blank?
      head(:forbidden) unless commer?
    elsif (%w(companies) - category_slugs).blank? || (%w(press_releases) - category_slugs).blank?
      head(:forbidden) if subscription?
    else
      head(:forbidden) if commer?
      case
        when category_slugs.include?('video')
          head(:forbidden) if !(web_editor? || video_editor?) && request_type?('delete', 'post')
        when category_slugs.include?('galleries')
          head(:forbidden) if !(web_editor? || photo_editor?) && request_type?('delete', 'post')
        else
          head(:forbidden) if !web_editor? && request_type?('delete', 'post')
      end
    end
  end

  def requires_roles(*args)
    head(:forbidden) if (user_group_slugs - args.map(&:to_s)) == user_group_slugs
  end

  def user_group_slugs
    @user_group_slugs ||= @sessions_manager.editor.groups.select(:slug).map(&:slug)
  end

  def request_type?(*args)
    args.map(&:upcase).include?(request.method)
  end
end
