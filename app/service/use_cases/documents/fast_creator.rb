module UseCases
  module Documents
    class FastCreator < UseCases::Base
      handler Handlers::Documents::Creator, :document, :editor
      handler Handlers::Urls::RawCreator, :url, :document
      hook Hooks::Documents::UrlAppender, :document, :url
      handler Handlers::Documents::Publisher, :document, :editor
      hook Hooks::Documents::FixUrls, :document
      hook Hooks::Documents::Pipeliner, :document
      hook Hooks::Documents::AddParticipant, :document, :editor
      hook Hooks::Documents::ListItems, :document
      hook Hooks::Documents::ListCache, :document
      
      def initialize(params, editor)
        proxy :params, params
        proxy :editor, editor
        proxy :document, Document.new
        proxy :url, Url.new
      end
    end
  end
end
