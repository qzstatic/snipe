module UseCases
  # A base class for all use cases.
  class Base
    # Return the hash with objects
    #
    # @return [<Hash>] proxy_values hash with objects
    attr_reader :proxy_values

    # Process all handlers and hooks for given use case
    #
    # @return [true] if process was successfully
    # @return [false] otherwise
    #
    def process
      Document.transaction do
        self.class.handlers.each do |handler_data|
          args = args_for_handler(handler_data.fetch(:type), handler_data.fetch(:attributes))
          handler = handler_data.fetch(:class_name).new(*args)

          proxy handler_data.fetch(:attributes).first, handler.object

          unless handler.save
            error_key = handler_data.fetch(:type) == :handler ? handler.error_key : :base
            errors.add(error_key, handler.errors.as_json)
            raise ActiveRecord::Rollback
          end
        end
      end
    end
    
    # Store object in proxy
    #
    # @param [<Symbol>] attribute the name of stored object
    # @param [<Object>] value the saved object
    #
    def proxy(attribute, value)
      @proxy_values ||= Hash.new
      @proxy_values[attribute] = value
    end
    
    # Returns the use_case's errors.
    #
    # @return [ActiveModel::Errors] the errors
    #
    def errors
      @errors ||= ActiveModel::Errors.new(Object.new)
    end

  private
    def method_missing(method_name, *args, &block)
      proxy_values.fetch(method_name) { super }
    end

    def args_for_handler(type, symbols)
      args = [self.send(symbols.first)]
      args << params if type == :handler
      attributes = symbols[1..-1].each_with_object(Hash.new) do |symbol, hash|
        hash[symbol] = self.send(symbol)
      end
      args.push(attributes)
    end
    
    class << self
      def handler(handler_class, *attributes)
        handlers.push({type: :handler, class_name: handler_class, attributes: attributes})
      end

      def hook(hook_class, *attributes)
        handlers.push({type: :hook, class_name: hook_class, attributes: attributes})
      end

      def handlers
        @handlers ||= []
      end
    end
  end
end
