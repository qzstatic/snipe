class SessionsManager
  # A sessions manager with access token authentication.
  #
  # This is the most widely used sessions manager type, it is responsible
  # for all requests except login requests and token refresh requests.
  #
  class AccessToken < self
    # Access token header name.
    TOKEN_HEADER = 'X-Access-Token'.freeze
    
  private
    def authenticate!
      if @access_token = Token::Editor::Access.find(@request.headers[TOKEN_HEADER])
        post_initialize!
      end
    end
    
    def post_initialize!
      @editor = Editor.find(access_token.editor_id)
      
      session = Session.find_by(access_token: access_token.token)
      @refresh_token = Token::Editor::Refresh.find(session.refresh_token)
    end
  end
end
