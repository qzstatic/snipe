class SessionsManager
  # A sessions manager without authentication - it allows direct
  # editor assignment.
  #
  # Designed just for authorization in rails console.
  #
  # @note Do not use in real code!
  #
  class Console < self
    # Sets the authenticated editor.
    #
    # @param [Editor] value the authenticated editor
    #
    # @return [undefined]
    #
    attr_writer :editor
    
  private
    def authenticate!; end
  end
end
