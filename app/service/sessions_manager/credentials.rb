class SessionsManager
  # A sessions manager with login/password or email/password authentication.
  #
  # It is used only on login requests.
  #
  class Credentials < self
  private
    def authenticate!
      @editor = Editor.authenticate(editor_params) || nil
    end
    
    def editor_params
      if @request.params.key?(:editor)
        @request.params[:editor].symbolize_keys
      else
        {}
      end
    end
  end
end
