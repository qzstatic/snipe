class SessionsManager
  # A sessions manager with authentication by `refresh_token`.
  #
  # It is used only on token refresh requests.
  #
  class RefreshToken < self
  private
    def authenticate!
      if @refresh_token = Token::Editor::Refresh.find(@request.params[:refresh_token])
        post_initialize!
      end
    end
    
    def post_initialize!
      @editor = Editor.find(refresh_token.editor_id)
      
      session = Session.find_by(refresh_token: refresh_token.token)
      @access_token = Token::Editor::Access.find(session.access_token)
    end
  end
end
