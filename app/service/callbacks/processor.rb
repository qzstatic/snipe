module Callbacks
  class Processor
    def initialize(event, separator = '.')
      @callbacks = event.to_s.split(separator).inject(Settings.callbacks) { |memo_obj, setting| memo_obj.send(setting) }
    end

    def process(object)
      @callbacks.map { |callback| Object.const_get(callback).new(object).call }
    end
  end
end
