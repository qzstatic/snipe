module Callbacks
  module Documents
    class DocumentPurgeCache

      def initialize(document)
        @document = document
      end

      def call
        Resque.enqueue(PurgeDocumentCache, @document.url.rendered) if @document.url
      end
    end
  end
end
