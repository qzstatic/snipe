module Callbacks
  module Documents
    class UpdateRestrictions

      def initialize(document)
        @document = document
      end

      def call
        if @document.url
          if @document.try(:pay_required)
            Resque.enqueue(UpdateRestrictionsJob, @document.url.rendered, @document.header_data['pay_required_expire'])
          else
            Resque.enqueue(DeleteRestrictionJob, @document.url.rendered)
          end
        end
      end
    end
  end
end
