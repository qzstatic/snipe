module Callbacks
  module Documents
    class RenewSocialImage

      def initialize(document)
        @document = document
      end

      def call
        Resque.enqueue(SharingImageUpdater, @document.url.rendered) if important_attributes_changed? && @document.url
      end

      private

      def important_attributes_changed?
        title_changed? || images_changed? || sharing_alt_changed? || first_rubric_changed? || test_box_changed?
      end

      def title_changed?
        @document.title.to_s != @document.packed_document[:title].to_s
      end

      def images_changed?
        image_changed?(:sharing) || image_changed?(:image)
      end

      def sharing_alt_changed?
        @document.try(:sharing_alt).to_s != @document.packed_document.try(:[], :sharing_alt).to_s
      end

      def first_rubric_changed?
          @document.categories.select { |cat| cat.slug_path.include?('rubrics') && cat.slug_path.size == 2 }.first.try(:slug).to_s !=
              @document.packed_document.try(:[], :categories).try(:[], :rubrics).try(:first).try(:[], :slug).to_s
      end

      def test_box_changed?
        if @document.categories.map(&:slug).include?('test')
          test_box = @document.boxes.where(type: 'test').first
          test_box ? (test_box.boxer_data['json'] != test_box.packed_box[:json]) : false
        else
          false
        end
      end

      private

      def image_changed?(slug)
        @document.try(slug).try(:directory).to_s !=
            @document.packed_document.try(:[], slug).try(:[], :original).try(:[], :url).to_s.scan(/[-_\w]*\//).join.chop
      end
    end
  end
end
