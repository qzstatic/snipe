module Savers
  # A class responsible for unpublishing documents.
  class DocumentsUnpublisher < Base
    # Initializes the unpublisher instance.
    #
    # @param [ActiveRecord::Base] object the object being published
    # @param [Hash] object_params the object's parameters (currently unused)
    # @param [Editor] editor the editor unpublishing the object
    #
    def initialize(object, object_params, editor)
      @object = object
      @editor = editor
      
      assign(object_params)
    end
    
    # Saves the object to the database.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      if object.save
        create_snapshot
        update_dependencies
        update_list_items
        update_list_cache
      end
    end
    
  private
    def assign(params)
      object.published = false
      object.milestones = object.milestones.except(:published)
      object.add_participant(editor.id)
    end
    
    def update_dependencies
      Savers::DependenciesUpdater.call(object)
    end
    
    def create_snapshot
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Document'
        snapshot.author_id    = editor.id
        snapshot.action       = 'document.unpublished'
        snapshot.data         = object.attributes
      end
    end
    
    def update_list_cache
      Hooks::Documents::ListCache.new(object).save
    end
    
    def update_list_items
      Hooks::Documents::ListItems.new(object).save
    end
  end
end
