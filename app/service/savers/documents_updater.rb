module Savers
  # A class responsible for updating documents.
  class DocumentsUpdater < BaseUpdater
    depends_on :category_ids
    permits :title, :url_id, participant_ids: [], exclude_list_ids: []

    converts_params do |params|
      normalize_categories(params)
      normalize_array(params, :category_ids)
      normalize_array(params, :exclude_list_ids)
    end

    after_initialize do
      object.add_participant(editor.id)
      reset_proofread_status
    end

    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Document'
        snapshot.author_id    = editor.id
        snapshot.action       = 'document.updated'
        snapshot.data         = object.attributes
      end
    end

    private
    def reset_proofread_status
      ProofreadReset.new(object, editor).reset
    end

    def normalize_categories(params)
      params[:category_ids] = Pipeline::CategoriesTreeNormalizer.new(params[:category_ids]).normalize! if params.key?(:category_ids)
    end
  end
end
