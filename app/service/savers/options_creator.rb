module Savers
  # A class responsible for creating options.
  class OptionsCreator < BaseCreator
    creates Option
    depends_on :type
    permits :position
  end
end
