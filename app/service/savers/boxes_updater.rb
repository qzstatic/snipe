module Savers
  # A class responsible for updating boxes.
  class BoxesUpdater < BaseUpdater
    depends_on :parent_id
    permits :position, :enabled
    
    attr_reader :updated_at
    
    after_initialize do
      object.document.add_participant(editor.id)
      reset_proofread_status
    end
    
    converts_params do |params|
      params[:body].gsub!("\u0003", '') if params.key?(:body) && params[:body].respond_to?(:gsub!)
    end
    
    def initialize(*args, updated_at:)
      @updated_at = Time.parse(updated_at.to_s)
      super(*args)
    end
    
    # Saves the box.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      unless stale_request?
        object.document.save
        super
      end
    end
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Box'
        snapshot.author_id    = editor.id
        snapshot.action       = 'box.updated'
        snapshot.data         = object.attributes
      end
    end
    
    def stale_request?
      updated_at.to_i != object.updated_at.to_i
    end
    
  private
    def reset_proofread_status
      ProofreadReset.new(porcupine(object.document), editor).reset
    end
  end
end
