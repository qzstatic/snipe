module Savers
  # A class responsible for marking documents as proofread.
  class DocumentsProofreader < Base
    # Initializes the proofreader instance.
    #
    # Sets the document attributes corresponding to the assigned editor.
    #
    # @param object [ActiveRecord::Base] the document to proofread
    # @param editor [Editor] the editor who is proofreading
    #
    def initialize(object, editor)
      @object = porcupine(object)
      @editor = editor
      
      mark_as_proofread if applicable?
    end
    
    # Saves the object to the database.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      create_snapshot if object.save
      object.valid?
    end
    
  private
    def create_snapshot
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Document'
        snapshot.author_id    = editor.id
        snapshot.action       = 'document.proofread'
        snapshot.data         = object.attributes
      end
    end
    
    def mark_as_proofread
      object.proofread = true
      
      object.milestones = object.milestones.merge(
        proofread: {
          at: Time.now.to_i,
          by: editor.id
        }
      )
      
      object.add_participant(editor.id)
    end
    
    def applicable?
      object.respond_to?(:proofread)
    end
  end
end
