module Savers
  # A simple use case to update document's dependencies.
  # Supports dependent boxes and links.
  class DependenciesUpdater
    include Procto.call
    
    # Returns the dependable document.
    #
    # @return [Document]
    #
    attr_reader :document
    
    # Initializes the updater instance.
    #
    # @param document [Document] the dependable document
    #
    def initialize(document)
      @document = document
    end
    
    # Updates the dependencies.
    #
    # @return [undefined]
    #
    def call
      document.dependencies.each do |dependency|
        case dependent = dependency.dependent
        when Link
          Pipeline::Links.new(dependent.document.porcupined).process
        when Box
          Pipeline::Box.new(dependent.porcupined).process
        end
      end
    end
  end
end
