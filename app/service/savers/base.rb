module Savers
  # A base class for all savers.
  class Base
    # Returns the editor who is saving the object.
    #
    # @return [Editor] editor
    #
    attr_reader :editor
    
    # Returns the object being saved.
    #
    # @return [ActiveRecord::Base] object
    #
    attr_reader :object
    
    # Returns the object's errors.
    #
    # @return [ActiveModel::Errors] the errors
    #
    def errors
      object.errors
    end
    
  private
    def convert_params(params)
      instance_exec(params, &self.class.params_converter)
    end
    
    def post_initialize
      instance_exec(&self.class.post_initializer)
    end
    
    def post_process
      self.class.post_processor.call(object, editor)
    end
    
    def permit(params)
      params.permit(permitted_params)
    end
    
    def permitted_params
      if object.respond_to?(:dynamic_attributes_names)
        self.class.permitted_attributes + object.dynamic_attributes_names
      else
        self.class.permitted_attributes
      end
    end
    
    def porcupine(object)
      if object.respond_to?(:porcupined)
        object.porcupined
      else
        object
      end
    end
    
    def normalize_array(hash, key)
      if hash.key?(key) && hash[key].nil?
        hash[key] = []
      end
    end
    
    class << self
      # Declares the key attributes
      # (fixed attributes that dynamic attributes depend on).
      #
      # @param [<Symbol>] attributes the key attributes
      #
      # @return [undefined]
      #
      # @example
      #   module Savers
      #     class DocumentsCreator < BaseCreator
      #       depends_on :category_ids
      #     end
      #   end
      #
      def depends_on(*attributes)
        @key_attributes = attributes
      end
      
      # Returns the list of key attributes.
      #
      # @return [<Symbol>] key attributes
      #
      def key_attributes
        @key_attributes || []
      end
      
      # Declares the permitted attributes list (in `strong_parameters` format).
      #
      # This list is later accessible via #permitted_attributes reader method.
      #
      # @param [<Symbol, Hash>] attributes the permitted attributes
      #
      # @return [undefined]
      #
      # @example
      #   module Savers
      #     class CategoriesUpdater < BaseUpdater
      #       permits :slug, :title, :position
      #     end
      #   end
      #
      def permits(*attributes)
        @permitted_attributes = attributes
      end
      
      # Returns the list of permitted attributes.
      #
      # @return [<Symbol, Hash>] permitted attributes
      #
      def permitted_attributes
        @permitted_attributes || []
      end
      
      # Declares how the saved object's parameters should be converted.
      #
      # @yieldparam params [Hash] the object's parameters
      #
      # @return [undefined]
      #
      # @example
      #   module Savers
      #     class DocumentsCreator < BaseCreator
      #       converts_params do |params|
      #         params[:title] = "News: #{params[:title]}"
      #       end
      #     end
      #   end
      #
      def converts_params(&block)
        fail ArgumentError unless block_given?
        @params_converter = block
      end
      
      # Returns the proc for converting the object's parameters.
      #
      # @return [Proc] the convertor proc
      #
      # @example
      #   Savers::DocumentsCreator.params_converter.call(title: 'great news')
      #   # => { title: 'News: great news' }
      #
      def params_converter
        @params_converter || -> (params) { params }
      end
      
      # Declares how the object should be processed after initialization.
      # `object` and `editor` variables are accessible from within the block.
      #
      # @return [undefined]
      #
      # @example
      #   module Savers
      #     class DocumentsCreator < BaseCreator
      #       after_initialize do
      #         object.add_participant(editor.id)
      #       end
      #     end
      #   end
      #
      def after_initialize(&block)
        @post_initializer = block
      end
      
      # Returns the proc for processing the object after the initialization.
      #
      # @return [Proc] the post-initializer proc
      #
      def post_initializer
        @post_initializer || -> { nil }
      end
      
      # Declares how the saved object should be processed after saving.
      #
      # @yieldparam object [ActiveRecord::Base, Porcupine] a persisted object
      #
      # @return [undefined]
      #
      # @example
      #   module Savers
      #     class DocumentsCreator < BaseCreator
      #       after_save do |document|
      #         document.destroy
      #       end
      #     end
      #   end
      #
      def after_save(&block)
        fail ArgumentError unless block_given?
        @post_processor = block
      end
      
      # Returns the proc for processing the object after saving.
      #
      # @return [Proc] the post-processor proc
      #
      # @example
      #   Savers::DocumentsCreator.post_processor.call(document)
      #   # oops
      #
      def post_processor
        @post_processor || -> (*params) { params }
      end
    end
  end
end
