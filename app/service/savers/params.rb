module Savers
  # A class representing a parameters set.
  class Params
    # Initializes the `Params` instance.
    #
    # @param [Hash] parameters the parameters
    #
    def initialize(parameters)
      @parameters = parameters
    end
    
    # Deletes the parameters with the specified names
    # and returns them in a hash.
    #
    # @param [<Symbol>] names parameter names to extract
    #
    # @return [Hash] extracted parameters
    #
    def extract(*names)
      names.flatten.each_with_object(Hash.new) do |key, hash|
        hash[key] = parameters.delete(key) if parameters.key?(key)
      end
    end
    
    # Delegates all unknown methods to the parameters hash.
    #
    def method_missing(method_name, *args, &block)
      if parameters.respond_to?(method_name)
        parameters.send(method_name, *args, &block)
      else
        super
      end
    end
    
  private
    attr_reader :parameters
  end
end
