module Savers
  # A class responsible for updating editors.
  class EditorsUpdater < BaseUpdater
    permits :login, :email, :password, :first_name, :last_name, :enabled, group_ids: []
    
    converts_params do |params|
      normalize_array(params, :group_ids)
    end
  end
end
