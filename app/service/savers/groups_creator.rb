module Savers
  # A class responsible for creating groups.
  class GroupsCreator < BaseCreator
    creates Group
    permits :slug, :title, :position
  end
end
