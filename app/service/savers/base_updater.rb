module Savers
  # A base class for all updaters.
  class BaseUpdater < ::Savers::Base
    # Initializes the updater instance.
    #
    # Saves the editor, converts the parameters and assigns them to the object
    # (w/o saving to the database).
    #
    # @param [ActiveRecord::Base] object the object being updated
    # @param [Hash] object_params the object's parameters
    # @param [Editor] editor the editor updating the object
    #
    def initialize(object, object_params, editor)
      params  = Params.new(object_params)
      @editor = editor
      
      convert_params(params)
      @object = build_object(params, object)
      @object.assign_attributes(permit(params)) if object_updatable?
      post_initialize
    end
    
    # Saves the object to the database.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      post_process if object.save
      object.valid?
    end
    
  private
    def build_object(params, object)
      key_attributes = params.extract(self.class.key_attributes)
      object.assign_attributes(key_attributes)
      porcupine(object)
    end
    
    def object_updatable?
      instance_exec(object, &self.class.update_condition)
    end
    
    class << self
      # Declares the condition by which the object is updated.
      #
      # @yieldparam object [ActiveRecord::Base] the object being updated
      #
      # @return [undefined]
      #
      # @example
      #   module Savers
      #     class UrlsUpdater < BaseUpdater
      #       updates_if do |url|
      #         !url.fixed?
      #       end
      #     end
      #   end
      #
      def updates_if(&block)
        @update_condition = block
      end
      
      # Returns the proc calculating if the object should be updated.
      #
      # @return [Proc] the condition proc
      #
      # @example
      #   url = Struct.new(:fixed?).new(true)
      #   Savers::UrlsUpdater.update_condition.call(url)
      #   # => false
      #
      def update_condition
        @update_condition || -> (object) { true }
      end
    end
  end
end
