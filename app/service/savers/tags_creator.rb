module Savers
  # A class responsible for creating tags.
  class TagsCreator < BaseCreator
    creates Tag
    permits :title
  end
end
