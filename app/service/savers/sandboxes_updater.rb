module Savers
  # A class responsible for updating sandboxes.
  class SandboxesUpdater < BaseUpdater
    permits :title, :color, :slug, :base_date, :date_offset, :published, :proofread,
      owner_ids: [], category_ids: [], participant_ids: []
    
    converts_params do |params|
      normalize_array(params, :category_ids)
      normalize_array(params, :participant_ids)
      normalize_array(params, :owner_ids)
    end
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Sandbox'
        snapshot.author_id    = editor.id
        snapshot.action       = 'sandbox.updated'
        snapshot.data         = object.attributes
      end
    end
  end
end
