module Savers
  # A class responsible for updating links.
  class LinksUpdater < BaseUpdater
    permits :section, :position
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Link'
        snapshot.author_id    = editor.id
        snapshot.action       = 'link.updated'
        snapshot.data         = object.attributes
      end
    end
  end
end
