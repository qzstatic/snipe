module Savers
  # A class responsible for creating taggings.
  class TaggingsCreator < BaseCreator
    creates Tagging
    permits :document_id, :type, tag_ids: []
    
    converts_params do |params|
      normalize_array(params, :tag_ids)
    end
    
    # Initializes the creator putting the `document` id into params
    # and calling `BaseCreator#initialize`.
    #
    # @param [Hash] tagging_params the tagging's parameters
    # @param [Editor] editor the editor creating the tagging
    # @param [Document] document the document owning the tagging
    #
    def initialize(tagging_params, editor, document)
      params = tagging_params.merge(document_id: document.id)
      super(params, editor)
      object.document.add_participant(editor.id)
    end
    
    # Saves the tagging.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      object.document.save
      super
    end
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Tagging'
        snapshot.author_id    = editor.id
        snapshot.action       = 'tagging.created'
        snapshot.data         = object.attributes
      end
    end
  end
end
