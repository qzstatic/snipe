module Savers
  # A class responsible for creating documents.
  class DocumentsCreator < BaseCreator
    creates Document
    depends_on :category_ids
    permits :title, milestones: { created: [:at, :by] }, participant_ids: []
    
    converts_params do |params|
      normalize_array(params, :category_ids)
      
      params[:milestones] = {
        created: {
          at: Time.now.to_i,
          by: editor.id
        }
      }
    end
    
    after_initialize do
      object.add_participant(editor.id)
    end
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Document'
        snapshot.author_id    = editor.id
        snapshot.action       = 'document.created'
        snapshot.data         = object.attributes
      end
    end
  end
end
