module Savers
  # A class responsible for updating attachments.
  class AttachmentsUpdater < BaseUpdater
    permits :type, :directory
  end
end
