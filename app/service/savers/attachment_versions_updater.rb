module Savers
  # A class responsible for updating attachment versions.
  class AttachmentVersionsUpdater < ::Savers::BaseUpdater
    depends_on :attachment_id
    permits :slug, :filename
  end
end
