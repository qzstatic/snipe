module Savers
  # A class responsible for updating pages.
  class PagesUpdater < BaseUpdater
    permits :title, :slug, :position, list_ids: []
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Page'
        snapshot.author_id    = editor.id
        snapshot.action       = 'page.updated'
        snapshot.data         = object.attributes
      end
    end
  end
end
