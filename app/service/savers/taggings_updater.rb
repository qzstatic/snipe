module Savers
  # A class responsible for updating taggings.
  class TaggingsUpdater < BaseUpdater
    permits :type, tag_ids: []
    
    converts_params do |params|
      normalize_array(params, :tag_ids)
    end
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Tagging'
        snapshot.author_id    = editor.id
        snapshot.action       = 'tagging.updated'
        snapshot.data         = object.attributes
      end
    end
  end
end
