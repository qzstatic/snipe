module Savers
  # A class responsible for creating urls.
  class UrlsCreator < BaseCreator
    creates Url
    depends_on :document_id
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Url'
        snapshot.author_id    = editor.id
        snapshot.action       = 'url.created'
        snapshot.data         = object.attributes
      end
    end
    
    # Initializes the creator putting the `document` id into params
    # and calling `BaseCreator#initialize`.
    #
    # @param [Hash] url_params the url's parameters
    # @param [Editor] editor the editor creating the url
    # @param [Document] document the document owning the url
    #
    def initialize(url_params, editor, document)
      params = url_params.merge(document_id: document.id)
      super(params, editor)
      object.document.add_participant(editor.id)
    end
    
    # Render URL to `rendered` attribute and saves record.
    #
    # @return [true, false]
    #
    # @see BaseCreator#save
    #
    def save
      object.render
      object.document.save
      super
    end
  end
end
