module Savers
  # A class responsible for creating urls.
  class RawUrlsCreator < BaseCreator
    creates Url
    depends_on :document_id
    permits :rendered
    
    # Initializes the creator putting the `document` id into params
    # and calling `BaseCreator#initialize`.
    #
    # @param url_params [Hash] the url's parameters
    # @param editor [Editor] the editor creating the url
    # @param document [Document] the document owning the url
    #
    def initialize(url_params, editor, document)
      params = url_params.merge(document_id: document.id)
      super(params, editor)
    end
    
    # Fixes URL and saves record.
    #
    # @return [true, false]
    #
    # @see BaseCreator#save
    #
    def save
      object.fix
      super
    end
  end
end
