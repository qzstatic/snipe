module Savers
  # A class responsible for updating document parts.
  class DocumentPartsUpdater < BaseUpdater
    attr_reader :part, :meta
    private :part, :meta
    
    # Initializes the updater.
    #
    # @param args [Array] usual updater params
    # @param part [Hash] the document part
    # @param meta [Hash] request's meta data
    #
    def initialize(*args, part:, meta:)
      super(*args)
      @part = part
      @meta = meta
    end
    
    # Saves the document unless the request is considered stale.
    #
    # @return [undefined]
    #
    def save
      super unless stale_request?
    end

    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Document'
        snapshot.author_id    = editor.id
        snapshot.action       = 'document.updated'
        snapshot.data         = object.attributes
      end
    end
    
    # Checks if the request is stale.
    #
    # @return [true] if the request is stale
    # @return [false] otherwise
    #
    def stale_request?
      if !object[part].key?('updated_at')
        false
      elsif meta.key?(:last_modified)
        Time.parse(meta[:last_modified]).to_i != Time.parse(object[part]['updated_at']).to_i
      else
        true
      end
    end
  end
end
