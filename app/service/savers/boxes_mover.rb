module Savers
  # A class responsible for move boxes.
  class BoxesMover < BaseUpdater

    attr_reader :updated_at, :object

    def initialize(object, position, editor, updated_at)
      @object = object
      @document = object.document.porcupined
      @position = position
      @editor = editor
      @updated_at = Time.parse(updated_at.to_s) rescue nil
    end

    def save
      if stale_request?
        object.transaction do
          object.record_timestamps = false
          object.position = @position
          if object.save
            @document.box_move_editor_id = editor.id
            @document.box_move_at = Time.current.as_json
            create_snapshots if @document.save
          else
            raise ActiveRecord::Rollback
          end
          object.record_timestamps = true
        end
      end
    end

    def stale_request?
      return true unless @document.box_move_at
      updated_at.to_i == ( Time.parse(@document.box_move_at.to_s) rescue nil ).to_i
    end

  private
    def create_snapshots
      Snapshot.transaction do
        Snapshot.create! do |snapshot|
          snapshot.subject_id   = object.id
          snapshot.subject_type = 'Box'
          snapshot.author_id    = editor.id
          snapshot.action       = 'box.moved'
          snapshot.data         = object.attributes
        end
        Snapshot.create! do |snapshot|
          snapshot.subject_id   = @document.id
          snapshot.subject_type = 'Document'
          snapshot.author_id    = editor.id
          snapshot.action       = 'box.moved'
          snapshot.data         = @document.attributes
        end
      end
    end
  end
end
