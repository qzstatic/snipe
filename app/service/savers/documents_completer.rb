module Savers
  # A class responsible for marking documents as completed.
  class DocumentsCompleter < Base
    # Initializes the completer instance.
    #
    # Sets the corresponding document attributes if the editor trying to mark
    # the document as completed is the editor who was assigned to it.
    #
    # @param [ActiveRecord::Base] object the object to mark as completed
    # @param [Editor] editor the editor trying to complete the document
    #
    def initialize(object, editor)
      @object = porcupine(object)
      @editor = editor
      
      assign_editor if applicable? && editor_allowed?
    end
    
    # Saves the object to the database.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      create_snapshot if object.save
      object.valid?
    end
    
  private
    def create_snapshot
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Document'
        snapshot.author_id    = editor.id
        snapshot.action       = 'document.completed'
        snapshot.data         = object.attributes
      end
    end
    
    def assign_editor
      object.assignee_status = :finished
      
      object.milestones = object.milestones.merge(
        completed: {
          at: Time.now.to_i,
          by: editor.id
        }
      )
      
      object.add_participant(editor.id)
    end
    
    def applicable?
      object.respond_to?(:assignee_status)
    end
    
    def editor_allowed?
      editor.id == object.assigned_editor_id
    end
  end
end
