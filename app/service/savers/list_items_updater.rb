module Savers
  # A class responsible for updating list items.
  class ListItemsUpdater < BaseUpdater
    permits :top
    
    after_initialize do
      object.set_effective_timestamp
    end
    
    def save
      if super
        Hooks::Lists::CacheUpdate.new(object.list).save
        create_snapshot
      end
    end
    
    def create_snapshot
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'ListItem'
        snapshot.author_id    = editor.id
        snapshot.action       = 'list_item.updated'
        snapshot.data         = object.attributes
      end
    end
  end
end
