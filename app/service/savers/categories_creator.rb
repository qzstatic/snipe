module Savers
  # A class responsible for creating categories.
  class CategoriesCreator < BaseCreator
    creates Category
    permits :slug, :title, :position, :parent_id,
      setting_keys: [], nested_categories: [], forbidden_combinations: []
    
    converts_params do |params|
      normalize_array(params, :setting_keys)
      
      if params.key?(:setting_keys)
        params[:setting_keys] = params[:setting_keys].reject(&:empty?)
      end
    end
    
    after_initialize do
      object.recalculate_slug_path
    end
  end
end
