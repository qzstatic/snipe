module Savers
  # A class responsible for creating attachments.
  class AttachmentsMultiUpdater
    attr_reader :editor, :attachment, :attachment_versions, :params
    
    # Initializes the updater instance.
    #
    # @param [Attachment] object attachment to update
    # @param [Hash] params the object's parameters
    # @param [Editor] editor the editor creating the object
    #
    def initialize(object, params, editor)
      @editor = editor
      @params = params
      @attachment = Savers::AttachmentsUpdater.new(object, params, editor)
    end
    
    # Returns the attachment object.
    #
    # @return [Attachment] attachment object
    #
    def object
      @attachment.object
    end
    
    # Returns the object's errors.
    #
    # @return [ActiveModel::Errors] the errors
    #
    def errors
      object.errors
    end
    
    # Saves the attachment.
    #
    # @return [undefined]
    #
    def save
      if @attachment.save
        build_attachment_versions
        
        @attachment_versions.each do |attachment_version|
          unless attachment_version.save
            @attachment.errors.add(:versions, attachment_version.errors.as_json)
            return false
          end
        end
        
        return true
      end
      
      return false
    end
    
  private
    def build_attachment_versions
      @attachment_versions = []
      @params[:versions] ||= {}
      
      @params[:versions].each do |version_slug, version_params|
        version_params[:slug] = version_slug
        version_params = ActionController::Parameters.new(version_params)
        
        version_object = find_version(@attachment.object.id, version_slug)
        
        if version_object
          version_saver = Savers::AttachmentVersionsUpdater.new(version_object, version_params, editor)
        else
          version_saver = Savers::AttachmentVersionsCreator.new(version_params, editor, object)
        end
        
        @attachment_versions.push(version_saver)
      end
    end
    
    def find_version(attachment_id, slug)
      version = AttachmentVersion.where(
        attachment_id: attachment_id,
        slug: slug
      ).first
      if version
        return version.porcupined
      end
      return nil
    end
  end
end
