module Savers
  # A class responsible for updating categories.
  class CategoriesUpdater < BaseUpdater
    permits :slug, :title, :position, :enabled,
      setting_keys: [], nested_categories: [], forbidden_combinations: []
    
    converts_params do |params|
      normalize_array(params, :setting_keys)
      
      if params.key?(:setting_keys)
        params[:setting_keys] = params[:setting_keys].reject(&:empty?)
      end
    end
    
    after_initialize do
      object.recalculate_slug_path
    end
    
    # Saves the category and recalculates and saves slug path
    # in all it's descendants.
    #
    # @return [undefined]
    #
    def save
      setting_keys_changed = object.setting_keys_changed?
      
      if super
        object.async_regenerate_setting_keys_cache if setting_keys_changed
        
        object.descendants.each do |descendant|
          descendant.recalculate_slug_path
          descendant.save
        end
      end
    end
  end
end
