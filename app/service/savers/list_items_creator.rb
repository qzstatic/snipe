module Savers
  # A class responsible for creating list items.
  class ListItemsCreator < BaseCreator
    creates ListItem
    permits :list_id, :document_id, :top
    
    after_initialize do
      object.set_effective_timestamp
    end
    
    # Initializes the creator putting the `list` id into params
    # and calling `BaseCreator#initialize`.
    #
    # @param [Hash] item_params the item's parameters
    # @param [Editor] editor the editor creating the item
    # @param [List] list the list owning the item
    #
    def initialize(item_params, editor, list)
      params = item_params.merge(list_id: list.id)
      super(params, editor)
    end
    
    def save
      if super
        Hooks::Lists::CacheUpdate.new(object.list).save
        create_snapshot
      end
    end
    
    def create_snapshot
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'ListItem'
        snapshot.author_id    = editor.id
        snapshot.action       = 'list_item.created'
        snapshot.data         = object.attributes
      end
    end
  end
end
