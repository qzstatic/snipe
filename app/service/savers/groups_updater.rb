module Savers
  # A class responsible for updating groups.
  class GroupsUpdater < BaseUpdater
    permits :slug, :title, :position
  end
end
