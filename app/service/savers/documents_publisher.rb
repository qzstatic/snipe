module Savers
  # A class responsible for publishing documents.
  class DocumentsPublisher < Base
    # Initializes the publisher instance.
    #
    # Assigns the parameters to the document if needed.
    #
    # @param [ActiveRecord::Base] object the object being published
    # @param [Hash] object_params the object's parameters
    # @param [Editor] editor the editor publishing the object
    #
    def initialize(object, object_params, editor)
      @object = object
      @editor = editor
      
      @object.errors.add_on_blank(:url_id) if object.settings[:urls]
      return unless @object.errors.empty?

      Callbacks::Processor.new('documents.before_publish').process(@object)

      assign(object_params)
    end
    
    # Saves the object to the database.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      create_snapshot if object.save
      object.valid?
    end

  private
    def create_snapshot
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Document'
        snapshot.author_id    = editor.id
        snapshot.action       = 'document.published'
        snapshot.data         = object.attributes
      end
    end
    
    def assign(params)
      object.published = true
      
      if params[:published_at]
        object.published_at = params[:published_at]
      elsif object.published_at.nil?
        object.published_at = Time.now
      end
      
      enable_boxes
      process_with_pipelines
      update_list_items
      assign_milestones
      fix_urls
      object.add_participant(editor.id)
      update_list_cache
    end
    
    def enable_boxes
      if object.changes.key?(:published)
        object.boxes
      else
        object.boxes.where(type: 'paragraph')
      end
    end
    
    def process_with_pipelines
      DocumentsPipeliner.new(document: object).process
    end
    
    def update_list_items
      Hooks::Documents::ListItems.new(object).save
    end
    
    def assign_milestones
      object.milestones = object.milestones.merge(
        published: {
          at: Time.now.to_i,
          by: editor.id
        }
      )
    end
    
    def fix_urls
      object.urls.each do |url|
        url.fix
        url.save
      end
    end
    
    def update_list_cache
      Hooks::Documents::ListCache.new(object).save
    end
  end
end
