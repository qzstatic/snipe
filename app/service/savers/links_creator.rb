module Savers
  # A class responsible for creating links.
  class LinksCreator < BaseCreator
    creates Link
    
    permits :document_id, :section, :bound_document_id, :position
    
    # Initializes the creator putting the `document` id into params
    # and calling `BaseCreator#initialize`.
    #
    # @param [Hash] link_params the link's parameters
    # @param [Editor] editor the editor creating the link
    # @param [Document] document the document owning the link
    #
    def initialize(link_params, editor, document)
      params = link_params.merge(document_id: document.id)
      super(params, editor)
      object.dependencies.build(
        dependable_id: object.bound_document_id,
        dependable_type: 'Document'
      )
      object.document.add_participant(editor.id)
    end
    
    # Saves the link.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      object.document.save
      super
    end
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Link'
        snapshot.author_id    = editor.id
        snapshot.action       = 'link.created'
        snapshot.data         = object.attributes
      end
    end
  end
end
