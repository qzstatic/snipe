module Savers
  # A class responsible for processing a document with all available pipelines.
  class DocumentsPipeliner
    include Anima.new(:document)
    
    # Available pipelines.
    PIPELINES = [
      Pipeline::Document,
      Pipeline::Taggings,
      Pipeline::Links,
      Pipeline::Boxes
    ].freeze
    
    # Process the document with pipelines.
    #
    # @param preview [true, false] preview mode (generate `packed_preview`
    #   instead of the usual processing)
    #
    # @return [undefined]
    #
    def process(preview: false)
      if preview
        document.update(packed_preview: packed_preview)
      else
        PIPELINES.each do |pipeline|
          pipeline.new(document).process
        end
      end
    end
    
    # Returns the document serialized with associated taggings, links, boxes.
    #
    # @return [Hash] packed document preview
    #
    def packed_preview
      Pipeline::Document.new(document).serialize.merge(
        taggings: Pipeline::Taggings.new(document).serialize,
        links:    Pipeline::Links.new(document).serialize,
        boxes:    Pipeline::Boxes.new(document).serialize(preview: true).values
      )
    end
  end
end
