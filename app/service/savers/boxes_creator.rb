module Savers
  # A class responsible for creating boxes.
  class BoxesCreator < BaseCreator
    creates Box
    depends_on :type, :parent_id
    permits :position, :enabled
    
    after_initialize do
      object.document.add_participant(editor.id)
      reset_proofread_status
    end
    
    converts_params do |params|
      params[:body].gsub!("\u0003", '') if params.key?(:body) && params[:body].respond_to?(:gsub!)
    end
    
    # Saves the box.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      object.document.save
      super
    end
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Box'
        snapshot.author_id    = editor.id
        snapshot.action       = 'box.created'
        snapshot.data         = object.attributes
      end
    end
    
  private
    def reset_proofread_status
      ProofreadReset.new(porcupine(object.document), editor).reset
    end
  end
end
