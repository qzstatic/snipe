module Savers
  # A class responsible for creating attachment versions.
  class AttachmentVersionsCreator < ::Savers::BaseCreator
    creates AttachmentVersion
    depends_on :attachment_id
    permits :slug, :filename
    
    # Initializes the creator putting the `attachment` id into params
    # and calling `BaseCreator#initialize`.
    #
    # @param [Hash] version_params the version's parameters
    # @param [Editor] editor the editor creating the version
    # @param [Document] attachment the attachment owning the version
    #
    def initialize(version_params, editor, attachment)
      params = version_params.merge(attachment_id: attachment.id)
      super(params, editor)
    end
  end
end
