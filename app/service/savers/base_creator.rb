module Savers
  # A base class for all creators.
  class BaseCreator < ::Savers::Base
    # Initializes the creator instance.
    #
    # Saves the editor, converts the parameters and builds a new object
    # (w/o saving to the database).
    #
    # @param [Hash] object_params the object's parameters
    # @param [Editor] editor the editor creating the object
    #
    def initialize(object_params, editor)
      params  = Params.new(object_params)
      @editor = editor
      
      convert_params(params)
      @object = build_object(params)
      @object.assign_attributes(permit(params))
      post_initialize
    end
    
    # Saves the object to the database in the creator's zone.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      post_process if object.save
      
      object.valid?
    end
    
    # Saves and returns the object.
    #
    # @return [ActiveRecord::Base] object
    #
    def saved_object
      save
      object
    end
    
  private
    def build_object(params)
      key_attributes = params.extract(self.class.key_attributes)
      porcupine(self.class.object_class.new(key_attributes))
    end
    
    class << self
      # Declares the created object's class.
      #
      # @param [Class] object_class the object's class
      #
      # @return [undefined]
      #
      # @example
      #   module Savers
      #     class DocumentsCreator < BaseCreator
      #       creates Document
      #     end
      #   end
      #
      def creates(object_class)
        @object_class = object_class
      end
      
      # Returns the created object's class.
      #
      # @return [Class] the object's class
      #
      attr_reader :object_class
    end
  end
end
