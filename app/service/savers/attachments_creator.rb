module Savers
  # A class responsible for creating attachments.
  class AttachmentsCreator < BaseCreator
    creates Attachment
    permits :type, :directory
  end
end
