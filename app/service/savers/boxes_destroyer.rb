module Savers
  # A class responsible for destroy boxes.
  class BoxesDestroyer < BaseUpdater
    permits :deleted

    after_initialize do
      object.document.add_participant(editor.id)
    end
    
    def initialize(box, editor)
      super(box, ActionController::Parameters.new(deleted: true), editor)
    end
    
    # Saves the box.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      object.document.save
      super
    end
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Box'
        snapshot.author_id    = editor.id
        snapshot.action       = 'box.destroyed'
        snapshot.data         = object.attributes
      end
    end
  end
end
