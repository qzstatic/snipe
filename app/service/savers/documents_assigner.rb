module Savers
  # A class responsible for assigning editors to documents.
  class DocumentsAssigner < Base
    # Initializes the assigner instance.
    #
    # Sets the document attributes corresponding to the assigned editor.
    #
    # @param [ActiveRecord::Base] object the object to assign to
    # @param [Hash] object_params parameters for assigning
    # @param [Editor] editor the editor who is assigning
    #
    def initialize(object, object_params, editor)
      @object = porcupine(object)
      @editor = editor
      
      if applicable? && assigned_editor_id = object_params[:editor_id]
        assign(assigned_editor_id)
      end
    end
    
    # Saves the object to the database.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      create_snapshot if object.save
      object.valid?
    end
    
  private
    def create_snapshot
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Document'
        snapshot.author_id    = editor.id
        snapshot.action       = 'document.assigned'
        snapshot.data         = object.attributes
      end
    end
    
    def assign(assigned_editor_id)
      object.assignee_status = :enabled
      object.assigned_editor_id = assigned_editor_id
      
      object.milestones = object.milestones.merge(
        assigned: {
          at: Time.now.to_i,
          by: assigned_editor_id
        }
      )
      
      object.add_participant(assigned_editor_id)
    end
    
    def applicable?
      object.respond_to?(:assignee_status)
    end
  end
end
