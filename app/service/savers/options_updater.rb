module Savers
  # A class responsible for updating options.
  class OptionsUpdater < BaseUpdater
    depends_on :type
    permits :position
  end
end
