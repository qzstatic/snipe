module Savers
  # A class responsible for creating editors.
  class EditorsCreator < BaseCreator
    creates Editor
    permits :login, :email, :password, :first_name, :last_name, :enabled, group_ids: []
  end
end
