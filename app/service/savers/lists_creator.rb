module Savers
  # A class responsible for creating lists.
  class ListsCreator < BaseCreator
    creates List
    depends_on :category_ids
    permits :slug, :title, :duration, :limit, :width, exclude_list_ids: [], blacklisted_category_ids: []
    
    converts_params do |params|
      normalize_array(params, :category_ids)
      normalize_array(params, :exclude_list_ids)
      normalize_array(params, :blacklisted_category_ids)
      params[:width] = params.delete(:cols)
      
      if params.key?(:category_ids)
        params[:blacklisted_category_ids] ||= []
        params[:blacklisted_category_ids] += params[:category_ids].select { |id| id < 0 }.map(&:abs)
        params[:blacklisted_category_ids] = params[:blacklisted_category_ids].uniq
        params[:category_ids] = params[:category_ids].select { |id| id > 0 }
      end
    end
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'List'
        snapshot.author_id    = editor.id
        snapshot.action       = 'list.created'
        snapshot.data         = object.attributes
      end
    end
  end
end
