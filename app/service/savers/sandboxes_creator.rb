module Savers
  # A class responsible for creating sandboxes.
  class SandboxesCreator < BaseCreator
    creates Sandbox
    permits :title, :color, :slug, :base_date, :date_offset, :published, :proofread,
      owner_ids: [], category_ids: [], participant_ids: []
    
    converts_params do |params|
      normalize_array(params, :category_ids)
      normalize_array(params, :participant_ids)
      
      params[:owner_ids] ||= []
      params[:owner_ids] |= [editor.id]
    end
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Sandbox'
        snapshot.author_id    = editor.id
        snapshot.action       = 'sandbox.created'
        snapshot.data         = object.attributes
      end
    end
  end
end
