module Savers
  # A class responsible for updating urls.
  class UrlsUpdater < BaseUpdater
    depends_on :document_id
    
    updates_if do |url|
      !url.fixed?
    end
    
    after_initialize do
      object.document.add_participant(editor.id)
    end
    
    after_save do |object, editor|
      Snapshot.create! do |snapshot|
        snapshot.subject_id   = object.id
        snapshot.subject_type = 'Url'
        snapshot.author_id    = editor.id
        snapshot.action       = 'url.updated'
        snapshot.data         = object.attributes
      end unless object.fixed?
    end
    
    # Renders the URL to `rendered` attribute and saves the record.
    #
    # @return [true, false]
    #
    # @see BaseUpdater#save
    #
    def save
      object.render
      object.document.save
      super
    end
  end
end
