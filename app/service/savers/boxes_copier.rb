module Savers
  # A class responsible for making copies of existing boxes.
  class BoxesCopier < BaseCreator
    creates Box
    depends_on :type
    permits :position, :enabled, :parent_id, :original_id
    
    converts_params do |params|
      params[:type] = :copy
    end
    
    # Initializes the creator putting the original box id into params
    # and calling `BaseCreator#initialize`.
    #
    # @param box_params [Hash] the box's parameters
    # @param editor [Editor] the editor copying the box
    # @param original [Document] the original to copy
    #
    def initialize(box_params, editor, original)
      params = box_params.merge(original_id: original.id)
      super(params, editor)
    end
  end
end
