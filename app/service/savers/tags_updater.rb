module Savers
  # A class responsible for updating tags.
  class TagsUpdater < BaseUpdater
    permits :title
  end
end
