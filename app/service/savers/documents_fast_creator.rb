module Savers
  # A class responsible for fast creating documents.
  class DocumentsFastCreator
    attr_reader :editor, :documents_creator, :urls_creator, :params, :savers
    
    # Initializes the fast creator instance.
    #
    # @param [Hash] params the object's parameters
    # @param [Editor] editor the editor creating the object
    #
    def initialize(params, editor)
      @editor = editor
      @params = params
      @savers = [
        :build_documents_creator,
        :build_raw_urls_creator,
        :build_documents_updater,
        :build_documents_publisher
      ]
    end
    
    # Returns the document object.
    #
    # @return [Document] document object
    #
    def object
      @documents_creator.object
    end
    
    # Returns the object's errors.
    #
    # @return [ActiveModel::Errors] the errors
    #
    def errors
      @errors ||= ActiveModel::Errors.new(object)
    end
    
    # Saves the document with publisher, url.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      Document.transaction do
        @savers.each do |builder|
          saver = self.send(builder)
          unless saver.save
            errors.add(:base, saver.errors.as_json)
            break
          end
        end
        raise ActiveRecord::Rollback unless errors.empty?
      end
      if errors.empty?
        object.reload
        return true
      else
        false
      end
    end
    
  private
    def build_documents_creator
      if params[:category_ids]
        category_ids = params[:category_ids]
      elsif params[:category_slugs]
        slugs = params[:category_slugs]
        category_ids = Category.where(slug: slugs).pluck(:id)
      else
        category_ids = []
      end
      
      document_params = ActionController::Parameters.new(
        title: @params[:title],
        category_ids: category_ids,
        source_id: @params[:source_id],
        source_url: @params[:source_url],
        source_title: @params[:source_title]
      )
      @documents_creator = Savers::DocumentsCreator.new(document_params, @editor)
    end
    
    def build_raw_urls_creator
      url_params = ActionController::Parameters.new(rendered: @params[:url])
      @urls_creator = Savers::RawUrlsCreator.new(url_params, @editor, object)
    end
    
    def build_documents_updater
      document_params = ActionController::Parameters.new(url_id: @urls_creator.object.id)
      Savers::DocumentsUpdater.new(object, document_params, @editor)
    end
    
    def build_documents_publisher
      publish_params = ActionController::Parameters.new(published_at: @params[:published_at])
      Savers::DocumentsPublisher.new(object, publish_params, @editor)
    end

    def list_updater
      Hooks::Documents::ListItems.new(object).save
      Hooks::Documents::ListCache.new(object).save
    end
  end
end
