# A class responsible for managing sessions.
#
# It can authorize requests, create/destroy tokens in redis,
# log the session to PG and collect the tokens data for rendering
# in JSON on login.
#
class SessionsManager
  # Returns editor.
  #
  # @return [Editor]
  #
  attr_reader :editor
  
  # Returns the access token.
  #
  # @return [Token::Editor::Access]
  #
  attr_reader :access_token
  
  # Returns the refresh token.
  #
  # @return [Token::Editor::Refresh]
  #
  attr_reader :refresh_token
  
  # Creates an instance with an HTTP request.
  #
  # @param [Rack::Request] request an HTTP request
  #
  def initialize(request)
    @request = request
    authenticate!
  end
  
  # Checks if the current session is valid (by ensuring the session manager
  # has an associated editor).
  #
  # @return [true] if the session is valid
  # @return [false] otherwise
  #
  def valid?
    editor.present? && editor.enabled?
  end
  
  # Returns the tokens data to be passed to the client as JSON.
  #
  # @return [Hash] the tokens data
  #
  def tokens_data
    {
      access_token:  access_token.token,
      editor_id:     editor.id,
      expires_in:    access_token.ttl,
      refresh_token: refresh_token.token
    }
  end
  
  # Opens the session creating both tokens and a session.
  #
  # @return [undefined]
  #
  def open_session
    create_tokens!
    create_session!
  end
  
  # Closes the session deleting tokens from redis.
  #
  # @return [undefined]
  #
  def close_session
    destroy_tokens!
  end
  
  # Closes the current session and opens a new one.
  #
  # @return [undefined]
  #
  def refresh_session
    close_session
    open_session
  end
  
private
  def authenticate!
    raise NotImplementedError, '#authenticate! must be implemented in a subclass.'
  end
  
  def create_tokens!
    @access_token  = Token::Editor::Access.create(editor.id)
    @refresh_token = Token::Editor::Refresh.create(editor.id)
  end
  
  def destroy_tokens!
    access_token.destroy! if access_token
    refresh_token.destroy!
    
    @access_token = @refresh_token = nil
  end
  
  def create_session!
    Session.create! do |session|
      session.editor_id    = editor.id
      session.access_token  = access_token.token
      session.refresh_token = refresh_token.token
      session.expires_in    = access_token.ttl
      session.ip            = @request.ip
    end
  end
end
