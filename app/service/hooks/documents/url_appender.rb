module Hooks
  module Documents
    # A class responsible for append main url id.
    class UrlAppender < Hooks::Base
      after_initialize do
        object.url_id = url.id
      end      
    end
  end
end
