module Hooks
  module Documents
    # A class responsible for add participant.
    class AddParticipant < Hooks::Base
      after_initialize do
        object.add_participant(editor.id)
      end
    end
  end
end
