module Hooks
  module Documents
    # A class responsible for fix urls.
    class FixUrls < Hooks::Base
      def save
        object.urls.each do |url|
          url.fix
          url.save
        end
      end
    end
  end
end
