module Hooks
  module Documents
    # A class responsible for add update list cache.
    class ListCache < Hooks::Base
      def save
        [object.lists + object.cached_lists].flatten.uniq(&:id).each do |list|
          Hooks::Lists::CacheUpdate.new(list).save
        end
      end
    end
  end
end
