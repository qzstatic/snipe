module Hooks
  module Documents
    # A class responsible for add update list cache.
    class ListItems < Hooks::Base
      def save
        if object.published?
          object.lists.each do |list|
            unless list.matches?(object)
              ListItem.where(document_id: object.id, list_id: list.id).destroy_all
            end
          end

          object.matching_lists.where.not(id: object.exclude_list_ids).each do |list|
            item = list.items.where(document_id: object.id).first_or_initialize
            item.set_effective_timestamp
            item.save
          end
          object.reload
        else
          object.list_items.destroy_all
        end
      end
    end
  end
end
