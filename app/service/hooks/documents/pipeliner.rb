module Hooks
  module Documents
    # A class responsible for run pipeline processor.
    class Pipeliner < Hooks::Base
      def save
        [
          Pipeline::Document,
          Pipeline::Taggings,
          Pipeline::Links,
          Pipeline::Boxes
        ].each do |pipeline|
          pipeline.new(object).process
        end
      end
    end
  end
end
