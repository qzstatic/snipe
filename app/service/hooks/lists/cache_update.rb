module Hooks
  module Lists
    # A class responsible for updating the list's cache.
    class CacheUpdate < Hooks::Base
      after_initialize do
        count = object.limit + object.excluded_lists.pluck(:limit).inject(:+).to_i
        object.cached_document_ids = object.documents.limit(count).pluck(:id)
      end
    end
  end
end
