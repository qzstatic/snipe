module Hooks
  module Lists
    # A class responsible for update list_items of list.
    class ListItemsUpdate < Hooks::Base
      def save
        level = Rails.logger.level
        Rails.logger.level = 0
        
        object.items.where.not(document_id: object.matching_documents.select(:id)).delete_all
        
        object.items.where('top > 0').each do |list_item|
          list_item.update_effective_timestamp
        end
        
        select_sql = object.matching_documents
                           .where.not(id: ListItem.select(:document_id).where(list_id: object.id))
                           .select(Arel::SqlLiteral.new(object.id.to_s), :id, :published_at)
                           .to_sql
        table_list_items = ListItem.arel_table
        manager = Arel::InsertManager.new(table_list_items.engine)
        manager.into table_list_items
        manager.columns << table_list_items[:list_id]
        manager.columns << table_list_items[:document_id]
        manager.columns << table_list_items[:effective_timestamp]
        insert_sql = manager.to_sql + ' ' + select_sql
        ActiveRecord::Base.connection.execute(insert_sql)
        
        Rails.logger.level = level
      end
    end
  end
end

