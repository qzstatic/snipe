module Hooks
  # A base class for all hooks.
  class Base
    include Hooks::AfterInitialize

    # Returns the object being saved.
    #
    # @return [ActiveRecord::Base] object
    #
    attr_reader :object

    # Return list of names of available models
    #
    # @return [Array<Symbol>] init_attributes
    #
    attr_reader :init_attributes
    
    # Saves the object to the database.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      object.save
    end

    # Returns the object's errors.
    #
    # @return [ActiveModel::Errors] the errors
    #
    def errors
      object.errors
    end
  private
    def initialize(*args)
      @object, @init_attributes = *args
      post_initialize
    end

    def method_missing(method_name, *args, &block)
      init_attributes.fetch(method_name) { super }
    end
  end
end
