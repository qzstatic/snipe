module Hooks
  # A module define converts helper.
  module ConvertsParams
    def self.included(base)
      base.extend(ClassMethods)
    end

    def convert_params
      instance_eval(&self.class.params_converter)
    end

    module ClassMethods
      def converts_params(&block)
        fail ArgumentError unless block_given?
        @params_converter = block
      end
      
      def params_converter
        @params_converter || -> (*) {}
      end
    end
  end
end
