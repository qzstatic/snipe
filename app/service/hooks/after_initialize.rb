module Hooks
  # A module define after initialize helper.
  module AfterInitialize
    def self.included(base)
      base.extend(ClassMethods)
    end

    def post_initialize
      instance_eval(&self.class.post_initializer)
    end
    
    module ClassMethods
      def after_initialize(&block)
        fail ArgumentError unless block_given?
        @post_initializer = block
      end
      
      def post_initializer
        @post_initializer || -> (*) {}
      end
    end
  end
end
