# A class used to render the diff between snapshots in HTML format.
class Differ
  # Old snapshot.
  #
  # @return [Snapshot]
  #
  attr_reader :old_snapshot
  
  # New snapshot.
  #
  # @return [Snapshot]
  #
  attr_reader :new_snapshot
  
  # Initializes the differ with 2 snapshots.
  #
  # @param old_snapshot [Snapshot] snapshot taken earlier
  # @param new_snapshot [Snapshot] snapshot taken later
  #
  def initialize(old_snapshot, new_snapshot)
    @old_snapshot = old_snapshot
    @new_snapshot = new_snapshot
  end
  
  # Renders the diff between snapshots' data in YAML.
  #
  # @return [String] the diff
  #
  def diff
    Diffy::Diff.new(
      Snapshot.full_document_at(old_snapshot).to_yaml,
      Snapshot.full_document_at(new_snapshot).to_yaml,
      include_plus_and_minus_in_html: true,
      context: 3
    ).to_s(:html)
  end
end
