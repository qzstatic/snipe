# A simple object responsible for resetting document's proofread status.
class ProofreadReset
  include Concord.new(:document, :editor)
  
  # Resets the proofread status if document is proofreadable and current
  # editor is not a proofreader.
  #
  # @return [undefined]
  #
  def reset
    return unless needs_reset?
    
    document.proofread = false
    document.milestones = document.milestones.except(:proofread)
  end
  
private
  def needs_reset?
    document.respond_to?(:proofread) && !editor_is_proofreader?
  end
  
  def editor_is_proofreader?
    if group_slug = document.settings.fetch(:proofreading, {})[:group]
      editor.groups.where(slug: group_slug).exists?
    end
  end
end
