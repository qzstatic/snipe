module Handlers
  # A base class for all hanlders.
  class Base
    include Hooks::AfterInitialize
    include Hooks::ConvertsParams

    # Returns the object being saved.
    #
    # @return [ActiveRecord::Base] object
    #
    attr_reader :object

    # Returns incoming parameters.
    #
    # @return [ActionController::Parameters] params
    #
    attr_reader :params

    # Return list of names of available models
    #
    # @return [Array<Symbol>] init_attributes
    #
    attr_reader :init_attributes

    # Saves the object to the database.
    #
    # @return [true] if object was successfully saved
    # @return [false] otherwise
    #
    def save
      object.save
    end

    # Returns the object's errors.
    #
    # @return [ActiveModel::Errors] the errors
    #
    def errors
      object.errors
    end

    # Return the key of error namespace for current object
    #
    # @return [<Symbol>] the name
    #
    def error_key
      self.class.object_name
    end
  
  private
    def initialize(*args)
      @object, @params, @init_attributes = *args
      convert_params
      assign_key_attributes
      @object = porcupine(@object)
      assign_attributes
      post_initialize
    end

    def method_missing(method_name, *args, &block)
      init_attributes.fetch(method_name) { super }
    end

    def porcupine(object)
      if object.respond_to?(:porcupined)
        object.porcupined
      else
        object
      end
    end
    
    def assign_attributes
      object.assign_attributes(permitted_params)
    end

    def assign_key_attributes
      object.assign_attributes(key_params)
    end
    
    def key_params
      object_params.extract!(*self.class.key_attributes).permit!
    end

    def permitted_params
      object_params.permit(permitted_attributes)
    end

    def permitted_attributes
      if object.respond_to?(:dynamic_attributes_names)
        self.class.permitted_static_attributes + object.dynamic_attributes_names
      else
        self.class.permitted_static_attributes
      end
    end

    def object_params
      @object_params ||= params.require(self.class.object_name)
    end

    def normalize_array(*attrs)
      attrs.each do |attribute|
        if object_params.key?(attribute) && object_params[attribute].nil?
          object_params[attribute] = []
        end
      end
    end

    class << self
      # Sets the name of the current object to obtain the parameters
      #
      # @param [<Symbol>] name the name of object
      #
      # @return [undefined]
      #
      def set_object_name(name)
        @object_name = name
      end

      # Return the name of the current object
      #
      # @return [<Symbol>]
      #
      def object_name
        raise ArgumentError, 'Object name not defined' if @object_name.nil?
        @object_name
      end

      # Declares the key attributes
      # (fixed attributes that dynamic attributes depend on).
      #
      # @param [<Symbol>] attributes the key attributes
      #
      # @return [undefined]
      #
      def depends_on(*attributes)
        @key_attributes = attributes
      end

      # Returns the list of key attributes.
      #
      # @return [<Symbol>] key attributes
      #
      def key_attributes
        @key_attributes || []
      end

      # Declares the permitted attributes list (in `strong_parameters` format).
      #
      # This list is later accessible via #permitted_attributes reader method.
      #
      # @param [<Symbol, Hash>] attributes the permitted attributes
      #
      # @return [undefined]
      #
      def permits(*attributes)
        @permitted_static_attributes = attributes
      end

      # Returns the list of permitted statis attributes.
      #
      # @return [<Symbol, Hash>] permitted statis attributes
      #
      def permitted_static_attributes
        @permitted_static_attributes || []
      end
    end
  end
end
