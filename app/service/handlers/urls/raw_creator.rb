module Handlers
  module Urls
    # A class responsible for creating urls.
    class RawCreator < Handlers::Base
      set_object_name :url

      converts_params do
        object_params[:document_id] = document.id
        object_params[:rendered] = URI.escape(object_params[:rendered])
      end

      depends_on :document_id
      permits :rendered
    end
  end
end
