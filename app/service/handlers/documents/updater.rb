module Handlers
  module Documents
    # A class responsible for updating documents.
    class Updater < Handlers::Base
      set_object_name :document

      converts_params do
        normalize_array :category_ids, :participant_ids
      end

      depends_on :category_ids
      permits :title, :url_id, participant_ids: []
    end
  end
end
