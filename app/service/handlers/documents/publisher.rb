module Handlers
  module Documents
    # A class responsible for publishing documents.
    class Publisher < Handlers::Base
      set_object_name :publisher

      converts_params do
        object_params[:published_at] ||= Time.now
      end

      permits :published_at, milestones: { published: [:at, :by] }

      after_initialize do
        object.published = true
        object.milestones = object.milestones.merge(
          published: {
            at: object.published_at.to_i,
            by: editor.id
          }
        )
      end
    end
  end
end
