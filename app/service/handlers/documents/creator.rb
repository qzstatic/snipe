module Handlers
  module Documents
    # A class responsible for creating documents.
    class Creator < Handlers::Base
      set_object_name :document
      
      converts_params do
        normalize_array :category_ids
        
        if object_params.has_key?(:category_slugs)
          slugs = object_params[:category_slugs]
          category_ids = Category.where(slug: slugs).pluck(:id)
          object_params[:category_ids] |= category_ids
        end
      end
      
      depends_on :category_ids
      permits :title
      
      after_initialize do
        object.milestones = {
          created: {
            at: Time.now.to_i,
            by: editor.id
          }
        }
      end
    end
  end
end
