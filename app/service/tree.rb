# A class representing some collection of objects organized in a tree.
class Tree
  # Returns all objects of the tree in a hash indexed by
  # object parent's `id`.
  #
  # @return [{nil, Fixnum => Nestable}] objects arranged as a hash
  #
  # @note Root objects are indexed by `nil`.
  #
  attr_reader :objects
  
  # Returns the class of the objects' serializer.
  #
  # @return [Class] serializer class
  #
  attr_reader :serializer_class
  
  # Returns the parent_id of the root element.
  #
  # @return [Fixnum, nil] root element's parent id
  #
  attr_reader :root_parent_id
  
  # Initializes the tree arranging the given objects in a `Hash`.
  #
  # @param [<Nestable>] objects the objects to build a tree from
  # @param [Class] serializer_class the serializer class
  # @param [Fixnum, nil] root_parent_id root element's id
  #
  def initialize(objects, serializer_class, root_parent_id = nil)
    arrange(objects)
    @serializer_class = serializer_class
    @root_parent_id   = root_parent_id
  end
  
  # Returns the tree as a nested hash with child nodes indexed by `:children`.
  #
  # @return [Hash] nested objects' hash
  #
  def as_json(*)
    sorted_data_for(root_objects)
  end
  
private
  def arrange(objects)
    hash = Hash.new { |hash, key| hash[key] = [] }
    
    @objects = objects.each_with_object(hash) do |object, hash|
      id = if object.root?
        nil
      else
        object.parent_id
      end
      
      hash[id] << object
    end
  end
  
  def sorted_data_for(objects)
    objects.sort_by(&:position).map do |object|
      attributes_hash(object)
    end
  end
  
  def attributes_hash(object)
    serializer_class.new(object).as_json.tap do |hash|
      children = objects[object.id]
      hash[:children] = sorted_data_for(children) unless children.empty?
    end
  end
  
  def root_objects
    objects[root_parent_id]
  end
end
