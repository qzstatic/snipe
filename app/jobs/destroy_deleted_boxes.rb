module DestroyDeletedBoxes
  @queue = :destroy_boxes

  TIMEOUT = 3.months

  class << self
    def perform(document_url)
      Box.deleted.where('updated_at < ?', TIMEOUT.ago).destroy_all
    end
  end
end