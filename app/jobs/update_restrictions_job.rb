class UpdateRestrictionsJob < Base
  @queue = :restrictions

  class << self
    # Send request to Aquarium to save document url to redis
    def perform(document_url, expire_date)
      aquarium_client.post('/snipe/restrictions', url: document_url, expire_date: expire_date)
    end
  end
end
