class Base

  class << self
    def aquarium_client
      @aquarium_client ||= AquariumClient.new
    end
  end
end