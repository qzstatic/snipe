# A `resque` job updating the document's dependencies.
module DependenciesUpdater
  @queue = :dependencies
  
  class << self
    # Updates dependencies of a given document.
    #
    # @param document_id [Fixnum] id of the document
    #
    # @return [undefined]
    #
    def perform(document_id)
      document = Document.find(document_id)
      Pipeline::Document.new(document).update_dependencies
    end
  end
end
