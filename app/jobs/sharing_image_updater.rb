module SharingImageUpdater
  @queue = :sharing

  class << self
    # Send request to Sharing Service to delete cached sharing image
    def perform(document_url)
      Settings.hosts.puffin.each do |puffin_url|
        uri = URI("#{puffin_url}/delete/#{document_url}.jpg")
        req = Net::HTTP::Delete.new(uri)
        Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') { |http| http.request(req) }
      end
    end
  end
end