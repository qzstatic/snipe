module PurgeDocumentCache
  @queue = :purge_cache

  class << self
    # Send request to Sharing Service to delete cached sharing image
    def perform(document_url)
      uri = URI("#{Settings.hosts.aquarium}/snipe/cache/purge")
      req = Net::HTTP::Post.new(uri)
      req.body = { path: document_url }.to_query
      Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') { |http| http.request(req) }
    end
  end
end