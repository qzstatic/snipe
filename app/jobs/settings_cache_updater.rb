# A `resque` job doing the regeneration of `setting_keys` cache in documents.
module SettingsCacheUpdater
  @queue = :settings_cache
  
  class << self
    # Regenerates the `setting_keys` cache in all documents of a given category.
    #
    # @param category_id [Fixnum] id of the category
    #
    # @return [undefined]
    #
    def perform(category_id)
      Category.find(category_id).regenerate_setting_keys_cache
    end
  end
end
