class DeleteRestrictionJob < Base
  @queue = :restrictions

  class << self
    # Send request to Aquarium to save document url to redis
    def perform(document_url)
      aquarium_client.delete('/snipe/restrictions', url: document_url)
    end
  end
end
