# A serializer for list item.
class ListItemSerializer < BasicListItemSerializer
  attributes :document_id
end
