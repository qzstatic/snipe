# A serializer for tags.
class TagSerializer < BaseSerializer
  attributes :title
end
