# A serializer for attachments.
class AttachmentSerializer < BaseSerializer
  attributes :type, :directory, :versions, :updated_at
  
  # Returns all attachment's versions as a hash indexed by version slugs.
  #
  # @return [Hash] versions
  #
  def versions
    object.versions.each_with_object(Hash.new) do |version, hash|
      attributes = AttachmentVersionSerializer.new(version.porcupined).as_json
      hash[version.slug.to_sym] = attributes
    end
  end
end
