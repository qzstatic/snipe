# A serializer for categories including their children.
class CategoriesWithChildrenSerializer
  attr_reader :categories
  
  def initialize(categories)
    @categories = categories
  end
  
  def as_json(*)
    categories.each_with_object(Hash.new) do |category, hash|
      hash[category.id] = CategorySerializer.new(category).as_json
      
      hash[category.id][:children] = categories.select do |child|
        child.path.last == category.id
      end.map(&:id)
    end
  end
end
