# A serializer for pages.
class PageSerializer < BaseSerializer
  attributes :title, :slug, :position, :list_ids
end
