# A serializer for sandbox.
class SandboxSerializer < BaseSerializer
  attributes :title, :color, :published, :proofread, :base_date, :date_offset
  attributes :category_ids, :participant_ids, :owner_ids
end
