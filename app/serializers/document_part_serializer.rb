# A serializer for document parts.
class DocumentPartSerializer < BaseDocumentPartSerializer
  # Returns the serialized document part (both attributes and meta information).
  #
  # @return [Hash]
  #
  def as_json(*)
    super.merge(data: data, settings: settings)
  end
  
private
  def data
    document[part_name].symbolize_keys.except(:updated_at)
  end
  
  def settings
    {
      module_synonym => module_settings,
      attachments: Settings.modules.attachments.common
    }
  end
  
  def module_settings
    settings = document.settings_for_module(module_synonym)
    return {} unless settings.respond_to?(:key?) && settings.key?(:attributes)
    
    attributes = settings.delete(:attributes).map do |attribute|
      process_attribute(*attribute)
    end.sort_by { |attribute| attribute.delete(:position) || 0 }
    
    settings.merge(attributes: attributes)
  end
  
  def process_attribute(attr_name, attr_settings)
    name = if attr_settings.first == 'association'
      "#{attr_name}_id"
    else
      attr_name.to_s
    end
    
    attr_settings.last.merge(name: name)
  end
  
  def module_synonym
    part_name.to_s.sub(/_data$/, '').to_sym
  end
end
