# A serializer for documents.
class DocumentSerializer < BaseSerializer
  include DynamicAttributesSerializer
  
  attributes :title, :published, :published_at, :milestones, :url_id, :locked?
  attributes :category_ids, :participant_ids, :exclude_list_ids, :illustrated?, :part
  
  # Returns milestones with timestamps converted to `Time` objects.
  #
  # @return [Hash] converted milestones
  #
  def milestones
    object.milestones.each_with_object(Hash.new) do |(name, data), hash|
      hash[name] = {
        at: Time.at(data[:at]),
        by: data[:by]
      }
    end
  end

  # Returns singularized document's part slug.
  #
  # @return [String, nil]
  # 
  def part
    object.view['part']
  end
end
