# A serializer for group.
class GroupSerializer < BaseSerializer
  attributes :slug, :title, :position, :editor_ids
  
  # Returns editors' internal ids.
  #
  # @return [<Fixnum>] editors' internal ids
  #
  def editor_ids
    object.editors.map(&:id)
  end
end
