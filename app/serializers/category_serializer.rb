# A serializer for categories.
class CategorySerializer < BaseSerializer
  attributes :slug, :title, :enabled, :path, :position
  attributes :setting_keys, :nested_categories, :forbidden_combinations
end
