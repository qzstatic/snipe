# A serializer for attachment versions.
class AttachmentVersionSerializer < BaseSerializer
  include DynamicAttributesSerializer
  
  attributes :slug, :url
  
  # Returns the version's url as a `String`.
  #
  # @return [String] url
  #
  def url
    object.url.to_s
  end
end
