# A serializer for documents.
class ListedDocumentSerializer < BaseSerializer
  include DynamicAttributesSerializer
  
  attributes :title, :published, :published_at, :milestones, :category_ids
  
  has_one :url
  
  # Returns milestones with timestamps converted to `Time` objects.
  #
  # @return [Hash] converted milestones
  #
  def milestones
    object.milestones.each_with_object(Hash.new) do |(name, data), hash|
      hash[name] = {
        at: Time.at(data[:at]),
        by: data[:by]
      }
    end
  end
end
