# Full serializer for urls including `document_id`.
class FullUrlSerializer < UrlSerializer
  attributes :document_id
end
