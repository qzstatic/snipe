# A custom serializer for documents of a particular list.
class ListDocumentsSerializer
  # Returns the list.
  #
  # @return [List] list
  #
  attr_reader :list
  
  # Initializes the serializer.
  #
  # @param list [List] the list
  #
  def initialize(list, *)
    @list = list
  end
  
  # Returns the serialized documents data with list items
  # (where they are present).
  #
  # @return [<Hash>] documents data
  #
  def as_json(*)
    map_documents do |document, document_hash|
      document_hash.merge item_data_for(document)
    end
  end
  
private
  def map_documents
    documents.includes(:url, :list_items).limit(list.limit + 3).map do |document|
      yield document, DocumentListItemSerializer.new(document).as_json
    end
  end
  
  def documents
    list.documents
  end
  
  def item_data_for(document)
    return {} unless list.respond_to?(:items)
    
    if item = document.list_items.find { |item| item.list_id == list.id }
      { list_item: BasicListItemSerializer.new(item).as_json }
    else
      {}
    end
  end
end
