# A serializer for document's title.
class DocumentTitleSerializer
  # Initializes the serializer.
  #
  # @param document [Document] the document
  #
  def initialize(document, *)
    @document = document
  end
  
  # Returns the serialized document's title along with it's settings.
  #
  # @return [Hash]
  #
  def as_json(*)
    { data: { title: document.title }, settings: settings }
  end
  
private
  attr_reader :document
  
  def settings
    {
      title: {
        attributes: [
          name:      'title',
          label:     'Заголовок',
          type:      'string',
          hint:      'Для новостей: до 90 знаков, для других материалов: до 75 знаков.',
          max_chars: 75
        ]
      }
    }
  end
end
