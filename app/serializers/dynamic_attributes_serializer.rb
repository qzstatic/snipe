# A mixin for adding dynamic attributes to the serializer output.
#
# Should be included into serializers of all models with dynamic attributes.
module DynamicAttributesSerializer
  # Returns all attributes (with dynamic attributes if present).
  #
  # @return [ActiveSupport::HashWithIndifferentAccess] attributes
  #
  def attributes
    super.merge(dynamic_attributes)
  end
  
private
  def dynamic_attributes
    if object.respond_to?(:dynamic_attributes_values)
      object.dynamic_attributes_values
    else
      {}
    end
  end
end
