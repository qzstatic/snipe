# A serializer for link.
class LinkSerializer < BaseSerializer
  attributes :document_id, :section, :position, :bound_document
  
  def bound_document
    ListedDocumentSerializer.new(object.bound_document).as_json
  end
end
