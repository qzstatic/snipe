# A serializer for options.
class OptionSerializer < BaseSerializer
  include DynamicAttributesSerializer
  
  attributes :type, :position
end
