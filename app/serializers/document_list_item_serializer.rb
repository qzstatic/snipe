# A serializer for documents in lists.
class DocumentListItemSerializer < DocumentSerializer
  attributes :url

  # Returns rendered document URL.
  #
  # @return [String] rendered URL
  #
  def url
    object.url.rendered if object.url
  end
end
