# A serializer for documents published on a given period.
class DocumentsByPeriodSerializer < BaseSerializer
  attributes :title, :url

  # Returns document's url as a rendered string.
  #
  # @return [String] rendered url
  #
  def url
    object.url ? object.url.rendered : ''
  end
end
