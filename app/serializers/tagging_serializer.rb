# A serializer for taggings.
class TaggingSerializer < BaseSerializer
  attributes :document_id, :type, :tag_ids
end
