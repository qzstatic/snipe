class HashSerializer
  def self.dump(hash)
    hash.to_json
  end

  def self.load(hash)
    JSON.parse((hash || {}).to_s).with_indifferent_access
  end
end
