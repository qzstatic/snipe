class ListDocumentsCacheSerializer < ListDocumentsSerializer
private
  def documents
    list.cached_documents
  end
end
