# Full serializer for documents including the `settings` attribute.
class FullDocumentSerializer < DocumentSerializer
  attributes :settings, :packed_document, :packed_links
  
  # Returns settings formatted for front-end application.
  #
  # @return [Hash] formatted settings
  #
  def settings
    settings = Hash.new { |hash, key| hash[key] = Hash.new(&hash.default_proc) }
    
    object.settings.deep_dup.each_with_object(settings) do |(module_name, module_settings), hash|
      if module_name == :boxes
        module_settings.each do |box_type, box_settings|
          process_settings_hash(box_type, box_settings, hash[:boxes])
        end
      else
        process_settings_hash(module_name, module_settings, hash)
      end
    end
  end
  
private
  def process_settings_hash(key, settings, hash)
    if settings.respond_to?(:key?) && settings.key?(:attributes)
      hash[key][:attributes] = settings.delete(:attributes).map do |attribute|
        process_attribute(*attribute)
      end.sort_by { |attribute| attribute.delete(:position) || 0 }
    end
    
    if settings.is_a?(Hash)
      hash[key].update(settings)
    else
      hash[key] = settings
    end
  end
  
  def process_attribute(attr_name, attr_settings)
    name = if attr_settings.first == 'association'
      "#{attr_name}_id"
    else
      attr_name.to_s
    end
    
    attr_settings.last.merge(name: name)
  end
end
