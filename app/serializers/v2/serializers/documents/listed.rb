# A serializer for documents in listed context.
module V2
  module Serializers
    module Documents
      class Listed < BaseSerializer
        include DynamicAttributesSerializer
        
        attributes :title, :published, :published_at, :milestones, :category_ids
        
        has_one :url
        
        # Returns milestones with timestamps converted to `Time` objects
        # and editor id
        #
        # @return [Hash] converted milestones
        #
        def milestones
          object.milestones.each_with_object(Hash.new) do |(name, data), hash|
            hash[name] = {
              at: Time.at(data[:at]),
              by: data[:by]
            }
          end
        end
      end
    end
  end
end
