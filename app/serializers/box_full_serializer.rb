class BoxFullSerializer < BoxSerializer
  attribute :children

  def children
    object.children.map { |box| BoxSerializer.new(BoxProxy.from(box)) }
  end
end
