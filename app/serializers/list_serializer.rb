# A serializer for list.
class ListSerializer < BaseSerializer
  include DynamicAttributesSerializer
  
  attributes :slug, :title, :limit, :duration, :cols, :category_slugs, :category_ids, :blacklisted_category_ids
  
  # Returns category slugs joined by comma.
  #
  # @return [String] category slugs
  #
  def category_slugs
    object.categories.map(&:slug).join(',')
  end
  
  # Returns the width for this list.
  #
  # @return [Fixnum, nil] width
  #
  def cols
    object.width
  end
end
