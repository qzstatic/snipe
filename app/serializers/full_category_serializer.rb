# An advanced serializer for categories containing additional attributes.
class FullCategorySerializer < CategorySerializer
  attributes :position
end
