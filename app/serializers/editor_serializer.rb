# A serializer for editor.
class EditorSerializer < BaseSerializer
  attributes :login, :email, :first_name, :last_name, :enabled, :group_ids
end
