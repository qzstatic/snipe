# A serializer for urls.
class UrlSerializer < BaseSerializer
  include DynamicAttributesSerializer
  
  attributes :rendered, :fixed
end
