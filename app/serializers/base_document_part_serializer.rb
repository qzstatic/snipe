# A base serializer for document parts rendering just meta information.
class BaseDocumentPartSerializer
  attr_reader :document, :part_name
  private     :document, :part_name
  
  # Initializes the serializer.
  #
  # @param document [Document] document
  # @param part_name [String] document's part name
  #
  def initialize(document, part_name, *)
    @document  = document
    @part_name = part_name
  end
  
  # Returns the serialized document part.
  #
  # @return [Hash]
  #
  def as_json(*)
    { meta: meta }
  end
  
private
  def meta
    { last_modified: document[part_name]['updated_at'] }
  end
end
