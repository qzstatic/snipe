# A common serializer for boxes.
class BoxSerializer < BaseBoxSerializer
  attributes :id, :enabled, :position, :readonly, :updated_at, :packed_box, :attachment
  
  # Returns the object's `copy?` attribute:
  # `true` for copies and `false` for originals.
  #
  # @return [true, false]
  #
  def readonly
    object.copy?
  end

  def attachment
    AttachmentSerializer.new(object.attachment) if object.attachment
  end
end
