# A serializer for options' settings.
class OptionTypesSerializer
  # Returns raw types data.
  #
  # @return [Hash]
  #
  attr_reader :types
  
  # Initializes the serializer.
  #
  # @param types [Hash] raw types data
  #
  def initialize(types)
    @types = types
  end
  
  # Renders an ibis-friendly structure.
  #
  # @return [<Hash>]
  #
  def as_json
    types.map do |type, settings|
      {
        name:       settings[:name],
        type:       type,
        attributes: settings[:attributes].keys
      }
    end
  end
end
