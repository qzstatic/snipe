# A base serializer class.
class BaseSerializer < ActiveModel::Serializer
  attributes :id
end
