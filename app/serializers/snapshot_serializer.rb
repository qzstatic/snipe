# A serializer for snapshots.
class SnapshotSerializer < BaseSerializer
  attributes :action, :subject_id, :subject_type, :editor, :created_at
  
  def editor
    object.author.full_name
  end
end
