# Base serializer for boxes.
class BaseBoxSerializer < ActiveModel::Serializer
  include DynamicAttributesSerializer
  
  attributes :type, :enabled
end
