# A serializer for categories' selectors.
class CategoryDataSourceSerializer < BaseSerializer
  attributes :slug, :name
  
  # Returns category's title.
  #
  # @return [String] category's title
  #
  def name
    object.title
  end
end
