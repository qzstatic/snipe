# A serializer for {SessionsManager}.
class SessionsManagerSerializer < ActiveModel::Serializer
  attribute :editor_id
  
  def editor_id
    object.editor.id
  end
end
