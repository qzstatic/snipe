# A collection of participants where editors are represented as
# positive numbers, groups as negative numbers and everyone as zero.
class Participants
  # A number representing everyone.
  EVERYONE = 0
  
  # Initializes a `Participants` instance.
  #
  # @param [<Fixnum>] editor_ids editors' ids
  # @param [<Fixnum>] group_ids groups' ids
  #
  def initialize(editor_ids, group_ids = [])
    process_editors(editor_ids)
    process_groups(group_ids)
  end
  
  # Returns an array of ids suitable for finding everything that matches
  # given editors and groups.
  #
  # @return [<Fixnum>] ids
  #
  def to_a
    editor_ids + group_ids.map(&:-@) + [EVERYONE]
  end
  
private
  def editor_ids
    @editor_ids ||= Set.new
  end
  
  def group_ids
    @group_ids ||= Set.new
  end
  
  def process_editors(ids)
    editors(ids).each do |editor|
      editor_ids << editor.id
      group_ids.merge editor.group_ids
    end
  end
  
  def process_groups(ids)
    groups(ids).each do |group|
      group_ids << group.id
      editor_ids.merge group.editors.pluck(:id)
    end
  end
  
  def editors(editor_ids)
    editor_ids.map { |id| Editor.find(id) }
  end
  
  def groups(group_ids)
    group_ids.map { |id| Group.find(id) }
  end
  
  class << self
    # Builds a new instance from a list of unsorted `participant_ids`.
    #
    # @param [<Fixnum>] participant_ids unsorted participant ids
    #
    # @return [Participants]
    #
    def from_participant_ids(participant_ids)
      ids = divide(participant_ids)
      new(ids[:editors], ids[:groups])
    end
    
    # Divides a list of `participant_ids` into editors' ids and groups' ids.
    #
    # @param [<Fixnum>] participant_ids participant ids
    #
    # @return [Hash{:editors, :groups => <Fixnum>}]
    #
    def divide(participant_ids)
      ids = {
        editors: [],
        groups:  []
      }
      
      participant_ids.each_with_object(ids) do |id, ids|
        if id > 0
          ids[:editors] << id
        elsif id < 0
          ids[:groups] << id.abs
        end
      end
    end
  end
end
