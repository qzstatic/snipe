# A namespace for classes of proxy objects wrapping a box.
module BoxProxy
  class << self
    # Constructs a proxy object around a given box.
    #
    # @param box [Box] the box
    #
    # @return [BoxProxy::Original] if the box is original
    # @return [BoxProxy::Copy] if the box is just a copy
    #
    def from(box)
      if box.copy?
        Copy.new(box)
      else
        Original.new(box)
      end
    end
    
    # Returns the box with descendants all wrapped in proxy objects.
    #
    # @param box [Box] the root box of the hierarchy
    #
    # @return [<BoxProxy::Original, BoxProxy::Copy>] box proxies
    #
    def with_descendants(box, only_deleted = false)
      root = from(box)

      root.descendants.send(only_deleted ? :deleted : :not_deleted).map do |box|
        if box.copy?
          with_descendants(box)
        else
          from(box)
        end
      end.flatten.unshift(root)
    end
  end
  
  # Base class for proxy objects around boxes.
  class Base
    include ActiveModel::Serialization
    
    # Returns the proxied box.
    #
    # @return [Box]
    #
    attr_reader :box
    
    # Initializes the proxy object.
    #
    # @param box [Box] the box
    #
    def initialize(box)
      @box = porcupine(box)
    end
    
  private
    def porcupine(object)
      if object.respond_to?(:porcupined)
        object.porcupined
      else
        object
      end
    end
  end
  
  # A class for proxy objects around original boxes.
  #
  # It delegates all methods to the box.
  class Original < Base
    # Delegates all methods to the box.
    #
    def method_missing(method_name, *args, &block)
      box.public_send(method_name, *args, &block)
    end
    
  private
    def respond_to_missing?(method_name, include_all = false)
      box.respond_to?(method_name, include_all)
    end
  end
  
  # A class for proxy objects around box copies.
  #
  # It delegates `id`, `position` and `enabled` methods to the copy
  # and other methods to the original box.
  class Copy < Base
    extend Forwardable
    
    def_delegators :box, :id, :position, :enabled, :parent_id, :copy?
    
    # Delegates all unknown methods to the original.
    #
    def method_missing(method_name, *args, &block)
      original.public_send(method_name, *args, &block)
    end
    
  private
    def respond_to_missing?(method_name, include_all = false)
      original.respond_to?(method_name, include_all)
    end
    
    def original
      porcupine(box.original)
    end
  end
end
