# API version 2

## Documents

### Fast create

    POST /v2/documents/fast
    {
        "document" => {
                   "title" => "Halo!",
                "rightcol" => "sub title",
            "category_ids" => [
                140737488355328,
                140737488355329,
                140737488355330,
                140737488355331
            ]
        },
              "url" => {
            "rendered" => "https://www.google.ru/"
        },
        "publisher" => {
            "published_at" => "2014-11-05T18:44:49.193+03:00"
        }
    }

#### Response

    Status: 201 Created
    {
                        "id" => 140737488355331,
                     "title" => "Halo!",
                 "published" => true,
              "published_at" => "2014-11-05T18:44:49.000+03:00",
                "milestones" => {
              "created" => {
                "at" => "2014-11-05T18:48:59.000+03:00",
                "by" => 140737488355328
            },
            "published" => {
                "at" => "2014-11-05T18:44:49.000+03:00",
                "by" => 140737488355328
            }
        },
              "category_ids" => [
                140737488355328,
                140737488355329,
                140737488355330,
                140737488355331
        ],
              "source_title" => "",
                "source_url" => "",
                "source_id" => 0,
        "assigned_editor_id" => 0,
           "assignee_status" => "disabled",
                       "url" => {
                  "id" => 140737488355329,
            "rendered" => "https://www.google.ru/",
               "fixed" => true
        }
    }
