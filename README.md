## Snipe

API-сервис для универсальной админки.

* `Rails::API`
* `ActiveRecord`
* `ActiveModel::Serializers`

[Разворачивание системы](INSTALL.md).

### Guard

`Guard` умеет автоматически гонять спеки и генерировать `YARD`-документацию (запуская сервер на http://0.0.0.0:8808).

Перед работой над проектом нужно его запустить:

``` sh
$ [bundle exec] guard
```

### Внутреннее устройство

Внутреннее устройство описано [здесь](https://github.com/vedomosti/snipe/blob/master/STRUCTURE.md).
