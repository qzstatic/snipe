module Token
  module App
    # An application's refresh token.
    class Refresh < Base
      path do |token|
        "tokens:apps:refresh:#{token}"
      end
      
      # Returns the application id as an integer.
      #
      # @return [Fixnum] application id
      #
      def app_id
        value.to_i
      end
    end
  end
end
