module Token
  module App
    # An application's access token.
    class Access < Base
      path do |token|
        "tokens:apps:access:#{token}"
      end
      
      # Default time to live for a token in redis.
      DEFAULT_TTL = 1.day
      
      # Returns the TTL of a token.
      #
      # @return [Fixnum] ttl
      #
      attr_reader :ttl
      
      # Initializes the access token saving the TTL and
      # calling {Base} constructor.
      #
      def initialize(value, ttl: DEFAULT_TTL, **options)
        @ttl = ttl
        super
      end
      
      # Returns the application id as an integer.
      #
      # @return [Fixnum] application id
      #
      def app_id
        value.to_i
      end
    end
  end
end
