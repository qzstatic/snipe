module Token
  # A base class for all token classes.
  #
  class Base
    # Returns the token owner id - it may be editor's id
    # or app's id.
    #
    # @return [String] the id
    #
    attr_reader :value
    
    # Returns the token string.
    #
    # @return [String] a token string
    #
    attr_reader :token
    
    # Initializes the token saving a value and a token string.
    #
    # @param [#to_s] value a value
    # @param [String] token a token string
    #
    def initialize(value, token: Token.generate, **options)
      @value = value.to_s
      @token = token.to_s
    end
    
    # Returns a key for storing the token in redis.
    #
    # @return [String] the key
    #
    def redis_key
      self.class.path.call(token)
    end
    
    # Saves the token to redis.
    #
    # @return [undefined]
    #
    def save
      if respond_to?(:ttl)
        Token.redis.set(redis_key, value, ex: ttl)
      else
        Token.redis.set(redis_key, value)
      end
    end
    
    # Reloads the TTL of a token from redis.
    #
    # @return [undefined]
    #
    def refresh_ttl!
      return unless respond_to?(:ttl)
      @ttl = Token.redis.ttl(redis_key).seconds
    end
    
    # Tests tokens for equality.
    # Tokens are considered equal if their values and tokens are equal.
    #
    # @param [Token::Base] other_token another token
    #
    # @return [true] if the tokens are equal
    # @return [false] if the tokens aren't equal
    #
    def ==(other_token)
      return false unless other_token.is_a?(Token::Base)
      token == other_token.token && value == other_token.value
    end
    
    # Deletes the token from redis.
    #
    # @return [undefined]
    #
    def destroy!
      Token.redis.del(redis_key)
    end
    
    class << self
      # Saves or returns the proc for computing a full redis key.
      #
      # @overload path(&block)
      #   Saves the passed block in a proc
      #
      #   @example
      #     path do |token|
      #       "tokens:editors:access:#{token}"
      #     end
      #
      #   @return [undefined]
      # @overload path
      #   Returns the saved proc
      #
      #   @return [Proc] the saved proc
      #
      def path(&block)
        if block
          @path_proc = block
        else
          @path_proc
        end
      end
      
      # Creates the new token and saves it to redis.
      #
      # @param [#to_s] value a value
      # @param [Hash] options a token's options
      #
      # @return [Token::Base] the new token
      #
      def create(value, **options)
        new(value, **options).tap(&:save)
      end
      
      # Finds the token by a token string.
      #
      # @param [String] token_string a token string
      #
      # @return [Token::Base] the token if found
      # @return [nil] if not found
      #
      def find(token_string)
        key = path.call(token_string)
        
        if value = Token.redis.get(key)
          new(value, token: token_string)
        end
      end
    end
  end
end
