module Token
  module Editor
    # An editor's access token.
    class Access < Base
      path do |token|
        "tokens:editors:access:#{token}"
      end
      
      # Default time to live for a token in redis.
      DEFAULT_TTL = 1.hour
      
      # Returns the TTL of a token.
      #
      # @return [Fixnum] ttl
      #
      attr_reader :ttl
      
      # Initializes the access token saving the TTL and
      # calling {Base} constructor.
      #
      def initialize(value, ttl: DEFAULT_TTL, **options)
        @ttl = ttl
        super
      end
      
      # Returns the editor id as an integer.
      #
      # @return [Fixnum] editor id
      #
      def editor_id
        value.to_i
      end
    end
  end
end
