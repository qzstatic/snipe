module Token
  module Editor
    # An editor's refresh token.
    class Refresh < Base
      path do |token|
        "tokens:editors:refresh:#{token}"
      end
      
      # Returns the editor id as an integer.
      #
      # @return [Fixnum] editor id
      #
      def editor_id
        value.to_i
      end
    end
  end
end
