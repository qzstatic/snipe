class Porcupine
  # A class representing the porcupined object and it's meta attribute.
  class Data
    # Returns the object.
    #
    # @return [Object] object
    #
    attr_reader :object
    
    attr_reader :meta_attribute
    private :meta_attribute
    
    # Initializes the `Data` instance.
    #
    # @param object [Object] the porcupined object
    # @param meta_attribute [Symbol] the meta attribute name
    #
    def initialize(object, meta_attribute)
      unless object.respond_to?(meta_attribute)
        fail ArgumentError, "#{object.inspect} does not respond to #{meta_attribute.inspect}"
      end
      
      @object         = object
      @meta_attribute = meta_attribute
    end
    
    # Reads the meta attribute value and
    # converts it to a `HashWithIndifferentAccess`.
    #
    # @return [HashWithIndifferentAccess] meta attribute value
    #
    def read
      object.send(reader_method).with_indifferent_access
    end
    
    # Writes a new value to the meta attribute.
    #
    # @param new_data [Hash] a new value
    #
    # @return [undefined]
    #
    def write(new_data)
      object.send(writer_method, new_data)
    end
    
  private
    def reader_method
      meta_attribute
    end
    
    def writer_method
      "#{meta_attribute}="
    end
  end
end
