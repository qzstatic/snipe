class Porcupine
  # A module with utility functions for {Porcupine}.
  module Utils
    class << self
      # Typecasts the value to a given type.
      #
      # @param value the value being typecasted
      # @param [:string, :integer, :boolean] type the type to which the value should be typecasted
      #
      # @return the typecasted value
      #
      def typecast(value, type)
        case type
        when :string
          value.to_s
        when :integer
          value.to_i
        when :boolean
          to_boolean(value)
        when :date
          to_date(value)
        else
          value
        end
      end
      
      # Returns a default value for a given type.
      #
      # @param [:string, :integer] type the type of the value
      #
      # @return the default value
      #
      # @see .typecast
      #
      def default_for(type)
        typecast(nil, type)
      end
      
      # Parses the attributes' params into {Attribute}
      # and {Association} instances.
      #
      # @param attributes [Hash] attributes' params
      #
      # @return [<Porcupine::Attribute>] the {Attribute} instances
      #
      def parse_attributes_params(attributes)
        attributes.map do |attr_name, (attr_type, *attr_params)|
          attr_params ||= {}
          
          if attr_type.to_sym == :association
            Porcupine::Association.new(attr_name, *attr_params)
          else
            Porcupine::Attribute.new(attr_name, attr_type, *attr_params)
          end
        end
      end
      
    private
      def to_boolean(value)
        case value
        when 'true'
          true
        when 'false'
          false
        else
          !!value
        end
      end
      
      def to_date(value)
        if value
          Date.parse(value)
        else
          Date.today
        end
      end
    end
  end
end
