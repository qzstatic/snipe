class Porcupine
  # A class representing a dynamic association.
  #
  # Holds the association's name.
  class Association < Attribute
    # Returns the association options.
    #
    # @return [Hash] options
    #
    attr_reader :options
    
    # Initializes the association.
    #
    # @param name [Symbol] the name of the association
    # @param type [Symbol] the type of the association
    # @param options [Hash] options of the association
    #
    def initialize(name, type, options = {})
      @name    = name
      @type    = type
      @options = options
    end
    
    # Always returns 0.
    #
    # @return [Fixnum]
    #
    def default_value
      0
    end
    
    # Compares the association with another association by their names, types
    # and options.
    #
    # @param other [Attribute] the attribute to compare to
    #
    # @return [true] if attributes are equal
    # @return [false] otherwise
    #
    def ==(other)
      %i(name type options).all? do |method|
        other.respond_to?(method) && send(method) == other.send(method)
      end
    end
    
    # Returns the attribute's name used in storing and accessing it's value.
    #
    # @return [Symbol] name
    #
    def stored_name
      "#{name}_id".to_sym
    end
    
    # Returns the attribute's name used in rendering it's value for the client.
    #
    # @return [Symbol] name
    #
    def public_name
      "#{name}_id".to_sym
    end
    
    # Typecasts the value to integer.
    #
    # @param value [Object] the value
    #
    # @return [Fixnum] the value as an integer
    #
    def typecast_value(value)
      Utils.typecast(value, :integer)
    end
    
    # Always returns true.
    #
    # @return [true]
    #
    def association?
      true
    end
    
    # Checks if the association should be ignored when serializing an object
    # nested inside another object.
    #
    # @return [true, false]
    #
    def ignored?
      options.fetch(:ignored, false)
    end
    
    # Finds the associated object.
    #
    # @param id [Fixnum] the id of the object
    #
    # @return [ActiveRecord::Base] the associated object
    #
    def get_object(id)
      if id.to_i.zero?
        nil
      else
        model_class.find(id)
      end
    end
    
  private
    def model_class
      type.to_s.classify.constantize
    end
  end
end
