class Porcupine
  # A class representing a single attribute.
  #
  # Holds the attribute's name, type and default value.
  class Attribute
    # Initializes an `Attribute` instance.
    #
    # @param name [Symbol] the attribute's name
    # @param type [Symbol] the attribute's type
    # @param options [Object] the attribute's options
    #
    def initialize(name, type, options = {})
      @name    = name
      @type    = type
      @default = options.fetch(:default, nil)
    end
    
    # Returns the attribute's name.
    #
    # @return [Symbol] attribute's name
    #
    def name
      @name.to_sym
    end
    
    # Returns the attribute's type.
    #
    # @return [Symbol] attribute's type
    #
    def type
      @type.to_sym
    end
    
    # Returns the attribute's default value
    #
    # @return [Object] attribute's default value
    #
    def default_value
      @default || Utils.default_for(type)
    end
    
    # Compares the attribute with another attribute by their names, types and
    # default values.
    #
    # @param other [Attribute] the attribute to compare to
    #
    # @return [true] if attributes are equal
    # @return [false] otherwise
    #
    def ==(other)
      %i(name type default_value).all? do |method|
        other.respond_to?(method) && send(method) == other.send(method)
      end
    end
    
    # Returns the attribute's name used in storing and accessing it's value.
    #
    # This will always be the attribute's name.
    #
    # @return [Symbol] name
    #
    def stored_name
      name
    end
    
    # Returns the attribute's name used in rendering it's value for the client.
    #
    # This will always be the attribute's name.
    #
    # @return [Symbol] name
    #
    def public_name
      name
    end
    
    # Returns the attribute's write callbacks (an empty array by default).
    #
    # @return [<Proc>] callbacks
    #
    def write_callbacks
      @write_callbacks ||= []
    end
    
    # Triggers all registered callbacks on an attribute
    # passing them the new value.
    #
    # @param new_value [Object] the new value of the attribute
    #
    # @return [undefined]
    #
    def trigger_write_callbacks(new_value)
      write_callbacks.each do |callback|
        callback.call(new_value)
      end
    end
    
    # Typecasts the value to the attribute's type.
    #
    # @param value [Object] the value
    #
    # @return [Object] the typecasted value
    #
    def typecast_value(value)
      Utils.typecast(value, type)
    end
    
    # Always returns false (overridden in {Association}).
    #
    # @return [false]
    #
    def association?
      false
    end
  end
end
