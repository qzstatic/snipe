# A monkeypatched version of core rails class.
class Rails::Application::Configuration
  # Reads the database configuration from `Settings`.
  # 
  # @return [Hash] database settings
  def database_configuration
    ::Settings.map(&:postgresql).stringify_keys
  end
end
