class ImageConverter
  attr_reader :boxes
  
  BASE_URL = "http://#{Settings.hosts.agami}/files".freeze
  
  NEW_VERSIONS = %i(thumbnail thumbnail_low thumbnail_high default mobile_low mobile_high fullscreen).freeze
  
  def initialize(boxes)
    @boxes = boxes
  end
  
  def convert
    boxes_count = boxes.count
    
    boxes.find_each.with_index do |box, index|
      next unless box.document.published?
      puts "#{index.next} (#{box.id}) / #{boxes_count}"
      
      porcupine = box.porcupined
      self.class.convert_box(porcupine)
      Pipeline::Box.new(porcupine).process
    end
  end
  
  class << self
    def convert_box(box)
      case box.type
      when 'gallery'
        box.children.each { |child_box| convert_box(child_box.porcupined) }
      when 'gallery_image', 'inset_image'
        convert_attachment(box.image) unless box.image.nil?
      else
        puts "Box##{box.id} skipped due to unknown type #{box.type}"
      end
    end
    
    def convert_attachment(attachment)
      return unless attachment.type == 'image'
      
      original_url = attachment.versions.find_by!(slug: 'original').url
      
      new_versions = NEW_VERSIONS.each_with_object(Hash.new) do |version_slug, hash|
        hash[version_slug] = Settings.modules.attachments.common.gallery_image.versions[version_slug]
      end
      
      convert_image(original_url, new_versions).each do |version_slug, version_attributes|
        upsert_version(attachment, version_slug, version_attributes)
      end
    end
    
    # @example
    #   convert_image(url, fullscreen: { width: 1920, height: 1280 })
    def convert_image(original_url, new_versions)
      params = new_versions.each_with_object(Hash.new) do |(slug, dimensions), hash|
        hash[slug] = "#{dimensions[:width]}x#{dimensions[:height]}>"
      end
      
      query_string = Rack::Utils.build_query(converts: Oj.dump(params))
      url = [original_url, query_string].join(??).sub(%r(^/), '')
      response = connection.patch(url)
      
      if response.success?
        response.body[:versions]
      else
        raise "Agami returned status #{response.status}"
      end
    end
    
    def upsert_version(attachment, version_slug, version_attributes)
      attributes = ActionController::Parameters.new(version_attributes.merge(slug: version_slug))
      
      if version = attachment.versions.find_by(slug: version_slug)
        updater = Savers::AttachmentVersionsUpdater.new(version, attributes, Editor.first)
        raise "AttachmentVersion##{version.id} update failed: #{updater.errors.ai}" unless updater.save
      else
        creator = Savers::AttachmentVersionsCreator.new(attributes, Editor.first, attachment)
        raise "AttachmentVersion create (#{version_attributes}) failed: #{creator.errors.ai}" unless creator.save
      end
    end
    
    def connection
      @connection ||= Faraday.new(url: BASE_URL) do |builder|
        builder.response :oj, content_type: 'application/json'
        builder.adapter Faraday.default_adapter
      end
    end
  end
end
