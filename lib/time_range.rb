# A range of two datetimes created from a base date and an offset
# (which can be either positive or negative).
class TimeRange
  # Returns the base of the range.
  #
  # @return [Time]
  #
  attr_reader :base
  
  # Returns the offset of the range.
  #
  # @return [Fixnum]
  #
  attr_reader :offset
  
  # Internal. Use {TimeRange.from} to create new instances.
  #
  # @api private
  #
  def initialize(base, offset)
    @base = base || Time.now
    @offset = offset
  end
  
  # Returns the range as a `Range` of two `Time` objects.
  #
  # @return [Range]
  #
  def range
    first..last
  end
  
  # Abstract definition overridden in the subclasses.
  #
  def first
    raise NotImplementedError
  end
  
  # Abstract definition overridden in the subclasses.
  #
  def last
    raise NotImplementedError
  end
  
  # Checks if the offset is zero (in which case the range is empty).
  #
  # @return [true] if the range is empty
  # @return [false] otherwise
  #
  def empty?
    offset.zero?
  end
  
  class << self
    protected :new
    
    # Builds a new instance - {WithPositiveOffset} for positive and zero offsets
    # and {WithNegativeOffset} for negative.
    #
    # @param [Time, nil] base the base date (interpreted as current time if nil is passed)
    # @param [Fixnum] offset the offset
    #
    # @return [WithPositiveOffset, WithNegativeOffset]
    #
    def from(base, offset)
      if offset >= 0
        WithPositiveOffset
      else
        WithNegativeOffset
      end.new(base, offset.abs)
    end
  end
  
  # A range of datetimes with a positive offset.
  class WithPositiveOffset < self
    # Returns the lower edge of the range.
    #
    # @return [Time]
    #
    def first
      base
    end
    
    # Returns the higher edge of the range.
    #
    # @return [Time]
    #
    def last
      base + offset
    end
  end
  
  # A range of datetimes with a negative offset.
  class WithNegativeOffset < self
    # Returns the lower edge of the range.
    #
    # @return [Time]
    #
    def first
      base - offset
    end
    
    # Returns the higher edge of the range.
    #
    # @return [Time]
    #
    def last
      base
    end
  end
end
