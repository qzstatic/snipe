class DatabaseProcessor
  TABLES = [
    :attachment_versions,
    :attachments,
    :boxes,
    :categories,
    :dependencies,
    :documents,
    :editors,
    :groups,
    :links,
    :list_items,
    :lists,
    :options,
    :pages,
    :sandboxes,
    :sessions,
    :snapshots,
    :taggings,
    :tags,
    :urls
  ]
  
  SHARD_ID = 1
  
  attr_reader :environment
  
  def initialize(environment = :development)
    @environment = environment
    
    connection_settings = Settings.for(environment).postgresql
    ActiveRecord::Base.establish_connection(connection_settings)
    ActiveRecord::Schema.verbose = false
  end
  
  def setup
    load_schema
    alter_ids_and_sequences
    load_seed
  end
  
  def reset
    truncate_database
    alter_ids_and_sequences
    load_seed
  end
  
  def hard_reset
    recreate_database
    load_schema
    alter_ids_and_sequences
    load_seed
  end
  
  def alter_ids_and_sequences
    TABLES.each do |table_name|
      ActiveRecord::Migration.send(:change_column, table_name, :id, :bigint)
      ActiveRecord::Base.connection.execute("ALTER SEQUENCE #{table_name}_id_seq RESTART WITH #{offset};")
    end
  end
  
private
  def truncate_database
    DatabaseCleaner.clean_with(:truncation)
  end
  
  def recreate_database
    ActiveRecord::Tasks::DatabaseTasks.drop_current(environment.to_s)
    ActiveRecord::Tasks::DatabaseTasks.create_current(environment.to_s)
  end
  
  def load_schema
    Rake::Task['db:schema:load'].invoke
  end
  
  def load_seed
    unless environment == :test
      ActiveRecord::Tasks::DatabaseTasks.seed_loader.load_seed
    end
  end
  
  def offset
    SHARD_ID << 47
  end
end
