require 'token/base'
require 'token/editor'
require 'token/app'

# A module for all access tokens and refresh tokens functionality.
module Token
  class << self
    # Generates a new token string.
    #
    # @return [String] a token string
    #
    def generate
      Digest::MD5.hexdigest(Time.now.to_f.to_s + rand.to_s)
    end
    
    # Returns the active Redis connection.
    #
    # @return [Redis] connection
    #
    def redis
      Snipe::Redis.connection
    end
  end
end
