# A namespace for classes responsible for generating denormalized data.
module Pipeline
  class << self
    # Returns all associated objects' pipelined attributes.
    #
    # @param object [Porcupine, Object] the object with attributes
    # @param nested [true, false] whether it is a nested serialization
    #
    # @return [Hash] associated objects' attributes
    #
    def dynamic_associations_for(object, nested: false)
      return {} unless object.respond_to?(:dynamic_associations)
      
      object.dynamic_associations(false, !nested).each_with_object(Hash.new) do |(name, object), hash|
        hash[name] = case object
        when ::Attachment
          Pipeline::Attachment.new(object).serialize
        when ::Document
          object = object.porcupined if object.respond_to?(:porcupined)
          Pipeline::Document.new(object).serialize_as_nested if object.published?
        when nil
          {}
        else
          object.attributes
        end
      end
    end
  end
end
