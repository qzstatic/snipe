# A namespace for application-specific modules.
module Snipe
  # Redis connection wrapper.
  module Redis
    class << self
      # Establishes a connection to redis.
      # 
      # @return [undefined]
      def connect!
        @connection = ::Redis.new(Settings.redis.to_hash)
      end
      
      # Closes the connection.
      # 
      # @return [undefined]
      def disconnect!
        @connection = nil
      end
      
      # Returns the current connection.
      # 
      # @return [Redis] connection
      def connection
        connect! unless @connection
        @connection
      end
    end
  end
end
