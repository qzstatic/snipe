require 'faraday'
require 'faraday_middleware'
require 'faraday_middleware/parse_oj'

require_relative '../../app/service/sessions_manager'
require_relative '../../app/service/sessions_manager/access_token'

module Snipe
  # A class for querying the API.
  class APIClient
    # Default authentication endpoint.
    AUTH_ENDPOINT = '/v1/access_token'.freeze
    
    # Temporary response file for browser.
    TMP_PATH = '/tmp/snipe_response.html'.freeze
    
    # Access token (available after authentication).
    #
    # @return [String]
    #
    attr_reader :access_token
    
    # Initializes a new API client with some connection parameters.
    #
    # @param [Hashie::Mash] params connection parameters
    # @param [Symbol] server_name API server name
    #
    def initialize(params, server_name)
      @params      = params
      @server_name = server_name
    end
    
    # Authenticates the client performing an HTTP request
    # to the authentication endpoint.
    #
    # @return [undefined]
    #
    def authenticate!
      authenticate_from_settings!
    end
    
    # Checks if the client is already authenticated.
    #
    # @return [true] if the client is authenticated
    # @return [false] otherwise
    #
    def authenticated?
      access_token.present?
    end
    
    %i(get post put patch delete).each do |http_verb|
      define_method(http_verb) do |*args|
        request(http_verb, *args) do |response|
          ap response.body
        end
      end
      
      define_method("j#{http_verb}") do |*args|
        request(http_verb, *args) do |response|
          if args.count > 1
            puts '+ Request (application/json)'.green
            puts
            output_as_json(args.last)
            puts
          end
          
          puts "+ Response #{response.status}".green
          puts
          if response.body
            output_as_json(response.body)
            puts
          end
        end
      end
    end
    
    # Open response body in the browser.
    #
    # @param [Faraday::Response] response API response
    #
    # @return [undefined]
    #
    def browse(response)
      File.open(TMP_PATH, 'w') { |f| f.write(response.body) }
      system("open #{TMP_PATH}")
    end
    
    class << self
      # Creates a new client with preset connection parameters.
      #
      # @param [Symbol] environment API server environment
      # @param [Symbol] server_name API server name
      #
      # @return [APIClient] a new client instance
      #
      def for(environment, server_name = :default)
        params = Settings.for(environment).api
        new(params, server_name)
      end
    end
    
  private
    def request(http_verb, *args)
      connection.send(http_verb, *args) do |request|
        request.headers['Accept'] = 'application/json'
      end.tap do |response|
        unless Rails.env.test?
          if response.body.is_a?(String)
            # open the HTML error page in browser
            browse(response)
          else
            # yield the parsed JSON response
            yield response
          end
        end
      end
    end
    
    def output_as_json(data)
      json = JSON.pretty_generate(data).each_line.map do |line|
        ' ' * 8 + line.rstrip
      end
      
      puts json.join(?\n).gsub(/\[\s*\]/, '[]').cyanish
    end
    
    def authenticate_from_settings!
      raise ArgumentError, 'No editor credentials in Settings' unless @params.editor?
      
      response = connection.post(AUTH_ENDPOINT, editor: @params.editor.to_hash)
      
      if response.status == 200
        @access_token = response.body.access_token
      else
        raise ArgumentError, 'Invalid credentials'
      end
    end
    
    def connection
      Faraday.new(domain_name) do |builder|
        builder.request :snipe_auth, access_token if authenticated?
        builder.request :json
        
        builder.response :mashify
        builder.response :oj, content_type: 'application/json'
        
        builder.adapter Faraday.default_adapter
      end
    end
    
    def domain_name
      @params.domains[@server_name]
    end
    
    # A simple faraday middleware which just adds
    # a given access token to request headers.
    class Auth < Faraday::Middleware
      # Initializes the middleware with a token.
      #
      # @param [Faraday::Middleware] app next middleware in the stack
      # @param [String] access_token an access token
      #
      def initialize(app, access_token)
        super(app)
        @access_token = access_token
      end
      
      # Adds the token to request headers and
      # passes the request further down the stack.
      #
      # @param [Faraday::Env] env the request environment
      #
      # @return [Faraday::Response] the response object
      #
      def call(env)
        env[:request_headers][SessionsManager::AccessToken::TOKEN_HEADER] = @access_token
        @app.call(env)
      end
    end
    
    Faraday::Request.register_middleware snipe_auth: Auth
  end
end
