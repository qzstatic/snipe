module Snipe
  # A namespace for modules designed to extend documents.
  #
  # Also keeps the registry of handler modules for various types.
  module Modules
    # Names of all modules that need to be loaded.
    NAMES = %w(
      base
      header
      trigger
      assigner
      url_renderer
      boxer
      tagger
      attacher
      illustrator
      sourcer
      lister
      proofreader
      box_mover
    )
    
    class << self
      # Fetches a handler module for a given type from the registry.
      #
      # @param [Symbol] type the type being handled
      #
      # @return [Module] the handler module
      #
      def fetch(type)
        registry[type]
      end
      
      # Adds the module to the registry.
      #
      # @param [Symbol] type the type this module should handle
      # @param [Module] handler the handler module
      #
      # @return [undefined]
      #
      def register(type, handler)
        registry[type] = handler
      end
      
      # Removes a given entry from the registry.
      #
      # @param [Symbol] type the type of handler being removed
      #
      # @return [undefined]
      #
      # @api private
      #
      def unregister(type)
        registry.delete(type)
      end
      
      # Loads all modules.
      #
      # @return [undefined]
      #
      def load_all
        NAMES.each do |module_name|
          load Rails.root.join("lib/snipe/modules/#{module_name}.rb")
        end
      end
      
    private
      def registry
        @registry ||= {}
      end
    end
  end
end

Snipe::Modules.load_all
