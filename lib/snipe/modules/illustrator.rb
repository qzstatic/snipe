module Snipe
  module Modules
    # A module for illustrating documents.
    module Illustrator
      extend Base
      
      register :title_image
      
      class << self
        # Returns a porcupine with accessors for title image information
        # in the `illustrator_data` hstore field.
        #
        # @param [Object] object the object storing the attributes
        # @param [Hash] settings the accessors settings
        #
        # @return [Porcupine] a proxy object if `object` responds to `#illustrator_data`
        # @return [Object] the untouched object otherwise
        #
        def process(object, settings)
          if valid_object?(object)
            Porcupine.new(object, :illustrator_data, settings[:attributes])
          else
            object
          end
        end
        
      private
        def valid_object?(object)
          object.respond_to?(:illustrator_data)
        end
      end
    end
  end
end
