module Snipe
  module Modules
    # A module for attaching files to documents and boxes.
    module Attacher
      extend Base
      
      register :attachments
      
      # Attributes added to the object (based on it's `type`).
      ATTRIBUTES = {
        image: {
          width:    :integer,
          height:   :integer,
          size:     :integer
        },
        audio: {
          duration: :integer,
          size:     :integer
        },
        video: {
          width:    :integer,
          height:   :integer,
          duration: :integer,
          size:     :integer
        },
        application: {
          size:     :integer
        }
      }.deep_freeze
      
      class << self
        # Returns a porcupine with accessors for media data
        # in the `attacher_data` hstore field.
        #
        # @param [Object] object the object storing the attributes
        # @param [Hash] settings settings for accessors (unused, left for compatibility)
        #
        # @return [Porcupine] a proxy object if `object` responds to `#attacher_data` and `#type`
        # @return [Object] the untouched object otherwise
        #
        def process(object, settings = { attributes: ATTRIBUTES })
          if valid_object?(object)
            attributes = settings[:attributes].fetch(object.type.to_sym, {})
            Porcupine.new(object, :attacher_data, attributes)
          else
            object
          end
        end
        
      private
        def valid_object?(object)
          %i(attacher_data type).all? do |attribute|
            object.respond_to?(attribute)
          end
        end
      end
    end
  end
end
