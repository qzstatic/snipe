module Snipe
  module Modules
    # A module for adding URL-attributes and rendered URL-string to Url object.
    module UrlRenderer
      extend Base
      
      register :urls
      
      # A pattern for finding variables in URL masks.
      MASK_VARIABLE_PATTERN = /:[a-z_]+/
      
      class << self
        # Returns a porcupine with accessors for url parts
        # in the `url_data` hstore field. Stores the URL rendered from
        # `mask` and `url_data` to `rendered`.
        #
        # @param [Object] object the object storing the attributes
        # @param [Hash] settings the settings for mask and accessors
        #
        # @return [Porcupine] a proxy object if `object` responds to `#url_data`
        # @return [Object] the untouched object otherwise
        #
        def process(object, settings)
          if valid_object?(object)
            build_porcupine(object, settings)
          else
            object
          end
        end
        
        # Returns the URL string rendered from `mask` and `url_data`.
        #
        # @param [String] mask URL mask
        # @param [Hash] attributes `url_data` attributes
        #
        # @return [String] rendered URL
        #
        def render(mask, attributes)
          raw_url = mask.gsub(MASK_VARIABLE_PATTERN) do |match|
            attr_name = match[1..-1] # ':domain' => 'domain'
            process_attribute(attributes[attr_name])
          end
          URI.escape(raw_url)
        end
        
      private
        def build_porcupine(object, settings)
          Porcupine.new(object, :url_data, settings[:attributes]).tap do |porcupine|
            porcupine.define_singleton_method(:render) do
              self.rendered = if url_data_is_complete?
                Snipe::Modules::UrlRenderer.render(settings[:mask], dynamic_attributes_values)
              else
                nil
              end
            end
            
            porcupine.define_singleton_method(:url_data_is_complete?) do
              dynamic_attributes.none? do |attribute|
                /:#{attribute.stored_name}/.match(settings[:mask]) && send(attribute.stored_name).blank?
              end
            end
          end
        end
        
        def valid_object?(object)
          object.respond_to?(:url_data)
        end
        
        def process_attribute(value)
          if value.respond_to?(:strftime)
            value.strftime('%Y/%m/%d')
          else
            value
          end
        end
      end
    end
  end
end
