module Snipe
  module Modules
    # A module for proofreading documents.
    module Proofreader
      extend Base
      
      register :proofreading
      
      # Attributes added to the object.
      ATTRIBUTES = { proofread: :boolean }.deep_freeze
      
      class << self
        # Returns a porcupine with accessors for proofreading information
        # in the `proofreader_data` hstore field.
        #
        # @param [Object] object the object storing the attributes
        # @param [Hash] settings the accessors settings
        #
        # @return [Porcupine] a proxy object if `object` responds to `#proofreader_data`
        # @return [Object] the untouched object otherwise
        #
        def process(object, settings)
          if settings[:on] && valid_object?(object)
            Porcupine.new(object, :proofreader_data, ATTRIBUTES)
          else
            object
          end
        end
        
      private
        def valid_object?(object)
          object.respond_to?(:proofreader_data)
        end
      end
    end
  end
end
