module Snipe
  module Modules
    # A module for adding header information to objects.
    module Header
      extend Base
      
      register :head
      
      class << self
        # Returns a porcupine with accessors for head information
        # in the `header_data` hstore field.
        #
        # @param [Object] object the object storing the attributes
        # @param [Hash] settings the accessors settings
        #
        # @return [Porcupine] a proxy object if `object` responds to `#header_data`
        # @return [Object] the untouched object otherwise
        #
        def process(object, settings)
          if valid_object?(object)
            Porcupine.new(object, :header_data, settings[:attributes])
          else
            object
          end
        end
        
      private
        def valid_object?(object)
          object.respond_to?(:header_data)
        end
      end
    end
  end
end
