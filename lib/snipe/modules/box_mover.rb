module Snipe
  module Modules
    # A module for box moving in document.
    module BoxMover
      extend Base
      
      register :box_moving
      
      class << self
        # Returns a porcupine with accessors for move information
        # in the `box_mover_data` hstore field.
        #
        # @param [Object] object the object storing the attributes
        # @param [Hash] settings the accessors settings
        #
        # @return [Porcupine] a proxy object if `object` responds to `#box_mover_data`
        # @return [Object] the untouched object otherwise
        #
        def process(object, _)
          if valid_object?(object)
            Porcupine.new(object, :box_mover_data, { box_move_editor_id: :integer, box_move_at: :datetime })
          else
            object
          end
        end

      private
        def valid_object?(object)
          object.respond_to?(:box_mover_data)
        end
      end
    end
  end
end
