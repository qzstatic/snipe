module Snipe
  module Modules
    # A module for adding service information to lists.
    module Lister
      extend Base
      
      register :lists
      
      class << self
        # Returns a porcupine with accessors for information
        # in the `lister_data` hstore field.
        #
        # @param [Object] object the object storing the attributes
        # @param [Hash] settings the accessors settings
        #
        # @return [Porcupine] a proxy object if `object` responds to `#lister_data`
        # @return [Object] the untouched object otherwise
        #
        def process(object, settings)
          if valid_object?(object)
            Porcupine.new(object, :lister_data, settings[:attributes])
          else
            object
          end
        end
        
      private
        def valid_object?(object)
          object.respond_to?(:lister_data)
        end
      end
    end
  end
end
