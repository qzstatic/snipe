module Snipe
  module Modules
    # A module for adding dynamic attributes to boxes
    # and controlling their hierarchy.
    module Boxer
      extend Base
      
      register :boxes
      
      class << self
        # Returns a porcupine with accessors for box data
        # in the `boxer_data` hstore field.
        #
        # @param [Object] object the object storing the attributes
        # @param [Hash] settings settings for accessors and associations
        #
        # @return [Porcupine] a proxy object if `object` responds to `#boxer_data`
        # @return [Object] the untouched object otherwise
        #
        def process(object, settings)
          return object unless valid_object?(object) && valid_type?(object.type, settings)
          
          attributes = extract_attributes(object, settings)

          Porcupine.new(object, :boxer_data, attributes) do
            after_changing(:document) do |new_document_id|
              Dependency.find_or_create_by!(
                dependable_id:   new_document_id,
                dependable_type: 'Document',
                dependent_id:    id,
                dependent_type:  'Box'
              )
            end
          end
        end
        
      private
        def valid_object?(object)
          object.respond_to?(:boxer_data)
        end
        
        def valid_type?(type, settings)
          settings.key?(type.to_sym) && settings[type.to_sym].key?(:attributes)
        end
        
        def extract_attributes(object, settings)
          settings[object.type.to_sym][:attributes]
        end
      end
    end
  end
end
