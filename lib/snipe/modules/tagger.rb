module Snipe
  module Modules
    # A module for tagging documents.
    module Tagger
      extend Base
      
      register :taggings
      
      class << self
        # Returns the untouched object.
        #
        # @param [Object] object the object
        # @param [Hash] settings module settings
        #
        # @return [Object] the untouched object
        #
        def process(object, settings)
          object
        end
      end
    end
  end
end
