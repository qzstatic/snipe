module Snipe
  module Modules
    # A module for adding source information to objects.
    module Sourcer
      extend Base
      
      register :source
      
      class << self
        # Returns a porcupine with accessors for head information
        # in the `sourcer_data` hstore field.
        #
        # @param [Object] object the object storing the attributes
        # @param [Hash] settings the accessors settings
        #
        # @return [Porcupine] a proxy object if `object` responds to `#sourcer_data`
        # @return [Object] the untouched object otherwise
        #
        def process(object, settings)
          if valid_object?(object)
            Porcupine.new(object, :sourcer_data, settings[:attributes])
          else
            object
          end
        end
        
      private
        def valid_object?(object)
          object.respond_to?(:sourcer_data)
        end
      end
    end
  end
end
