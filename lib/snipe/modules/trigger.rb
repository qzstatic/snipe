module Snipe
  module Modules
    # A module for adding simple boolean flags to objects.
    module Trigger
      extend Base
      
      register :trigger
      
      class << self
        # Returns a porcupine with accessors for trigger information
        # in the `trigger_data` hstore field.
        #
        # @param [Object] object the object storing the attributes
        # @param [Hash] settings the accessors settings
        #
        # @return [Porcupine] a proxy object if `object` responds to `#trigger_data`
        # @return [Object] the untouched object otherwise
        #
        def process(object, settings)
          if valid_object?(object)
            Porcupine.new(object, :trigger_data, settings[:attributes])
          else
            object
          end
        end
        
      private
        def valid_object?(object)
          object.respond_to?(:trigger_data)
        end
      end
    end
  end
end
