module Snipe
  module Modules
    # A module for allowing objects to be assigned to an editor and
    # keep track of it's status.
    module Assigner
      extend Base
      
      register :assignees
      
      # Attributes added to the object.
      ATTRIBUTES = {
        assigned_editor: [:association, :editor],
        assignee_status: [:string, default: :disabled]
      }.freeze
      
      class << self
        # Returns a porcupine with accessors for the added attributes
        # in the `assigner_data` hstore field.
        #
        # @param [Object] object the object storing the attributes
        # @param [Hash] settings the accessors settings
        #
        # @return [Porcupine] a proxy object if `object` responds to `#assigner_data`
        # @return [Object] the untouched object otherwise
        #
        def process(object, settings)
          if valid_object?(object) && settings
            Porcupine.new(object, :assigner_data, ATTRIBUTES, public_attributes: false)
          else
            object
          end
        end
        
      private
        def valid_object?(object)
          object.respond_to?(:assigner_data)
        end
      end
    end
  end
end
