module Snipe
  module Modules
    # A mixin for models extendable with modules from {Snipe::Modules}.
    #
    # @note You should implement #settings method in order to use `Snipe::Modules::Resolver`.
    module Resolver
      # Wraps the object in a porcupine for each applied module (if any).
      #
      # @return [Porcupine, Object] object (porcupined or not)
      #
      def porcupined
        settings.inject(self) do |object, (module_type, module_settings)|
          if handler = Modules.fetch(module_type)
            handler.process(object, module_settings)
          else
            object
          end
        end
      end
      
      # An abstract method that should be implemented in a class that includes `Resolver`.
      #
      def settings(*)
        raise NotImplementedError, "#{self.class} must implement #settings"
      end
    end
  end
end
