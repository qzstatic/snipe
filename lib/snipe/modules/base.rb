module Snipe
  module Modules
    # A mixin for all registerable modules providing the `.register` method.
    module Base
      # Adds the module to the registry.
      # 
      # @param [Symbol] type the type this module should handle
      # 
      # @return [undefined]
      # 
      # @example
      #   module DateHandler
      #     extend Base
      #     
      #     register :date
      #   end
      def register(type)
        Modules.register(type, self)
      end
      
      # An abstract method that should be implemented in each registerable module.
      def process(*)
        raise NotImplementedError, "#{self} must implement .process"
      end
    end
  end
end
