class SnapshotsCleaner
  attr_reader :documents
  
  def initialize(documents = Document.published)
    @documents = documents
  end
  
  def clean
    count = documents.count
    
    documents.find_each.with_index do |document, index|
      snapshots = document.snapshots.order(:id)
      first_publication = snapshots.find do |snapshot|
        %w(document.published document_published).include?(snapshot.action)
      end
      next if first_publication.nil?
      
      puts "#{index.next} (#{document.id}) / #{count}"
      
      snapshots.where('id < ?', first_publication.id).delete_all
    end
  end
end
