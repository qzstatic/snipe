class SettingKeysMigrator
  DEFAULT_LIMIT = 20_000
  
  attr_reader :setting_keys
  
  def initialize
    load_setting_keys
  end
  
  def process(limit = DEFAULT_LIMIT)
    Document.where("cached_setting_keys = '{}'").limit(limit).pluck(:id, :category_ids).each_with_index do |(id, category_ids), index|
      puts "Processing #{index.succ}/#{limit}"
      cache = setting_keys.values_at(*category_ids).flatten
      Document.where(id: id).update_all(cached_setting_keys: cache)
    end
    
    limit
  end
  
  def processed
    Document.where("cached_setting_keys != '{}'").count
  end
  
  def remaining
    Document.where("cached_setting_keys = '{}'").count
  end
  
private
  def load_setting_keys
    @setting_keys = Category.pluck(:id, :setting_keys).inject(Hash.new) do |hash, (id, setting_keys)|
      hash.merge(id => setting_keys)
    end
  end
end
