class InsetImageConverter
  attr_reader :relation
  private     :relation
  
  def initialize(relation = Box.where(type: 'inset_image'))
    @relation = relation
  end
  
  def convert
    convert_each do |box|
      self.class.copy_attributes(box)
      self.class.pipeline(box)
      box.save
    end
  end
  
  def delete_old_attributes
    convert_each do |box|
      box.boxer_data = box.boxer_data.except('title', 'copyright')
      self.class.pipeline(box)
      box.save
    end
  end
  
private
  def convert_each
    raise ArgumentError unless block_given?
    total_count = relation.count
    
    relation.find_each.with_index do |box, index|
      puts "#{index.next} (#{box.id}) / #{total_count}"
      yield box
    end
  end
  
  class << self
    def copy_attributes(box)
      if box.boxer_data['description'].to_s.empty?
        box.boxer_data = box.boxer_data.merge('description' => box.boxer_data['title'])
      end
      
      if box.boxer_data['credits'].to_s.empty?
        box.boxer_data = box.boxer_data.merge('credits' => box.boxer_data['copyright'])
      end
    end
    
    def pipeline(box)
      if box.document.published?
        box.packed_box = Pipeline::Box.new(box.porcupined).serialize
      end
    end
  end
end
