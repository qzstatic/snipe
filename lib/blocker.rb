# This class allow manage blockers for some ActiveRecord::Base objects
module Blocker
  class Object
    attr_accessor :object, :editor_id, :blocked_at
    def initialize(object)
      self.object = object
      load!
    end

    # Remove block from redis if will be blocked by this editor
    #
    # @param editor [Editor] editor for unblock
    #
    # @return [Boolean] true if success other case false
    #
    def unblock!(editor)
      if self.blocked?
        if self.editor_id == editor.try!(:id) and reset!
          self.editor_id = nil
          self.blocked_at = nil
        else
          return false
        end
      end
      true
    end

    # Create block in redis for editor
    #
    # @param editor [Editor] editor for block
    #
    # @return [Boolean] true if success other case false
    #
    def block!(editor)
      if self.blocked? or editor.nil?
        false
      else
        self.editor_id = editor.id
        self.blocked_at = Time.current
        store!
      end
    end

    # Check for already blocked or not
    #
    # @return [Boolean] true if blocked other case false
    #
    def blocked?
      namespace.exists(key)
    end

    # Serialize block info to json
    #
    # @return [Boolean] true if blocked other case false
    #
    def to_json
      { 'editor_id' => editor_id, 'blocked_at' =>  blocked_at }.to_json
    end

    # Force remove block
    #
    # @return [Boolean] true all time
    #
    def reset!
      namespace.del(key)
      true
    end

  private

    def key
      Blocker.key_for(self.object)
    end

    def load!
      data = Oj.safe_load(namespace.get(key) || '{}')
      self.editor_id = (id = data['editor_id']) ? id.to_i : id
      self.blocked_at = DateTime.parse(data['blocked_at']) rescue nil
      true
    end

    def store!
      namespace.setnx(key, to_json)
      true
    end

    def namespace
      Blocker.namespace_for_class(object.class)
    end

  end

  class Attribute < Object
    attr_accessor :attribute
    def initialize(object, attribute)
      self.attribute = attribute
      super(object)
    end

  private

    def key
      Blocker.key_for(self.object, self.attribute)
    end
  end

  module Controller
    extend ActiveSupport::Concern
    included do

      def self.blocker_methods_for(&block)

        # Block for object by editor
        #
        define_method :block do
          variable = instance_eval(&block)
          if variable.updated_at.present? and (Time.zone.parse(params.require(:attributes).require(:updated_at)).to_i < variable.updated_at.to_i)
            render nothing: true, status: 412
          else
            if variable.block!(sessions_manager.editor)
              render nothing: true, status: 200
            else
              render json: Blocker::Object.new(variable).to_json, status: 423
            end
          end
        end

        # Unblock object
        #
        define_method :unblock do
          variable = instance_eval(&block)
          if variable.blocked?
            render nothing: true, status: variable.unblock!(sessions_manager.editor) ? 200 : 403
          else
            render nothing: true, status: 200
          end
        end
      end

    end
  end

  module Model
    extend ActiveSupport::Concern
    included do

      # Return scope with record what has been blocked
      #
      scope :by_blocked_models, -> {
        ids = Blocker.namespace_for_class(self).keys('*[0-9]').collect {|key| key.scan(/\d+/).first.to_i }
        current_scope.where(id: ids)
      }

      # Return scope with record what has block attribute
      #
      scope :by_blocked_attributes, ->(attribute = nil) {
        ids = Blocker.namespace_for_class(self).keys("*[0-9]/#{attribute|| '*'}").collect {|key| key.scan(/\d+/).first.to_i }
        scope = current_scope.where(id: ids)
        scope.define_singleton_method(:blocked_attributes) { scope.all.map(&:blocked_attributes).flatten }
        scope
      }

      # Get blocked attributes
      #
      # @return [Array]
      #
      def blocked_attributes
        Blocker.namespace_for_class(self.class).keys("#{self.id}/*").collect {|key| key.scan(/[a-z_]*$/).first.to_sym }
      end

      # Create blocker for object
      #
      # @param attribute [String,Symbol] create Blocker::Attribute if present in other case Blocker::Object
      #
      # @return [Blocker::Object,Blocker::Attribute]
      #
      def blocker(attribute = nil)
        attribute ? Blocker::Attribute.new(self, attribute) : Blocker::Object.new(self)
      end

      # Block model/attribute by editor
      #
      # @params attribute [String, Symbol] if present will be block attribute in other case model
      #
      # @return [Boolean] true if success, false if already blocked
      #
      def block!(editor, attribute = nil)
        blocker(attribute).block!(editor)
      end

      # Unblock model/attribute by editor
      #
      # @params attribute [String, Symbol] if present will be unblock! attribute in other case model
      #
      # @return [Boolean] true if success, false if blocked by other editor
      #
      def unblock!(editor, attribute = nil)
        blocker(attribute).unblock!(editor)
      end

      # Check for blocked model/attribute by editor
      #
      # @params attribute [String, Symbol] if present will be check for block attribute in other case model
      #
      # @return [Boolean] true if success, false if not blocked
      #
      def blocked?(attribute = nil)
        blocker(attribute).blocked?
      end

      # Reset block for model/attribute by editor
      #
      # @params attribute [String, Symbol] if present will be reset attribute in other case model
      #
      # @return [Boolean] true
      #
      def reset!(attribute = nil)
        blocker(attribute).reset!
      end
    end
  end

  # Generate key for pair object/attribute
  #
  # @params attribute [String, Symbol] if present will be generate key for attribute in other case for object
  #
  # @return [String]
  #
  def self.key_for(object, attribute = nil)
    [(object ? object.id : '*'), attribute].compact.join('/')
  end

  # Generate namespace for pair class
  #
  # @params klass [ActiveRecord::Base]
  #
  # @return [Redis::Namespace]
  #
  def self.namespace_for_class(klass)
    Redis::Namespace.new(klass.table_name, redis: Redis::Namespace.new('blockers', redis: Snipe::Redis.connection))
  end

end
