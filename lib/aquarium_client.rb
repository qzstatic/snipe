class AquariumClient
  extend Forwardable

  def_delegators :connection, *%i(get post put patch delete)

  def connection
    Faraday.new(url: Settings.hosts.aquarium) do |builder|
      builder.request  :url_encoded
      builder.response :mashify
      builder.response :oj, content_type: 'application/json'
      builder.adapter  :net_http_persistent
    end
  end
end