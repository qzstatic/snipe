# A class for sorting a collection of objects in a given order by some attribute.
class Sorter
  # Returns the name of an attribute to sort by.
  #
  # @return [Symbol] the attribute name
  #
  attr_reader :attribute_name
  
  # Returns the values of an attribute in correct order.
  #
  # @return [<Object>] sorted values of an attribute
  #
  attr_reader :sorted_values
  
  # Initializes a sorter with attribute name and it's sorted values.
  #
  # @param [Symbol] attribute_name name of the attribute to sort by
  # @param [Array] sorted_values list of sorted values of this attribute
  #
  def initialize(attribute_name, sorted_values)
    @attribute_name = attribute_name
    @sorted_values  = sorted_values
  end
  
  # Returns sorted objects.
  #
  # @param [Array] objects unsorted objects
  #
  # @return [Array] sorted objects
  #
  def sort(objects)
    sorted_values.flat_map do |value|
      objects.select do |object|
        object.send(attribute_name) == value
      end
    end
  end
end
