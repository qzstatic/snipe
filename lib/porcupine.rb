require 'porcupine/data'

# A proxy class adding dynamic attributes to it's delegate object.
class Porcupine
  include ActiveModel::ForbiddenAttributesProtection
  
  attr_reader :data, :dynamic_attributes, :changed_attributes
  private :data, :dynamic_attributes, :changed_attributes
  
  # Initializes a porcupine creating all necessary accessor methods.
  #
  # @param [Object] object the delegate object
  # @param [Symbol] meta_attribute the object's meta attribute holding dynamic attributes
  # @param [{Symbol => Symbol, Array(Symbol, Object)}] attributes dynamic attributes' params
  # @param [true, false] public_attributes can the attributes be used in a public API
  #
  def initialize(object, meta_attribute, attributes, public_attributes: true, &block)
    @data = Data.new(object, meta_attribute)
    @dynamic_attributes = Utils.parse_attributes_params(attributes)
    @public_attributes = public_attributes
    @changed_attributes = {}
    
    define_accessors
    
    instance_eval(&block) if block
  end
  
  # Returns all dynamic attributes' names
  # (including those from a delegate object).
  #
  # @return [Array<Symbol>] attributes' names
  #
  def dynamic_attributes_names
    if object.respond_to?(:dynamic_attributes_names)
      object.dynamic_attributes_names
    else
      []
    end + dynamic_attributes.map(&:stored_name)
  end
  
  # Returns all dynamic attributes as a hash.
  #
  # @param [true, false] include_internal whether to include internal attributes
  # @param [true, false] include_associations whether to include associations' ids
  #
  # @return [Hash{Symbol => Object}] attributes
  #
  def dynamic_attributes_values(include_internal = true, include_associations = true)
    values = HashWithIndifferentAccess.new
    
    if public_attributes? || include_internal
      dynamic_attributes.each_with_object(values) do |attribute, hash|
        next if !include_associations && attribute.association?
        hash[attribute.public_name] = send(attribute.stored_name)
      end
    end
    
    if object.respond_to?(:dynamic_attributes_values)
      object.dynamic_attributes_values(include_internal, include_associations)
    else
      {}
    end.merge(values)
  end
  
  # Returns all dynamic associations as a hash with association names as keys
  # and associated objects as values.
  #
  # @param include_internal [true, false] whether to include internal associations
  # @param include_ignored [true, false] whether to include ignored associations
  #
  # @return [Hash] associations
  #
  def dynamic_associations(include_internal = true, include_ignored = true)
    values = HashWithIndifferentAccess.new
    
    if public_attributes? || include_internal
      dynamic_attributes.each_with_object(values) do |attribute, hash|
        next unless attribute.association?
        next if !include_ignored && attribute.ignored?
        
        if id = data.read[attribute.stored_name]
          hash[attribute.name] = attribute.get_object(id)
        end
      end
    end
    
    if object.respond_to?(:dynamic_associations)
      object.dynamic_associations(include_internal, include_ignored)
    else
      {}
    end.merge(values)
  end
  
  # Assigns the given attributes to the object
  # (checking for forbidden attributes).
  #
  # @param [Hash] attributes the attributes to assign
  #
  # @return [undefined]
  #
  def assign_attributes(attributes)
    sanitize_for_mass_assignment(attributes).each do |attr_name, attr_value|
      send("#{attr_name}=", attr_value)
    end
  end
  
  # Saves the object and triggers write callbacks on each changed attribute.
  #
  # @return [true] if object was successfully saved
  # @return [false] otherwise
  #
  def save
    if changed_attributes.any?
      data.write(data.read.merge(updated_at: Time.now))
    end
    
    object.save.tap do |result|
      if result
        changed_attributes.each do |attribute, new_value|
          attribute.trigger_write_callbacks(new_value)
        end
        
        changed_attributes.clear
      end
    end
  end
  
private
  def after_changing(association_type, &block)
    dynamic_attributes.each do |attribute|
      if attribute.association? && attribute.type == association_type
        attribute.write_callbacks << block
      end
    end
  end
  
  def define_accessors
    dynamic_attributes.each do |attribute|
      define_reader(attribute)
      define_checker(attribute)
      define_writer(attribute)
      
      define_object_reader(attribute) if attribute.association?
    end
  end
  
  def define_reader(attribute)
    define_singleton_method(attribute.stored_name) do
      if data.read.key?(attribute.stored_name)
        attribute.typecast_value(data.read[attribute.stored_name])
      else
        attribute.default_value
      end
    end
  end
  
  def define_checker(attribute)
    define_singleton_method("#{attribute.stored_name}?") do
      data.read.key?(attribute.stored_name)
    end
  end
  
  def define_writer(attribute)
    define_singleton_method("#{attribute.stored_name}=") do |new_value|
      new_data = data.read
      old_value = new_data[attribute.stored_name]
      new_data[attribute.stored_name] = new_value
      data.write(new_data)
      @changed_attributes[attribute] = new_value unless new_value == old_value
    end
  end
  
  def define_object_reader(attribute)
    define_singleton_method(attribute.name) do
      if id = data.read[attribute.stored_name]
        attribute.get_object(id)
      end
    end
  end
  
  def public_attributes?
    @public_attributes
  end
  
  def object
    data.object
  end
  
  # Delegates all unknown methods to the object.
  def method_missing(method_name, *args, &block)
    object.send(method_name, *args, &block)
  end
  
  def respond_to_missing?(method_name, include_private)
    object.respond_to?(method_name, include_private)
  end
end
