# A module for scrubbing strings.
module Scrubber
  class << self
    # Scrubs the string, removing and collapsing spaces.
    #
    # @param [String] string dirty string
    #
    # @return [String] scrubbed string
    #
    def scrub(string)
      string.split.join(' ')
    end
  end
end
