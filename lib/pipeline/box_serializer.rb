module Pipeline
  # A custom box serializer for dumping denormalized data of the box
  # and all it's descendants including associated attachments.
  class BoxSerializer < BaseBoxSerializer
    attribute :id
    
    include DynamicAssociationsSerializer
  end
end
