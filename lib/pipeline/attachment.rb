module Pipeline
  # A processor for attachment's data.
  class Attachment
    # Returns the attachment.
    #
    # @return [Attachment] attachment
    #
    attr_reader :attachment
    
    # Initializes the attachment pipeline.
    #
    # @param [Attachment] attachment the attachment to process
    #
    def initialize(attachment)
      @attachment = attachment
    end
    
    # Returns the denormalized data of the attachment's versions.
    #
    # @return [Hash] denormalized data
    #
    def serialize
      attachment.versions.includes(:attachment).each_with_object(Hash.new) do |version, hash|
        hash[version.slug.to_sym] = Serializer.new(version.porcupined).as_json
      end.deep_symbolize_keys
    end
    
    # A custom attachment version serializer for dumping denormalized data.
    class Serializer < ActiveModel::Serializer
      include DynamicAttributesSerializer
      
      attributes :attachment_id, :url
      
      # Returns the version's url as a `String`.
      #
      # @return [String] url
      #
      def url
        object.url.to_s
      end
    end
  end
end
