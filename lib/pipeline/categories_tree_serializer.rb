module Pipeline
  # A custom serializer for a categories tree.
  #
  # Renders a nested hash of categories where "real" categories are represented
  # with attributes and children, and "pseudo" categories are just collections
  # of their children.
  class CategoriesTreeSerializer
    # Returns the categories list.
    attr_reader :categories
    
    # Initializes the serializer instance.
    #
    # @param [<Category>] categories
    #
    def initialize(categories)
      @categories = categories
    end
    
    # Returns a formatted tree of categories.
    #
    # @return [Hash] categories tree
    #
    def as_json
      children_as_hash(nil)
    end
    
  private
    def serialize(category)
      if category.real?
        [attributes_of(category), children_attributes_of(category)].inject(:merge)
      else
        children_attributes_of(category)
      end
    end
    
    def attributes_of(category)
      {
        title:    category.title,
        slug:     category.slug,
        position: category.position
      }
    end
    
    def children_attributes_of(category)
      if category.nested_categories.include?('required') && children_of(category).empty?
        raise ArgumentError, "Category '#{category.title}' requires child categories."
      end
      
      if category.real?
        children_as_hash(category)
      elsif category.nested_categories.include?('single')
        serialize(children_of(category).first)
      else
        children_as_array(category)
      end
    end
    
    def children_as_hash(category)
      children_of(category).each_with_object(Hash.new) do |subcategory, hash|
        hash[slug_to_key(subcategory.slug)] = serialize(subcategory)
      end
    end
    
    def children_as_array(category)
      children_of(category).map do |subcategory|
        serialize(subcategory)
      end
    end
    
    def children_of(parent)
      if parent.nil?
        categories.select { |category| category.path.empty? }
      else
        categories.select { |category| category.path.last == parent.id }
      end
    end
    
    def slug_to_key(slug)
      slug.tr('.', '_').to_sym
    end
  end
end
