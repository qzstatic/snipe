module Pipeline
  # A processor for boxes' data.
  class Boxes
    # Returns the document.
    #
    # @return [Document] the document
    #
    attr_reader :document
    
    # Initializes the boxes pipeline.
    #
    # @param [Document] document document with the boxes to process
    #
    def initialize(document)
      @document = document
    end
    
    # Saves the denormalized data inside each first-level box of the document.
    #
    # @return [undefined]
    #
    def process
      boxes = document.boxes.inject(Hash.new) { |hash, box| hash.merge(box.id => box) }
      
      ::Box.transaction do
        serialize.each do |box_id, packed_box|
          boxes[box_id].update_column(:packed_box, boxes[box_id].deleted ? {} : packed_box)
        end
      end
    end
    
    # Returns the denormalized data for first-level boxes indexed by box's id.
    #
    # @return [Hash] denormalized data
    #
    def serialize(preview: false)
      top_level_boxes, child_boxes = document.boxes.send(preview ? :not_deleted : :all).partition do |box|
        box.path.one?
      end
      
      top_level_boxes = top_level_boxes.sort_by(&:position)
      child_boxes = child_boxes.reject {|box| box.deleted }.group_by { |box| box.path.second }
      
      top_level_boxes.each_with_object(Hash.new) do |box, hash|
        box_with_children = porcupine(child_boxes.fetch(box.id, []).unshift(box))
        tree = Tree.new(box_with_children, Pipeline::BoxSerializer, box.parent_id)
        hash[box.id] = tree.as_json.first
      end
    end
    
  private
    def porcupine(boxes)
      boxes.map do |box|
        box.document = document
        
        if box.respond_to?(:porcupined)
          box.porcupined
        else
          box
        end
      end
    end
  end
end
