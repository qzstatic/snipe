module Pipeline
  # A pipeline for a single box.
  class Box
    # Returns the box.
    #
    # @return [Box]
    #
    attr_reader :box
    
    # Initializes the pipeline instance.
    #
    # @param box [Box] the box
    #
    def initialize(box)
      @box = box
    end
    
    # Returns the serialized box data.
    #
    # @return [Hash] serialized data
    #
    def serialize
      box_with_children = porcupine(box.descendants).unshift(box)
      Tree.new(box_with_children, Pipeline::BoxSerializer, box.parent_id).as_json.first
    end
    
    # Updates the box's `packed_box` with serialized data.
    #
    # @return [undefined]
    #
    def process
      box.update_column(:packed_box, serialize)
    end
    
  private
    def porcupine(boxes)
      document = box.document
      
      boxes.map do |box|
        box.document = document
        
        if box.respond_to?(:porcupined)
          box.porcupined
        else
          box
        end
      end
    end
  end
end
