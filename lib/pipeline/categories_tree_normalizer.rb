module Pipeline
  class CategoriesTreeNormalizer
    def initialize(category_ids)
      @category_ids = category_ids
      @categories   = Category.where(id: category_ids)
    end

    def normalize!
      @categories.each do |category|
        @category_ids.delete(category.id) if category.pseudo? && children_of(category).blank?
      end
      @category_ids
    end

    private

    def children_of(category)
      @categories.select { |cat| cat.path.last == category.id }
    end
  end
end
