module Pipeline
  # A processor for links' data.
  class Links
    # Returns the document.
    #
    # @return [Document] the document
    #
    attr_reader :document
    
    # Initializes the links pipeline.
    #
    # @param [Document] document document with the links to process
    #
    def initialize(document)
      @document = document
    end
    
    # Saves the denormalized data inside the document.
    #
    # @return [undefined]
    #
    def process
      document.update(packed_links: serialize)
    end
    
    # Returns the denormalized data with links grouped by their sections.
    #
    # @return [Hash] denormalized data
    #
    def serialize
      links_data = Hash.new { |hash, key| hash[key] = [] }
      
      document.links.sort_by(&:position).each_with_object(links_data) do |link, links_data|
        if link.bound_document.published
          links_data[link.section] << Serializer.new(link).as_json
        end
      end
    end
    
    class Serializer < ActiveModel::Serializer
      attributes :position, :bound_document
      
      def bound_document
        object.bound_document.packed_document
      end
    end
  end
end
