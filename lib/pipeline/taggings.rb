module Pipeline
  # A processor for taggings' data.
  class Taggings
    # Returns the document.
    #
    # @return [Document] the document
    #
    attr_reader :document
    
    # Initializes the taggings pipeline.
    #
    # @param [Document] document document with the taggings to process
    #
    def initialize(document)
      @document = document
    end
    
    # Saves the denormalized data inside the document.
    #
    # @return [undefined]
    #
    def process
      document.update(packed_taggings: serialize)
    end
    
    # Returns the denormalized data with taggings sorted in the order
    # specified in settings.
    #
    # @return [Array<Hash>] denormalized data
    #
    def serialize
      [].tap do |taggings_data|
        document.settings[:taggings][:allowed_types].each do |type|
          if tagging = document.taggings.find { |tagging| tagging.type == type }
            taggings_data << Serializer.new(tagging).as_json
          end
        end if document.settings.key?(:taggings)
      end
    end
    
    # A custom tagging serializer for dumping denormalized data.
    class Serializer < ActiveModel::Serializer
      attributes :type, :tags
      
      # Returns titles of all tagging's tags
      #
      # @return [<String>] tags' titles
      #
      def tags
        object.tags.map(&:title)
      end
    end
  end
end
