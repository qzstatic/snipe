module Pipeline
  # A mixin for adding serialized objects from dynamic associations
  # to the serializer output.
  module DynamicAssociationsSerializer
    # Returns all object's attributes including both dynamic attributes
    # and serialized objects from dynamic associations.
    #
    # @return [Hash] attributes
    #
    def attributes
      super.merge(dynamic_attributes).merge(dynamic_associations)
    end
    
  private
    def dynamic_attributes
      if object.respond_to?(:dynamic_attributes_values)
        object.dynamic_attributes_values(false, false)
      else
        {}
      end
    end
    
    def nested?
      false
    end
    
    def dynamic_associations
      Pipeline.dynamic_associations_for(object, nested: nested?)
    end
  end
end
