module Pipeline
  # A processor for document's data.
  class Document
    # Returns the document.
    #
    # @return [Document] the document
    #
    attr_reader :document
    
    # Initializes the document pipeline.
    #
    # @param document [Document] the document to process
    #
    def initialize(document)
      @document = document
    end
    
    # Saves the denormalized data inside the document.
    #
    # @return [undefined]
    #
    def process
      document.update(packed_document: serialize)
      async_update_dependencies
    end
    
    # Returns the denormalized data.
    #
    # @return [Hash] denormalized data
    #
    def serialize
      Serializer.new(document).as_json
    end
    
    # Serializes the document as a nested object
    # (ignoring some associations to prevent infinite recursion).
    #
    # @return [Hash] serialized data
    #
    def serialize_as_nested
      Serializer::Nested.new(document).as_json
    end
    
    # Updates all the depending objects (currently only supports links).
    #
    # @return [undefined]
    #
    def update_dependencies
      Savers::DependenciesUpdater.call(document)
    end
    
    # Enqueues a dependency update task in resque.
    #
    # @return [undefined]
    #
    def async_update_dependencies
      Resque.enqueue(DependenciesUpdater, document.id)
    end
    
    # A custom document serializer for dumping denormalized data.
    class Serializer < ActiveModel::Serializer
      include DynamicAssociationsSerializer
      
      attributes :id, :title, :published_at, :url, :categories
      
      # Returns the main document's URL rendered to a string.
      #
      # @return [String] main URL
      #
      def url
        object.url.rendered if object.url.present?
      end
      
      # Returns document's categories attributes as a tree-like structure.
      #
      # @return [Hash] categories' attributes
      #
      def categories
        CategoriesTreeSerializer.new(object.categories).as_json
      end
      
      # Returns attributes without `source_id`.
      #
      # @return [Hash] attributes
      #
      def attributes
        super.except('source_id')
      end
      
      # A custom document serializer for nested serialization.
      class Nested < self
      private
        def nested?
          true
        end
      end
      
      # A custom category serializer for dumping all document's categories.
      class CategorySerializer < ActiveModel::Serializer
        attributes :title, :slug, :type
      end
    end
  end
end
