EDITORS_DUMP_PATH = Pathname.new('~/editors.sql').expand_path

namespace :db do
  %i(dev test production).each do |environment|
    namespace(environment) do
      environment = :development if environment == :dev
      
      desc "Sets up the empty '#{environment}' database"
      task :setup => :environment do
        DatabaseProcessor.new(environment).setup
      end
      
      desc "Resets redis, truncates and sets up the '#{environment}' database"
      task :reset => :environment do
        Rake::Task['redis:flush'].invoke
        DatabaseProcessor.new(environment).reset
      end
      
      desc "Resets redis, drops the '#{environment}' database and recreates it from scratch"
      task :hard_reset => :environment do
        # Rake::Task['redis:flush'].invoke
        DatabaseProcessor.new(environment).hard_reset
      end
      
      namespace :editors do
        desc "Dumps editors & groups from the '#{environment}' database to '#{EDITORS_DUMP_PATH}'"
        task :dump do
          pg_settings = Settings.for(environment).postgresql
          `pg_dump #{pg_settings.database} -h #{pg_settings.host} -U #{pg_settings.username} -a -t editors -t groups > #{EDITORS_DUMP_PATH}`
        end
        
        desc "Restores editors & groups in the '#{environment}' database from '#{EDITORS_DUMP_PATH}'"
        task :restore do
          pg_settings = Settings.for(environment).postgresql
          `psql -d #{pg_settings.database} -h #{pg_settings.host} -U #{pg_settings.username} < #{EDITORS_DUMP_PATH}`
        end
      end
    end
  end
end
