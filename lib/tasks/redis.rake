namespace :redis do
  desc 'Flush everything from redis'
  task :flush do
    Snipe::Redis.connection.flushall
  end
end
