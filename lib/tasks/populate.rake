require 'snipe/redis'

def p(raw_parameters)
  ActionController::Parameters.new(raw_parameters)
end

def category(slug)
  Category.find_by(slug: slug)
end

namespace :db do
  desc 'Populate the database with some data.'
  task :populate => :environment do
    admin  = Editor.first
    admins = Group.first
    
    domains   = category('domains')
    vedomosti = category('vedomosti.ru')
    rubrics   = category('rubrics')
    business  = category('business')
    parts     = category('parts')
    news      = category('news')
    articles  = category('articles')
    galleries = category('galleries')
    
    guests = Group.create! do |group|
      group.slug     = 'guests'
      group.title    = 'Гости'
      group.position = 20
    end
    
    Editor.first.update(group_ids: [admins.id]) if admins.present?
    
    guest = Editor.create! do |editor|
      editor.login      = 'harrypotter'
      editor.email      = 'potter@example.com'
      editor.password   = 'guest'
      editor.first_name = 'Harry'
      editor.last_name  = 'Potter'
      editor.enabled    = true
      editor.group_ids = [guests.id]
    end
    
    vedomosti_document = Savers::DocumentsCreator.new(
      p(
        title: 'Hello from snipe!',
        announce: 'Announce.',
        participant_ids: [admin.id, -admins.id],
        category_ids: [
          domains,
          vedomosti,
          rubrics,
          business,
          parts,
          news
        ].map(&:id)
      ),
      admin
    ).saved_object
    vedomosti_document.create_root_box
    
    vedomosti_main_url = Savers::UrlsCreator.new(
      p(
        domain: 'vedomosti.ru',
        rubric: 'business',
        type:   'news',
        date:   Date.new(2014, 7, 17),
        slug:   'boeing'
      ),
      admin,
      vedomosti_document
    ).saved_object
    vedomosti_document.update(url_id: vedomosti_main_url.id)
    
    Savers::UrlsCreator.new(
      p(
        domain: 'vedomosti.ru',
        rubric: 'business',
        type:   'articles',
        date:   Date.new(2014, 7, 18),
        slug:   'crash'
      ),
      admin,
      vedomosti_document
    ).save
    
    Savers::BoxesCreator.new(
      p(
        type:      :paragraph,
        parent_id: vedomosti_document.root_box.id,
        body:      'First paragraph'
      ),
      admin
    ).save
    
    inset_image = Savers::AttachmentsCreator.new(
      p(
        type: 'image',
        directory: 'some/dir'
      ),
      admin
    ).saved_object
    
    Savers::AttachmentVersionsCreator.new(
      p(
        slug:     'original',
        filename: 'original.jpg',
        width:    800,
        height:   600,
        size:     50.kilobytes
      ),
      admin,
      inset_image
    ).save
    
    Savers::LinksCreator.new(
      p(
        section:           :reference,
        type:              :inner,
        bound_document_id: vedomosti_document.id
      ),
      admin,
      vedomosti_document
    ).save
    
    Savers::LinksCreator.new(
      p(
        section: :reference,
        type:    :outer,
        url:     'http://vedomosti.ru',
        label:   'vedomosti.ru',
        date:    Date.today
      ),
      admin,
      vedomosti_document
    ).save
    
    title_image = Savers::AttachmentsCreator.new(
      p(
        type: 'image',
        directory: 'some/dir'
      ),
      admin
    ).saved_object
    
    Savers::AttachmentVersionsCreator.new(
      p(
        slug:     'original',
        filename: 'original.jpg',
        width:    800,
        height:   600,
        size:     50.kilobytes
      ),
      admin,
      title_image
    ).save
    
    gallery_document = Savers::DocumentsCreator.new(
      p(
        title:        'Photogallery with cars on vdmsti.ru',
        rightcol:     'Some rightcol',
        category_ids: [
          domains,
          vedomosti,
          rubrics,
          business,
          parts,
          galleries
        ].map(&:id)
      ),
      admin
    ).saved_object
    gallery_document.create_root_box
    
    vdmsti_main_url = Savers::UrlsCreator.new(
      p(
        domain: 'vdmsti.ru',
        rubric: 'business',
        type:   'galleries',
        date:   Date.new(2014, 7, 29),
        slug:   'auto'
      ),
      admin,
      gallery_document
    ).saved_object
    gallery_document.update(url_id: vdmsti_main_url.id)
    
    gallery_image = Savers::AttachmentsCreator.new(
      p(
        directory: 'path/to/directory',
        type:      :image
      ),
      admin
    ).saved_object
    
    Savers::AttachmentVersionsCreator.new(
      p(
        filename: 'original.jpg',
        slug:     'original',
        width:    1000,
        height:   800,
        size:     120.kilobytes
      ),
      admin,
      gallery_image
    ).save
    
    Savers::BoxesCreator.new(
      p(
        type:        :inset_link,
        parent_id:   gallery_document.root_box.id,
        align:       'center',
        document_id: vedomosti_document.id
      ),
      admin
    ).save
    
    vdmsti_gallery = Savers::BoxesCreator.new(
      p(
        type:        :gallery,
        parent_id:   gallery_document.root_box.id,
        title:       'Заголовок галереи',
        body:        'Описание галереи'
      ),
      admin
    ).saved_object
    
    Savers::BoxesCreator.new(
      p(
        type:        :gallery_image,
        parent_id:   vdmsti_gallery.id,
        description: 'Чёрный квадрат',
        author:      'J. Smith',
        credits:     'AFP',
        position:    2,
        image_id:    gallery_image.id
      ),
      admin
    ).save
    
    Savers::BoxesCreator.new(
      p(
        type:        :gallery_image,
        parent_id:   vdmsti_gallery.id,
        description: 'Белый квадрат',
        author:      'J. Smith',
        credits:     'WTF',
        position:    1,
        image_id:    gallery_image.id
      ),
      admin
    ).save
    
    Savers::SandboxesCreator.new(
      p(
        title:           'Последние за сутки',
        color:           '#ffff00',
        date_offset:     -1.day,
        participant_ids: [admin.id, -admins.id],
        category_ids:    [domains.id, vedomosti.id]
      ),
      admin
    ).save
    
    news_list = Savers::ListsCreator.new(
      p(
        title:        'Новости',
        slug:         'news',
        type:         'List::Mixed',
        limit:        7,
        category_ids: [domains.id, vedomosti.id, parts.id, news.id]
      ),
      admin
    ).saved_object
    
    Savers::ListItemsCreator.new(
      p(
        document_id: vedomosti_document.id,
        top:    1
      ),
      admin,
      news_list
    ).save
    
    articles_list = Savers::ListsCreator.new(
      p(
        title:        'Статьи',
        slug:         'articles',
        type:         'List::Timeline',
        limit:        10,
        category_ids: [domains.id, vedomosti.id, parts.id, articles.id]
      ),
      admin
    ).saved_object
  end
end
