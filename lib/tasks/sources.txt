http://www.bloomberg.com/ Bloomberg
http://www.ft.com/ Finacial Times
http://www.mergermarket.com/ Mergermarket / dealReporter
http://www.reuters.com/ Reuters
http://online.wsj.com/ The Wall Street Journal
http://www.vedomosti.ru/ Ведомости
http://www.gazeta.ru/ Газета.ru
http://www.interfax.ru/ Интерфакс
http://www.kommersant.ru/ Коммерсантъ
http://lenta.ru/ Лента.Ру
http://pravo.ru/ Право.ру
http://www.1prime.ru/ Прайм
http://www.rbc.ru/ РБК
http://www.rosbalt.ru/ Росбалт
http://itar-tass.com/ ТАСС
http://www.finmarket.ru/ Финмаркет
http://www.dowjones.com/ Dow Jones
http://www.forbes.ru/ Forbes
http://www.kremlin.ru/ kremlin.ru
http://rsport.ru/ Р-спорт
http://www.sport-express.ru/ Спорт-экспресс
http://www.echo.msk.ru/ Эхо Москвы
http://kp.vedomosti.ru/ Ведомости. Как потратить
http://ria.ru/ РИА Новости
http://www.economist.com/ The Economist
http://www.telegraph.co.uk/ The Telegraph
http://www.xinhuanet.com/english/ Xinhua
http://www.akm.ru/ АК&М
http://www.webplanet.ru/ Вебпланета
http://www.hse.ru/ НИУ ВШЭ
http://radiomayak.ru/ Радио Маяк
http://www.svoboda.org/ Радио Свобода
http://rapsinews.ru/ РАПСИ
http://www.rusnovosti.ru/ Русская служба новостей
http://www.ap.org/ Associated Press
http://www.bbc.com/ BBC
http://www.bbc.co.uk/russian BBC Russian
http://www.cnn.com/ CNN
http://www.dw.de/ Deutsche Welle
http://www.marketwatch.com/ MarketWatch
http://slon.ru/ Slon.ru
http://www.vesti.ru/ Вести
http://izvestia.ru/ Известия
http://www.unian.net/ УНИАН
http://online.barrons.com/ Barron's
http://tvrain.ru/ Телеканал Дождь
