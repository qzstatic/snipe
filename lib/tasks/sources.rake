def p(raw_parameters)
  ActionController::Parameters.new(raw_parameters)
end

def create_object(params, creator_class)
  creator_class.new(p(params), Editor.first).saved_object
end

def create_option(params)
  create_object(params, Savers::OptionsCreator).tap do |option|
    if option.persisted?
      puts "created option #{option.title}"
    else
      puts "#{option.title} #{option.errors.messages[:value][0]}"
    end
  end
end

namespace :db do
  desc 'Populate the database with sources from txt-file.'
  task :populate_sources => :environment do
    File.readlines(Rails.root.join('lib/tasks', 'sources.txt')).map.with_index do |line, index|
      next if line.strip.empty?
      url, title = line.strip.split(/\s/, 2)
      create_option(
        type:     'source',
        title:    title,
        url:      url,
        position: index.succ * 10
      )
    end
  end
end
