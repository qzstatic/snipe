require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
# require 'sprockets/railtie'
# require 'rails/test_unit/railtie'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

Settings::Utils.environments << :staging

module Snipe
  class Application < Rails::Application
    %w(app/service lib).each do |path|
      config.autoload_paths << config.root.join(path).to_s
    end
    
    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Moscow'
    
    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    I18n.enforce_available_locales = false
    config.i18n.default_locale = :ru

    config.active_record.schema_format = :sql
    
    # Disable the asset pipeline.
    config.assets.enabled = false
    
    # Политика доступа через CORS
    config.middleware.use Rack::Cors do
      allow do
        origins  '*'
        resource '*', headers: :any, methods: %i(get post put patch delete options)
      end
    end
  end
end

require 'db_config'
