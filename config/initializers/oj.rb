Oj.default_options = {
  mode: :compat,
  symbol_keys: true
}

# monkeypatched JSON.generate for pow happiness
def JSON.generate(*args)
  Oj.dump(*args)
end
