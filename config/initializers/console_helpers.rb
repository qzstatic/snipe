module ConsoleHelpers
  def sign_in(editor = Editor.first)
    @sessions_manager = SessionsManager::Console.new(fake_request)
    @sessions_manager.editor = editor
    @sessions_manager.open_session
  end
  
  %i(get post put patch delete).each do |method|
    define_method(method) do |path, params = {}, headers = {}|
      headers = headers.dup.merge(
        header_name => @sessions_manager.access_token.token
      )
      
      super(path, params, headers)
    end
  end
  
  def sign_out
    @sessions_manager.close_session
    @sessions_manager = nil
  end
  
private
  def header_name
    SessionsManager::AccessToken::TOKEN_HEADER
  end
  
  def fake_request
    Struct.new(:ip).new('0.0.0.0')
  end
end

ActionDispatch::Integration::Session.class_eval do
  include ConsoleHelpers
end
