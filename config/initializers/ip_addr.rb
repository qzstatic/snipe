class IPAddr
  alias_method :original_equal, :==
  
  # Monkeypatched version of equality check
  # (for awesome_print happiness).
  def ==(other_ipaddr)
    return false unless other_ipaddr.is_a?(IPAddr) || other_ipaddr.is_a?(String)
    original_equal(other_ipaddr)
  end
end
