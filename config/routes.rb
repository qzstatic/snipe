require 'resque/server'

Snipe::Application.routes.draw do
  mount Resque::Server.new, at: '/resque'
  
  # Load balancer
  get '/check_alive' => 'main#check_alive'
  
  namespace :v2, defaults: { format: :json } do
    resources :documents, only: %w(index) do
      collection do
        post :fast
      end
    end
  end
  
  namespace :v1, defaults: { format: :json } do
    post   '/access_token' => 'sessions#create'
    patch  '/access_token' => 'sessions#refresh'
    delete '/access_token' => 'sessions#destroy'
    get    '/session'      => 'sessions#show'
    
    scope :search do
      scope :documents do
        get '/index/(:page)'    => 'search/documents#index', page:      /[0-9]+/
        get '/fresh/:timestamp' => 'search/documents#fresh', timestamp: /[0-9]+/
      end
    end
    
    scope except: %i(new edit) do

      resources :documents, except: %i(new edit destroy), shallow: true do
        collection do
          get :search
          get :by_period
          get :categorized
          post :fast
        end
        
        member do
          scope object: :document do
            put :block
            put :unblock
          end
          patch :assign
          patch :complete
          patch :proofread
          
          patch :publish
          patch :unpublish
          
          get :preview
          patch '/preview' => 'documents#update_preview'
          
          get :root_box
          get :bound
          get :full_bound
          get :matching_lists

          get '/bound_section/:section'      => 'documents#bound_section',      section: /[a-z_\d]+/
          get '/full_bound_section/:section' => 'documents#full_bound_section', section: /[a-z_\d]+/
        end
        
        get '/boxes' => 'boxes#index'
        get '/blocked_boxes' => 'boxes#blocked'
        get '/snapshots' => 'snapshots#for_document'
        
        resources :links
        resources :urls, only: %w(index create) do
          collection do
            post '/raw' => 'urls#raw_create'
          end
        end
        
        resources :taggings, shallow: true do
          get '/tags' => 'tags#index'
        end
        
        get   '/title_data' => 'document_parts#show_title'
        get   '/:part'      => 'document_parts#show',   part: /[a-z_]+/
        patch '/title_data' => 'document_parts#update_title'
        patch '/:part'      => 'document_parts#update', part: /[a-z_]+/
      end
      
      resources :categories do
        collection do
          get '/with_children' => 'categories#index_with_children'
          get :tree
          get :available_setting_keys
          get '/:slug' => :by_parent, slug: /[a-z_]+/
        end
      end
      
      resources :editors, shallow: true do
        get '/groups' => 'groups#by_editor'
      end
      
      resources :groups
      
      resources :tags, except: %w(index new edit) do
        collection do
          get :search
          get :find_by_title
        end
      end
      
      resources :urls, only: %w(show update destroy) do
        collection do
          get :find_by_url
        end
      end
      
      resources :boxes, except: %w(index new edit) do
        collection do
          post :multi
          post :multi_move
        end
        
        member do
          post :move
          scope object: :box do
            put :block
            put :unblock
          end
          post :copy
          patch :restore
        end
      end
      
      resources :attachments, shallow: true do
        get '/versions'  => 'attachment_versions#index'
        post '/versions' => 'attachment_versions#create'
      end
      
      resources :attachment_versions, only: [:show, :update, :destroy]
      
      resources :snapshots, only: :show do
        collection do
          get '/:old_id..:new_id' => :diff, old_id: /\d+/, new_id: /\d+/, defaults: { format: :html }
        end
      end
      
      resources :sandboxes do
        get '/documents' => 'documents#by_sandbox'
        get '/snapshots' => 'snapshots#for_sandbox'
      end
      
      resources :pages do
        get '/snapshots' => 'snapshots#for_page'
        
        member do
          get :last_documents
        end
      end
      
      resources :lists do
        get '/items'  => 'list_items#index'
        post '/items' => 'list_items#create'
        
        member do
          get :documents
          get :cached_documents
          get :item
        end
        
        get '/snapshots' => 'snapshots#for_list'
      end
      
      resources :list_items, only: [:show, :update, :destroy]
      
      resources :options do
        collection do
          get :types
          get '/:type' => :by_type, type: /[a-z0-9_]+/
        end
      end
    end
  end
end
